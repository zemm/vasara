{ pkgs ? import ../../nix {}
, name ? "artifact"
}:

with pkgs;

let

  env = buildEnv {
    name = "env";
    paths = [
      jre
      bashInteractive
      coreutils
      gnugrep
      gnused
      keycloak
      netcat
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${gnugrep}/bin \
  --prefix PATH : ${gnused}/bin \
  --prefix PATH : ${keycloak}/bin \
  --prefix PATH : ${netcat}/bin \
  --prefix PATH : ${jre}/bin \
  --prefix PATH : ${tini}/bin \
  --set JAVA_HOME ${jre}/lib/openjdk/jre/ \
  --set LOG4J_FORMAT_MSG_NO_LOOKUPS true \
  --set KEYCLOAK_APP_DIR ${keycloak}

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --exclude="*/pixman-1/pixman-1/*" \
  --files-from=${closure} \
  usr > $out || true
''
