const webpack = require('webpack');
const path = require('path');

module.exports = {
  webpack: {
    plugins: [
      new webpack.NormalModuleReplacementPlugin(/Labeled/, path.resolve(__dirname, './src/Overrides/Labeled.tsx')),
      new webpack.NormalModuleReplacementPlugin(
        /FieldTitle/,
        path.resolve(__dirname, './src/Overrides/FieldTitle.tsx')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /RadioButtonGroupInputItem/,
        path.resolve(__dirname, './src/Overrides/RadioButtonGroupInputItem.tsx')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /InputHelperText/,
        path.resolve(__dirname, './src/Overrides/InputHelperText.tsx')
      ),
      //      new webpack.NormalModuleReplacementPlugin(/jose\/jwt\/verify/, path.resolve(__dirname, './node_modules/jose/dist/browser/jwt/verify.js')),
      //     new webpack.NormalModuleReplacementPlugin(/jose\/jwk\/parse/, path.resolve(__dirname, './node_modules/jose/dist/browser/jwk/parse.js')),
      new webpack.NormalModuleReplacementPlugin(
        /jsrsasign/,
        path.resolve(__dirname, './node_modules/oidc-client/jsrsasign/dist/jsrsasign.js')
      ),
    ],
  },
}
