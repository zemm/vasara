{ pkgs ? import ../../nix {}
, build ? import ./default.nix { inherit pkgs REACT_APP_BUILD_TARGET; }
, name ? "artifact"
, REACT_APP_BUILD_TARGET ? "modeler"
}:

with pkgs;

let


  env = buildEnv {
    name = "env";
    paths = [
      bashInteractive
      build
      coreutils
      findutils
      gnused
      netcat
      nginx
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${build}/bin \
  --prefix PATH : ${findutils}/bin \
  --prefix PATH : ${gnused}/bin \
  --prefix PATH : ${netcat}/bin \
  --prefix PATH : ${nginx}/bin \
  --prefix PATH : ${tini}/bin

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --files-from=${closure} \
  usr > $out || true
''
