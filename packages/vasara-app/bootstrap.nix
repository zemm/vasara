{ pkgs ? import ../../nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    node2nix
    nodejs-14_x
  ];
}
