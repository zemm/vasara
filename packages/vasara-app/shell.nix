{ pkgs ? import ../../nix {}
}:

let node_modules = (import ./node-composition.nix { inherit pkgs; }).package.override {
  name = "node_modules";
  src = builtins.filterSource (path: type:
    (baseNameOf path) == "final-form.patch" ||
    (baseNameOf path) == "package.json" ||
    (baseNameOf path) == "package-lock.json" ) ./.;
  buildInputs = with pkgs; [ pkgconfig ];
  preRebuild = ''
    # Fix: https://github.com/final-form/react-final-form/issues/899
    patch -p1 < $src/final-form.patch
    # Disable opencollective postinstall
    substituteInPlace node_modules/inferno/package.json \
      --replace "opencollective postinstall" "" \
      --replace '"postinstall": "opencollective-postinstall"' ""
    # Fix eslint cache location
    substituteInPlace node_modules/react-scripts/config/webpack.config.js \
      --replace "cacheLocation:" "cacheLocation: true ? './' : "
    # Do not download Cypress
    substituteInPlace node_modules/cypress/package.json \
      --replace '"postinstall": "node index.js --exec install",' ""
  '';
  postInstall = ''
    rm -rf $out/lib/node_modules/*/node_modules/@types/react-native
    mv $out/lib/node_modules/*/node_modules /tmp/_; rm -rf $out; mv /tmp/_ $out
  '';
};

in

pkgs.mkShell {
  buildInputs = with pkgs; [
    jfrog-cli
    node2nix
    nodejs-14_x
    cypress
### cannot be provided, because must match system version eg. on RHEL:
#   fuse
#   bindfs
  ];
  shellHook = ''
    fusermount -qu node_modules
    mkdir -p node_modules
    bindfs ${node_modules} node_modules -o nonempty
    export PATH=$(pwd)/node_modules/.bin:$PATH
  '';
}
