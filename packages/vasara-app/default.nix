{ pkgs ? import ../../nix {}
, REACT_APP_ZIPKIN_COLLECT_URL ? "REACT_APP_ZIPKIN_COLLECT_URL_VALUE"
, REACT_APP_GRAPHQL_API_URL ? "REACT_APP_GRAPHQL_API_URL_VALUE"
, REACT_APP_CLIENT_ID ? "REACT_APP_CLIENT_ID_VALUE"
, REACT_APP_AUTHORITY ? "REACT_APP_AUTHORITY_VALUE"
, REACT_APP_REDIRECT_URI ? "REACT_APP_REDIRECT_URI_VALUE"
, REACT_APP_LOGOUT_URI ? "REACT_APP_LOGOUT_URI_VALUE"
, REACT_APP_URI ? "REACT_APP_URI_VALUE"
, REACT_APP_DEFAULT_TENANT ? "REACT_APP_DEFAULT_TENANT_VALUE"
, REACT_APP_TRANSIENT_USER_PUBLIC_KEY ? "REACT_APP_TRANSIENT_USER_PUBLIC_KEY_VALUE"
, REACT_APP_BUILD_TARGET ? "modeler"
}:

let node_modules = (import ./node-composition.nix { inherit pkgs; }).package.override {
  name = "node_modules";
  src = builtins.filterSource (path: type:
    (baseNameOf path) == "final-form.patch" ||
    (baseNameOf path) == "package.json" ||
    (baseNameOf path) == "package-lock.json" ) ./.;
  buildInputs = with pkgs; [ pkgconfig ];
  preRebuild = ''
    # Fix: https://github.com/final-form/react-final-form/issues/899
    patch -p1 < $src/final-form.patch
    # Disable opencollective postinstall
    substituteInPlace node_modules/inferno/package.json \
      --replace "opencollective postinstall" "" \
      --replace '"postinstall": "opencollective-postinstall"' ""
    # Fix eslint cache location
    substituteInPlace node_modules/react-scripts/config/webpack.config.js \
      --replace "cacheLocation:" "cacheLocation: true ? './' : "
    # Do not download Cypress
    substituteInPlace node_modules/cypress/package.json \
      --replace '"postinstall": "node index.js --exec install",' ""
  '';
  postInstall = ''
    rm -rf $out/lib/node_modules/*/node_modules/@types/react-native
    mv $out/lib/node_modules/*/node_modules /tmp/_; rm -rf $out; mv /tmp/_ $out
  '';
};

in

pkgs.stdenv.mkDerivation {
  name = "build";
  src = pkgs.gitignoreSource ./.;
  buildInputs = with pkgs; [ nodejs-14_x ];
  builder = builtins.toFile "builder.sh" ''
    source $stdenv/setup;

    # Copy sources, node_modules and build
    cp -a $src src && chmod u+w -R src && cd src
    cp -a $node_modules ./node_modules && chmod u+w -R node_modules
    home=$(pwd) INLINE_RUNTIME_CHUNK=false npm run-script build

    # Install
    mkdir -p $out/var/
    cp -a build $out/var/www

    # Create builder
    mkdir -p $out/bin/
    cat > $out/bin/init << EOF
#!$bash/bin/sh

# Exit on error
set -e

# Copy output to new location
local=\$(pwd)
if [[ -d \$local/var ]]; then chmod u+w -R \$local/var; fi
cp -a $out/var \$local
chmod u+w -R \$local/var

# Replace environment variables
find \$local/var/www -name "*.js" -print0|xargs -0 sed -i "\
s|REACT_APP_ZIPKIN_COLLECT_URL_VALUE|\$REACT_APP_ZIPKIN_COLLECT_URL|g; \
s|REACT_APP_GRAPHQL_API_URL_VALUE|\$REACT_APP_GRAPHQL_API_URL|g; \
s|REACT_APP_CLIENT_ID_VALUE|\$REACT_APP_CLIENT_ID|g; \
s|REACT_APP_AUTHORITY_VALUE|\$REACT_APP_AUTHORITY|g; \
s|REACT_APP_REDIRECT_URI_VALUE|\$REACT_APP_REDIRECT_URI|g; \
s|REACT_APP_LOGOUT_URI_VALUE|\$REACT_APP_LOGOUT_URI|g; \
s|REACT_APP_URI_VALUE|\$REACT_APP_URI|g; \
s|REACT_APP_DEFAULT_TENANT_VALUE|\$REACT_APP_DEFAULT_TENANT|g; \
s|REACT_APP_TRANSIENT_USER_PUBLIC_KEY_VALUE|\$REACT_APP_TRANSIENT_USER_PUBLIC_KEY|g; \
"

mkdir -p \$local/etc \$local/var/nginx/logs
cat > \$local/etc/nginx.conf << END_OF_FILE
daemon off;
worker_processes 4;
error_log /dev/stdout info;

events {
  use epoll;
  worker_connections 128;
}

http {
  include $nginx/conf/mime.types;
  default_type application/octet-stream;
  sendfile on;
  access_log /dev/stdout;
  server {
    listen 8080;
    server_name localhost;
    gzip on;
    gzip_types text/html text/css application/javascript;
    gzip_min_length 1000;
    root \$local/var/www;
    location / {
      index index.html;
      # kill cache
      add_header Last-Modified \\\$date_gmt;
      add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
      if_modified_since off;
      expires off;
      etag off;
      # CSP
      add_header X-Frame-Options "deny" always;
      add_header X-XSS-Protection "mode=block" always;
      add_header X-Content-Type-Options "nosniff" always;
      add_header Referrer-Policy "same-origin" always;
      add_header Content-Security-Policy "default-src 'self'; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
      add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
      #
      try_files \\\$uri /index.html =440;
    }
  }
}
END_OF_FILE
EOF
    chmod u+x $out/bin/init
  '';
  inherit REACT_APP_ZIPKIN_COLLECT_URL;
  inherit REACT_APP_GRAPHQL_API_URL;
  inherit REACT_APP_CLIENT_ID;
  inherit REACT_APP_AUTHORITY;
  inherit REACT_APP_REDIRECT_URI;
  inherit REACT_APP_LOGOUT_URI;
  inherit REACT_APP_URI;
  inherit REACT_APP_DEFAULT_TENANT;
  inherit REACT_APP_TRANSIENT_USER_PUBLIC_KEY;
  inherit REACT_APP_BUILD_TARGET;
  inherit (pkgs) bash nginx;
  inherit node_modules;
}
