import * as _ from 'lodash';

import { SimplifiedTypeWithIDs } from './types';

export function stringifyWrappers(wrappers: ('LIST' | 'NON_NULL')[]) {
  let left = '';
  let right = '';

  for (const wrapper of wrappers) {
    switch (wrapper) {
      case 'NON_NULL':
        right = '!' + right;
        break;
      case 'LIST':
        left = left + '[';
        right = ']' + right;
        break;
    }
  }
  return [left, right];
}

export function buildId(...parts: string[]) {
  return parts.join('::');
}

export function typeNameToId(name: string) {
  return buildId('TYPE', name);
}

export function extractTypeId(id: string) {
  let [, type] = id.split('::');
  return buildId('TYPE', type);
}

export function isSystemType(type: SimplifiedTypeWithIDs) {
  return _.startsWith(type.name, '__');
}

export function isBuiltInScalarType(type: SimplifiedTypeWithIDs) {
  return ['Int', 'Float', 'String', 'Boolean', 'ID'].indexOf(type.name) !== -1;
}

export function isScalarType(type: SimplifiedTypeWithIDs) {
  return type.kind === 'SCALAR' || type.kind === 'ENUM';
}

export function isObjectType(type: SimplifiedTypeWithIDs) {
  return type.kind === 'OBJECT';
}

export function isInputObjectType(type: SimplifiedTypeWithIDs) {
  return type.kind === 'INPUT_OBJECT';
}
