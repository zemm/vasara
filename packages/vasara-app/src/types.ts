import { ApolloQueryResult } from 'apollo-client';
import { IntrospectionQuery } from 'graphql/utilities/getIntrospectionQuery';

import { GraphQLDataProvider, HasuraSchema } from './DataProviders/types';

export interface Settings {
  isLite: boolean;
}

export interface IAppProps {}

export interface IAppState {
  isGuest?: boolean;
  introspectionQueryResponse?: ApolloQueryResult<IntrospectionQuery>;
  camundaDataProvider?: GraphQLDataProvider;
  hasuraDataProvider?: GraphQLDataProvider;
  actionsDataProvider?: GraphQLDataProvider;
  customDataProvider?: GraphQLDataProvider;
  hasuraIntrospectionResults?: HasuraSchema;
}
