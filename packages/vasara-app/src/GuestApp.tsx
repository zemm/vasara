import React from 'react';
import { Admin, Loading, Resource } from 'react-admin';
import { Helmet } from 'react-helmet';

import { DataProvider } from './DataProviders';
import { RaFetchType } from './DataProviders/types';
import GuestLayout from './Layout/GuestLayout';
import i18nProvider, { DEFAULT_LOCALE, resolveLocale } from './messages';
import { customRoutes } from './Routes';
import { IAppState } from './types';
import { theme } from './util/theme';

export const GuestApp: React.FC<IAppState> = props => {
  const { camundaDataProvider, hasuraDataProvider, hasuraIntrospectionResults } = props;
  return camundaDataProvider && hasuraDataProvider && hasuraIntrospectionResults ? (
    <Admin
      theme={theme}
      authProvider={undefined}
      loginPage={undefined}
      i18nProvider={i18nProvider}
      layout={GuestLayout}
      dashboard={undefined}
      disableTelemetry
      dataProvider={(type: string, resource: string, params: any) =>
        DataProvider(type as RaFetchType, resource, params, props)
      }
      customRoutes={customRoutes}
    >
      <Helmet
        htmlAttributes={{ lang: resolveLocale(DEFAULT_LOCALE) }}
        title={`Vasara — ${i18nProvider.translate('vasara.ui.subtitle')}`}
      />
      <Resource name="camunda_guest_ProcessDefinition" options={{ hideFromMenu: true }} />
      <Resource name="camunda_insert_guest_ProcessInstance" options={{ hideFromMenu: true }} />
    </Admin>
  ) : (
    <Loading loadingPrimary={''} loadingSecondary={'Loading...'} />
  );
};
