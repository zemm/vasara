import { getNotification } from 'ra-core';
import React from 'react';
import { Notification, useTranslate } from 'react-admin';
import { useSelector } from 'react-redux';

const CustomNotification = (props: any) => {
  const translate = useTranslate();
  const notification = useSelector(getNotification);
  // Item is "lost" every time we complete a task and we want to hide it
  try {
    if (notification.message === 'ra.notification.item_doesnt_exist') {
      return null;
    }
    if ((notification?.message || '').startsWith('GraphQL error:')) {
      notification.message = notification.message.substring('GraphQL error:'.length);
    }
    if ((notification?.message || '').startsWith('vasara.')) {
      notification.message = translate(notification.message);
    }
  } catch (e) {}
  return notification?.type === 'warning' || notification?.type === 'error' ? (
    <Notification {...props} message={notification.message} autoHideDuration={5000} />
  ) : (
    <Notification {...props} autoHideDuration={1000} />
  );
};

export default CustomNotification;
