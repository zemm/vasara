import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { AppBar } from 'react-admin';

import LanguageMenu from './LanguageMenu';

const useStyles = makeStyles(theme => ({
  title: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

const CustomAppBar = (props: any) => {
  const classes = useStyles();
  return (
    <AppBar {...props}>
      <Box flex="1">
        <Typography variant="h6" id="react-admin-title" className={classes.title} />
      </Box>
      <LanguageMenu />
    </AppBar>
  );
};

export default CustomAppBar;
