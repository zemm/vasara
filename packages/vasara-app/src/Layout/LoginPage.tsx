import { Container } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import { Button, useLogin } from 'react-admin';

const LoginPage = ({ theme }: any) => {
  const login = useLogin();
  return (
    <ThemeProvider theme={theme}>
      <Container
        style={{
          height: '100vh',
          width: '100wh',
          display: 'flex',
          alignContent: 'center',
          justifyContent: 'center',
        }}
      >
        {/* @ts-ignore */}
        <Button onClick={(e: any) => login({})} label={'Login'} />
      </Container>
    </ThemeProvider>
  );
};

export default LoginPage;
