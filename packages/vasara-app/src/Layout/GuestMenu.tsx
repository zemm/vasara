import { Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Description, Inbox } from '@material-ui/icons';
import React from 'react';
import { MenuItemLink, useTranslate } from 'react-admin';
import { useSelector } from 'react-redux';

import img from '../static/logo.png';

const useStyles = makeStyles({
  img: {
    maxWidth: '200px',
    margin: '2em auto 2em auto',
  },
  spacer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
});

const Menu = ({ onMenuClick }: any) => {
  const translate = useTranslate();
  const classes = useStyles();
  const open = useSelector((state: any) => state.admin.ui.sidebarOpen);
  return (
    <>
      <MenuItemLink
        key="guest"
        to={window.location.hash.replace(/^#/, '')}
        primaryText={translate('vasara.ui.guestForm')}
        leftIcon={<Inbox />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <Divider />
      <MenuItemLink
        leftIcon={<Description />}
        onClick={(e: any) => {
          window.location.href = translate('vasara.ui.privacyNotice');
          e.preventDefault();
          e.stopPropagation();
        }}
        to=""
        sidebarIsOpen={open}
        primaryText={translate('vasara.ui.privacyNoticeLabel')}
      />
      {open ? (
        <>
          <div className={classes.spacer} />
          <img src={img} alt="" className={classes.img} />
        </>
      ) : null}
    </>
  );
};

export default Menu;
