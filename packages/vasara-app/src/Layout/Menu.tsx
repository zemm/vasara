import { Divider, Theme, useMediaQuery } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Description, Gavel, Inbox, ViewList, Work } from '@material-ui/icons';
import SearchIcon from '@material-ui/icons/Search';
import { GetListResult } from 'ra-core';
import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import { MenuItemLink, getResources, useDataProvider, useTranslate } from 'react-admin';
import { useSelector } from 'react-redux';

import { getUser } from '../Auth/authProvider';
import Settings from '../Settings';
import img from '../static/logo.png';
import { isAllowedInMainMenu } from '../util/helpers';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '8px 4px 4px 4px',
    display: 'flex',
    alignItems: 'center',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 1,
  },
  img: {
    maxWidth: '200px',
    margin: '2em auto 2em auto',
  },
  spacer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
}));

interface FilterProps {
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

const Filter: React.FC<FilterProps> = ({ onChange }: any) => {
  const translate = useTranslate();
  const classes = useStyles();

  return (
    <Paper component="form" className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder={translate('vasara.form.filterResources')}
        inputProps={{ 'aria-label': translate('vasara.form.helperText.filterResources') }}
        onChange={onChange}
      />
      <SearchIcon />
    </Paper>
  );
};

const Menu = ({ onMenuClick, logout }: any) => {
  const classes = useStyles();
  const context = useContext(Settings);
  const translate = useTranslate();

  const isXSmall = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
  const open = useSelector((state: any) => state.admin.ui.sidebarOpen);
  const allResources: any = useSelector(getResources);
  const isGuest = (getUser()?.profile?.preferred_username || '').match(/^guest-.*/);

  const dataProvider = useDataProvider();
  const [resources, setResources] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [filter, setFilter]: [string | RegExp, any] = useState('');

  useEffect(() => {
    if (allResources.length && !loaded && !loading) {
      const candidateResources = allResources.filter(
        (resource: any) =>
          resource &&
          !resource.name.match(/^_/) &&
          !resource.options.hideFromMenu &&
          isAllowedInMainMenu(resource.name) &&
          !['ProcessDefinition', 'DecisionDefinition', 'ProcessInstance', 'UserTask'].includes(resource.name)
      );
      candidateResources.sort((a: any, b: any) => {
        if ((a?.options?.label ?? a.name) < (b?.options?.label ?? b.name)) {
          return -1;
        }
        if ((a?.options?.label ?? a.name) > (b?.options?.label ?? b.name)) {
          return 1;
        }
        return 0;
      });
      if (candidateResources.length) {
        setLoading(true);
        dataProvider
          .getList('AvailableResources', {
            pagination: { page: 0, perPage: 1000 },
            sort: { field: 'id', order: 'asc' },
            filter: { resources: candidateResources.map((resource: any) => resource.name) },
          })
          .then(({ data }: GetListResult) => {
            const available = data.map((item: any) => item.id);
            setResources(candidateResources.filter((resource: any) => available.includes(resource.name)));
          })
          .catch((error: any) => {
            console?.error(error);
          })
          .finally(() => {
            setLoading(false);
            setLoaded(true);
          });
      }
    }
  }, [dataProvider, allResources, loaded, loading, resources]);

  return (
    <>
      <MenuItemLink
        key="UserTask"
        to="/UserTask"
        primaryText={translate('vasara.resource.userTask')}
        leftIcon={<Inbox />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      {!isGuest ? (
        <MenuItemLink
          key="ProcessDefinition"
          to="/ProcessDefinition"
          primaryText={translate('vasara.resource.processDefinition')}
          leftIcon={<Work />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
        />
      ) : null}
      {context.isLite || isGuest ? null : (
        <MenuItemLink
          key="DecisionDefinition"
          to="/DecisionDefinition"
          primaryText={translate('vasara.resource.decisionDefinition')}
          leftIcon={<Gavel />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
        />
      )}
      {resources.length > 0 ? <Divider /> : null}
      {resources.length > 5 && open ? (
        <Filter
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            setFilter(new RegExp(e.target.value, 'i'));
          }}
        />
      ) : null}
      {resources
        .filter(
          (resource: any) =>
            !filter || resource.name.match(filter) || (resource?.options?.label && resource.options.label.match(filter))
        )
        .map((resource: any) => (
          <MenuItemLink
            key={resource.name}
            to={`/${resource.name}`}
            primaryText={(resource.options && resource.options.label) || resource.name}
            leftIcon={resource.icon ? <resource.icon /> : <ViewList />}
            onClick={onMenuClick}
            sidebarIsOpen={open}
          />
        ))}
      {isXSmall && logout}
      <Divider />
      <MenuItemLink
        leftIcon={<Description />}
        onClick={(e: any) => {
          window.location.href = translate('vasara.ui.privacyNotice');
          e.preventDefault();
          e.stopPropagation();
        }}
        to=""
        sidebarIsOpen={open}
        primaryText={translate('vasara.ui.privacyNoticeLabel')}
      />
      {open ? (
        <>
          <div className={classes.spacer} />
          <img src={img} alt="" className={classes.img} />
        </>
      ) : null}
    </>
  );
};

export default Menu;
