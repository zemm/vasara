import React from 'react';
import { Layout } from 'react-admin';

import AppBar from './AppBar';
import Menu from './Menu';
import CustomNotification from './Notification';

const CustomLayout = (props: any) => (
  <Layout {...props} menu={Menu} appBar={AppBar} notification={CustomNotification} />
);

export default CustomLayout;
