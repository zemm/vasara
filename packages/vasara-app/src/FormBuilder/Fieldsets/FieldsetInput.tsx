import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useTranslate } from 'ra-core';
import React, { useState } from 'react';
import { useLocale } from 'react-admin';
import { v4 as uuid } from 'uuid';

type Props = {
  fields: any;
  expand: () => void;
};

const FieldsetInput: React.FC<Props> = props => {
  const [fieldsetName, setFieldsetName] = useState<string>('');
  const translate = useTranslate();
  const locale = useLocale();
  const htmlId = uuid();

  const addNewFieldset = (): boolean => {
    if (fieldsetName !== '') {
      props.fields.push({ label: { [locale]: fieldsetName }, fields: [], id: uuid() });
      setFieldsetName('');
      return true;
    }
    return false;
  };

  const handleNewFieldsetNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFieldsetName(event.target.value);
  };

  return (
    <FormControl fullWidth size="small" variant="outlined" style={{ marginBottom: '1rem' }}>
      <InputLabel htmlFor={htmlId}>{translate('vasara.form.helperText.fieldset')}</InputLabel>
      <OutlinedInput
        type="text"
        inputProps={{ id: htmlId }}
        value={fieldsetName}
        onChange={handleNewFieldsetNameChange}
        onKeyPress={e => e.key === 'Enter' && addNewFieldset() && props.expand()}
        labelWidth={130}
        endAdornment={
          <InputAdornment position="end">
            <IconButton aria-label="add fieldset" onClick={addNewFieldset} disabled={fieldsetName === ''}>
              <AddCircleIcon />
            </IconButton>
          </InputAdornment>
        }
      />
    </FormControl>
  );
};

export default FieldsetInput;
