import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FileCopy from '@material-ui/icons/FileCopy';
import makeStyles from '@material-ui/styles/makeStyles/makeStyles';
import React from 'react';
import { useLocale, useTranslate } from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import CopyToClipboard from 'react-copy-to-clipboard';
import { useFormState } from 'react-final-form';
import { Field } from 'react-final-form';

import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices?: Choice[];
}

const useAccordionStyles = makeStyles({
  root: {
    borderRadius: '4px !important',
    marginBottom: '4px',
  },
});

const useSummaryStyles = makeStyles({
  root: {
    '&.MuiButtonBase-root': {
      backgroundColor: 'inherit',
      borderRadius: '4px !important',
      filter: 'drop-shadow(1px 1px 1px grey)',
    },
    borderRadius: '4px !important',
  },
  focused: {
    '&.MuiButtonBase-root': {
      filter: 'none',
    },
  },
  content: {
    margin: '4px 0 !important',
  },
});

const getFieldStyle = (snapshot: DraggableStateSnapshot, draggableStyles: any) => ({
  userSelect: 'none',
  background:
    !snapshot.isDragging && !snapshot.draggingOver
      ? '#fdfcfc'
      : snapshot.isDragging && snapshot.draggingOver
      ? 'lightgreen'
      : 'red',
  ...draggableStyles,
});

const FieldsetField: React.FC<Props> = props => {
  const locale = useLocale();
  const field = props.input.value;
  const accordionStyles = useAccordionStyles();
  const summaryStyles = useSummaryStyles();
  const translate = useTranslate();
  const form = useFormState();

  return (
    <>
      {/* TODO: is there any other solution?
        Without this hack following console error will be given (TypeError: state.change is not a function)
       in case fieldset order has been changed and you will try to enter some value into some field's input.*/}
      {Object.keys(props.input.value).map((value: any) => (
        <Field key={`${props.input.name}.${value}`} name={`${props.input.name}.${value}`}>
          {({ input, meta }) => <></>}
        </Field>
      ))}
      <Accordion
        expanded={props.expanded === props.input.name}
        ref={props.provided.innerRef}
        {...props.provided.draggableProps}
        onChange={() => {
          if (!props.expanded || !form.hasValidationErrors) {
            props.setExpanded(props.expanded !== props.input.name ? props.input.name : '');
          }
        }}
        style={getFieldStyle(props.snapshot, props.provided.draggableProps.style)}
        classes={{ ...accordionStyles }}
      >
        <AccordionSummary
          classes={{ ...summaryStyles }}
          expandIcon={<ExpandMoreIcon />}
          aria-controls={`${props.input.name}-content`}
        >
          <Typography>
            <IconButton
              {...props.provided.dragHandleProps}
              disableRipple={false}
              aria-label={translate('vasara.action.move')}
              title={translate('vasara.action.move')}
              onFocus={event => event.stopPropagation()}
              onMouseDown={event => event.stopPropagation()}
              onClick={event => event.stopPropagation()}
            >
              <DragIndicatorIcon />
            </IconButton>
            <strong>
              {props.snapshot.isDragging && !props.snapshot.draggingOver ? (
                <span style={{ display: 'inline-flex', verticalAlign: 'middle' }}>
                  <DeleteIcon />
                  &nbsp;
                  {translate(`vasara.form.${field.type}`)}
                </span>
              ) : (
                translate(`vasara.form.${field.type}`)
              )}
            </strong>
            : {field.label?.[locale] ?? ''} {field.required && '*'}
          </Typography>
          <Box flexGrow="1" textAlign="end" style={{ zIndex: 1 }}>
            <CopyToClipboard text={JSON.stringify(props.input.value)}>
              <IconButton
                title={translate('vasara.action.copy')}
                aria-label={translate('vasara.action.copy')}
                onClick={(e: any) => {
                  e.stopPropagation();
                }}
              >
                <FileCopy />
              </IconButton>
            </CopyToClipboard>
          </Box>
        </AccordionSummary>
        <AccordionDetails id={`${props.input.name}-content`} style={{ padding: '1rem', display: 'block' }}>
          {props.expanded === props.input.name ? props.children : null}
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default FieldsetField;
