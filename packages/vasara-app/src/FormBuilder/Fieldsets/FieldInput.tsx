import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import { InsertDriveFile } from '@material-ui/icons';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import React, { ChangeEvent, useState } from 'react';
import { useTranslate } from 'react-admin';
import { v4 as uuid } from 'uuid';

import { EnabledFieldTypes, FieldDefaultProps } from '../fields';
import { Choice } from '../types';

type Props = {
  fields: any;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
  expand: () => void;
};

interface WidgetType {
  name?: string | undefined;
  value: unknown;
}

const FieldInput: React.FC<Props> = props => {
  const htmlId = uuid();
  const translate = useTranslate();
  const [widgetType, setWidgetType] = useState<WidgetType>({ value: 'string' });

  const handleChange = (event: ChangeEvent<WidgetType>) => {
    setWidgetType({ value: event.target.value });
  };

  const addNewWidget = (): boolean => {
    if (widgetType.value !== '') {
      props.fields.push({ id: uuid(), ...FieldDefaultProps[widgetType.value as string] });
      return true;
    }
    return false;
  };

  const addNewFromClipboard = (text: string) => {
    const sources = props.readonlySourceChoices.map(choice => choice.id);
    const ids = props.fieldChoices.map(choice => choice.id).concat(sources);
    const push = (field: any, fieldIds: string[]) => {
      const allIds = ids.concat(fieldIds);
      field.sources = (field.sources ?? []).filter((source: string) => sources.indexOf(source) > -1);
      field.variables = (field.variables ?? []).filter((variable: any) => [allIds.indexOf(variable.source) > -1]);
      if (field.dependency) {
        if (allIds.indexOf(field.dependency) < 0) {
          field.dependency = null;
        }
      }
      props.fields.push(field);
    };
    try {
      const data: any = JSON.parse(text);
      if (typeof data === 'object' && data.id && data.length === undefined) {
        data.id = uuid();
        push(data, [data.id]);
      }
      if (typeof data === 'object' && data.length && data.length > 0) {
        const ids = [];
        for (let field of data) {
          const id = uuid();
          text = text.replaceAll(field.id, id);
          ids.push(id);
        }
        for (let field of JSON.parse(text)) {
          if (typeof field === 'object' && field.id && field.length === undefined) {
            push(field, ids);
          }
        }
      }
    } catch (e) {}
  };

  return (
    <Grid container spacing={3} justify="flex-start" alignContent="center" alignItems="center">
      <Grid item xs={7} md={3}>
        <FormControl fullWidth variant="filled">
          <InputLabel htmlFor={htmlId}>{translate('vasara.form.helperText.field')}</InputLabel>
          <Select inputProps={{ id: htmlId }} native value={widgetType.name} onChange={handleChange} fullWidth={true}>
            {EnabledFieldTypes.map(type => (
              <option key={type} value={type}>
                {translate(`vasara.form.${type}`)}
              </option>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={2}>
        <Button
          variant="contained"
          color="primary"
          startIcon={<AddCircleIcon />}
          onClick={() => addNewWidget() && props.expand()}
        >
          {translate('vasara.action.add')}
        </Button>
      </Grid>
      <Grid item xs={1}>
        <IconButton
          aria-label={translate('vasara.action.paste')}
          title={translate('vasara.action.paste')}
          onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
            (async () => {
              const data: string = await navigator.clipboard.readText();
              addNewFromClipboard(data);
            })();
            e.stopPropagation();
          }}
        >
          <InsertDriveFile />
        </IconButton>
      </Grid>
    </Grid>
  );
};

export default FieldInput;
