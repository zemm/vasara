import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FileCopy from '@material-ui/icons/FileCopy';
import makeStyles from '@material-ui/styles/makeStyles/makeStyles';
import React, { useCallback, useState } from 'react';
import { TextInput, useLocale, useTranslate } from 'react-admin';
import {
  DragDropContext,
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
  Droppable,
  DroppableStateSnapshot,
} from 'react-beautiful-dnd';
import CopyToClipboard from 'react-copy-to-clipboard';
import { Field } from 'react-final-form';
import { useFieldArray } from 'react-final-form-arrays';

import { EditableFieldComponent } from '../fields';
import { Choice } from '../types';
import FieldInput from './FieldInput';

interface Props {
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  expanded: string;
  setExpanded: (fieldset: string) => void;
  fieldName: string;
  messageChoices: Choice[];
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  vocabularyChoices: Choice[];
}

const useAccordionStyles = makeStyles({
  root: {
    borderRadius: '4px !important',
    marginBottom: '5px',
  },
});

const useSummaryStylesCalm = makeStyles({
  root: {
    '&.MuiButtonBase-root': {
      backgroundColor: '#e8e8e8',
    },
    borderRadius: '4px !important',
  },
  focused: {
    filter: 'none',
  },
});

const useSummaryStylesDragging = makeStyles({
  root: {
    backgroundColor: 'lightgreen !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(0px 1px 1px grey)',
  },
});

const useSummaryStylesOutside = makeStyles({
  root: {
    backgroundColor: '#red !important',
    borderRadius: '4px !important',
    filter: 'drop-shadow(0px 1px 1px grey)',
  },
});

const getFieldStyle = (snapshot: DraggableStateSnapshot, draggableStyles: any) => ({
  userSelect: 'none',
  background:
    !snapshot.isDragging && !snapshot.draggingOver
      ? 'inherit'
      : snapshot.isDragging && snapshot.draggingOver
      ? 'lightgreen'
      : 'red',
  ...draggableStyles,
});

const getFieldsDropStyle = (snapshot: DroppableStateSnapshot) => ({
  background: snapshot.isDraggingOver ? '#e7e7e7' : snapshot.draggingFromThisWith ? '#ecc0c0' : 'inherit',
  borderRadius: '4px',
  padding: '5px',
  minHeight: '50px',
  width: 'auto',
  marginBottom: '1rem',
});

const Fieldset: React.FC<Props> = ({
  provided,
  snapshot,
  expanded,
  setExpanded,
  fieldName,
  messageChoices,
  sourceChoices,
  readonlySourceChoices,
  vocabularyChoices,
}) => {
  const locale = useLocale();
  const { fields } = useFieldArray(`${fieldName}.fields`);
  const [expandedField, setExpandedField] = useState<string>('');
  const expand = () => setExpandedField(`${fieldName}.fields[${fields.length}]`);
  const accordionStyles = useAccordionStyles();
  const summaryStylesCalm = useSummaryStylesCalm();
  const summaryStylesDragging = useSummaryStylesDragging();
  const summaryStylesOutside = useSummaryStylesOutside();
  const translate = useTranslate();

  const fieldChoicePrefix = translate('vasara.form.field');
  const fieldChoices: Choice[] = (fields.value ?? [])
    .filter(field => field?.label?.[locale])
    .map(field => {
      return { id: field.id, name: `${fieldChoicePrefix}: ${field.label[locale]}` };
    });

  const onDragEnd = useCallback(
    (result: any) => {
      if (!result.destination) {
        fields.remove(result.source.index);
      } else if (result.source.index === result.destination.index) {
        return;
      } else {
        fields.move(result.source.index, result.destination.index);
      }
    },
    [fields]
  );

  return (
    <Accordion
      ref={provided.innerRef}
      {...provided.draggableProps}
      expanded={expanded === fieldName}
      onChange={() => setExpanded(expanded !== fieldName ? fieldName : '')}
      classes={{ ...accordionStyles }}
      style={getFieldStyle(snapshot, provided.draggableProps.style)}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`${fieldName}-content`}
        classes={
          !snapshot.isDragging && !snapshot.draggingOver
            ? { ...summaryStylesCalm }
            : snapshot.isDragging && snapshot.draggingOver
            ? { ...summaryStylesDragging }
            : { ...summaryStylesOutside }
        }
      >
        <Typography>
          <IconButton
            {...provided.dragHandleProps}
            disableRipple={false}
            aria-label={translate('vasara.action.move')}
            title={translate('vasara.action.move')}
            onFocus={event => event.stopPropagation()}
            onMouseDown={event => event.stopPropagation()}
            onClick={event => event.stopPropagation()}
          >
            <DragIndicatorIcon />
          </IconButton>
          <strong>
            {snapshot.isDragging && !snapshot.draggingOver ? (
              <span style={{ display: 'inline-flex', verticalAlign: 'middle' }}>
                <DeleteIcon />
                &nbsp;
              </span>
            ) : (
              translate('vasara.form.fieldset') + ': '
            )}
          </strong>
          <Field name={`${fieldName}`}>
            {({ input }) => (
              <>
                {(input.value.label?.[locale] ?? '') + ' '}
                {fields && (
                  <span style={{ fontSize: '65%' }}>
                    ({translate('vasara.form.size')}: {fields.length})
                  </span>
                )}
              </>
            )}
          </Field>
        </Typography>
        <Box flexGrow="1" textAlign="end" style={{ zIndex: 1 }}>
          <CopyToClipboard text={JSON.stringify(fields.value)}>
            <IconButton
              aria-label={translate('vasara.action.copy')}
              title={translate('vasara.action.copy')}
              onClick={(e: any) => {
                e.stopPropagation();
              }}
            >
              <FileCopy />
            </IconButton>
          </CopyToClipboard>
        </Box>
      </AccordionSummary>
      <AccordionDetails id={`${fieldName}-content`}>
        <Grid container spacing={1} justify="center" alignItems="center">
          <Grid item xs={12}>
            <TextInput
              id={`${fieldName}.label.${locale}`}
              label="vasara.form.label"
              source={`${fieldName}.label.${locale}`}
              fullWidth={true}
              placeholder={translate('vasara.form.helperText.label')}
            />
          </Grid>
          <Grid item xs={12}>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId={`${fieldName}`} type="WIDGET">
                {(provided, snapshot) => (
                  <div ref={provided.innerRef} style={getFieldsDropStyle(snapshot)}>
                    {fields.map((name, index) => (
                      <Draggable key={`${name}-${fields.length}`} draggableId={name} index={index}>
                        {(provided, snapshot) => (
                          <Field key={`${name}-${fields.length}`} name={name}>
                            {({ input }) => {
                              const FormComponent = EditableFieldComponent[input.value.type];
                              return (
                                <FormComponent
                                  expanded={expandedField}
                                  setExpanded={setExpandedField}
                                  input={input}
                                  provided={provided}
                                  snapshot={snapshot}
                                  messageChoices={messageChoices}
                                  sourceChoices={sourceChoices}
                                  readonlySourceChoices={readonlySourceChoices}
                                  vocabularyChoices={vocabularyChoices}
                                  fieldChoices={fieldChoices}
                                />
                              );
                            }}
                          </Field>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </Grid>
          <Grid item xs={12}>
            <FieldInput
              fields={fields}
              sourceChoices={sourceChoices}
              readonlySourceChoices={readonlySourceChoices}
              fieldChoices={fieldChoices}
              expand={expand}
            />
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default Fieldset;
