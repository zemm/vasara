import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useTranslate } from 'ra-core';
import React, { useCallback, useContext, useRef, useState } from 'react';
import { BooleanInput, FormDataConsumer, SelectInput, TextInput, TopToolbar, useLocale } from 'react-admin';
import { DragDropContext, Draggable, Droppable, DroppableStateSnapshot } from 'react-beautiful-dnd';
import { useFieldArray } from 'react-final-form-arrays';

import BackButton from '../Components/BackButton';
import { getUserTaskForm } from '../DataProviders/Camunda/helpers';
import UserTaskContext from '../DataProviders/Camunda/UserTaskContext';
import HasuraContext from '../DataProviders/HasuraContext';
import { localeChoices } from '../messages';
import { getSourceChoices, labelFromSchema } from '../util/helpers';
import Fieldset from './Fieldsets/Fieldset';
import FieldsetInput from './Fieldsets/FieldsetInput';
import { FormBuilderProps } from './Form';
import Preview from './Preview';
import { Choice } from './types';

interface Props {
  title: string;
  messageChoices: Choice[];
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  vocabularyChoices: Choice[];
}

const getFieldsetDropStyle = (snapshot: DroppableStateSnapshot) => ({
  background: snapshot.isDraggingOver ? '#edf2f7' : snapshot.draggingFromThisWith ? '#ecc0c0' : 'inherit',
  borderRadius: '4px',
  paddingBottom: '10px',
  width: 'auto',
});

const useStyles = makeStyles({
  mr: {
    marginRight: '1em',
  },
});

const FormBuilderContainer: React.FC<Props> = props => {
  const { fields: fieldsets } = useFieldArray('schema', {
    subscription: { submitting: true, pristine: true },
  });
  const [expanded, setExpanded] = useState<string>(`${fieldsets.name}[0]`);
  const expand = () => setExpanded(`${fieldsets.name}[${fieldsets.length}]`);
  const onDragEnd = useCallback(
    (result: any) => {
      if (!result.destination) {
        fieldsets.remove(result.source.index);
      } else if (result.source.index === result.destination.index) {
        return;
      } else {
        fieldsets.move(result.source.index, result.destination.index);
      }
    },
    [fieldsets]
  );
  const locale = useLocale();
  const toolbarRef = useRef();
  const translate = useTranslate();
  const classes = useStyles();

  return (
    <>
      <TopToolbar>
        <BackButton className={classes.mr} />
        <Preview title={props.title} toolbarRef={toolbarRef} />
      </TopToolbar>
      <Grid container spacing={1} justify="center" alignItems="center" className="VasaraForm-root VasaraForm-builder">
        <Grid item xs={12}>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="fieldset">
              {(provided, snapshot) => (
                <div ref={provided.innerRef} style={getFieldsetDropStyle(snapshot)}>
                  {(fieldsets || []).map((name, index) => (
                    <Draggable key={`${name}-${fieldsets.length}`} draggableId={name} index={index}>
                      {(provided, snapshot) => (
                        <Fieldset
                          fieldName={name}
                          provided={provided}
                          snapshot={snapshot}
                          expanded={expanded}
                          setExpanded={setExpanded}
                          messageChoices={props.messageChoices}
                          vocabularyChoices={props.vocabularyChoices}
                          sourceChoices={props.sourceChoices}
                          readonlySourceChoices={props.readonlySourceChoices}
                        />
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </Grid>
        <Grid item xs={11}>
          <FieldsetInput fields={fieldsets} expand={expand} />
        </Grid>
        <Grid item xs={11}>
          <Accordion elevation={0} variant={'outlined'}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography>{translate('vasara.form.formSettingsLabel')}</Typography>
            </AccordionSummary>
            <AccordionDetails style={{ display: 'block' }}>
              <SelectInput
                label="vasara.form.language"
                source={`settings.language`}
                choices={localeChoices.concat([{ id: 'all', name: translate('vasara.form.multilingual') }])}
                initialValue={'all'}
                fullWidth={true}
                helperText={false}
              />
              <BooleanInput
                label="vasara.form.customSaveAndSubmit"
                helperText="vasara.form.customSaveAndSubmitHelp"
                source={`settings.saveAndSubmit.customize`}
                initialValue={false}
                fullWidth={true}
              />
              <FormDataConsumer subscription={{ values: true }}>
                {({ formData, ...rest }: any) => {
                  return formData?.settings?.saveAndSubmit?.customize ? (
                    <>
                      <TextInput
                        label="vasara.form.customSaveAndSubmitLabel"
                        source={`settings.saveAndSubmit.label.${locale}`}
                        initialValue={translate('vasara.action.saveAndSubmit')}
                        fullWidth={true}
                        helperText={false}
                      />

                      <TextInput
                        label="vasara.form.customSaveAndSubmitSuccessMessage"
                        source={`settings.saveAndSubmit.helperText.${locale}`}
                        initialValue={translate('ra.notification.updated', { smart_count: 1 })}
                        fullWidth={true}
                        helperText={false}
                      />
                    </>
                  ) : null;
                }}
              </FormDataConsumer>
              <BooleanInput
                label="vasara.form.waitForNextTask"
                source="settings.waitForNextTask"
                helperText="vasara.form.waitForNextTaskHelp"
                initialValue={false}
                fullWidth={true}
              />
            </AccordionDetails>
          </Accordion>
        </Grid>
      </Grid>
    </>
  );
};

const FormBuilder: React.FC<FormBuilderProps> = ({ title }) => {
  const translate = useTranslate();
  const context = useContext(UserTaskContext);
  const { introspection, vocabularies } = useContext(HasuraContext);
  const form = getUserTaskForm(context);
  const entitySourceChoices = getSourceChoices(introspection, (context as any).processDefinition.entities);
  const messageChoices = ((context as any).messages || []).map((message: any) => {
    return {
      id: message.name,
      name: message.name,
    };
  });
  const mutableSourceChoices = form
    .map((field: any) => {
      return {
        id: `context.${field.id}`,
        name: `${translate('vasara.form.process')}: ${field.id} (${field.type})`,
      };
    })
    .concat(entitySourceChoices);
  const readonlySourceChoices = (context as any).processDefinition.variables
    .map((field: any) => {
      return {
        id: `context.${field.id}`,
        name: `${translate('vasara.form.process')}: ${field.id} (${field.type})`,
      };
    })
    .concat(entitySourceChoices);
  const vocabularyChoices = [
    { id: 'camunda_User', name: translate('vasara.vocabulary.user') },
    { id: 'camunda_Group', name: translate('vasara.vocabulary.group') },
  ].concat(
    Array.from(vocabularies.values()).map(type => {
      return {
        id: type.name,
        name: labelFromSchema(type),
      };
    })
  );
  vocabularyChoices.sort((a, b) => {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });
  return (
    <FormBuilderContainer
      title={title ? title : context ? (context as any).name : ''}
      messageChoices={messageChoices}
      vocabularyChoices={vocabularyChoices}
      sourceChoices={mutableSourceChoices}
      readonlySourceChoices={readonlySourceChoices}
    />
  );
};

export default FormBuilder;
