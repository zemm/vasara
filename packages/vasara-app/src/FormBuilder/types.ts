export type IndexableObject = {
  [index: string]: any;
};

export interface Choice {
  id: string;
  name: string;
}
