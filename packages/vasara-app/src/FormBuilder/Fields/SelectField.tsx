import { makeStyles } from '@material-ui/styles';
import { IntrospectionField, IntrospectionObjectType, IntrospectionOutputTypeRef } from 'graphql';
import get from 'lodash/get';
import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  Labeled,
  SelectField as RASelectField,
  SelectInput as RASelectInput,
  RadioButtonGroupInput,
  ReferenceField,
  ReferenceInput,
  SimpleFormIterator,
  TextField,
  TextInput,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { withSecret } from '../../Components/VaultTextInput';
import VaultTextReadOnlyInputPlain, { withReadOnlySecret } from '../../Components/VaultTextReadOnlyInputPlain';
import { getUserTaskForm } from '../../DataProviders/Camunda/helpers';
import UserTaskContext from '../../DataProviders/Camunda/UserTaskContext';
import HasuraContext from '../../DataProviders/HasuraContext';
import { unary } from '../../util/feel';
import {
  getFieldTypeName,
  getReferenceResourceName,
  getRelationFromFkField,
  isEnumField,
  isFkField,
  labelFromField,
  labelFromSchema,
} from '../../util/helpers';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

// very large number for autocomplete-type selects so the list shows everything
const UNLIMITED_PER_PAGE = 10000;

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  vocabularyChoices: Choice[];
  fieldChoices: Choice[];
}

const SelectField: React.FC<Props> = (props: any) => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const { sourceChoices, readonlySourceChoices } = props;
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();
  const fullWidth = input.value?.fullWidth ?? true;

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={validateRequired}
        initialValue=""
        fullWidth={fullWidth}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        onChange={(e: ChangeEvent) => e.stopPropagation()}
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={fullWidth}
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          // source options from given list (mutually exclusive with other source options)
          const vocabulary = get(formData, `${input.name}.vocabulary`);
          const varSource = get(formData, `${input.name}.variableSource`);
          return !vocabulary && !varSource ? (
            <ArrayInput source={`${input.name}.options`} label="vasara.form.options">
              <SimpleFormIterator variant="outlined" className="sparse">
                <TextInput source={`id`} label="vasara.form.value" helperText={false} />
                <TextInput source={`name.${locale}`} label="vasara.form.label" helperText={false} />
              </SimpleFormIterator>
            </ArrayInput>
          ) : null;
        }}
      </FormDataConsumer>

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          // source options from vocabulary (mutually exclusive with other source options)
          const options = get(formData, `${input.name}.options`) || [];
          const varSource = get(formData, `${input.name}.variableSource`);
          return options.length === 0 && !varSource ? (
            <AutocompleteInput
              id={`${input.name}-vocabulary`}
              label="vasara.form.vocabulary"
              source={`${input.name}.vocabulary`}
              choices={props.vocabularyChoices}
              fullWidth={true}
              helperText={false}
              clearAlwaysVisible={true}
              resettable={true}
              allowEmpty={true}
            />
          ) : null;
        }}
      </FormDataConsumer>

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          // source options from process variable
          const options = get(formData, `${input.name}.options`) || [];
          const vocabulary = get(formData, `${input.name}.vocabulary`);
          const choices = props.readonlySourceChoices
            // only list process variables
            .filter((src: Choice) => src.id.startsWith('context'))
            // remove the "context." prefix
            .map(
              (src: Choice): Choice => {
                return { id: src.id.substring(8), name: src.name };
              }
            );
          return options.length === 0 && !vocabulary ? (
            <AutocompleteInput
              id={`${input.name}-variableSource`}
              label="vasara.form.variableSource"
              source={`${input.name}.variableSource`}
              choices={choices}
              fullWidth={true}
              helperText="vasara.form.helperText.variableSource"
              clearAlwaysVisible={true}
              resettable={true}
              allowEmpty={true}
            />
          ) : null;
        }}
      </FormDataConsumer>

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const sources = get(formData, `${input.name}.sources`) || [];
          const label = get(formData, `${input.name}.label.${locale}`) || '';
          const readonly = get(formData, `${input.name}.readonly`);
          for (const source of sources) {
            if ((source || '').endsWith('.{}')) {
              return (
                <>
                  <AutocompleteArrayInput
                    id={`${input.name}-sources`}
                    label="vasara.form.sources"
                    helperText="vasara.form.helperText.options"
                    source={`${input.name}.sources`}
                    choices={readonly ? readonlySourceChoices : sourceChoices}
                    validate={validateRequired}
                    fullWidth={true}
                  />
                  <TextInput
                    id={`${input.name}-key`}
                    label="vasara.form.key"
                    helperText="vasara.form.helperText.key"
                    source={`${input.name}.key`}
                    validate={validateRequired}
                    initialValue={label}
                    fullWidth={true}
                  />
                </>
              );
            }
          }
          return (
            <AutocompleteArrayInput
              id={`${input.name}-sources`}
              label="vasara.form.sources"
              helperText="vasara.form.helperText.options"
              source={`${input.name}.sources`}
              choices={readonly ? readonlySourceChoices : sourceChoices}
              validate={validateRequired}
              fullWidth={true}
            />
          );
        }}
      </FormDataConsumer>

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          const vocabulary = get(formData, `${input.name}.vocabulary`);
          return readonly ? null : (
            <>
              <BooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
              />

              {!vocabulary ? (
                <BooleanInput
                  id={`${input.name}-confidential`}
                  label="vasara.form.confidential"
                  source={`${input.name}.confidential`}
                  initialValue={false}
                  className={classes.floatLeft}
                />
              ) : null}

              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
        allowEmpty={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <RASelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
});

const SecretSelectField = withReadOnlySecret(RASelectField);
const SecretRadioButtonGroupInput = withSecret(RadioButtonGroupInput);
const SecretAutocompleteInput = withSecret(AutocompleteInput);

const SimpleSelectInput: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();
  const context = useContext(UserTaskContext);
  const form = getUserTaskForm(context);
  const value = input.value;

  let options = (value.options || []).map((option: any) => {
    return {
      id: option.id,
      name: option.name[locale],
    };
  });

  // Resolve options from user task form
  if (!options || options.length === 0) {
    for (let field of form) {
      const id = `context.${field.id}`;
      if (field.values && value.sources.indexOf(id) > -1) {
        options = field.values;
        break;
      }
    }
  }

  // Resolve options from schema
  const { schemata, fields: fieldsByName, enums } = useContext(HasuraContext);

  if (!options || options.length === 0) {
    for (const source of value?.sources || []) {
      const parts = source.split('.');
      if (fieldsByName.has(parts[0])) {
        const fields = fieldsByName.get(parts[0]) as Map<string, IntrospectionField>;
        const field = fields.get(parts[1]);
        if (field && isEnumField(field)) {
          const type_ = enums.get(getFieldTypeName((field.type as unknown) as IntrospectionOutputTypeRef));
          if (type_) {
            options = type_.enumValues.map(value => {
              return {
                id: value.name,
                name: value.description,
              };
            });
          }
        } else if (field && isFkField(field)) {
          const relation = getRelationFromFkField(field);

          if (fields.has(relation)) {
            const relationField = fields.get(relation) as IntrospectionField;
            const referencedResourceName = getReferenceResourceName(relationField);

            if (referencedResourceName === 'camunda_User') {
              return value.readonly ? (
                <FormDataConsumer subscription={{ values: true }}>
                  {({ formData, ...rest }: any) => (
                    <Labeled label={value.label[locale]} className={classes.fullWidth}>
                      <ReferenceField
                        basePath={`/${referencedResourceName}`}
                        record={formData}
                        label={labelFromField(field)}
                        source={value.id}
                        reference={referencedResourceName}
                        link={false}
                      >
                        <TextField source="name" />
                      </ReferenceField>
                    </Labeled>
                  )}
                </FormDataConsumer>
              ) : (
                <ReferenceInput
                  label={labelFromField(field)}
                  source={value.id}
                  reference={referencedResourceName}
                  fullWidth={true}
                  clearAlwaysVisible={true}
                  resettable={true}
                  allowEmpty={true}
                  filterToQuery={(q: string) => {
                    if (q) {
                      return {
                        q,
                      };
                    }
                  }}
                  perPage={UNLIMITED_PER_PAGE}
                >
                  <AutocompleteInput optionText={'name'} />
                </ReferenceInput>
              );
            }

            const referencedResourceSchema = schemata.get(referencedResourceName) as IntrospectionObjectType;

            if (referencedResourceSchema) {
              return value.readonly ? (
                <FormDataConsumer subscription={{ values: true }}>
                  {({ formData, ...rest }: any) => {
                    return (
                      <Labeled label={value.label[locale]} className={classes.fullWidth}>
                        <ReferenceField
                          basePath={`/${referencedResourceName}`}
                          record={formData}
                          label={labelFromField(field, labelFromSchema(referencedResourceSchema))}
                          source={value.id}
                          reference={referencedResourceName}
                          link="show"
                        >
                          <TextField source="name" />
                        </ReferenceField>
                      </Labeled>
                    );
                  }}
                </FormDataConsumer>
              ) : (
                <ReferenceInput
                  label={labelFromField(field, labelFromSchema(referencedResourceSchema))}
                  source={value.id}
                  reference={referencedResourceName}
                  allowEmpty={true}
                  fullWidth={true}
                  filterToQuery={(q: string) => {
                    if (q) {
                      return {
                        name: {
                          format: 'hasura-raw-query',
                          value: {
                            _ilike: `%${q}%`,
                          },
                        },
                      };
                    }
                  }}
                  perPage={UNLIMITED_PER_PAGE}
                  sort={{ field: 'name', order: 'ASC' }}
                >
                  <AutocompleteInput optionText={'name'} />
                </ReferenceInput>
              );
            }
          }
        }
        if (options.length > 0) {
          break;
        }
      }
      if (options.length > 0) {
        break;
      }
    }
  }

  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        return options.length > 0 ? (
          <Labeled label={value.label[locale]} className={classes.fullWidth}>
            {input.value.confidential ? (
              <SecretSelectField
                record={formData}
                label={value.label[locale]}
                source={value.id}
                choices={options}
                fullWidth={true}
              />
            ) : (
              <RASelectField
                record={formData}
                label={value.label[locale]}
                source={value.id}
                choices={options}
                fullWidth={true}
              />
            )}
          </Labeled>
        ) : (
          // @ts-ignore
          <Labeled label={value.label[locale]} className={classes.fullWidth}>
            {input.value.confidential ? (
              <VaultTextReadOnlyInputPlain
                record={formData}
                label={value.label[locale]}
                source={value.id}
                fullWidth={true}
              />
            ) : (
              <TextField record={formData} label={value.label[locale]} source={value.id} fullWidth={true} />
            )}
          </Labeled>
        );
      }}
    </FormDataConsumer>
  ) : options.length < 6 ? (
    input.value.confidential ? (
      <SecretRadioButtonGroupInput
        label={value.label[locale]}
        helperText={(value.helperText?.[locale] ?? '') || ''}
        source={value.id}
        choices={options}
        row={false}
        validate={value.required ? [required()] : []}
        fullWidth={true}
      />
    ) : (
      <RadioButtonGroupInput
        label={value.label[locale]}
        helperText={(value.helperText?.[locale] ?? '') || ''}
        source={value.id}
        choices={options}
        row={false}
        validate={value.required ? [required()] : []}
        fullWidth={true}
      />
    )
  ) : input.value.confidential ? (
    <SecretAutocompleteInput
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? '') || ''}
      source={value.id}
      choices={options}
      validate={value.required ? [required()] : []}
      fullWidth={true}
      clearAlwaysVisible={true}
      resettable={true}
      allowEmpty={true}
    />
  ) : (
    <AutocompleteInput
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? '') || ''}
      source={value.id}
      choices={options}
      validate={value.required ? [required()] : []}
      fullWidth={true}
      clearAlwaysVisible={true}
      resettable={true}
      allowEmpty={true}
    />
  );
};

const VocabularySelectInput: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();
  const value = input.value;
  const options: any[] = [];
  const fullWidth = input.value?.fullWidth ?? true;

  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const fieldValue = get(formData, value.id);
        return fieldValue !== undefined && fieldValue !== null && fieldValue !== '' ? (
          options.length > 0 ? (
            // @ts-ignore
            <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : null}>
              <RASelectField
                record={formData}
                label={value.label[locale]}
                source={value.id}
                choices={options}
                fullWidth={fullWidth}
              />
            </Labeled>
          ) : (
            // @ts-ignore
            <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : null}>
              <ReferenceField
                record={formData}
                label={value.label[locale]}
                source={value.id}
                reference={value.vocabulary}
                link={false}
              >
                <TextField source="name" />
              </ReferenceField>
            </Labeled>
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : (
    <ReferenceInput
      label={value.label[locale]}
      source={`${value.id}`}
      validate={value.required ? [required()] : []}
      filterToQuery={(q: string) => {
        if (q) {
          return value.vocabulary.startsWith('camunda_')
            ? {
                q,
              }
            : {
                name: {
                  format: 'hasura-raw-query',
                  value: {
                    _ilike: `%${q}%`,
                  },
                },
              };
        }
      }}
      perPage={UNLIMITED_PER_PAGE}
      sort={{ field: 'name', order: 'ASC' }}
      reference={value.vocabulary}
      fullWidth={fullWidth}
      clearAlwaysVisible={true}
      resettable={true}
      allowEmpty={true}
    >
      <AutocompleteInput optionText={'name'} helperText={(value.helperText?.[locale] ?? '') || ''} />
    </ReferenceInput>
  );
};

/** A select input that takes its values from a process variable.
 * The variable's content must be a JSON list of objects with { id, name }.
 */
const VariableSourceSelectInput: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();
  const value = input.value;

  return (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const optionSource = formData.variables.find(
          (procVar: { key: string; valueType: string; value: any }) => procVar.key === value.variableSource
        );
        if (!optionSource) {
          return <>Error: process variable not found</>;
        }
        if (optionSource.valueType !== 'JSON') {
          return <>Error: process variable type incorrect</>;
        }
        const options = JSON.parse(optionSource.value);
        // input elements copied from SimpleSelectInput.
        // not all of them tested, but should work the same
        if (value.readonly) {
          return (
            <FormDataConsumer subscription={{ values: true }}>
              {({ formData, ...rest }: any) => {
                if (options.length > 0) {
                  return (
                    <Labeled label={value.label[locale]} className={classes.fullWidth}>
                      {input.value.confidential ? (
                        <SecretSelectField
                          record={formData}
                          label={value.label[locale]}
                          source={value.id}
                          choices={options}
                          fullWidth={true}
                        />
                      ) : (
                        <RASelectField
                          record={formData}
                          label={value.label[locale]}
                          source={value.id}
                          choices={options}
                          fullWidth={true}
                        />
                      )}
                    </Labeled>
                  );
                }
                return (
                  // @ts-ignore
                  <Labeled label={value.label[locale]} className={classes.fullWidth}>
                    {input.value.confidential ? (
                      <VaultTextReadOnlyInputPlain
                        record={formData}
                        label={value.label[locale]}
                        source={value.id}
                        fullWidth={true}
                      />
                    ) : (
                      <TextField record={formData} label={value.label[locale]} source={value.id} fullWidth={true} />
                    )}
                  </Labeled>
                );
              }}
            </FormDataConsumer>
          );
        }
        if (options.length < 6) {
          if (input.value.confidential) {
            return (
              <SecretRadioButtonGroupInput
                label={value.label[locale]}
                helperText={(value.helperText?.[locale] ?? '') || ''}
                source={value.id}
                choices={options}
                row={false}
                validate={value.required ? [required()] : []}
                fullWidth={true}
              />
            );
          } else {
            return (
              <RadioButtonGroupInput
                label={value.label[locale]}
                helperText={(value.helperText?.[locale] ?? '') || ''}
                source={value.id}
                choices={options}
                row={false}
                validate={value.required ? [required()] : []}
                fullWidth={true}
              />
            );
          }
        }
        if (input.value.confidential) {
          return (
            <SecretAutocompleteInput
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? '') || ''}
              source={value.id}
              choices={options}
              validate={value.required ? [required()] : []}
              fullWidth={true}
              clearAlwaysVisible={true}
              resettable={true}
              allowEmpty={true}
            />
          );
        } else {
          return (
            <AutocompleteInput
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? '') || ''}
              source={value.id}
              choices={options}
              validate={value.required ? [required()] : []}
              fullWidth={true}
              clearAlwaysVisible={true}
              resettable={true}
              allowEmpty={true}
            />
          );
        }
      }}
    </FormDataConsumer>
  );
};

const SelectInputImpl: React.FC<FieldRenderProps> = props => {
  const dependency = (props.input.value.dependency || '').match('\\.')
    ? `${props.input.value.id}:${props.input.value.dependency}`
    : props.input.value.dependency;
  const condition = props.input.value.condition;
  const variables = props.input.value.variables || [];

  if (dependency) {
    return (
      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const context: Record<string, any> = Object.fromEntries(
            variables.map((variable: any) => {
              return formData[variable.source] !== undefined
                ? [variable.id, formData[variable.source]]
                : [variable.id, formData[`${props.input.value.id}:${variable.source}`]];
            })
          );

          if (
            formData[dependency] === undefined ||
            (!condition && formData[dependency]) ||
            (condition && unary(condition, formData[dependency], context))
          ) {
            if (props.input.value?.vocabulary) {
              return <VocabularySelectInput {...props} />;
            }
            if (props.input.value?.variableSource) {
              return <VariableSourceSelectInput {...props} />;
            }
            return <SimpleSelectInput {...props} />;
          }

          return null;
        }}
      </FormDataConsumer>
    );
  }
  if (props.input.value?.vocabulary) {
    return <VocabularySelectInput {...props} />;
  }
  if (props.input.value?.variableSource) {
    return <VariableSourceSelectInput {...props} />;
  }
  return <SimpleSelectInput {...props} />;
};

export const SelectInput = React.memo(SelectInputImpl);
export default React.memo(SelectField);
