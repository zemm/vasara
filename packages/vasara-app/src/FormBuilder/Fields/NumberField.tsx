import { makeStyles } from '@material-ui/styles';
import get from 'lodash/get';
import round from 'lodash/round';
import set from 'lodash/set';
import React, { ChangeEvent, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  Labeled,
  NumberField as RANumberField,
  NumberInput as RANumberInput,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { unary } from '../../util/feel';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const NumberField: React.FC<Props> = (props: any) => {
  const classes = useStyles();
  const input = props.input;
  const locale = useLocale();
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={validateRequired}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        onChange={(e: ChangeEvent) => e.stopPropagation()}
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <SelectInput
        id={`${input.name}-behavior`}
        label="vasara.form.behavior"
        helperText={false}
        source={`${input.name}.behavior`}
        initialValue="default"
        choices={[
          {
            id: 'default',
            name: 'vasara.form.integerBehavior.default',
          },
          {
            id: 'percentage',
            name: 'vasara.form.integerBehavior.percentage',
          },
          {
            id: 'euro',
            name: 'vasara.form.integerBehavior.euro',
          },
        ]}
        validate={validateRequired}
        fullWidth={true}
      />

      <RANumberInput
        id={`${input.name}-decimals`}
        label="vasara.form.decimals"
        helperText={false}
        source={`${input.name}.decimals`}
        format={(v: number) => round(v, 0)}
        parse={(v: string) => round(parseFloat(v), 0)}
        min={0}
        initialValue={0}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const sources = get(formData, `${input.name}.sources`) || [];
          const label = get(formData, `${input.name}.label.${locale}`) || '';
          const readonly = get(formData, `${input.name}.readonly`);
          for (const source of sources) {
            if ((source || '').endsWith('.{}')) {
              return (
                <>
                  <AutocompleteArrayInput
                    id={`${input.name}-sources`}
                    label="vasara.form.sources"
                    source={`${input.name}.sources`}
                    choices={readonly ? props.readonlySourceChoices : props.sourceChoices}
                    validate={validateRequired}
                    fullWidth={true}
                    helperText={false}
                  />
                  <TextInput
                    id={`${input.name}-key`}
                    label="vasara.form.key"
                    helperText="vasara.form.helperText.key"
                    source={`${input.name}.key`}
                    validate={validateRequired}
                    initialValue={label}
                    fullWidth={true}
                  />
                </>
              );
            }
          }
          return (
            <AutocompleteArrayInput
              id={`${input.name}-sources`}
              label="vasara.form.sources"
              source={`${input.name}.sources`}
              choices={readonly ? props.readonlySourceChoices : props.sourceChoices}
              validate={validateRequired}
              fullWidth={true}
              helperText={false}
            />
          );
        }}
      </FormDataConsumer>

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <BooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
              />

              {/*<BooleanInput*/}
              {/*  id={`${input.name}-confidential`}*/}
              {/*  label="vasara.form.confidential"*/}
              {/*  source={`${input.name}.confidential`}*/}
              {/*  initialValue={false}*/}
              {/*  className={classes.floatLeft}*/}
              {/*/>*/}

              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <SelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
  clearLeft: {
    clear: 'left',
  },
});

const NumberInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const [hasFocus, setHasFocus] = useState(false);

  const classes = useStyles();
  const locale = useLocale();
  const value = input.value;
  const fullWidth = input.value?.fullWidth ?? true;

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];

  const behaviors: Record<string, Record<string, string>> = {
    euro: {
      style: 'currency',
      currency: 'EUR',
    },
    percentage: {
      style: 'percent',
    },
  };
  const options: Record<string, string> = value.behavior ? behaviors[value.behavior as string] ?? {} : {};
  const formDataByOptions = (formData: any) => {
    // euro and percentage formatting are only applied for float values
    if (value.behavior === 'euro') {
      const adapted = {
        ...formData,
      };
      set(adapted, value.id, get(adapted, value.id) * 1.0);
      return adapted;
    }
    if (value.behavior === 'percentage') {
      const adapted = {
        ...formData,
      };
      set(adapted, value.id, Math.min(get(adapted, value.id) * 0.01, 1.0));
      return adapted;
    }
    return formData;
  };
  let format: any = undefined; // legacy behavior when no decimals set
  let parse: any = (v: string) => parseInt(v, 10);
  const decimals = parseInt(value.decimals, 10);
  if (!isNaN(decimals) && decimals > 0) {
    setTimeout(() => {
      // work around some weird react-admin bug
      format = (v: number) => (isNaN(v) ? v : hasFocus ? round(v, decimals) : round(v, decimals).toFixed(decimals));
    });
    parse = (v: string) => (isNaN(parseFloat(v)) ? 0 : round(parseFloat(v), decimals));
  }
  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        const fieldValue = get(formData, value.id);
        return fieldValue !== undefined &&
          fieldValue !== null &&
          fieldValue !== '' &&
          (!dependency ||
            formData[dependency] === undefined ||
            (!condition && formData[dependency]) ||
            (condition && unary(condition, formData[dependency], context))) ? (
          // @ts-ignore
          <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : null}>
            <RANumberField
              record={formDataByOptions(formData)}
              label={value.label[locale]}
              source={value.id}
              fullWidth={fullWidth}
              locales={['fi']}
              options={options}
            />
          </Labeled>
        ) : null;
      }}
    </FormDataConsumer>
  ) : dependency ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        return formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          // @ts-ignore
          <RANumberInput
            source={value.id}
            label={value.label[locale]}
            helperText={(value.helperText?.[locale] ?? '') || ''}
            validate={value.required ? [required()] : []}
            min={0}
            format={format}
            parse={parse}
            onFocus={() => setHasFocus(true)}
            onBlur={() => setHasFocus(false)}
            inputProps={hasFocus ? { lang: 'fi' } : { lang: 'en' }}
            step={!isNaN(decimals) && decimals > 0 ? 1.0 / 10 ** decimals : 'any'}
            fullWidth={fullWidth}
          />
        ) : null;
      }}
    </FormDataConsumer>
  ) : (
    <RANumberInput
      source={value.id}
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? '') || ''}
      validate={value.required ? [required()] : []}
      min={0}
      format={format}
      parse={parse}
      onFocus={() => setHasFocus(true)}
      onBlur={() => setHasFocus(false)}
      inputProps={hasFocus ? { lang: 'fi' } : { lang: 'en' }}
      step={!isNaN(decimals) && decimals > 0 ? 1.0 / 10 ** decimals : 'any'}
      fullWidth={fullWidth}
    />
  );
};

export const NumberInput = React.memo(NumberInputImpl);
export default React.memo(NumberField);
