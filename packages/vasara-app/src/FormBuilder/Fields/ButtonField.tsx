import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import ContentSave from '@material-ui/icons/Save';
import get from 'lodash/get';
import React, { ChangeEvent, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteInput,
  BooleanInput,
  Button,
  FormDataConsumer,
  RadioButtonGroupInput,
  SimpleFormIterator,
  TextInput,
  required,
  useDataProvider,
  useGetList,
  useLocale,
  useMutation,
  useNotify,
  useRedirect,
  useTranslate,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import ReactDOM from 'react-dom';

import { StartButton } from '../../Resources/Camunda/ProcessDefinition/StartButton';
import { unary } from '../../util/feel';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const useStyles = makeStyles(theme => ({
  help: {
    color: '#000000',
    fontSize: '1rem',
  },
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
}));

const ButtonField: React.FC<Props> = (props: any) => {
  const locale = useLocale();
  const input = props.input;
  const classes = useStyles();
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();
  const [definitionChoices, setDefinitionChoices] = useState<Choice[]>([]);

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  const { data: definitions } = useGetList(
    'ProcessDefinition',
    { page: 1, perPage: 1000 },
    { field: 'id', order: 'DESC' },
    { latest: true }
  );

  useEffect(() => {
    setDefinitionChoices(
      Object.keys(definitions).map(id => {
        return {
          id: definitions[id].key,
          name: definitions[id].name,
        };
      })
    );
  }, [definitions]);

  return (
    <FieldsetField {...props}>
      <TextInput
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        initialValue=""
        fullWidth={true}
        relperText={false}
      />

      <RadioButtonGroupInput
        source={`${input.name}.buttonType`}
        label="vasara.form.buttonType"
        choices={[
          { id: 'message', name: 'vasara.form.buttonTypeMessage' },
          { id: 'start', name: 'vasara.form.buttonTypeStart' },
        ]}
        initialValue="message"
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const buttonType = get(formData, `${input.name}.buttonType`);
          return buttonType === 'start' ? (
            <>
              <AutocompleteInput
                id={`${input.name}-definition`}
                label="vasara.form.processDefinitionName"
                source={`${input.name}.definitionKey`}
                choices={definitionChoices}
                fullWidth={true}
                helperText={false}
                className={classes.clearLeft}
                resettable={true}
              />
            </>
          ) : (
            <>
              <AutocompleteInput
                id={`${input.name}-message`}
                label="vasara.form.messageName"
                source={`${input.name}.message`}
                choices={props.messageChoices}
                fullWidth={true}
                helperText={false}
                className={classes.clearLeft}
                resettable={true}
              />

              <ArrayInput source={`${input.name}.variables`} label="vasara.form.messageVariables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>

              <BooleanInput
                id={`${input.name}-confirmation`}
                label="vasara.form.confirmation"
                source={`${input.name}.confirmation`}
                initialValue={true}
              />

              <TextInput
                id={`${input.name}-helperText`}
                label="vasara.form.messageSuccess"
                onChange={(e: ChangeEvent) => e.stopPropagation()}
                source={`${input.name}.helperText.${locale}`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>
    </FieldsetField>
  );
};

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export const ButtonImpl: React.FC<any> = ({ input, actionsRef, record }) => {
  const [taskId, setTaskId] = React.useState(null);
  const [businessKey, setBusinessKey] = React.useState(null);
  const [payload, setPayload] = React.useState<any>(null);
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const locale = useLocale();
  const translate = useTranslate();
  const value = input.value;

  const [mutate] = useMutation();
  const dataProvider = useDataProvider();
  const notify = useNotify();
  const redirect = useRedirect();

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];

  if (!actionsRef || !actionsRef.current) {
    return null;
  }

  const sendMessage = (payload: any) => {
    new Promise<any>((resolve, reject) => {
      mutate(
        {
          type: 'update',
          resource: 'ProcessInstance',
          payload,
        },
        {
          onSuccess: ({ data }) => {
            resolve(data);
          },
          onFailure: error => {
            notify(error.message, 'error', { _: error.message });
            resolve(null);
          },
        }
      );
    });
  };

  const correlate = async (taskId_?: string, businessKey_?: string, payload_?: any) => {
    taskId_ = taskId || taskId_;
    businessKey_ = businessKey || businessKey_;
    payload_ = payload || payload_;
    setLoading(true);
    await sendMessage(payload_);
    if (input.value?.helperText?.[locale]) {
      notify(input.value.helperText?.[locale], 'info', {
        _: input.value.helperText?.[locale],
      });
    }
    // TODO: Refactor this loop to useEffect instead of loop to allow canceling
    // on manual location change.
    for (let i = 1; i < 3; i++) {
      await sleep(300 * i); // wait for task to change or disappear
      const results = await dataProvider.getList('UserTask', {
        pagination: {
          page: 1,
          perPage: 1000,
        },
        sort: {
          field: 'taskDefinitionKey',
          order: 'asc',
        },
        filter: { businessKey: businessKey_ },
      });
      if (results.total && taskId_ !== results.data[0].id) {
        redirect('edit', '/UserTask', results.data[0].id);
        return;
      }
    }
    redirect('/UserTask');
  };

  const dialog = (
    <Dialog
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{translate('vasara.form.areYouSure')}</DialogTitle>
      <DialogActions>
        <Button
          onClick={async () => await correlate()}
          color="primary"
          variant="contained"
          label={translate('vasara.form.yesAction', { action: input.value?.label?.[locale] })}
          disabled={loading}
          autoFocus
        >
          {loading ? <CircularProgress size={25} thickness={2} /> : <ContentSave />}
        </Button>
        <Button onClick={() => setOpen(false)} color="primary" label="vasara.form.cancel" disabled={loading} />
      </DialogActions>
    </Dialog>
  );
  return (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        const button = (
          <>
            <Button
              color="primary"
              variant="contained"
              label={value?.label?.[locale]}
              onClick={async () => {
                const businessKey_ = formData?.processInstance?.businessKey;
                const payload_ = {
                  id: formData.processInstanceId,
                  data: {
                    message: input.value['message'],
                    variables: Object.keys(context).map((key: string) => {
                      return { key, value: context[key] };
                    }),
                  },
                };
                if (value?.confirmation === true) {
                  setTaskId(formData.id);
                  setBusinessKey(businessKey_);
                  setPayload(payload_);
                  setOpen(true);
                } else {
                  await correlate(formData.id, businessKey_, payload_);
                }
              }}
            />
            {dialog}
          </>
        );
        return dependency ? (
          formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
            value.buttonType === 'start' ? (
              <StartButton
                record={{
                  key: value.definitionKey,
                  startableInTasklist: true,
                }}
                label={value?.label?.[locale]}
              />
            ) : (
              ReactDOM.createPortal(button, actionsRef.current)
            )
          ) : null
        ) : value.buttonType === 'start' ? (
          <StartButton
            record={{
              key: value.definitionKey,
              startableInTasklist: true,
            }}
            label={value?.label?.[locale]}
          />
        ) : (
          ReactDOM.createPortal(button, actionsRef.current)
        );
      }}
    </FormDataConsumer>
  );
};

export const ButtonDisplay = React.memo(ButtonImpl);
export default React.memo(ButtonField);
