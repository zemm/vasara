import { makeStyles } from '@material-ui/styles';
import get from 'lodash/get';
import React, { ChangeEvent, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  FormDataConsumer,
  Labeled,
  BooleanField as RABooleanField,
  BooleanInput as RABooleanInput,
  RadioButtonGroupInput,
  SelectField,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  choices,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { unary } from '../../util/feel';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const BooleanField: React.FC<Props> = props => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={validateRequired}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        onChange={(e: ChangeEvent) => e.stopPropagation()}
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <RadioButtonGroupInput
        source={`${input.name}.inputType`}
        label="vasara.form.inputType"
        choices={[
          { id: 'toggle', name: 'vasara.form.toggle' },
          { id: 'radio', name: 'vasara.form.radio' },
        ]}
        initialValue="radio"
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const sources = get(formData, `${input.name}.sources`) || [];
          const label = get(formData, `${input.name}.label.${locale}`) || '';
          const readonly = get(formData, `${input.name}.readonly`);
          for (const source of sources) {
            if ((source || '').endsWith('.{}')) {
              return (
                <>
                  <AutocompleteArrayInput
                    id={`${input.name}-sources`}
                    label="vasara.form.sources"
                    source={`${input.name}.sources`}
                    choices={readonly ? props.readonlySourceChoices : props.sourceChoices}
                    validate={validateRequired}
                    fullWidth={true}
                    helperText={false}
                  />
                  <TextInput
                    id={`${input.name}-key`}
                    label="vasara.form.key"
                    helperText="vasara.form.helperText.key"
                    source={`${input.name}.key`}
                    validate={validateRequired}
                    initialValue={label}
                    fullWidth={true}
                  />
                </>
              );
            }
          }
          return (
            <AutocompleteArrayInput
              id={`${input.name}-sources`}
              label="vasara.form.sources"
              source={`${input.name}.sources`}
              choices={readonly ? props.readonlySourceChoices : props.sourceChoices}
              validate={validateRequired}
              fullWidth={true}
              helperText={false}
            />
          );
        }}
      </FormDataConsumer>

      <RABooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <RABooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
              />

              {/*<RABooleanInput*/}
              {/*  id={`${input.name}-confidential`}*/}
              {/*  label="vasara.form.confidential"*/}
              {/*  source={`${input.name}.confidential`}*/}
              {/*  initialValue={false}*/}
              {/*  className={classes.floatLeft}*/}
              {/*/>*/}

              <RABooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <SelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
  clearLeft: {
    clear: 'left',
  },
});

export const BooleanInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();
  const value = input.value;
  const fullWidth = input.value?.fullWidth ?? true;

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];

  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }} classes={fullWidth ? classes.fullWidth : fullWidth}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        const fieldValue = get(formData, value.id);
        return (fieldValue === true || fieldValue === false) &&
          (!dependency ||
            formData[dependency] === undefined ||
            (!condition && formData[dependency]) ||
            (condition && unary(condition, formData[dependency], context))) ? (
          // @ts-ignore
          value.inputType === 'toggle' ? (
            <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : fullWidth}>
              <RABooleanField record={formData} label={value.label[locale]} source={value.id} fullWidth={fullWidth} />
            </Labeled>
          ) : (
            <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : ''}>
              <SelectField
                record={formData}
                label={value.label[locale]}
                source={value.id}
                fullWidth={fullWidth}
                choices={[
                  { id: true, name: 'vasara.form.yes' },
                  { id: false, name: 'vasara.form.no' },
                ]}
              />
            </Labeled>
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : dependency ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        return formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          value.inputType === 'toggle' ? (
            <RABooleanInput
              source={value.id}
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? '') || ''}
              validate={
                value.required
                  ? [required(), choices([true], 'ra.validation.required')]
                  : [choices([true, false], 'ra.validation.required')]
              }
              initialValue={false}
              fullWidth={fullWidth}
            />
          ) : (
            <RadioButtonGroupInput
              source={value.id}
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? '') || ''}
              validate={
                value.required
                  ? [required(), choices([true, false], 'ra.validation.required')]
                  : [choices([true, false], 'ra.validation.required')]
              }
              choices={[
                { id: true, name: 'vasara.form.yes' },
                { id: false, name: 'vasara.form.no' },
              ]}
              fullWidth={fullWidth}
            />
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : value.inputType === 'toggle' ? (
    <RABooleanInput
      source={value.id}
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? '') || ''}
      validate={
        value.required
          ? [required(), choices([true], 'ra.validation.required')]
          : [choices([true, false], 'ra.validation.required')]
      }
      initialValue={false}
      fullWidth={fullWidth}
    />
  ) : (
    <RadioButtonGroupInput
      source={value.id}
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? '') || ''}
      validate={
        value.required
          ? [required(), choices([true, false], 'ra.validation.required')]
          : [choices([true, false], 'ra.validation.required')]
      }
      choices={[
        { id: true, name: 'vasara.form.yes' },
        { id: false, name: 'vasara.form.no' },
      ]}
      fullWidth={fullWidth}
    />
  );
};

export const BooleanInput = React.memo(BooleanInputImpl);
export default React.memo(BooleanField);
