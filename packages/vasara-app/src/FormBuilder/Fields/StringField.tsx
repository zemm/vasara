import { makeStyles } from '@material-ui/styles';
import { evaluate } from 'feelin';
import get from 'lodash/get';
import React, { useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  Labeled,
  SelectInput,
  SimpleFormIterator,
  TextField,
  TextInput,
  UrlField,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { getUserEmail, getUserFullName } from '../../Auth/authProvider';
import VaultTextField from '../../Components/VaultTextField';
import VaultTextInput from '../../Components/VaultTextInput';
import { unary } from '../../util/feel';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const StringField: React.FC<Props> = props => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const { sourceChoices, readonlySourceChoices } = props;
  const combinedChoices = props.fieldChoices.concat(readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  // TODO: Filtering choice options is a good idea, but requires care, because camunda
  // and postgres report slightly different type names
  // const sourceChoices = props.sourceChoices.filter(
  //   choice => choice.name.endsWith('string)') || choice.name.endsWith('jsonb)') || choice.name.endsWith('text)')
  // );
  // const readonlySourceChoices = props.readonlySourceChoices.filter(
  //   choice => choice.name.endsWith('string)') || choice.name.endsWith('jsonb)') || choice.name.endsWith('text)')
  // );

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={[required()]}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const sources = get(formData, `${input.name}.sources`) || [];
          const label = get(formData, `${input.name}.label.${locale}`) || '';
          const readonly = get(formData, `${input.name}.readonly`);
          for (const source of sources) {
            if ((source || '').endsWith('.{}')) {
              return (
                <>
                  {/* This is technically a FEEL expression and this could be a free text field with an evaluation preview. */}
                  {readonly ? null : (
                    <SelectInput
                      id={`${input.name}-initialValue`}
                      label="vasara.form.initialValue"
                      helperText={false}
                      source={`${input.name}.initialValue`}
                      initialValue=""
                      choices={[
                        {
                          id: 'profile.name',
                          name: 'vasara.form.initialValue.name',
                        },
                        {
                          id: 'profile.email',
                          name: 'vasara.form.initialValue.email',
                        },
                      ]}
                      validate={[]}
                      fullWidth={true}
                    />
                  )}
                  <AutocompleteArrayInput
                    id={`${input.name}-sources`}
                    label="vasara.form.sources"
                    source={`${input.name}.sources`}
                    choices={readonly ? readonlySourceChoices : sourceChoices}
                    validate={validateRequired}
                    fullWidth={true}
                    helperText={false}
                  />
                  <TextInput
                    id={`${input.name}-key`}
                    label="vasara.form.key"
                    helperText="vasara.form.helperText.key"
                    source={`${input.name}.key`}
                    validate={validateRequired}
                    initialValue={label}
                    fullWidth={true}
                  />
                </>
              );
            }
          }
          return (
            <>
              {/* This is technically a FEEL expression and this could be a free text field with an evaluation preview. */}
              {readonly ? null : (
                <SelectInput
                  id={`${input.name}-initialValue`}
                  label="vasara.form.initialValue.label"
                  helperText={false}
                  source={`${input.name}.initialValue`}
                  initialValue=""
                  choices={[
                    {
                      id: 'profile.name',
                      name: 'vasara.form.initialValue.name',
                    },
                    {
                      id: 'profile.email',
                      name: 'vasara.form.initialValue.email',
                    },
                  ]}
                  validate={[]}
                  fullWidth={true}
                />
              )}
              <AutocompleteArrayInput
                id={`${input.name}-sources`}
                label="vasara.form.sources"
                source={`${input.name}.sources`}
                choices={readonly ? readonlySourceChoices : sourceChoices}
                validate={validateRequired}
                fullWidth={true}
                helperText={false}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? (
            <SelectInput
              id={`${input.name}-behavior`}
              label="vasara.form.behavior"
              helperText={false}
              source={`${input.name}.behavior`}
              initialValue="default"
              choices={[
                {
                  id: 'default',
                  name: 'vasara.form.stringBehavior.default',
                },
                {
                  id: 'link',
                  name: 'vasara.form.stringBehavior.link',
                },
                {
                  id: 'error',
                  name: 'vasara.form.stringBehavior.error',
                },
              ]}
              validate={validateRequired}
              fullWidth={true}
            />
          ) : (
            <>
              <BooleanInput
                id={`${input.name}-multiline`}
                label="vasara.form.multiline"
                source={`${input.name}.multiline`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />

              <BooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />

              <BooleanInput
                id={`${input.name}-confidential`}
                label="vasara.form.confidential"
                source={`${input.name}.confidential`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />

              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <SelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
  multiline: {
    whiteSpace: 'pre-line',
  },
  error: {
    color: 'red',
    whiteSpace: 'pre-line',
  },
});

const StringInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();

  const value = input.value;
  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];
  const fullWidth = input.value?.fullWidth ?? true;
  let initialValue = input.value?.initialValue || '';
  if (initialValue) {
    const profile = {
      name: getUserFullName(),
      email: getUserEmail(),
    };
    try {
      initialValue = evaluate(initialValue, { profile });
    } catch (e) {
      console?.warn(e);
      initialValue = '';
    }
  } else {
    initialValue = '';
  }
  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        const fieldValue = get(formData, value.id);
        return fieldValue !== undefined &&
          fieldValue !== null &&
          fieldValue !== '' &&
          (!dependency ||
            formData[dependency] === undefined ||
            (!condition && formData[dependency]) ||
            (condition && unary(condition, formData[dependency], context))) ? (
          input.value.confidential ? (
            <VaultTextField
              record={formData}
              label={value.label[locale]}
              source={value.id}
              fullWidth={fullWidth}
              className={classes.multiline}
            />
          ) : (
            // @ts-ignore
            <Labeled label={value.label[locale]} className={fullWidth ? classes.fullWidth : null}>
              {value.behavior === 'link' ? (
                <UrlField
                  record={formData}
                  label={value.label[locale]}
                  source={value.id}
                  fullWidth={fullWidth}
                  target="_blank"
                  className={classes.multiline}
                />
              ) : value.behavior === 'error' ? (
                <TextField
                  record={formData}
                  label={value.label[locale]}
                  source={value.id}
                  fullWidth={fullWidth}
                  className={classes.error}
                />
              ) : (
                <TextField
                  record={formData}
                  label={value.label[locale]}
                  source={value.id}
                  fullWidth={fullWidth}
                  className={classes.multiline}
                />
              )}
            </Labeled>
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : dependency ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        return formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          input.value.confidential ? (
            <VaultTextInput
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? false) || false}
              source={value.id}
              validate={value.required ? [required()] : []}
              multiline={value.multiline}
              parse={(v: string) => v}
              initialValue={initialValue}
              fullWidth={fullWidth}
            />
          ) : (
            <TextInput
              label={value.label[locale]}
              helperText={(value.helperText?.[locale] ?? false) || false}
              source={value.id}
              validate={value.required ? [required()] : []}
              multiline={value.multiline}
              parse={(v: string) => v}
              initialValue={initialValue}
              fullWidth={fullWidth}
            />
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : input.value.confidential ? (
    <VaultTextInput
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? false) || false}
      source={value.id}
      validate={value.required ? [required()] : []}
      multiline={value.multiline}
      parse={(v: string) => v}
      initialValue={initialValue}
      fullWidth={fullWidth}
    />
  ) : (
    <TextInput
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? false) || false}
      source={value.id}
      validate={value.required ? [required()] : []}
      multiline={value.multiline}
      parse={(v: string) => v}
      initialValue={initialValue}
      fullWidth={fullWidth}
    />
  );
};

export const StringInput = React.memo(StringInputImpl);
export default React.memo(StringField);
