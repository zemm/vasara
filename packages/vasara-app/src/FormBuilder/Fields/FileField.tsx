import { makeStyles } from '@material-ui/styles';
import get from 'lodash/get';
import round from 'lodash/round';
import React, { ChangeEvent, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  NumberInput,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import ByteaFileField from '../../Components/ByteaFileField';
import ByteaFileInput from '../../Components/ByteaFileInput';
import { unary } from '../../util/feel';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
});

const acceptChoices: Choice[] = [
  { id: 'text/csv', name: 'Comma-separated values (CSV) (.csv)' },
  { id: 'application/msword', name: 'Microsoft Word (.doc)' },
  {
    id: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    name: 'Microsoft Word (OpenXML) (.docx)',
  },
  { id: 'image/jpeg', name: 'JPEG images (.jpeg, .jpg)' },
  { id: 'application/vnd.oasis.opendocument.presentation', name: 'OpenDocument presentation document (.odp)' },
  { id: 'application/vnd.oasis.opendocument.spreadsheet', name: 'OpenDocument spreadsheet document (.ods)' },
  { id: 'application/vnd.oasis.opendocument.text', name: 'OpenDocument text document (.odt)' },
  { id: 'image/png', name: 'Portable Network Graphics (.png)' },
  { id: 'application/pdf', name: 'Adobe Portable Document Format (PDF) (.pdf)' },
  { id: 'application/vnd.ms-powerpoint', name: 'Microsoft PowerPoint (.ppt)' },
  {
    id: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    name: 'Microsoft PowerPoint (OpenXML) (.pptx)',
  },
  { id: 'application/rtf', name: 'Rich Text Format (RTF) (.rtf)' },
  { id: 'image/svg+xml', name: 'Scalable Vector Graphics (SVG) (.svg)' },
  { id: 'image/tiff', name: 'Tagged Image File Format (TIFF) (.tif, .tiff)' },
  { id: 'text/plain', name: 'Text, (generally ASCII or ISO 8859-n) (.txt)' },
  { id: 'application/vnd.ms-excel', name: 'Microsoft Excel (.xls)' },
  {
    id: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    name: 'Microsoft Excel (OpenXML) (.xlsx)',
  },
  { id: 'application/xml, text/xml', name: 'XML document (.xml)' },
  { id: 'application/zip', name: 'ZIP archive (.zip)' },
];

const FileField: React.FC<Props> = (props: any) => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={validateRequired}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        onChange={(e: ChangeEvent) => e.stopPropagation()}
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <AutocompleteArrayInput
                id={`${input.name}-accept`}
                label="vasara.form.mimeTypes"
                source={`${input.name}.accept`}
                choices={acceptChoices}
                fullWidth={true}
                helperText={false}
                resettable={true}
                allowEmpty={true}
                initialValue={[]}
              />
              <NumberInput
                id={`${input.name}-max`}
                label="vasara.form.maxFileSize"
                helperText="vasara.form.maxFileSizeHelp"
                source={`${input.name}.max`}
                format={(v: number) => round(v, 0)}
                parse={(v: string) => round(parseFloat(v), 0)}
                min={1}
                initialValue={10}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        label="vasara.form.sources"
        source={`${input.name}.sources`}
        choices={props.sourceChoices}
        validate={validateRequired}
        fullWidth={true}
        helperText={false}
      />

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        fullWidth={true}
        helperText={false}
        className={classes.floatLeft}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                fullWidth={true}
                helperText={false}
                className={classes.floatLeft}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
        allowEmpty={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <SelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const maxFileSize = (max: number = 0) => (value: any) => {
  return max && value?.rawFile?.size && value.rawFile.size > max * 1024 * 1024
    ? { message: 'vasara.validation.maxFileSize', args: { smart_count: max } }
    : undefined;
};

export const FileInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const locale = useLocale();
  const value = input.value;

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];
  const accept = Array.isArray(value?.accept) && value.accept.length ? value.accept.join(',') : undefined;
  const validator = maxFileSize(value?.max || 0);

  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${input.value.id}:${variable.source}`]];
          })
        );
        const record: any = {
          metadata: {},
        };
        record[value.id] = formData[value.id];
        record.metadata[value.id] = formData?.[`${value.id}.metadata`] ?? {};
        return !dependency ||
          formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          <ByteaFileField record={record} label={value.label[locale]} source={value.id} fullWidth={true} />
        ) : null;
      }}
    </FormDataConsumer>
  ) : (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${input.value.id}:${variable.source}`]];
          })
        );
        if (
          !dependency ||
          formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context))
        ) {
          const record: any = {
            metadata: {},
          };
          record[value.id] = formData[value.id];
          record.metadata[value.id] = formData?.[`${value.id}.metadata`] ?? {};
          return (
            <ByteaFileInput
              label={value.label[locale]}
              record={record}
              helperText={(value.helperText?.[locale] ?? '') || ''}
              source={value.id}
              validate={value.required ? [required(), validator] : [validator]}
              fullWidth={true}
              accept={accept}
            />
          );
        } else {
          return null;
        }
      }}
    </FormDataConsumer>
  );
};

export const FileInput = React.memo(FileInputImpl);
export default React.memo(FileField);
