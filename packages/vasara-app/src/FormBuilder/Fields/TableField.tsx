import { makeStyles } from '@material-ui/styles';
import get from 'lodash/get';
import round from 'lodash/round';
import { SelectInput as RASelectInput } from 'ra-ui-materialui/lib/input';
import React, { useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  NumberInput,
  ArrayInput as RAArrayInput,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  Validator,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { Field } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import { FieldRenderProps } from 'react-final-form-hooks';

import { unary } from '../../util/feel';
import { FieldTypes } from '../constants';
import { EnabledFieldTypesChoices, FieldComponent } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  vocabularyChoices: Choice[];
  fieldChoices: Choice[];
}

const TableField: React.FC<Props> = props => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const { sourceChoices, readonlySourceChoices } = props;
  const combinedChoices = props.fieldChoices.concat(readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={[required()]}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <RAArrayInput source={`${input.name}.fields`} label="vasara.form.columns">
        <SimpleFormIterator>
          <RASelectInput
            source={`type`}
            label="vasara.form.type"
            choices={EnabledFieldTypesChoices.filter((choice: Choice) =>
              [FieldTypes.STRING, FieldTypes.NUMBER, FieldTypes.BOOLEAN, FieldTypes.DATE, FieldTypes.SELECT].includes(
                choice.id
              )
            )}
            validate={validateRequired}
          />
          <TextInput source={`id`} label="vasara.form.id" validate={validateRequired} />
          <TextInput source={`label.${locale}`} label="vasara.form.label" validate={validateRequired} />
          <FormDataConsumer subscription={{ values: true }}>
            {({ formData, getSource, ...rest }: any) => {
              const type = get(formData, getSource('type')) || '';
              const behavior = getSource('behavior');
              const decimals = getSource('decimals');
              const fullWidth = getSource('fullWidth');
              const multiline = getSource('multiline');
              const readonly = getSource('readonly');
              const required = getSource('required');
              const vocabulary = getSource('vocabulary');
              return type === FieldTypes.SELECT && vocabulary ? (
                <>
                  <AutocompleteInput
                    label="vasara.form.vocabulary"
                    source={vocabulary}
                    choices={props.vocabularyChoices}
                    helperText={false}
                    clearAlwaysVisible={true}
                    resettable={true}
                    allowEmpty={true}
                    fullWidth={false}
                    validate={validateRequired}
                  />
                  <BooleanInput
                    source={readonly}
                    label="vasara.form.readonly"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={fullWidth}
                    label="vasara.form.fullWidth"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={required}
                    label="ra.validation.required"
                    helperText={false}
                    initialValue={false}
                  />
                </>
              ) : type === FieldTypes.NUMBER ? (
                <>
                  <SelectInput
                    id={`${input.name}-behavior`}
                    label="vasara.form.behavior"
                    helperText={false}
                    source={behavior}
                    initialValue="default"
                    choices={[
                      {
                        id: 'default',
                        name: 'vasara.form.integerBehavior.default',
                      },
                      {
                        id: 'percentage',
                        name: 'vasara.form.integerBehavior.percentage',
                      },
                      {
                        id: 'euro',
                        name: 'vasara.form.integerBehavior.euro',
                      },
                    ]}
                    validate={validateRequired}
                    fullWidth={true}
                  />
                  <NumberInput
                    id={`${input.name}-decimals`}
                    label="vasara.form.decimals"
                    helperText={false}
                    source={decimals}
                    format={(v: number) => round(v, 0)}
                    parse={(v: string) => round(parseFloat(v), 0)}
                    initialValue={0}
                  />
                  <BooleanInput
                    source={readonly}
                    label="vasara.form.readonly"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={fullWidth}
                    label="vasara.form.fullWidth"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={required}
                    label="ra.validation.required"
                    helperText={false}
                    initialValue={false}
                  />
                </>
              ) : type === FieldTypes.STRING && multiline ? (
                <>
                  <BooleanInput
                    source={readonly}
                    label="vasara.form.readonly"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    id={`${input.name}-multiline`}
                    label="vasara.form.multiline"
                    helperText={false}
                    source={multiline}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={fullWidth}
                    label="vasara.form.fullWidth"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={required}
                    label="ra.validation.required"
                    helperText={false}
                    initialValue={false}
                  />
                </>
              ) : (
                <>
                  <BooleanInput
                    source={readonly}
                    label="vasara.form.readonly"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={fullWidth}
                    label="vasara.form.fullWidth"
                    helperText={false}
                    initialValue={false}
                  />
                  <BooleanInput
                    source={required}
                    label="ra.validation.required"
                    helperText={false}
                    initialValue={false}
                  />
                </>
              );
            }}
          </FormDataConsumer>
        </SimpleFormIterator>
      </RAArrayInput>

      <NumberInput source={`${input.name}.min`} min={0} label="vasara.form.min" defaultValue={0} />
      <NumberInput source={`${input.name}.max`} min={0} label="vasara.form.max" defaultValue={0} />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return (
            <AutocompleteArrayInput
              id={`${input.name}-sources`}
              label="vasara.form.sources"
              source={`${input.name}.sources`}
              choices={readonly ? readonlySourceChoices : sourceChoices}
              validate={validateRequired}
              fullWidth={true}
              helperText={false}
            />
          );
        }}
      </FormDataConsumer>

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
        helperText={false}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <BooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />

              {/*<BooleanInput*/}
              {/*  id={`${input.name}-confidential`}*/}
              {/*  label="vasara.form.confidential"*/}
              {/*  source={`${input.name}.confidential`}*/}
              {/*  initialValue={false}*/}
              {/*  className={classes.floatLeft}*/}
              {/*  helperText={false}*/}
              {/*/>*/}

              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
                helperText={false}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>

      <SelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
});

const maxRequired = (max: number = 1) => (value: any) => {
  return value && value.length > max
    ? { message: 'vasara.validation.tooManyRows', args: { smart_count: max } }
    : undefined;
};

const minRequired = (min: number = 1) => (value: any) => {
  return value && value < min ? { message: 'vasara.validation.notEnoughRows', args: { smart_count: min } } : undefined;
};

export const TableInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const locale = useLocale();
  const value = input.value;

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];
  const validator: Validator[] = [];
  if ((value.min || -1) > 0) {
    validator.push(minRequired(value.min));
    if (value.required) {
      validator.push(required());
    }
  }
  if (value.max || -1 > 0) {
    validator.push(maxRequired(Math.max(value.min, value.max)));
  }
  return value.readonly ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        const fieldValue = get(formData, value.id);
        return fieldValue !== undefined &&
          fieldValue !== null &&
          fieldValue !== '' &&
          (!dependency ||
            formData[dependency] === undefined ||
            (!condition && formData[dependency]) ||
            (condition && unary(condition, formData[dependency], context))) ? (
          // @ts-ignore
          <RAArrayInput
            label={value.label[locale]}
            helperText={(value.helperText?.[locale] ?? false) || ''}
            source={value.id}
            fullWidth={true}
          >
            <SimpleFormIterator disableAdd={true} disableRemove={true} className="VasaraTableFieldIterator">
              <FieldArray name={`${input.name}.fields`}>
                {({ fields, ...rest }) =>
                  fields.map(name => (
                    <Field key={`${(rest as any).id}.${name}`} name={name}>
                      {props => {
                        const FormComponent = FieldComponent[props.input.value.type];
                        const id = `${(rest as any).id}.${props.input.value.id}`;
                        const column = {
                          ...props,
                          key: id,
                          input: {
                            ...props.input,
                            value: {
                              ...props.input.value,
                              id,
                              fullWidth: !!props.input.value?.fullWidth,
                              readonly: true,
                            },
                          },
                        };
                        return <FormComponent {...column} actionsRef={null} toolbarRef={null} />;
                      }}
                    </Field>
                  ))
                }
              </FieldArray>
            </SimpleFormIterator>
          </RAArrayInput>
        ) : null;
      }}
    </FormDataConsumer>
  ) : dependency ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        return formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          // @ts-ignore
          <RAArrayInput
            label={value.label[locale]}
            helperText={(value.helperText?.[locale] ?? false) || ''}
            source={value.id}
            fullWidth={true}
            validate={validator}
          >
            <SimpleFormIterator className="VasaraTableFieldIterator">
              <FieldArray name={`${input.name}.fields`}>
                {({ fields, ...rest }) =>
                  fields.map(name => (
                    <Field key={`${(rest as any).id}.${name}`} name={name}>
                      {props => {
                        const FormComponent = FieldComponent[props.input.value.type];
                        const id = `${(rest as any).id}.${props.input.value.id}`;
                        const column = {
                          ...props,
                          key: id,
                          input: {
                            ...props.input,
                            value: {
                              ...props.input.value,
                              id,
                              fullWidth: !!props.input.value?.fullWidth,
                              readonly: !!props.input.value?.readonly,
                            },
                          },
                        };
                        return <FormComponent {...column} actionsRef={null} toolbarRef={null} />;
                      }}
                    </Field>
                  ))
                }
              </FieldArray>
            </SimpleFormIterator>
          </RAArrayInput>
        ) : null;
      }}
    </FormDataConsumer>
  ) : (
    <RAArrayInput
      label={value.label[locale]}
      helperText={(value.helperText?.[locale] ?? false) || ''}
      source={value.id}
      fullWidth={true}
      validate={validator}
    >
      <SimpleFormIterator className="VasaraTableFieldIterator">
        <FieldArray name={`${input.name}.fields`}>
          {({ fields, ...rest }) =>
            fields.map(name => (
              <Field key={`${(rest as any).id}.${name}`} name={name}>
                {props => {
                  const FormComponent = FieldComponent[props.input.value.type];
                  const id = `${(rest as any).id}.${props.input.value.id}`;
                  const column = {
                    ...props,
                    key: id,
                    input: {
                      ...props.input,
                      value: {
                        ...props.input.value,
                        id,
                        fullWidth: !!props.input.value?.fullWidth,
                        readonly: !!props.input.value?.readonly,
                      },
                    },
                  };
                  return <FormComponent {...column} actionsRef={null} toolbarRef={null} />;
                }}
              </Field>
            ))
          }
        </FieldArray>
      </SimpleFormIterator>
    </RAArrayInput>
  );
};

export const TableInput = React.memo(TableInputImpl);
export default React.memo(TableField);
