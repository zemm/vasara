import { makeStyles } from '@material-ui/styles';
import { IntrospectionField, IntrospectionOutputTypeRef } from 'graphql';
import get from 'lodash/get';
import { SelectInput as RASelectInput } from 'ra-ui-materialui/lib/input';
import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import {
  AutocompleteArrayInput,
  BooleanInput,
  CheckboxGroupInput,
  FormDataConsumer,
  NumberInput,
  ArrayInput as RAArrayInput,
  SimpleFormIterator,
  TextInput,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { getUserTaskForm } from '../../DataProviders/Camunda/helpers';
import UserTaskContext from '../../DataProviders/Camunda/UserTaskContext';
import HasuraContext from '../../DataProviders/HasuraContext';
import { getFieldTypeName, isEnumField } from '../../util/helpers';
import { EnabledFieldTypesChoices } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
}

const ArrayField: React.FC<Props> = props => {
  const classes = useStyles();
  const locale = useLocale();
  const input = props.input;
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        id={`${input.name}-label`}
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        validate={validateRequired}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <TextInput
        id={`${input.name}-helperText`}
        label="vasara.form.help"
        onChange={(e: ChangeEvent) => e.stopPropagation()}
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />

      <NumberInput source={`${input.name}.min`} label="vasara.form.min" defaultValue={1} />
      <NumberInput source={`${input.name}.max`} label="vasara.form.max" defaultValue={1} />

      <RAArrayInput source={`${input.name}.options`} label="vasara.form.options">
        <SimpleFormIterator>
          <TextInput source={`id`} label="vasara.form.value" />
          <TextInput source={`name.${locale}`} label="vasara.form.label" />
        </SimpleFormIterator>
      </RAArrayInput>

      <AutocompleteArrayInput
        id={`${input.name}-sources`}
        label="vasara.form.sources"
        helperText="vasara.form.helperText.options"
        source={`${input.name}.sources`}
        choices={props.sourceChoices}
        validate={validateRequired}
        fullWidth={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const sources = get(formData, `${input.name}.sources`) || [];
          const label = get(formData, `${input.name}.label.${locale}`) || '';
          for (const source of sources) {
            if ((source || '').endsWith('.{}')) {
              return (
                <TextInput
                  id={`${input.name}-key`}
                  label="vasara.form.key"
                  helperText="vasara.form.helperText.key"
                  source={`${input.name}.key`}
                  validate={validateRequired}
                  initialValue={label}
                  fullWidth={true}
                />
              );
            }
          }
        }}
      </FormDataConsumer>

      <BooleanInput
        id={`${input.name}-readonly`}
        label="vasara.form.readonly"
        source={`${input.name}.readonly`}
        initialValue={false}
        className={classes.floatLeft}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const readonly = get(formData, `${input.name}.readonly`);
          return readonly ? null : (
            <>
              <BooleanInput
                id={`${input.name}-PII`}
                label="vasara.form.PII"
                source={`${input.name}.PII`}
                initialValue={false}
                className={classes.floatLeft}
              />

              {/*<BooleanInput*/}
              {/*  id={`${input.name}-confidential`}*/}
              {/*  label="vasara.form.confidential"*/}
              {/*  source={`${input.name}.confidential`}*/}
              {/*  initialValue={false}*/}
              {/*  className={classes.floatLeft}*/}
              {/*/>*/}

              <BooleanInput
                id={`${input.name}-required`}
                label="ra.validation.required"
                source={`${input.name}.required`}
                initialValue={false}
                className={classes.floatLeft}
              />
            </>
          );
        }}
      </FormDataConsumer>

      <RASelectInput
        id={`${input.name}-type`}
        label="vasara.form.type"
        helperText="vasara.form.helperText.type"
        source={`${input.name}.type`}
        choices={EnabledFieldTypesChoices}
        validate={validateRequired}
        fullWidth={true}
      />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  floatLeft: {
    float: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
});

const maxRequired = (max: number = 1) => (value: any) => {
  return value && value.length > max ? `You've checked more than ${max} option(s)` : undefined;
};

const minRequired = (min: number = 1) => (value: any) => {
  return value && value < min ? `Please check at least ${min} option(s)` : undefined;
};

export const ArrayInputImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const locale = useLocale();
  const context = useContext(UserTaskContext);
  const form = getUserTaskForm(context);
  const value = input.value;

  let options = (value.options || []).map((option: any) => {
    return {
      id: option.id,
      name: option.name[locale],
    };
  });

  // Resolve options from schema
  const { fields: fieldsByName, enums } = useContext(HasuraContext);

  if (!options || options.length === 0) {
    for (const source of value.sources) {
      const parts = source.split('.');
      if (fieldsByName.has(parts[0])) {
        const fields = fieldsByName.get(parts[0]) as Map<string, IntrospectionField>;
        const field = fields.get(parts[1]);
        if (field && isEnumField(field)) {
          const type_ = enums.get(getFieldTypeName((field.type as unknown) as IntrospectionOutputTypeRef));
          if (type_) {
            options = type_.enumValues.map(value => {
              return {
                id: value.name,
                name: value.description,
              };
            });
          }
        }
        if (options.length > 0) {
          break;
        }
      }
      if (options.length > 0) {
        break;
      }
    }
  }

  // Resolve options from user task form
  if (!options || options.length === 0) {
    for (let field of form) {
      const id = `context.${field.id}`;
      if (field.values && value.sources.indexOf(id) > -1) {
        options = field.values;
        break;
      }
    }
  }
  return (
    <>
      {options.length <= 6 ? (
        <CheckboxGroupInput
          source={value.id}
          label={value.label[locale]}
          helperText={(value.helperText?.[locale] ?? '') || ''}
          choices={options}
          row={false}
          validate={[minRequired(value.min), maxRequired(value.max)]}
          fullWidth={true}
        />
      ) : (
        <AutocompleteArrayInput
          source={value.id}
          label={value.label[locale]}
          helperText={(value.helperText?.[locale] ?? '') || ''}
          choices={options}
          validate={[minRequired(value.min), maxRequired(value.max)]}
          fullWidth={true}
        />
      )}
    </>
  );
};

export const ArrayInput = React.memo(ArrayInputImpl);
export default React.memo(ArrayField);
