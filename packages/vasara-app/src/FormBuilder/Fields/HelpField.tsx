import { makeStyles } from '@material-ui/core/styles';
import get from 'lodash/get';
import RichTextInput from 'ra-input-rich-text';
import React, { useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteInput,
  BooleanInput,
  FormDataConsumer,
  SimpleFormIterator,
  TextInput,
  required,
  useLocale,
} from 'react-admin';
import { DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd';
import { FieldRenderProps } from 'react-final-form-hooks';

import { unary } from '../../util/feel';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';

interface Props {
  expanded: string;
  setExpanded: (fieldName: string) => void;
  input: any;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  sourceChoices: Choice[];
  readonlySourceChoices: Choice[];
  fieldChoices: Choice[];
}

const useStyles = makeStyles(theme => ({
  help: {
    color: '#000000',
    fontSize: '1rem',
  },
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
}));

const HelpField: React.FC<Props> = (props: any) => {
  const locale = useLocale();
  const input = props.input;
  const classes = useStyles();
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<any>();

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === input.name) {
      setValidateRequired([required()]);
    } else {
      setValidateRequired(undefined);
    }
  }, [props.expanded, input.name]);

  return (
    <FieldsetField {...props}>
      <TextInput
        label="vasara.form.label"
        source={`${input.name}.label.${locale}`}
        initialValue=""
        fullWidth={true}
        helperText={false}
      />
      <RichTextInput
        label="vasara.form.help"
        source={`${input.name}.helperText.${locale}`}
        initialValue=""
        helperText={false}
      />
      <BooleanInput
        fullWidth={true}
        label="vasara.form.showLabel"
        source={`${input.name}.showLabel`}
        initialValue={false}
        helperText={false}
      />

      <AutocompleteInput
        id={`${input.name}-dependency`}
        label="vasara.form.dependency"
        source={`${input.name}.dependency`}
        choices={combinedChoices}
        fullWidth={true}
        helperText={false}
        className={classes.clearLeft}
        resettable={true}
      />

      <FormDataConsumer subscription={{ values: true }}>
        {({ formData, ...rest }: any) => {
          const dependency = get(formData, `${input.name}.dependency`);
          return dependency ? (
            <>
              <TextInput
                id={`${input.name}-condition`}
                label="vasara.form.dependencyExpression"
                source={`${input.name}.condition`}
                initialValue=""
                fullWidth={true}
                helperText={false}
              />
              <ArrayInput source={`${input.name}.variables`} label="vasara.form.variables">
                <SimpleFormIterator>
                  <TextInput
                    source={`id`}
                    label="vasara.form.variable"
                    helperText={false}
                    validate={validateRequired}
                  />
                  <AutocompleteInput
                    label="vasara.form.source"
                    source={`source`}
                    choices={combinedChoices}
                    validate={validateRequired}
                    helperText={false}
                  />
                </SimpleFormIterator>
              </ArrayInput>
            </>
          ) : null;
        }}
      </FormDataConsumer>
    </FieldsetField>
  );
};

export const HelpDisplayImpl: React.FC<FieldRenderProps> = ({ input }) => {
  const classes = useStyles();
  const locale = useLocale();
  const value = input.value;

  const dependency = (input.value.dependency || '').match('\\.')
    ? `${value.id}:${input.value.dependency}`
    : input.value.dependency;
  const condition = input.value.condition;
  const variables = input.value.variables || [];

  return dependency ? (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const context: Record<string, any> = Object.fromEntries(
          variables.map((variable: any) => {
            return formData[variable.source] !== undefined
              ? [variable.id, formData[variable.source]]
              : [variable.id, formData[`${value.id}:${variable.source}`]];
          })
        );
        return !dependency ||
          formData[dependency] === undefined ||
          (!condition && formData[dependency]) ||
          (condition && unary(condition, formData[dependency], context)) ? (
          value.showLabel && value.label?.[locale] ? (
            <>
              <h3>{value.label[locale]}</h3>
              <div className={classes.help} dangerouslySetInnerHTML={{ __html: value.helperText?.[locale] ?? '' }} />
            </>
          ) : (
            <div className={classes.help} dangerouslySetInnerHTML={{ __html: value.helperText?.[locale] ?? '' }} />
          )
        ) : null;
      }}
    </FormDataConsumer>
  ) : value.showLabel && value.label?.[locale] ? (
    <>
      <h3>{value.label[locale]}</h3>
      <div className={classes.help} dangerouslySetInnerHTML={{ __html: value.helperText?.[locale] ?? '' }} />
    </>
  ) : (
    <div className={classes.help} dangerouslySetInnerHTML={{ __html: value.helperText?.[locale] ?? '' }} />
  );
};

export const HelpDisplay = React.memo(HelpDisplayImpl);
export default React.memo(HelpField);
