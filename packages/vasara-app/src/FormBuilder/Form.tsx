import React, { useRef } from 'react';
import { useLocale } from 'react-admin';
import { Field } from 'react-final-form';
import { FieldArray, useFieldArray } from 'react-final-form-arrays';

import { FieldComponent } from './fields';

const fsStyle: React.CSSProperties = {
  padding: '1rem',
  border: '1px solid #eee',
};

export interface FormBuilderProps {
  title: string;
}

export interface FormProps extends FormBuilderProps {
  toolbarRef: any;
}

const Form: React.FC<FormProps> = ({ title, toolbarRef }) => {
  const { fields: fieldsets } = useFieldArray('schema');
  const actionsRef = useRef();
  const locale = useLocale();
  return (
    <div className="VasaraForm-root">
      <div ref={actionsRef as any} style={{ float: 'right' }} />
      <h2>{title}</h2>
      {fieldsets.map((name, index) => (
        <div key={name}>
          {(fieldsets?.length ?? 1) > 1 ? <h3>{fieldsets.value[index].label[locale]}</h3> : null}
          <FieldArray name={`${name}.fields`}>
            {({ fields }) => (
              <div style={fsStyle}>
                {fields.map(name => (
                  <Field key={name} name={name}>
                    {props => {
                      const FormComponent = FieldComponent[props.input.value.type];
                      return <FormComponent {...props} actionsRef={actionsRef} toolbarRef={toolbarRef} />;
                    }}
                  </Field>
                ))}
              </div>
            )}
          </FieldArray>
        </div>
      ))}
    </div>
  );
};

export default Form;
