import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import SlideshowIcon from '@material-ui/icons/Slideshow';
import arrayMutators from 'final-form-arrays';
import React, { useState } from 'react';
import { Button, FormDataConsumer } from 'react-admin';
import { Form } from 'react-final-form';

import FormBuilderForm, { FormProps } from './Form';

const ModalStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  zIndex: 2, // To be able to see dropdown suggestions since RaAutocompleteSuggestionList-suggestionsContainer has z-index: 2
};

const PaperStyle: React.CSSProperties = {
  padding: '2rem 1rem',
  minWidth: '50%',
};

const Preview: React.FC<FormProps> = ({ title, toolbarRef }) => {
  const [openModal, setOpenModal] = useState(false);

  const handleOpen = () => {
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  };

  return (
    <>
      <Button size="small" startIcon={<SlideshowIcon />} label="vasara.form.preview" onClick={handleOpen} />
      <Modal style={ModalStyle} open={openModal} onClose={handleClose}>
        <Paper style={PaperStyle} elevation={3} className="VasaraForm-preview">
          <FormDataConsumer subscription={{ values: true }}>
            {({ formData }: any) => (
              <Form
                onSubmit={() => {}}
                mutators={{ ...arrayMutators }}
                initialValues={{ schema: (formData?.schema ?? []) || [] }}
                render={() => <FormBuilderForm title={title} toolbarRef={toolbarRef} />}
              />
            )}
          </FormDataConsumer>
        </Paper>
      </Modal>
    </>
  );
};

export default Preview;
