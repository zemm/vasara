import { parseUnaryTests, unaryTest } from 'feelin';

// These are the legacy conditions we should support:
//(condition && condition === formData[dependency]) ||
//(condition === 'true' && formData[dependency] === true) ||
//(condition === 'false' && formData[dependency] === false) ||
//(condition?.startsWith('not ') && formData[dependency] !== condition.substring(4)) ||
//(condition?.startsWith('!') && formData[dependency] !== condition.substring(1)) ? (

export const normalize = (expression: string): string => {
  // NOTE: See feel.test.js why we try to compare boolean values as strings
  if (expression === 'true' || expression === 'false' || expression === 'null') {
    return `"${expression}"`;
  }
  if (expression.startsWith('!')) {
    return `not(${normalize(expression.substring(1))})`;
  }
  if (expression.startsWith('not ')) {
    return `not(${normalize(expression.substring(4))})`;
  }
  if (/^[0-9.]+$/.exec(expression) !== null) {
    return expression;
  }
  if (/[/()[\]'"<=>,.]/.exec(expression) === null) {
    return `"${expression}"`;
  }
  expression = expression.replace(/([^a-zA-Z"']|^)true([^a-zA-Z"']|$)/g, '$1"true"$2');
  expression = expression.replace(/([^a-zA-Z"']|^)false([^a-zA-Z"']|$)/g, '$1"false"$2');
  expression = expression.replace(/([^a-zA-Z"']|^)null([^a-zA-Z"']|$)/g, '$1"null"$2');
  return expression;
};

export const validate = (expression: string): boolean => {
  try {
    parseUnaryTests(expression, {});
    return true;
  } catch (e) {
    return false;
  }
};

export const unary = (expression: string, value: any, context?: any): boolean => {
  const normalizedContext = (context?: any) =>
    Object.fromEntries(
      Object.keys(context || {}).map((key: string) => [
        key,
        [true, false, null].includes(context[key]) ? `${context[key]}` : context[key],
      ])
    );
  const normalized = normalize(expression);
  // console.log(normalized, value, JSON.stringify(context));
  if ([true, false, null].includes(value)) {
    return normalized.match(/"true"|"false"|"null"/) || value === null
      ? !!unaryTest(normalized, {
          ...normalizedContext(context),
          '?': `${value}`,
        })
      : !!unaryTest(normalized, {
          ...context,
          '?': value,
        });
  } else if (Array.isArray(value)) {
    return !!unaryTest(expression, {
      ...context,
      '?': value,
    });
  } else {
    return normalized.match(/"true"|"false"|"null"/)
      ? !!unaryTest(normalized, {
          ...normalizedContext(context),
          '?': value,
        })
      : !!unaryTest(normalized, {
          ...context,
          '?': value,
        });
  }
};
