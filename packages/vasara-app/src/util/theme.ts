import { Theme, createMuiTheme } from '@material-ui/core/styles';
import merge from 'lodash/merge';
import { defaultTheme } from 'react-admin';

/*
$red: #f1563f;
$blue: #002957;
$gold: #caa260;
*/

export const theme: Theme = createMuiTheme(
  merge({}, defaultTheme, {
    palette: {
      primary: {
        main: '#002957',
      },
      success: {
        main: '#388e3c',
        contrastText: '#ffffff',
      },
      error: {
        main: '#cc0000',
      },
      secondary: {
        main:
          window?.location?.href?.match(/accp/) ?? false
            ? '#f1563f'
            : window?.location?.href?.match(/localhost|apptest/) ?? false
            ? '#000000'
            : '#002957',
      },
    },
    overrides: {
      MuiCardContent: {
        root: {
          '& .VasaraForm-root h3': {
            paddingBottom: '1ex',
            borderBottom: '0.5ex solid #caa260',
          },
        },
      },
      MuiButton: {
        text: {
          '&.Mui-focusVisible': {
            outline: '1px dotted black',
          },
        },
      },
      MuiPaper: {
        root: {
          '&.VasaraForm-preview': {
            maxHeight: '80vh',
            maxWidth: '80vw',
            overflowY: 'scroll',
          },
          '&.VasaraForm-preview h3': {
            paddingBottom: '1ex',
            borderBottom: '0.5ex solid #caa260',
          },
        },
      },
      RaSimpleFormIterator: {
        root: {
          '& .ra-input': {
            float: 'left',
          },
          '&.VasaraTableFieldIterator .ra-input': {
            width: '100%',
            float: 'none',
          },
          '& .MuiFormControl-root': {
            marginRight: '1em',
          },
          '& .MuiFormLabel-root': {
            fontWeight: '400',
            fontSize: '1rem',
            marginBottom: '0',
          },
          '& .MuiButtonBase-root': {
            paddingTop: '3px',
          },
          '& div[role="combobox"]': {
            display: 'inline-flex',
          },
        },
      },
      MuiFormLabel: {
        root: {
          color: 'rgba(0, 0, 0, 0.87)',
          fontWeight: '600',
          fontSize: '1.6rem',
          marginBottom: '0.5rem',
          '&.MuiInputLabel-animated': {
            fontSize: 'inherit',
            marginBottom: '0',
          },
          '&.MuiInputLabel-animated.MuiFormLabel-filled': {
            fontWeight: '400',
          },
        },
      },
      RaAutocompleteInput: {
        container: {
          clear: 'left',
        },
      },
      RaDatagrid: {
        rowCell: {
          '& .MuiFormLabel-root': {
            display: 'none',
          },
        },
      },
      MuiFormHelperText: {
        root: {
          fontSize: '1rem',
        },
      },
      MuiFormControl: {
        root: {
          '&:hover .MuiFormHelperText-root': {
            color: 'black',
          },
          '&:focus-within .MuiFormHelperText-root': {
            color: 'black',
          },
          '&.MuiFormControl-marginDense': {
            marginBottom: '8px',
          },
        },
      },
      MuiIconButton: {
        root: {
          '&:focus': {
            backgroundColor: 'rgba(0, 0, 0, 0.04)',
          },
        },
      },
      MuiFormControlLabel: {
        root: {
          '&:focus-within': {
            outline: '1px dotted #888888',
          },
        },
      },
    },
  })
);
