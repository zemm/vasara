import { unaryTest } from 'feelin';

import { normalize, unary } from '../feel';

/*

These are the legacy conditions we should support:

(condition && condition === formData[dependency]) ||
(condition === 'true' && formData[dependency] === true) ||
(condition === 'false' && formData[dependency] === false) ||
(condition?.startsWith('not ') && formData[dependency] !== condition.substring(4)) ||
(condition?.startsWith('!') && formData[dependency] !== condition.substring(1))

*/

test('migrate legacy condition to normalized feel expression', () => {
  expect(normalize('null')).toEqual('"null"');
  expect(normalize('true')).toEqual('"true"');
  expect(normalize('false')).toEqual('"false"');
  expect(normalize('string')).toEqual('"string"');
  expect(normalize('!string')).toEqual('not("string")');
  expect(normalize('not string')).toEqual('not("string")');
  expect(normalize('not true')).toEqual('not("true")');
  expect(normalize('not false')).toEqual('not("false")');
});

test('normalize boolean and null to string in expression', () => {
  // not sure if its just "feeling" library, but true, false and null behave surprisingly
  expect(normalize('null')).toEqual('"null"');
  expect(normalize('true')).toEqual('"true"');
  expect(normalize('false')).toEqual('"false"');
  expect(normalize('1,true')).toEqual('1,"true"');
  expect(normalize('false,1')).toEqual('"false",1');
  expect(normalize('[true]')).toEqual('["true"]');
  expect(normalize('[false]')).toEqual('["false"]');
  expect(normalize('[null]')).toEqual('["null"]');
});

test('do not normalize already normally looking expressions', () => {
  expect(normalize('1')).toEqual('1');
  expect(normalize('1.0')).toEqual('1.0');
  expect(normalize('"string"')).toEqual('"string"');
  expect(normalize('> 10')).toEqual('> 10');
  expect(normalize('[0..10]')).toEqual('[0..10]');
  expect(normalize('["a", "b"]')).toEqual('["a", "b"]');
});

test('test normalized feel expression', () => {
  // null is always false with raw unaryTest
  expect(!!unaryTest('null', { '?': null })).toEqual(false);
  expect(!!unaryTest('null', { '?': false })).toEqual(false);
  expect(!!unaryTest('null', { '?': true })).toEqual(false);

  // [null] matches to null and false with raw unaryTest
  expect(!!unaryTest('[null]', { '?': null })).toEqual(true);
  expect(!!unaryTest('[null]', { '?': false })).toEqual(true);
  expect(!!unaryTest('[null]', { '?': true })).toEqual(false);

  // but our version only compares it to null, because they are normalized as strings
  expect(unary('null', null)).toEqual(true);
  expect(unary('null', true)).toEqual(false);
  expect(unary('null', false)).toEqual(false);
  expect(unary('[null]', null)).toEqual(true);
  expect(unary('[null]', false)).toEqual(false);
  expect(unary('[null]', true)).toEqual(false);

  // true is always true with raw unaryTest
  expect(!!unaryTest('true', { '?': null })).toEqual(true);
  expect(!!unaryTest('true', { '?': false })).toEqual(true);
  expect(!!unaryTest('true', { '?': true })).toEqual(true);

  expect(!!unaryTest('not(true)', { '?': true })).toEqual(false);
  expect(!!unaryTest('not(false)', { '?': true })).toEqual(true);
  expect(!!unaryTest('not(null)', { '?': true })).toEqual(false);

  expect(!!unaryTest('not(true)', { '?': false })).toEqual(false);
  expect(!!unaryTest('not(false)', { '?': false })).toEqual(true);
  expect(!!unaryTest('not(null)', { '?': false })).toEqual(false);

  expect(!!unaryTest('not(true)', { '?': null })).toEqual(false);
  expect(!!unaryTest('not(false)', { '?': null })).toEqual(true);
  expect(!!unaryTest('not(null)', { '?': null })).toEqual(false);

  // but our version only compares it to true
  expect(unary('true', null)).toEqual(false);
  expect(unary('true', false)).toEqual(false);
  expect(unary('true', true)).toEqual(true);
  expect(unary('[true]', true)).toEqual(true);
  expect(unary('[true]', false)).toEqual(false);

  // anything with true is always true with raw unaryTest
  expect(!!unaryTest('true,1', { '?': null })).toEqual(true);

  // but our version only compares it to true
  expect(unary('true,1', true)).toEqual(true);
  expect(unary('true,1', false)).toEqual(false);
  expect(unary('true,1', null)).toEqual(false);

  // similarly [false] should match only with false
  expect(unary('[false]', null)).toEqual(false);
  expect(unary('[false]', false)).toEqual(true);
  expect(unary('[false]', true)).toEqual(false);

  // misc tests
  expect(unary('"true"', false)).toEqual(false);
  expect(unary('1', 2)).toEqual(false);
  expect(unary('1,2', 2)).toEqual(true);
  expect(unary('[0..1]', 2)).toEqual(false);
  expect(unary('[0..1],2', 2)).toEqual(true);
  expect(unary('[0..2]', 2)).toEqual(true);

  expect(unary('not(true)', true)).toEqual(false);
  expect(unary('not(true)', false)).toEqual(true);
  expect(unary('not([true])', true)).toEqual(false);
  expect(unary('not([true])', false)).toEqual(true);

  expect(unary('"foo","bar"', 'foo')).toEqual(true);
  expect(unary('"foo","bar"', 'FOO')).toEqual(false);
  expect(unary('"foo","bar"', true)).toEqual(false);
  expect(unary('["foo","bar"]', 'foo')).toEqual(true);
  expect(unary('["foo","bar"]', 'FOO')).toEqual(false);
  expect(unary('["foo","bar"]', true)).toEqual(false);

  // multiple variables can be tested by passing ? explicitly
  expect(unary('all(? = "foo", bar > 10)', 'foo', { bar: 10 })).toEqual(false);
  expect(unary('all(? = "foo", bar > 10)', 'foo', { bar: 20 })).toEqual(true);
  expect(unary('all(?, bar > 10)', true, { bar: 10 })).toEqual(false);
  expect(unary('all(?, bar > 10)', true, { bar: 20 })).toEqual(true);
  expect(unary('all(?, bar > 10)', false, { bar: 20 })).toEqual(false);

  expect(unary('all(? = "TÄRKEÄ ASIA")', 'TÄRKEÄ ASIA', { boolean: false })).toEqual(true);

  expect(unary('all(? = "TÄRKEÄ ASIA", foo = "bar")', 'TÄRKEÄ ASIA', { foo: 'bar' })).toEqual(true);
  expect(unary('all(? = "TÄRKEÄ ASIA", foo = "bar")', 'TÄRKEÄ ASIA', { foo: 'foo' })).toEqual(false);

  expect(unary('all(? = "TÄRKEÄ ASIA", boolean = true)', 'TÄRKEÄ ASIA', { boolean: false })).toEqual(false);
  expect(unary('all(? = "TÄRKEÄ ASIA", boolean = true)', 'TÄRKEÄ ASIA', { boolean: true })).toEqual(true);

  expect(unary('all(? = "TÄRKEÄ ASIA", boolean)', 'TÄRKEÄ ASIA', { boolean: true })).toEqual(true);
  expect(unary('all(? = "TÄRKEÄ ASIA", boolean)', 'TÄRKEÄ ASIA', { boolean: false })).toEqual(false);

  expect(unary('? = true, lapsi = true', true, { lapsi: false })).toEqual(true);
  expect(unary('? = true, lapsi = true', false, { lapsi: true })).toEqual(true);
  expect(unary('? = true, lapsi = true', false, { lapsi: false })).toEqual(false);

  expect(unary('puoliso = true, lapsi = true', false, { puoliso: false, lapsi: false })).toEqual(false);
  expect(unary('puoliso = true, lapsi = true', false, { puoliso: true, lapsi: false })).toEqual(true);
  expect(unary('puoliso = true, lapsi = true', false, { puoliso: false, lapsi: false })).toEqual(false);

  expect(unary('? = false, huollettavia_lapsia = true', false, { huollettavia_lapsia: false })).toEqual(true);
  expect(unary('? = false, huollettavia_lapsia = true', true, { huollettavia_lapsia: false })).toEqual(false);
  expect(unary('? = false, huollettavia_lapsia = true', true, { huollettavia_lapsia: true })).toEqual(true);
  expect(unary('? = false, huollettavia_lapsia = true', false, { huollettavia_lapsia: true })).toEqual(true);

  expect(unary('all(puoliso = true, lapsi = true)', false, { puoliso: false, lapsi: false })).toEqual(false);
  expect(unary('all(puoliso = true, lapsi = true)', false, { puoliso: true, lapsi: false })).toEqual(false);
  expect(unary('all(puoliso = true, lapsi = true)', false, { puoliso: false, lapsi: false })).toEqual(false);
  expect(unary('all(puoliso = true, lapsi = true)', false, { puoliso: true, lapsi: true })).toEqual(true);

  expect(unary('?, lapsi', true, { lapsi: false })).toEqual(true);
  expect(unary('?, lapsi', false, { lapsi: true })).toEqual(true);
  expect(unary('?, lapsi', false, { lapsi: false })).toEqual(false);

  expect(unary('all(? != false, ? != null)', null, {})).toEqual(false);
  expect(unary('all(? != false, ? != null)', false, {})).toEqual(false);
  expect(unary('all(? != false, ? != null)', true, {})).toEqual(true);

  expect(unary('not(false, null)', null, {})).toEqual(false);
  expect(unary('not(false, null)', false, {})).toEqual(false);
  expect(unary('not(false, null)', true, {})).toEqual(true);

  expect(unary('false, null', false, {})).toEqual(true);
  expect(unary('false, null', null, {})).toEqual(true);
  expect(unary('false, null', true, {})).toEqual(false);

  expect(unary('[false, null]', false, {})).toEqual(true);
  expect(unary('[false, null]', null, {})).toEqual(true);
  expect(unary('[false, null]', true, {})).toEqual(false);

  expect(unary('? = "puoltaa", ? = "hylatty"', 'puoltaa', {})).toEqual(true);
  expect(unary('? = "puoltaa", ? = "hylatty"', 'hylatty', {})).toEqual(true);
  expect(unary('? = "puoltaa", ? = "hylatty"', 'foobar', {})).toEqual(false);

  expect(unary('"puoltaa","hylatty"', 'puoltaa', {})).toEqual(true);
  expect(unary('"puoltaa","hylatty"', 'hylatty', {})).toEqual(true);
  expect(unary('"puoltaa","hylatty"', 'foobar', {})).toEqual(false);

  // table tests
  expect(
    unary(
      'not(every x in ? satisfies (get value (x, "accepted")) = true)',
      [{ accepted: true }, { accepted: false }],
      {}
    )
  ).toEqual(true);
  expect(
    unary(
      'not(every x in ? satisfies (get value (x, "accepted")) = true)',
      [{ accepted: true }, { accepted: true }],
      {}
    )
  ).toEqual(false);
  expect(unary('not(every x in ? satisfies (get value (x, "accepted")) = true)', [], {})).toEqual(false);
});
