import { parse } from 'query-string';
import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';

const ProcessInstanceCreate = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  return (
    <Create {...props}>
      <SimpleForm defaultValue={{ key: processDefinitionId }}>
        <TextInput source="key" label="Process definition" />
      </SimpleForm>
    </Create>
  );
};

export default ProcessInstanceCreate;
