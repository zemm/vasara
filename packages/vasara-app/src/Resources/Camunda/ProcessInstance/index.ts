import { Timeline } from '@material-ui/icons';

import messages from '../../../messages';
import ProcessInstanceCreate from './ProcessInstanceCreate';
import ProcessInstanceList from './ProcessInstanceList';

const resource = {
  name: 'ProcessInstance',
  create: ProcessInstanceCreate,
  list: ProcessInstanceList,
  icon: Timeline,
  options: {
    label: messages.translate('vasara.resource.processInstance'),
  },
};

export default resource;
