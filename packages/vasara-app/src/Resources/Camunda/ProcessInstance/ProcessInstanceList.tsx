import React from 'react';
import { Datagrid, List, TextField } from 'react-admin';

import { toLabel } from '../../../util/helpers';

const ProcessInstanceList = (props: any) => {
  return (
    <List {...props} title={toLabel(props.resource)} bulkActionButtons={false}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="name" />
      </Datagrid>
    </List>
  );
};

export default ProcessInstanceList;
