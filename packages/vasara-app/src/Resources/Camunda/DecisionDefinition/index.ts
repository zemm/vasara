import { Work } from '@material-ui/icons';

import messages from '../../../messages';
import DecisionDefinitionCreate from './DecisionDefinitionCreate';
import DecisionDefinitionList from './DecisionDefinitionList';
import DecisionDefinitionShow from './DecisionDefinitionShow';

const resource = {
  name: 'DecisionDefinition',
  create: DecisionDefinitionCreate,
  list: DecisionDefinitionList,
  show: DecisionDefinitionShow,
  icon: Work,
  options: {
    label: messages.translate('vasara.resource.decisionDefinition'),
  },
};

export default resource;
