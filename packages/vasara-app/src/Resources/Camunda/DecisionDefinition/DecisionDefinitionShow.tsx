import { Theme } from '@material-ui/core/styles';
import { SaveAlt } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import { ShowProps } from 'ra-core/esm/controller/details/useShowController';
import React, { useEffect, useState } from 'react';
import { Loading, Show, SimpleShowLayout, TopToolbar, useDataProvider } from 'react-admin';

import BackButton from '../../../Components/BackButton';
import DmnField from '../../../Components/DmnField';
import TextFileField from '../../../Components/TextFileField';
import TracedError from '../../../Components/TracedError';
import { theme } from '../../../util/theme';

const CustomActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <BackButton />
    </TopToolbar>
  );
};

const CustomTitle = ({ record }: any) => {
  return <span>{record.name}</span>;
};

const useStyles = makeStyles((theme: Theme) => ({
  download: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    '& > :nth-child(2) a': {
      color: theme.palette.secondary.main,
      textDecoration: 'none',
      display: 'inline-block',
      marginLeft: '-2em',
      paddingLeft: '2.5em',
    },
  },
}));

const DecisionDefinitionShow: React.FC<ShowProps> = props => {
  const { id } = props;
  const dataProvider = useDataProvider();
  const [loading, setLoading]: [any, any] = useState(true);
  const [error, setError]: [any, any] = useState();
  const classes = useStyles(theme);

  // Required GET_ONE to fetch DMN details
  useEffect(() => {
    (async () => {
      try {
        await dataProvider.getOne('DecisionDefinition', { id: id as string });
      } catch (e) {
        setError(e);
      }
      setLoading(false);
    })();
  }, [dataProvider, id]);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <TracedError error={error} />;
  }

  const { hasShow, ...rest } = props;

  return (
    // @ts-ignore
    <Show {...rest} title={<CustomTitle />} actions={<CustomActions />}>
      <SimpleShowLayout>
        <TextFileField
          source="diagram"
          title="name"
          filename="resource"
          contentType="application/xml"
          className={classes.download}
        >
          <SaveAlt />
        </TextFileField>
        <DmnField source="diagram" />
      </SimpleShowLayout>
    </Show>
  );
};

export default DecisionDefinitionShow;
