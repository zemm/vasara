import React, { useContext } from 'react';
import { CreateButton, Datagrid, List, TextField, TopToolbar, useTranslate } from 'react-admin';

import BackButton from '../../../Components/BackButton';
import Settings from '../../../Settings';
import { PAGE_SIZE } from '../../../util/constants';

const CustomActions = ({ basePath, data }: any) => {
  const settings = useContext(Settings);
  return (
    <TopToolbar>
      <BackButton redirectTo={'/'} />
      {settings.isLite ? null : (
        <CreateButton label="ra.action.create" basePath="/DecisionDefinition" resource="DecisionDefinition" />
      )}
    </TopToolbar>
  );
};

const DecisionDefinitionList = (props: any) => {
  const translate = useTranslate();
  return (
    <List
      {...props}
      filterDefaultValues={{ latest: true }}
      title={translate('vasara.view.decisionDefinitionList')}
      bulkActionButtons={false}
      exporter={false}
      perPage={PAGE_SIZE}
      actions={<CustomActions />}
    >
      <Datagrid rowClick="show">
        <TextField source="name" label={translate('vasara.column.decisionDefinitionName')} />
      </Datagrid>
    </List>
  );
};

export default DecisionDefinitionList;
