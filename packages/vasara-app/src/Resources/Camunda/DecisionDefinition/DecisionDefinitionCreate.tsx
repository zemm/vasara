import React, { cloneElement, useCallback } from 'react';
import {
  CreateContextProvider,
  FileField,
  FileInput,
  SimpleForm,
  Title,
  TopToolbar,
  useCreateController,
  useTranslate,
} from 'react-admin';

import BackButton from '../../../Components/BackButton';
import { DMN } from '../../../DataProviders/Camunda/helpers';
import { FileContainer, convertFileToText } from '../../../util/helpers';

interface Resource {
  name: string;
  payload: string;
}

const CustomCreate = (props: any) => {
  const createControllerProps = useCreateController(props);
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Create Post'
    record, // empty object, unless some values were passed in the location state to prefill the form
    redirect, // the default redirection route. Defaults to 'edit', unless the resource has no edit view, in which case it's 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the create callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to create the record
    version, // integer used by the refresh feature
  } = createControllerProps;

  const saveWithFile = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      (async () => {
        let deploymentName = null;
        const resources: Resource[] = await Promise.all(
          data.resources.map(async (file: FileContainer) => {
            const name = file.rawFile.name;
            const payload = await convertFileToText(file);
            if (name.endsWith('.dmn')) {
              const dmn = await DMN(payload);
              for (const drgElement of dmn.rootElement.drgElement) {
                if (drgElement && drgElement.name) {
                  deploymentName = drgElement.name;
                  if (drgElement.$type === 'dmn:Decision') {
                    break;
                  }
                }
              }
            }
            return { name, payload };
          })
        );
        if (deploymentName === null && resources.length > 0) {
          deploymentName = resources[0].name;
        }
        save(
          {
            name: deploymentName,
            resources,
          },
          redirectTo,
          { onSuccess, onFailure }
        );
      })();
    },
    [save]
  );

  return (
    <CreateContextProvider value={createControllerProps}>
      <div>
        <TopToolbar>
          <BackButton />
        </TopToolbar>
        <Title title={props.title || defaultTitle} />
        {cloneElement(props.children, {
          basePath,
          record,
          redirect,
          resource,
          save: saveWithFile,
          saving,
          version,
        })}
      </div>
    </CreateContextProvider>
  );
};

const DecisionDefinitionCreate = (props: any) => {
  const translate = useTranslate();
  return (
    <CustomCreate {...props} title={translate('vasara.page.deployDecisionDefinition')}>
      <SimpleForm>
        <FileInput source="resources" label="vasara.page.decisionDeploymentFiles" multiple={true}>
          <FileField source="src" title="title" />
        </FileInput>
      </SimpleForm>
    </CustomCreate>
  );
};

export default DecisionDefinitionCreate;
