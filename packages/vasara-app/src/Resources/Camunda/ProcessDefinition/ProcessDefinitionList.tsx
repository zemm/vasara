import React, { useContext } from 'react';
import {
  BooleanInput,
  CreateButton,
  Datagrid,
  Filter,
  List,
  Pagination,
  PaginationProps,
  TextField,
  TopToolbar,
  useTranslate,
} from 'react-admin';

import BackButton from '../../../Components/BackButton';
import Settings from '../../../Settings';
import { PAGE_SIZE } from '../../../util/constants';
import { StartButton } from './StartButton';

const CustomActions = ({ basePath, data }: any) => {
  const settings = useContext(Settings);
  return (
    <TopToolbar>
      <BackButton redirectTo={'/'} />
      {settings.isLite ? null : (
        <CreateButton label="ra.action.create" basePath="/ProcessDefinition" resource="ProcessDefinition" />
      )}
    </TopToolbar>
  );
};

const Filters = (props: any) => (
  <Filter {...props}>
    <BooleanInput label="vasara.filters.startableInTasklistOnly" source="startableInTasklist" alwaysOn />
  </Filter>
);

const ListPagination = (props: PaginationProps) => <Pagination rowsPerPageOptions={[10, 25, 50, 100]} {...props} />;

const ProcessDefinitionList = (props: any) => {
  const settings = useContext(Settings);
  const translate = useTranslate();
  return (
    <List
      {...props}
      filterDefaultValues={{ latest: true }}
      title={translate('vasara.view.processDefinitionList')}
      bulkActionButtons={false}
      exporter={false}
      pagination={<ListPagination {...props} />}
      perPage={PAGE_SIZE}
      actions={<CustomActions />}
      filter={settings.isLite ? { startableInTasklist: true } : {}}
      filters={settings.isLite ? null : <Filters />}
    >
      <Datagrid rowClick={settings.isLite ? undefined : 'show'}>
        <TextField source="name" label={translate('vasara.column.processDefinitionName')} />
        <StartButton />
      </Datagrid>
    </List>
  );
};

export default ProcessDefinitionList;
