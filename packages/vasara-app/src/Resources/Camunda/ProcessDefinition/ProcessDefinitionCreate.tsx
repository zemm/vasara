import React, { cloneElement, useCallback } from 'react';
import {
  BooleanInput,
  CreateContextProvider,
  FileField,
  FileInput,
  SimpleForm,
  Title,
  TopToolbar,
  useCreateController,
  useTranslate,
} from 'react-admin';

import BackButton from '../../../Components/BackButton';
import { BPMN } from '../../../DataProviders/Camunda/helpers';
import { FileContainer, convertFileToText } from '../../../util/helpers';

interface Resource {
  name: string;
  payload: string;
}

const CustomCreate = (props: any) => {
  const createControllerProps = useCreateController(props);
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Create Post'
    record, // empty object, unless some values were passed in the location state to prefill the form
    redirect, // the default redirection route. Defaults to 'edit', unless the resource has no edit view, in which case it's 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the create callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to create the record
    version, // integer used by the refresh feature
  } = createControllerProps;

  const saveWithFile = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      (async () => {
        let deploymentName = null;
        const resources: Resource[] = await Promise.all(
          data.resources.map(async (file: FileContainer) => {
            const name = file.rawFile.name;
            const payload = await convertFileToText(file);
            if (name.endsWith('.bpmn')) {
              const bpmn = await BPMN(payload);
              for (const rootElement of bpmn.getDefinitions().rootElements) {
                if (rootElement && rootElement.name) {
                  deploymentName = rootElement.name;
                  if (rootElement.$type === 'bpmn:Process') {
                    break;
                  }
                }
              }
            }
            return { name, payload };
          })
        );
        if (deploymentName === null && resources.length > 0) {
          deploymentName = resources[0].name;
        }
        save(
          {
            name: deploymentName,
            resources,
            migrate: !!data.migrate,
          },
          redirectTo,
          { onSuccess, onFailure }
        );
      })();
    },
    [save]
  );

  return (
    <CreateContextProvider value={createControllerProps}>
      <div>
        <TopToolbar>
          <BackButton />
        </TopToolbar>
        <Title title={props.title || defaultTitle} />
        {cloneElement(props.children, {
          basePath,
          record,
          redirect,
          resource,
          save: saveWithFile,
          saving,
          version,
        })}
      </div>
    </CreateContextProvider>
  );
};

const ProcessDefinitionCreate = (props: any) => {
  const translate = useTranslate();
  return (
    <CustomCreate {...props} title={translate('vasara.page.deployProcessDefinition')}>
      <SimpleForm>
        <FileInput source="resources" label="vasara.page.processDeploymentFiles" multiple={true}>
          <FileField source="src" title="title" />
        </FileInput>
        <BooleanInput
          source="migrate"
          initialValue={true}
          label="vasara.page.migrate"
          helperText="vasara.page.helperText.migrate"
          fullWidth={true}
        />
      </SimpleForm>
    </CustomCreate>
  );
};

export default ProcessDefinitionCreate;
