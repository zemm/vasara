import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { UserTask } from 'bpmn-moddle';
import React, { useContext, useEffect, useState } from 'react';
import {
  Button,
  CreateButton,
  Datagrid,
  Link,
  ReferenceManyField,
  Show,
  SimpleShowLayout,
  TextField,
  TopToolbar,
  useDataProvider,
  useTranslate,
} from 'react-admin';

import { AccordionBpmnFieldWithDownload, AccordionReferenceManyField } from '../../../Components/Accordion';
import BackButton from '../../../Components/BackButton';
import TracedError from '../../../Components/TracedError';
import Settings from '../../../Settings';
import { StartButton } from './StartButton';

interface UserTaskProcessDefinition {
  id: string;
  key: string;
  name: string;
  version: number;
}

interface UserTaskWithProcessDefinition extends UserTask {
  processDefinition: UserTaskProcessDefinition;
}

interface VasaraUserTaskForm {
  id: string;
  metadata: any;
  process_definition_key: string;
  process_definition_version: number;
  user_task_id: string;
  schema: any;
  process_definition_version_tag: string | null;
}

interface ProcessDefinitionUserTaskFormProps {
  basePath: string;
  resource: string;
  record: UserTaskWithProcessDefinition;
}

export const AddOrEditFormButtonImpl: React.FC<ProcessDefinitionUserTaskFormProps> = ({
  basePath,
  resource,
  record: userTask,
}) => {
  const [loading, setLoading] = useState(true);
  const [userTaskForm, setUserTaskForm] = useState<VasaraUserTaskForm | undefined>(undefined);
  const [error, setError] = useState();

  const dataProvider = useDataProvider();

  useEffect(() => {
    (async () => {
      const { id, processDefinition } = userTask;
      const params = {
        filter: {
          user_task_id: id,
          process_definition_key: processDefinition.key,
          process_definition_version: {
            format: 'hasura-raw-query',
            value: { _lte: processDefinition.version },
          },
        },
        sort: { field: 'process_definition_version', order: 'DESC' },
        pagination: { page: 1, perPage: 1 },
      };
      try {
        const { data } = await dataProvider.getList('vasara_user_task_form', params);
        if (data.length > 0) {
          setUserTaskForm(data[0] as VasaraUserTaskForm);
        }
        setLoading(false);
      } catch (error) {
        console.error(error);
        setError(error);
        setLoading(false);
      }
    })();
  }, [dataProvider, userTask]);

  if (error) {
    return <TracedError error={error || {}} />;
  }

  if (userTask && userTaskForm) {
    if (userTask.processDefinition.version === userTaskForm.process_definition_version) {
      return (
        <Link
          to={{
            // @ts-ignore
            pathname: `/vasara_user_task_form/${userTaskForm.id}`,
            search: `?processDefinitionId=${userTask.processDefinition.id}`,
          }}
        >
          <Button label="ra.action.edit">
            <EditIcon />
          </Button>
        </Link>
      );
    } else {
      return (
        <Link
          to={{
            pathname: '/vasara_user_task_form/create',
            search: `?processDefinitionId=${userTask.processDefinition.id}&processDefinitionKey=${userTask.processDefinition.key}&processDefinitionVersion=${userTask.processDefinition.version}&userTaskId=${userTask.id}`,
          }}
        >
          <Button label="ra.action.edit">
            <EditIcon />
          </Button>
        </Link>
      );
    }
  } else if (userTask) {
    return (
      <Link
        to={{
          pathname: '/vasara_user_task_form/create',
          search: `?processDefinitionId=${userTask.processDefinition.id}&processDefinitionKey=${userTask.processDefinition.key}&processDefinitionVersion=${userTask.processDefinition.version}&userTaskId=${userTask.id}`,
        }}
      >
        <Button label="ra.action.add" disabled={loading}>
          <AddIcon />
        </Button>
      </Link>
    );
  } else {
    return null;
  }
};

const AddOrEditFormButton = React.memo(AddOrEditFormButtonImpl, (a, b): any => {
  return a.record.id === b.record.id && a.record.processDefinition.id === b.record.processDefinition.id;
});

const CustomActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <BackButton />
      <CreateButton label="vasara.action.update" basePath="/ProcessDefinition" resource="ProcessDefinition" />
    </TopToolbar>
  );
};

const ProcessDefinitionTitle = ({ record }: any) => {
  return <span>{record.name}</span>;
};

const ProcessDefinitionShow = (props: any) => {
  const translate = useTranslate();
  const settings = useContext(Settings);

  const { hasShow, ...rest } = props;

  return settings.isLite ? (
    <Show {...rest} title={<ProcessDefinitionTitle />} actions={settings.isLite ? null : <CustomActions />}>
      <SimpleShowLayout>
        <StartButton named={true} />
      </SimpleShowLayout>
    </Show>
  ) : (
    <Show {...rest} title={<ProcessDefinitionTitle />} actions={<CustomActions />}>
      <SimpleShowLayout>
        <AccordionBpmnFieldWithDownload source="diagram" label={translate('vasara.column.diagram')} />
        <ReferenceManyField label="" reference="camunda_ProcessDefinition_UserTask" target="id">
          <Datagrid>
            <TextField source="name" sortable={false} label="" />
            {/* @ts-ignore */}
            <AddOrEditFormButton />
          </Datagrid>
        </ReferenceManyField>
        <AccordionReferenceManyField
          label={translate('vasara.view.versions')}
          reference="ProcessDefinition"
          source="key"
          target="key"
          sort={{ field: 'version', order: 'DESC' }}
        >
          <Datagrid rowClick={(id: any) => `/ProcessDefinition/${id}/show`}>
            <TextField source="version" sortable={false} />
            <TextField source="id" sortable={false} />
          </Datagrid>
        </AccordionReferenceManyField>
      </SimpleShowLayout>
    </Show>
  );
};

export default ProcessDefinitionShow;
