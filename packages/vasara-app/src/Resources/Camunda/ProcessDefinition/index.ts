import { Build } from '@material-ui/icons';

import messages from '../../../messages';
import ProcessDefinitionCreate from './ProcessDefinitionCreate';
import ProcessDefinitionList from './ProcessDefinitionList';
import ProcessDefinitionShow from './ProcessDefinitionShow';

const resource = {
  name: 'ProcessDefinition',
  create: ProcessDefinitionCreate,
  list: ProcessDefinitionList,
  edit: ProcessDefinitionShow,
  show: ProcessDefinitionShow,
  icon: Build,
  options: {
    label: messages.translate('vasara.resource.processDefinition'),
  },
};

export default resource;
