import PlayCircleFilledWhiteIcon from '@material-ui/icons/PlayCircleFilledWhite';
import React from 'react';
import { Button, useDataProvider, useMutation, useNotify, useRedirect } from 'react-admin';
import { useTranslate } from 'react-admin';

export const StartButton = ({ record, named, label }: any) => {
  const redirect = useRedirect();
  const dataProvider = useDataProvider();
  const notify = useNotify();
  const translate = useTranslate();
  const [start, { loading }] = useMutation(
    {
      type: 'create',
      resource: 'ProcessInstance',
      payload: { data: { processDefinitionKey: record.key } },
    },
    {
      onSuccess: ({ data }: any) => {
        (async () => {
          const results = await dataProvider.getList('camunda_ProcessInstance_UserTask', {
            pagination: {
              page: 1,
              perPage: 1000,
            },
            sort: {
              field: 'taskDefinitionKey',
              order: 'asc',
            },
            filter: { id: data.id },
          });
          if (results.total) {
            redirect('edit', '/UserTask', results.data[0].id);
          } else {
            redirect('/UserTask');
          }
        })();
      },
      onFailure: (error: ExceptionInformation) => {
        notify(`${error}`, 'error');
      },
    }
  );

  return record.startableInTasklist ? (
    <Button
      label={label ? label : named ? translate('vasara.action.startNamed', record) : 'vasara.action.start'}
      onClick={(e: any) => {
        e.stopPropagation();
        start(e);
      }}
      disabled={loading}
    >
      <PlayCircleFilledWhiteIcon />
    </Button>
  ) : null;
};
