import { Check } from '@material-ui/icons';

import messages from '../../../messages';
import UserTaskCreate from './UserTaskCreate';
import UserTaskEdit from './UserTaskEdit';
import UserTaskList from './UserTaskList';
import UserTaskShow from './UserTaskShow';

const resource = {
  name: 'UserTask',
  edit: UserTaskEdit,
  show: UserTaskShow,
  create: UserTaskCreate,
  list: UserTaskList,
  icon: Check,
  options: { label: messages.translate('vasara.resource.userTask') },
};

export default resource;
