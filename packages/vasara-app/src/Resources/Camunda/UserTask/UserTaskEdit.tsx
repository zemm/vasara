import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';
import Description from '@material-ui/icons/Description';
import { UserTask } from 'bpmn-moddle';
import React, { cloneElement, useCallback, useContext, useEffect, useRef, useState } from 'react';
import {
  EditContextProvider,
  Link,
  Loading,
  SaveButton,
  SimpleForm,
  Title,
  Toolbar,
  TopToolbar,
  useDataProvider,
  useEditController,
  useLocale,
  useMutation,
  useNotify,
  useRedirect,
  useTranslate,
} from 'react-admin';
import { v4 as uuid } from 'uuid';

import { AccordionBpmnField } from '../../../Components/Accordion';
import BackButton from '../../../Components/BackButton';
import CancelButton from '../../../Components/CancelButton';
import TracedError from '../../../Components/TracedError';
import { vault_transit_encrypt_request } from '../../../DataProviders/Actions/types';
import UserTaskContext from '../../../DataProviders/Camunda/UserTaskContext';
import HasuraContext from '../../../DataProviders/HasuraContext';
import Form from '../../../FormBuilder/Form';
import { fileToBytea, labelFromSchema, normalizeAndMigrateForm } from '../../../util/helpers';

type FORM_STATE = 'RECORD_UPDATED' | 'LOAD_FORM_DATA' | 'RESOLVE_ENTITIES' | 'READY';

const CustomEdit = (props: any) => {
  const editControllerProps = useEditController(props);
  const {
    basePath, // deduced from the location, useful for action buttons
    // defaultTitle, // the translated title based on the resource, e.g. 'Post #123'
    loaded, // boolean that is false until the record is available
    loading, // boolean that is true on mount, and false once the record was fetched
    record, // record fetched via dataProvider.getOne() based on the id from the location
    redirect, // the default redirection route. Defaults to 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the update callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to update the record
    version, // integer used by the refresh feature
  } = editControllerProps;

  const [formState, setFormState] = useState<FORM_STATE>('RECORD_UPDATED');

  const [userTask, setUserTask] = useState<any>(null);
  const [userTaskForm, setUserTaskForm] = useState<any>(null);
  const [userTaskEntities, setUserTaskEntities] = useState<Record<string, Record<string, any>>>({ context: {} });

  const dataProvider = useDataProvider();
  const { schemata } = useContext(HasuraContext);

  const [loading_, setLoading] = useState(true);
  const [saving_, setSaving] = useState(false);
  const [error, setError] = useState();

  const translate = useTranslate();
  const notify = useNotify();
  const redirect_ = useRedirect();
  const [mutate] = useMutation();
  const locale = useLocale();

  useEffect(() => {
    if (!!record?.processDefinition?.id) {
      setLoading(true);
      setFormState('LOAD_FORM_DATA');
    }
  }, [record]);

  // Wait until record; Then fetch additional information
  useEffect(() => {
    if (formState !== 'LOAD_FORM_DATA' || !record?.processDefinition?.id) {
      return;
    }
    void (async () => {
      const { taskDefinitionKey, processDefinition } = record;
      try {
        const data = await Promise.all([
          await dataProvider.getOne('camunda_ProcessDefinition_UserTask', {
            id: processDefinition.id,
            // @ts-ignore
            filter: {
              user_task_id: taskDefinitionKey,
            },
          }),
          await dataProvider.getList(
            'vasara_user_task_form',
            // @ts-ignore
            {
              filter: {
                user_task_id: taskDefinitionKey,
                process_definition_key: processDefinition.key,
                process_definition_version: {
                  format: 'hasura-raw-query',
                  value: { _lte: processDefinition.version },
                },
              },
              sort: { field: 'process_definition_version', order: 'DESC' },
              pagination: { page: 1, perPage: 1 },
            }
          ),
        ]);
        const { data: userTask } = data[0];
        const { data: userTaskForms } = data[1];
        setUserTask(userTask);
        setUserTaskForm(userTaskForms[0]);
        setFormState('RESOLVE_ENTITIES');
      } catch (e) {
        setError(e);
      } finally {
        setSaving(false);
        setLoading(false);
      }
    })();
  }, [dataProvider, record, formState]);

  useEffect(() => {
    if (formState !== 'RESOLVE_ENTITIES' || !record?.processDefinition?.id) {
      return;
    }
    void (async () => {
      try {
        const entities: any = {};
        if (record?.processInstance.businessKey) {
          for (const entity of userTask.processDefinition.entities) {
            const { data: instances } = schemata.get(entity)
              ? await dataProvider.getList(
                  entity,
                  // @ts-ignore
                  {
                    filter: {
                      business_key: record.processInstance.businessKey,
                    },
                  }
                )
              : { data: [] };
            if (instances.length) {
              entities[entity] = instances[0];
            }
          }
        }
        const fields = ((userTaskForm as any).schema || []).reduce(
          (acc: any, cur: any) => acc.concat((cur || {}).fields || []),
          []
        );
        for (const field of fields) {
          record[field.id] = field.type === 'table' ? [] : null; // initialize field so conditions know it exists
          for (const source of field.sources || []) {
            const parts = source.split('.');
            if (parts.length >= 2 && parts[0] === 'context') {
              for (const variable of record.formVariables || {}) {
                if (variable.key === parts[1]) {
                  if (variable.value === true || variable.value === 'true') {
                    record[field.id] = true;
                  } else if (variable.value === false || variable.value === 'false') {
                    record[field.id] = false;
                  } else if (field.type === 'table') {
                    try {
                      record[field.id] = JSON.parse(variable.value) || [];
                    } catch (e) {
                      // pass
                    }
                  } else {
                    record[field.id] = variable.value || null;
                  }
                }
              }
              for (const variable of record.variables || {}) {
                if (variable.key === parts[1]) {
                  if (variable.value === true || variable.value === 'true') {
                    record[field.id] = true;
                  } else if (variable.value === false || variable.value === 'false') {
                    record[field.id] = false;
                  } else if (field.type === 'table') {
                    try {
                      record[field.id] = JSON.parse(variable.value) || [];
                    } catch (e) {
                      // pass
                    }
                  } else {
                    record[field.id] = variable.value || null;
                  }
                }
              }
            } else if (parts.length >= 2 && entities[parts[0]]) {
              // Copy metadata from entity to form field
              if (entities[parts[0]]?.['metadata']?.[parts[1]] !== undefined) {
                record[`${field.id}.metadata`] = entities[parts[0]]['metadata'][parts[1]];
              }
              // Copy value from entity to form field
              if (parts.length === 2 && entities[parts[0]][parts[1]] !== undefined) {
                record[field.id] = entities[parts[0]][parts[1]];
              } else if (parts.length === 3 && parts[2] === '{}' && field.type === 'table') {
                // JSONB for table or array
                record[field.id] = entities[parts[0]][parts[1]] || [];
              } else if (parts.length === 3 && parts[2] === '{}') {
                // JSONB
                for (const key of Object.keys(entities[parts[0]]?.[parts[1]] ?? {})) {
                  if (key === field.key) {
                    record[field.id] = entities[parts[0]][parts[1]][key];
                  }
                }
              }
            }
          }
          for (const variable_ of (field?.variables || []).concat(
            field?.dependency ? [{ source: field.dependency }] : []
          )) {
            const source = variable_?.source || '';
            const parts = source.split('.');
            if (parts.length >= 2 && parts[0] === 'context') {
              for (const variable of record.formVariables || {}) {
                if (variable.key === parts[1]) {
                  if (variable.value === true || variable.value === 'true') {
                    record[`${field.id}:${source}`] = true;
                  } else if (variable.value === false || variable.value === 'false') {
                    record[`${field.id}:${source}`] = false;
                  } else if (field.type === 'table') {
                    try {
                      record[`${field.id}:${source}`] = JSON.parse(variable.value) || [];
                    } catch (e) {
                      // pass
                    }
                  } else {
                    record[`${field.id}:${source}`] = variable.value || null;
                  }
                }
              }
              for (const variable of record.variables || {}) {
                if (variable.key === parts[1]) {
                  if (variable.value === true || variable.value === 'true') {
                    record[`${field.id}:${source}`] = true;
                  } else if (variable.value === false || variable.value === 'false') {
                    record[`${field.id}:${source}`] = false;
                  } else if (field.type === 'table') {
                    try {
                      record[`${field.id}:${source}`] = JSON.parse(variable.value) || [];
                    } catch (e) {
                      // pass
                    }
                  } else {
                    record[`${field.id}:${source}`] = variable.value || null;
                  }
                }
              }
            } else if (parts.length >= 2 && entities[parts[0]]) {
              if (parts.length === 2 && entities[parts[0]][parts[1]] !== undefined) {
                record[`${field.id}:${source}`] = entities[parts[0]][parts[1]];
              } else if (parts.length === 3 && parts[2] === '{}' && field.type === 'table') {
                // JSONB for table or array
                record[`${field.id}:${source}`] = entities[parts[0]][parts[1]] || [];
              } else if (parts.length === 3 && parts[2] === '{}') {
                // JSONB
                for (const key of Object.keys(entities[parts[0]]?.[parts[1]] ?? {})) {
                  if (key === field.key) {
                    record[`${field.id}:${source}`] = entities[parts[0]][parts[1]][key];
                  }
                }
              }
            }
          }
        }
        setUserTaskEntities(entities);
        setFormState('READY');
      } catch (e) {
        setError(e);
      } finally {
        setSaving(false);
        setLoading(false);
      }
    })();
  }, [dataProvider, record, schemata, userTask, userTaskForm, formState]);

  const saveAll = useCallback(
    (data: any, redirectTo: string, { onFailure } = {}) => {
      setSaving(true);
      (async () => {
        const create = (resource: any, data: any) =>
          new Promise<any>((resolve, reject) => {
            mutate(
              {
                type: 'create',
                resource: resource,
                payload: { data },
              },
              {
                onSuccess: ({ data }) => {
                  resolve(data);
                },
                onFailure: error => {
                  notify(error.message, 'error', { _: error.message });
                  reject(error);
                },
              }
            );
          });
        const update = (resource: any, id: any, data: any, action: any) =>
          new Promise<any>((resolve, reject) => {
            mutate(
              action
                ? {
                    type: 'update',
                    resource: resource,
                    payload: { id, action, data },
                  }
                : {
                    type: 'update',
                    resource: resource,
                    payload: { id, data },
                  },
              {
                onSuccess: ({ data }) => {
                  resolve(data);
                  reject(); // Did prevent react-admin from refresh...
                },
                onFailure: error => {
                  notify(error.message, 'error', { _: error.message });
                  reject(error);
                },
              }
            );
          });
        try {
          const entities: Record<string, Record<string, any>> = { context: {} };
          const fields = ((userTaskForm as any).schema || []).reduce(
            (acc: any, cur: any) => acc.concat((cur || {}).fields || []),
            []
          );
          // Encryption pass
          const secrets: vault_transit_encrypt_request[] = [];
          const secretsKeys: string[] = [];
          for (const field of fields) {
            if (
              record &&
              (field.type === 'string' || field.type === 'select') &&
              field.confidential &&
              data[field.id] &&
              !`${data[field.id]}`.startsWith('vault:')
            ) {
              secrets.push({
                businessKey: record.processInstance.businessKey,
                sources: field.sources,
                confidential: field.confidential,
                pii: field.PII,
                plaintext: data[field.id],
              });
              secretsKeys.push(field.id);
            }
          }
          if (secrets.length > 0) {
            const results = await create('vault_transit_encrypt', { batch: secrets });
            for (let i = 0; i < results.batch.length; i++) {
              data[secretsKeys[i]] = results.batch[i].ciphertext;
            }
          }
          for (const field of fields) {
            // Do not submit data for readonly fields
            if (field.readonly === true) {
              continue;
            }
            // Ensure that empty for boolean is false
            if (field.type === 'boolean' && !data[field.id]) {
              data[field.id] = false;
            }
            // Double check unconditional required fields without value
            if (
              !field.readonly &&
              !field.dependency &&
              field.required &&
              (data[field.id] === null || data[field.id] === undefined)
            ) {
              throw translate('ra.message.invalid_form');
            }
            // Map field values to sources
            for (const source of field.sources || []) {
              const parts = source.split('.');
              if (parts.length >= 2 && parts[0] && data[field.id] !== undefined) {
                let current: any = {
                  business_key: record?.processInstance.businessKey ?? uuid(),
                };
                if (record && !entities[parts[0]]) {
                  current = entities[parts[0]] = userTaskEntities[parts[0]]
                    ? {
                        metadata: {
                          ...userTaskEntities[parts[0]].metadata,
                        },
                      }
                    : {
                        ...current,
                      };
                  if (!current['metadata']) {
                    current['metadata'] = {};
                  }
                } else {
                  current = entities[parts[0]];
                }
                if (field.type === 'file') {
                  if (data[field.id] === null) {
                    current[parts[1]] = null;
                    current['metadata'][parts[1]] = null;
                  } else if (data[field.id] && typeof data[field.id] !== 'string') {
                    current[parts[1]] = await fileToBytea(data[field.id]);
                    current['metadata'][parts[1]] = {
                      name: data[field.id].rawFile.name,
                      type: data[field.id].rawFile.type,
                      size: data[field.id].rawFile.size,
                    };
                  }
                } else if (field.type === 'table') {
                  if (data[field.id] === null) {
                    current[parts[1]] = null;
                  } else {
                    current[parts[1]] = data[field.id];
                    if (field.PII && current['metadata']) {
                      if (!current['metadata']['PII']) {
                        current['metadata']['PII'] = [];
                      }
                      if (!current['metadata']['PII'].includes(parts[1])) {
                        current['metadata']['PII'].push(parts[1]);
                      }
                    }
                  }
                } else if (record && parts.length === 3 && parts[2] === '{}') {
                  // JSONB
                  if (!current[parts[1]]) {
                    if (userTaskEntities[parts[0]]) {
                      current[parts[1]] = userTaskEntities[parts[0]][parts[1]] || {};
                    } else {
                      current[parts[1]] = {};
                    }
                  }
                  if (!current[parts[1]]['@order']) {
                    current[parts[1]]['@order'] = [];
                  }
                  if (!current[parts[1]]['@order'].includes(field.key)) {
                    current[parts[1]]['@order'].push(field.key);
                  }
                  current[parts[1]][field.key] = data[field.id];
                  if (field.PII && current['metadata']) {
                    if (!current['metadata']['PII']) {
                      current['metadata']['PII'] = [];
                    }
                    const key = `${parts[1]}.${field.key}`;
                    if (!current['metadata']['PII'].includes(key)) {
                      current['metadata']['PII'].push(key);
                    }
                  }
                } else {
                  current[parts[1]] = data[field.id];
                  if (field.PII && current['metadata']) {
                    if (!current['metadata']['PII']) {
                      current['metadata']['PII'] = [];
                    }
                    if (!current['metadata']['PII'].includes(parts[1])) {
                      current['metadata']['PII'].push(parts[1]);
                    }
                  }
                }
              }
            }
          }
          // Create or update
          for (const resource in entities) {
            if (resource !== 'context' && entities.hasOwnProperty(resource)) {
              if (record && userTaskEntities[resource]) {
                const { id } = userTaskEntities[resource];
                await update(resource, id, entities[resource], null);
              } else {
                await create(resource, entities[resource]);
              }
            }
          }
          // Complete
          if (redirectTo !== 'edit') {
            setLoading(true);
            save(
              {
                ...data,
                variables: entities['context'],
              },
              false,
              {
                onSuccess: async () => {
                  const message = userTaskForm?.settings?.saveAndSubmit?.customize
                    ? userTaskForm?.settings?.saveAndSubmit?.helperText?.[locale] || 'ra.notification.updated'
                    : 'ra.notification.updated';
                  notify(message, 'info', { smart_count: 1, _: message }, false);
                  const retries = !!userTaskForm?.settings?.waitForNextTask ? 5 : 3;
                  if (record?.processInstance?.businessKey) {
                    // TODO: Refactor this loop to useEffect instead of loop to allow canceling
                    // on manual location change.
                    for (let i = 1; i < retries; i++) {
                      await sleep(300 * i); // wait for task to change or disappear
                      const results = await dataProvider.getList('UserTask', {
                        pagination: {
                          page: 1,
                          perPage: 1000,
                        },
                        sort: {
                          field: 'taskDefinitionKey',
                          order: 'asc',
                        },
                        filter: { businessKey: record?.processInstance?.businessKey },
                      });
                      if (results.total) {
                        redirect_('edit', '/UserTask', results.data[0].id);
                        return;
                      }
                    }
                    redirect_('/UserTask');
                  } else {
                    redirect_('/UserTask');
                  }
                },
                onFailure,
              }
            );
          } else {
            await update(
              'UserTask',
              data.id,
              {
                ...data,
                variables: entities['context'],
              },
              'update'
            );
            notify('vasara.notification.updated', 'info');
          }
        } catch (e) {
          notify(e, 'error', { _: e });
        } finally {
          setSaving(false);
        }
      })();
    },
    [userTaskForm, mutate, notify, record, save, dataProvider, redirect_, userTaskEntities, locale, translate]
  );

  if (loading || !loaded || !record || loading_ || saving) {
    return <Loading />;
  }

  if (error) {
    return <TracedError error={error || {}} />;
  }

  normalizeAndMigrateForm(userTaskForm);

  return (
    <EditContextProvider value={editControllerProps}>
      <UserTaskContext.Provider value={userTask as UserTask}>
        <Title title={record.name} />
        <TopToolbar>
          <BackButton redirectTo="/UserTask" />
        </TopToolbar>
        {cloneElement(props.children, {
          basePath,
          record: { ...userTaskForm, ...record, entities: userTaskEntities },
          redirect,
          resource,
          save: saveAll,
          saving: saving || saving_,
          version,
        })}
      </UserTaskContext.Provider>
    </EditContextProvider>
  );
};

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const useStyles = makeStyles({
  button: {
    fontWeight: 'bold',
    marginTop: '1.5em',
    marginBottom: '1.5em',
  },
  entityLink: {
    color: '#002957',
  },
  entityLinkIcon: {
    marginRight: '5px',
  },
  mr: {
    marginRight: '1em',
  },
  floatRight: {
    display: 'flex',
    marginLeft: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const CustomLink = React.forwardRef((props: any, ref: any) => {
  // XXX: What to do with the ref?
  const { children, ...rest } = props;
  return <Link {...rest}>{children}</Link>;
});

const ListItemLink = (props: any) => {
  return <ListItem button component={CustomLink} {...props} />;
};

const UserTaskForm = (props: any) => {
  const { schemata } = useContext(HasuraContext);
  const classes = useStyles();
  const translate = useTranslate();
  return (
    <>
      <List>
        {Object.keys((props.record && props.record.entities) || {}).map((key: string, index) => {
          const schema = schemata.get(key);
          return (
            <ListItemLink
              key={index}
              to={{
                pathname: `/${key}/${props.record.entities[key].id}/show`,
                search: `user_task_id=${props.record.id}`,
              }}
              className={classes.entityLink}
            >
              <Description className={classes.entityLinkIcon} />
              {schema ? labelFromSchema(schema) : translate('vasara.form.record')}
            </ListItemLink>
          );
        })}
      </List>
      <Form title={props.record.name} toolbarRef={props.toolbarRef} />
    </>
  );
};

const ToolbarRef = (props: any) => {
  // This wrapper is required to filter props passed for toolbar children
  const classes = useStyles();
  const { toolbarRef } = props;
  return <div className={classes.floatRight} ref={toolbarRef} />;
};

const UserTaskEditToolbar = (props: any) => {
  const translate = useTranslate();
  const classes = useStyles();
  const locale = useLocale();

  const { toolbarRef, ...rest } = props;
  return (
    <Toolbar {...rest}>
      <SaveButton label={translate('vasara.action.saveDraft')} redirect="edit" color="default" className={classes.mr} />
      <SaveButton
        label={
          props?.record?.settings?.saveAndSubmit?.customize
            ? props?.record?.settings?.saveAndSubmit?.label?.[locale] || translate('vasara.action.saveAndSubmit')
            : translate('vasara.action.saveAndSubmit')
        }
        redirect="list"
        submitOnEnter={true}
        className={classes.mr}
      />
      <CancelButton />
      <ToolbarRef toolbarRef={toolbarRef} />
    </Toolbar>
  );
};

const UserTaskEdit = (props: any) => {
  const toolbarRef = useRef();
  return (
    <CustomEdit {...props} undoable={false} redirect="list">
      <SimpleForm
        subscription={{ submitting: true, pristine: true }}
        toolbar={<UserTaskEditToolbar toolbarRef={toolbarRef} />}
      >
        <AccordionBpmnField source="processDefinition.diagram" label={'%{processDefinition.name}: %{name}'} />
        <UserTaskForm toolbarRef={toolbarRef} />
        {/*<ReferenceManyField addLabel={false} reference="camunda_UserTask_Comment" target="id">*/}
        {/*  <Datagrid>*/}
        {/*    <TextField source="message" label="vasara.column.comment" sortable={false} />*/}
        {/*    <TextField source="user.firstName" label="vasara.column.firstName" sortable={false} />*/}
        {/*    <TextField source="user.lastName" label="vasara.column.lastName" sortable={false} />*/}
        {/*    <TextField source="time" sortable={false} label="vasara.columnTime" />*/}
        {/*  </Datagrid>*/}
        {/*</ReferenceManyField>*/}
      </SimpleForm>
    </CustomEdit>
  );
};

export default UserTaskEdit;
