import Typography from '@material-ui/core/Typography';
import React, { memo } from 'react';
import {
  Datagrid,
  DateField,
  List,
  Pagination,
  PaginationProps,
  TextField,
  TextFieldProps,
  sanitizeFieldRestProps,
} from 'react-admin';

import { getUser } from '../../../Auth/authProvider';
import { PAGE_SIZE } from '../../../util/constants';

const SummaryField: React.FC<TextFieldProps> = memo<TextFieldProps>(
  ({ className, source, record = {}, emptyText, ...rest }) => {
    const variables: any = new Map(
      Object.values((record as any)?.variables ?? {}).map((variable: any) => [variable.key, variable.value])
    );
    const value: any = variables.get('name') || 'n/a';
    return (
      <Typography component="span" variant="body2" className={className} {...sanitizeFieldRestProps(rest)}>
        {value != null && typeof value !== 'string' ? JSON.stringify(value) : value || emptyText}
      </Typography>
    );
  }
);

const ListPagination = (props: PaginationProps) => <Pagination rowsPerPageOptions={[10, 25, 50, 100]} {...props} />;

const UserTaskList = (props: any) => {
  const isGuest = (getUser()?.profile?.preferred_username || '').match(/^guest-.*/);
  return (
    <List
      {...props}
      hasCreate={!isGuest}
      title="vasara.resource.userTask"
      bulkActionButtons={false}
      exporter={false}
      pagination={<ListPagination {...props} />}
      perPage={PAGE_SIZE}
    >
      <Datagrid rowClick="edit">
        <TextField source="processDefinition.name" label="vasara.column.processDefinitionName" sortable={false} />
        <DateField source="historicProcessInstance.startTime" label="Luotu" locales="fi-FI" sortable={false} />
        <SummaryField source="" label="Kuvaus" sortable={false} />
        <TextField source="name" label="vasara.column.userTask" sortable={false} />
        <TextField source="assignee.name" label="vasara.column.assignee" sortable={false} />
      </Datagrid>
    </List>
  );
};

export default UserTaskList;
