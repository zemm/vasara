import { makeStyles } from '@material-ui/core/styles';
import { UserTask } from 'bpmn-moddle';
import { parse } from 'query-string';
import React, { cloneElement, useCallback, useEffect, useState } from 'react';
import {
  CreateContextProvider,
  CreateControllerProps,
  CreateProps,
  Loading,
  ResourceComponentProps,
  SaveButton,
  SimpleForm,
  Toolbar,
  useCreateController,
  useDataProvider,
  useLocale,
  useNotify,
  useRedirect,
} from 'react-admin';
import { v4 as uuid } from 'uuid';

import CancelButton from '../../../Components/CancelButton';
import TracedError from '../../../Components/TracedError';
import UserTaskContext from '../../../DataProviders/Camunda/UserTaskContext';
import FormBuilder from '../../../FormBuilder/FormBuilder';
import { normalizeAndMigrateForm } from '../../../util/helpers';

const CustomCreate: React.FC<CreateProps & CreateControllerProps & ResourceComponentProps> = props => {
  const createControllerProps = useCreateController(props);
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Create Post'
    record, // empty object, unless some values were passed in the location state to prefill the form
    redirect, // the default redirection route. Defaults to 'edit', unless the resource has no edit view, in which case it's 'list'
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the create callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to create the record
    version, // integer used by the refresh feature
  } = createControllerProps;

  const customSave = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      // Prevent react-final-form-array artifacts from being persisted
      delete data.schemaIds;
      for (const fieldset of data.schema || {}) {
        delete fieldset.fieldsIds;
      }
      (async () => {
        save(data, redirectTo, { onSuccess, onFailure });
      })();
    },
    [save]
  );

  return (
    <CreateContextProvider value={createControllerProps}>
      <div>
        <h1>{props.defaultTitle ?? defaultTitle}</h1>
        {React.Children.map(props.children, child =>
          React.isValidElement(child)
            ? cloneElement(child, {
                basePath,
                record,
                redirect,
                resource,
                save: customSave,
                saving,
                version,
              })
            : null
        )}
      </div>
    </CreateContextProvider>
  );
};

const useStyles = makeStyles({
  mr: {
    marginRight: '1em',
  },
  delete: {
    marginLeft: 'auto',
  },
});

const UserTaskFormCreate: React.FC<CreateControllerProps & ResourceComponentProps> = props => {
  const { processDefinitionId, processDefinitionKey, processDefinitionVersion, userTaskId } = parse(
    props.location?.search ?? ''
  );
  const initialValues = {
    process_definition_key: processDefinitionKey,
    process_definition_version: processDefinitionVersion,
    user_task_id: userTaskId,
  };
  const locale = useLocale();
  const notify = useNotify();
  const redirect = useRedirect();
  const redirectTo = processDefinitionId ? `/ProcessDefinition/${processDefinitionId}/show` : 'list';

  const dataProvider = useDataProvider();
  const [context, setContext]: [UserTask | undefined, any] = useState();
  const [loadingContext, setLoadingContext]: [boolean, any] = useState(true);
  const [template, setTemplate]: [Record<any, any>, any] = useState({});
  const [loadingTemplate, setLoadingTemplate]: [boolean, any] = useState(true);
  const [error, setError]: [Record<any, any> | undefined, any] = useState();

  useEffect(() => {
    (async () => {
      await Promise.all([
        dataProvider
          .getOne('camunda_ProcessDefinition_UserTask', {
            // @ts-ignore
            id: processDefinitionId,
            filter: {
              user_task_id: userTaskId,
            },
          })
          .then(({ data }) => {
            setContext(data);
            setLoadingContext(false);
          })
          .catch(error => {
            setError(error);
            setLoadingContext(false);
          }),
        dataProvider
          .getList('vasara_user_task_form', {
            filter: {
              user_task_id: userTaskId,
              process_definition_key: processDefinitionKey,
              process_definition_version: {
                format: 'hasura-raw-query',
                value: { _lte: parseInt(processDefinitionVersion as string, 10) || 0 },
              },
            },
            sort: { field: 'process_definition_version', order: 'DESC' },
            pagination: { page: 1, perPage: 1 },
          })
          .then(({ data }) => {
            if (data.length) {
              setTemplate(data[0]);
            }
            setLoadingTemplate(false);
          })
          .catch(error => {
            setError(error);
            setLoadingTemplate(false);
          }),
      ]);
    })();
  }, [dataProvider, processDefinitionId, processDefinitionKey, processDefinitionVersion, userTaskId]);

  if (loadingContext || loadingTemplate) {
    return <Loading />;
  }

  if (error) {
    return <TracedError error={error || {}} />;
  }

  // Remove ID from copied template, to allow save as new
  if (template['id']) {
    delete template['id'];
  }

  const defaults: any = {
    schema: [
      {
        id: uuid(),
        label: {},
        fields: [],
      },
    ],
  };
  defaults.schema[0].label[locale] = context?.name ?? '';

  normalizeAndMigrateForm(template);

  const onSuccess = () => {
    notify('ra.notification.created', 'info', { smart_count: 1 }, false);
    redirect(redirectTo);
  };

  const CustomToolbar = (props: any) => {
    const classes = useStyles();
    return (
      <Toolbar {...props}>
        <SaveButton
          label="vasara.action.saveAndClose"
          redirect={redirectTo}
          submitOnEnter={false}
          className={classes.mr}
        />
        <CancelButton className={classes.mr} />
      </Toolbar>
    );
  };

  return (
    <UserTaskContext.Provider value={context as UserTask}>
      <CustomCreate {...props} defaultTitle={context?.name ?? ''} onSuccess={onSuccess}>
        <SimpleForm
          initialValues={{ ...defaults, ...template, ...initialValues }}
          toolbar={<CustomToolbar redirect={redirectTo} />}
          validateOnBlur={true}
        >
          <FormBuilder title={context?.name ?? ''} />
        </SimpleForm>
      </CustomCreate>
    </UserTaskContext.Provider>
  );
};

export default UserTaskFormCreate;
