import { makeStyles } from '@material-ui/core/styles';
import { UserTask } from 'bpmn-moddle';
import { parse } from 'query-string';
import React, { cloneElement, useCallback, useEffect, useState } from 'react';
import {
  DeleteButton,
  EditContextProvider,
  Loading,
  SaveButton,
  SimpleForm,
  Title,
  Toolbar,
  useDataProvider,
  useEditController,
} from 'react-admin';

import CancelButton from '../../../Components/CancelButton';
import TracedError from '../../../Components/TracedError';
import UserTaskContext from '../../../DataProviders/Camunda/UserTaskContext';
import FormBuilder from '../../../FormBuilder/FormBuilder';
import { normalizeAndMigrateForm } from '../../../util/helpers';

const CustomEdit = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  const editControllerProps = useEditController(props);
  const {
    basePath, // deduced from the location, useful for action buttons
    defaultTitle, // the translated title based on the resource, e.g. 'Post #123'
    loaded, // boolean that is false until the record is available
    loading, // boolean that is true on mount, and false once the record was fetched
    record, // record fetched via dataProvider.getOne() based on the id from the location
    resource, // the resource name, deduced from the location. e.g. 'posts'
    save, // the update callback, to be passed to the underlying form as submit handler
    saving, // boolean that becomes true when the dataProvider is called to update the record
    version, // integer used by the refresh feature
  } = editControllerProps;

  const dataProvider = useDataProvider();
  const [context, setContext]: [UserTask | undefined, any] = useState();
  const [loading_, setLoading] = useState(!saving);
  const [error, setError]: [Record<any, any> | undefined, any] = useState();

  useEffect(() => {
    if (record && !saving) {
      dataProvider
        .getOne(
          'camunda_ProcessDefinition_UserTask',
          processDefinitionId
            ? {
                id: processDefinitionId,
                // @ts-ignore
                filter: {
                  user_task_id: record.user_task_id,
                },
              }
            : {
                key: record.process_definition_key,
                version: record.process_definition_version,
                filter: {
                  user_task_id: record.user_task_id,
                },
              }
        )
        .then(({ data }: any) => {
          setContext(data);
          setLoading(false);
        })
        .catch((error: any) => {
          setError(error);
          setLoading(false);
        });
    }
  }, [dataProvider, processDefinitionId, record, saving]);

  const customSave = useCallback(
    (data: any, redirectTo: string, { onSuccess, onFailure } = {}) => {
      // Prevent react-final-form-array artifacts from being persisted
      delete data.schemaIds;
      for (const fieldset of data.schema || {}) {
        delete fieldset.fieldsIds;
      }
      (async () => {
        save(data, redirectTo, { onSuccess, onFailure });
      })();
    },
    [save]
  );

  if (loading || !loaded || !record || loading_) {
    return <Loading />;
  }

  if (error) {
    return <TracedError error={error || {}} />;
  }

  const redirect_ = processDefinitionId
    ? `/ProcessDefinition/${processDefinitionId}/show`
    : context
    ? `/ProcessDefinition/${(context as any).processDefinition.id}/show`
    : 'list';

  // XXX: Remove once all forms have been migrated
  normalizeAndMigrateForm(record);

  return (
    <EditContextProvider value={editControllerProps}>
      <UserTaskContext.Provider value={context as UserTask}>
        <Title title={context ? (context as any).name : defaultTitle} />
        {cloneElement(props.children, {
          basePath,
          record,
          redirect: redirect_,
          resource,
          save: customSave,
          saving,
          version,
        })}
      </UserTaskContext.Provider>
    </EditContextProvider>
  );
};

const useStyles = makeStyles({
  mr: {
    marginRight: '1em',
  },
  delete: {
    marginLeft: 'auto',
  },
});

const CustomToolbar = (props: any) => {
  const classes = useStyles();
  return (
    <Toolbar {...props}>
      <SaveButton label="ra.action.save" redirect="edit" color="default" className={classes.mr} />
      <SaveButton
        label="vasara.action.saveAndClose"
        redirect={props.redirect}
        submitOnEnter={false}
        className={classes.mr}
      />
      <CancelButton className={classes.mr} />
      <DeleteButton
        label="ra.action.delete"
        redirect={props.redirect}
        submitOnEnter={false}
        className={classes.delete}
      />
    </Toolbar>
  );
};

const UserTaskFormEdit = (props: any) => {
  const { processDefinitionId } = parse(props.location.search);
  const redirect = processDefinitionId ? `/ProcessDefinition/${processDefinitionId}/show` : 'list';
  return (
    <CustomEdit {...props} undoable={false}>
      <SimpleForm
        redirect={redirect}
        toolbar={<CustomToolbar redirect={redirect} />}
        validateOnBlur={true}
        subscription={{ submitting: true, pristine: true }}
      >
        <FormBuilder title={''} />
      </SimpleForm>
    </CustomEdit>
  );
};

export default UserTaskFormEdit;
