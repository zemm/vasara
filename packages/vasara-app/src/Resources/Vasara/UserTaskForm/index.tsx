import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import { ResourceComponentProps } from 'ra-core';

import UserTaskFormCreate from './UserTaskFormCreate';
import UserTaskFormEdit from './UserTaskFormEdit';

const resource = {
  name: 'vasara_user_task_form',
  create: UserTaskFormCreate as React.FC<ResourceComponentProps>,
  edit: UserTaskFormEdit as React.FC<ResourceComponentProps>,
  icon: ChatBubbleIcon,
  options: { hideFromMenu: true },
};

export default resource;
