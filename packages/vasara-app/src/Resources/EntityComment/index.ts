import ChatBubbleIcon from '@material-ui/icons/ChatBubble';

import EntityCommentCreate from './EntityCommentCreate';
import EntityCommentEdit from './EntityCommentEdit';
import EntityCommentShow from './EntityCommentShow';

const resource = {
  create: EntityCommentCreate,
  edit: EntityCommentEdit,
  show: EntityCommentShow,
  icon: ChatBubbleIcon,
  options: { hideFromMenu: true },
};

export default resource;
