import React from 'react';
import { Show, SimpleShowLayout, TextField } from 'react-admin';

const CommentShow = (props: any) => (
  <Show {...props}>
    {/* @ts-ignore */}
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="body" />
    </SimpleShowLayout>
  </Show>
);

export default CommentShow;
