import { makeStyles } from '@material-ui/core/styles';
import RichTextInput from 'ra-input-rich-text';
import React from 'react';
import { DeleteButton, Edit, SaveButton, SimpleForm, Toolbar, TopToolbar, required } from 'react-admin';

import BackButton from '../../Components/BackButton';
import CancelButton from '../../Components/CancelButton';

const redirect = (basePath: string, id: string, data: any) =>
  `${basePath.replace('_comment', '')}/${data.entity_id}/show/comments`;

const useStyles = makeStyles({
  delete: {
    marginLeft: 'auto',
  },
  mr: {
    marginRight: '1em',
  },
});

const CustomToolbar = (props: any) => {
  const classes = useStyles();
  return (
    <Toolbar {...props}>
      <SaveButton className={classes.mr} />
      <CancelButton className={classes.mr} />
      <DeleteButton
        redirect={basePath => redirect(basePath, props.record.id, props.record)}
        className={classes.delete}
      />
    </Toolbar>
  );
};

const CustomActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <BackButton go={-1} />
    </TopToolbar>
  );
};

const EntityCommentEdit = (props: any) => {
  return (
    <Edit {...props} actions={<CustomActions />} undoable={false} title="vasara.resource.comment">
      <SimpleForm redirect={redirect as any} toolbar={<CustomToolbar />}>
        {/*<TextInput source="subject" label="vasara.column.subject" validate={[required()]} />*/}
        <RichTextInput source="body" label="vasara.column.body" validate={[required()]} multiline={true} />
      </SimpleForm>
    </Edit>
  );
};

export default EntityCommentEdit;
