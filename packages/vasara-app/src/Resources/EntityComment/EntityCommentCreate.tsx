import { makeStyles } from '@material-ui/core/styles';
import { parse } from 'query-string';
import RichTextInput from 'ra-input-rich-text';
import React from 'react';
import { Create, SaveButton, SimpleForm, Toolbar, TopToolbar, required } from 'react-admin';

import BackButton from '../../Components/BackButton';
import CancelButton from '../../Components/CancelButton';

const useStyles = makeStyles({
  mr: {
    marginRight: '1em',
  },
});

const CustomActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <BackButton go={-1} />
    </TopToolbar>
  );
};

const CustomToolbar = (props: any) => {
  const classes = useStyles();
  return (
    <Toolbar {...props}>
      <SaveButton className={classes.mr} />
      <CancelButton />
    </Toolbar>
  );
};

const EntityCommentCreate = (props: any) => {
  const { entity_id, business_key } = parse(props.location.search);
  const { resource } = props;
  const entity_resource = resource.split('_comment')[0];
  const redirect = `/${entity_resource}/${entity_id}/show/comments`;
  let metadata: any = {};

  return (
    <Create {...props} title="vasara.resource.comment" actions={<CustomActions />}>
      <SimpleForm defaultValue={{ entity_id, business_key, metadata }} redirect={redirect} toolbar={<CustomToolbar />}>
        {/*<TextInput source="subject" validate={[required()]} />*/}
        <RichTextInput source="body" label="vasara.column.body" validate={[required()]} multiline={true} />
      </SimpleForm>
    </Create>
  );
};

export default EntityCommentCreate;
