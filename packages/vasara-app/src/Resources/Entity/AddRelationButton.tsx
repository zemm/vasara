import { withStyles } from '@material-ui/core/styles';
import { Add } from '@material-ui/icons';
import React from 'react';
import { Button, useRedirect } from 'react-admin';

const styles = {
  button: {},
};

const AddRelationButton = ({ classes, basePath, reference, resource, target, record, ...rest }: any) => {
  const redirect = useRedirect();
  return (
    <Button
      label="ra.action.add"
      className={classes.button}
      onClick={() => {
        redirect(
          `/${reference}/create?initialField=${target}&initialValue=${record.id}&redirectTo=${basePath}/${record.id}`
        );
      }}
    >
      <Add />
    </Button>
  );
};

export default withStyles(styles)(AddRelationButton);
