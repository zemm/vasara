import { Theme, makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/styles';
import { IntrospectionEnumValue, IntrospectionField, IntrospectionObjectType } from 'graphql';
import React, { useContext } from 'react';
import {
  AutocompleteInput,
  ChipField,
  Edit,
  Labeled,
  ListContext,
  NumberInput,
  ReferenceArrayField,
  ReferenceInput,
  ReferenceManyField,
  ReferenceManyFieldController,
  SaveButton,
  SelectInput,
  SimpleForm,
  SingleFieldList,
  TextField,
  Toolbar,
  TopToolbar,
  required,
  useMutation,
  useNotify,
  useRefresh,
} from 'react-admin';

import BackButton from '../../Components/BackButton';
import CancelButton from '../../Components/CancelButton';
import VaultTextInput from '../../Components/VaultTextInput';
import VaultTextReadOnlyInput from '../../Components/VaultTextReadOnlyInput';
import VaultTextReadOnlyInputPlain from '../../Components/VaultTextReadOnlyInputPlain';
import { vault_transit_encrypt_request } from '../../DataProviders/Actions/types';
import HasuraContext from '../../DataProviders/HasuraContext';
import { SCALARINPUTS_MAP } from '../../util/constants';
import {
  base64ToBytea,
  convertFileToBase64,
  getFieldTypeName,
  getMainIdentifierColumn,
  getOtherReferenceFields,
  getReferenceFields,
  getReferenceResourceName,
  getReferencingResourceField,
  getReferencingResourceFieldName,
  getSourceResourceField,
  isAggregateField,
  isComputedField,
  isEnumField,
  isFkField,
  isImplicitField,
  isM2MField,
  isM2OField,
  isO2MField,
  isScalarField,
  labelFromField,
  labelFromSchema,
  orderFromField,
} from '../../util/helpers';
import AddRelatedEntityButton from './AddRelatedEntityButton';
import AddRelationButton from './AddRelationButton';

const useStyles = makeStyles({
  mr: {
    marginRight: '1em',
  },
});

const styles = (theme: Theme) => ({
  relationButtons: {
    display: 'flex',
  },
});

const CustomToolbar = (props: any) => {
  const classes = useStyles();
  const { record, resource } = props;
  const [mutate] = useMutation();
  const notify = useNotify();
  const create = (resource: any, data: any) =>
    new Promise<any>((resolve, reject) => {
      mutate(
        {
          type: 'create',
          resource: resource,
          payload: { data },
        },
        {
          onSuccess: ({ data }) => {
            resolve(data);
          },
          onFailure: error => {
            notify(error.message, 'error', { _: error.message });
            reject(error);
          },
        }
      );
    });
  const transform = (data: any) => {
    const promises: Promise<any>[] = [];
    const secrets: vault_transit_encrypt_request[] = [];
    const secretsKeys: string[] = [];
    for (const name in data) {
      // Encryption pass
      if (
        data.hasOwnProperty(name) &&
        (record[name] || '').startsWith &&
        (record[name] || '').startsWith('vault:') &&
        !((data[name] || '').startsWith && (data[name] || '').startsWith('vault:'))
      ) {
        secrets.push({
          businessKey: record.business_key,
          sources: [`${resource}.${name}`],
          confidential: true,
          pii: false,
          plaintext: data[name],
        });
        secretsKeys.push(name);
      }
      // Handle file input
      if (data.hasOwnProperty(name) && data[name] && data[name].rawFile) {
        if (!data['metadata']) {
          data['metadata'] = {};
        }
        data['metadata'][name] = {
          name: data[name].rawFile.name,
          type: data[name].rawFile.type,
          size: data[name].rawFile.size,
        };
        promises.push(
          new Promise(success => {
            convertFileToBase64(data[name]).then((a: any) => {
              data[name] = base64ToBytea(a);
              success(data);
            });
          })
        );
      }
    }
    if (secrets.length > 0) {
      return create('vault_transit_encrypt', { batch: secrets }).then(results => {
        for (let i = 0; i < results.batch.length; i++) {
          data[secretsKeys[i]] = results.batch[i].ciphertext;
          return new Promise(success => {
            Promise.all(promises).then(() => {
              success(data);
            });
          });
        }
      });
    } else if (promises.length > 0) {
      return new Promise(success => {
        Promise.all(promises).then(() => {
          success(data);
        });
      });
    } else {
      return data;
    }
  };

  return (
    <Toolbar {...props}>
      <SaveButton transform={transform} className={classes.mr} />
      <CancelButton />
    </Toolbar>
  );
};

const CustomActions = ({ basePath, data }: any) => {
  return (
    <TopToolbar>
      <BackButton go={-1} />
    </TopToolbar>
  );
};

const withAddRelationButton = (WrappedComponent: any) => {
  return class extends React.Component<any, any> {
    render() {
      const { classes, referenced, ...rest } = this.props;
      return (
        <>
          <WrappedComponent {...rest} />
          <div className={classes.relationButtons}>
            <AddRelationButton {...rest} />
            <AddRelatedEntityButton {...rest} referenced={referenced} />
          </div>
        </>
      );
    }
  };
};

const ReferenceManyFieldControllerWithAdd = withStyles(styles)(withAddRelationButton(ReferenceManyFieldController));

const withRecordOnDelete = (WrappedComponent: any) => {
  return class extends React.Component<any, any> {
    render() {
      const { onDelete, ...rest } = this.props;
      return (
        <WrappedComponent
          {...rest}
          onDelete={(e: MouseEvent) => {
            onDelete(e, this.props.record);
          }}
        />
      );
    }
  };
};

const ChipFieldWithDelete = withRecordOnDelete(ChipField);

const EntityEdit = (props: any) => {
  const { schemata, enums, fields: fieldsByName } = useContext(HasuraContext);
  const resourceName = props.resource;
  const schema = schemata.get(resourceName) as IntrospectionObjectType;
  const label = labelFromSchema(schema);
  const fields = fieldsByName.get(resourceName) as Map<string, IntrospectionField>;
  const sorted: IntrospectionField[] = schema.fields
    .map(field => [
      orderFromField(
        field,
        fields.has(`${field.name}_id`)
          ? orderFromField(fields.get(`${field.name}_id`) as IntrospectionField)
          : undefined
      ),
      field.name,
    ])
    .sort()
    .map(pair => fields.get(pair[1]) as IntrospectionField);

  const [mutate] = useMutation();
  const refresh = useRefresh();

  return schema ? (
    <Edit {...props} actions={<CustomActions />} undoable={false} title={label}>
      <SimpleForm redirect="show" toolbar={<CustomToolbar />}>
        {sorted.map((field: IntrospectionField, i: number) => {
          if (isImplicitField(field) || isFkField(field) || isAggregateField(field) || isComputedField(field)) {
            return null;
          } else if (isScalarField(field)) {
            const typeName = getFieldTypeName(field.type);
            const InputComponent = SCALARINPUTS_MAP[typeName] || TextField;
            return InputComponent === NumberInput ? (
              <NumberInput
                key={i}
                source={field.name}
                label={labelFromField(field)}
                fullWidth={true}
                min={0}
                validate={[required()]}
                helperText={false}
              />
            ) : (
              <InputComponent
                key={i}
                source={field.name}
                label={labelFromField(field)}
                fullWidth={true}
                multiline={
                  [VaultTextInput, VaultTextReadOnlyInput, VaultTextReadOnlyInputPlain].includes(InputComponent)
                    ? true
                    : null
                }
                helperText={false}
              />
            );
          } else if (isEnumField(field)) {
            const type_ = enums.get(getFieldTypeName(field.type));
            if (type_) {
              const choices = type_.enumValues.map((value: IntrospectionEnumValue) => {
                return {
                  id: value.name,
                  name: value.description,
                };
              });
              return <SelectInput key={i} source={field.name} choices={choices} fullWidth={true} helperText={false} />;
            } else {
              return null;
            }
          } else if (isM2MField(field)) {
            const intermediateResourceName = getReferenceResourceName(field);
            const intermediateResourceSchema = schemata.get(intermediateResourceName) as IntrospectionObjectType;
            const intermediateReferenceFieldName = getReferencingResourceFieldName(field, resourceName, schemata);

            const referenceFieldCandidates = getOtherReferenceFields(schema.name, intermediateResourceSchema);
            const referencedResourceName = getReferenceResourceName(referenceFieldCandidates[0]);
            const referencedFieldCandidates = getReferenceFields(referencedResourceName, intermediateResourceSchema);

            if (!intermediateReferenceFieldName || referencedFieldCandidates.length === 0) {
              return null;
            }

            const referencedField = getSourceResourceField(
              referencedFieldCandidates[0],
              intermediateResourceName,
              schemata
            );

            if (!referencedField) {
              return null;
            }
            const referencedResourceIdentifierColumn = getMainIdentifierColumn(referencedResourceName, schemata);

            console.log(
              `M2M ${field.name}: ${intermediateResourceName}.${intermediateReferenceFieldName} => ${intermediateResourceName}.${referencedField.name} => ${referencedResourceName}.id (${referencedResourceIdentifierColumn})`
            );

            return (
              <ReferenceManyFieldControllerWithAdd
                key={`${i}`}
                reference={intermediateResourceName}
                referenced={referencedResourceName}
                source={'id'}
                resource={intermediateResourceName}
                basePath={`/${intermediateResourceName}`}
                target={intermediateReferenceFieldName}
              >
                {(props: any) => {
                  return (
                    <Labeled label={labelFromField(referencedField)}>
                      <ListContext.Provider
                        value={{
                          ...props,
                          basePath: `/${referencedResourceName}`,
                          resource: { referencedResourceName },
                        }}
                      >
                        <ReferenceArrayField
                          basePath={`/${referencedResourceName}`}
                          resource={intermediateResourceName}
                          reference={referencedResourceName}
                          record={{
                            id: '', // this is a faux record for passing ids
                            ids: Object.keys(props.data || {}).map(key => props.data[key][referencedField.name]),
                          }}
                          source={'ids'}
                        >
                          <SingleFieldList linkType="show">
                            <ChipFieldWithDelete
                              source={referencedResourceIdentifierColumn}
                              onDelete={(e: MouseEvent, record: any) => {
                                e.preventDefault();
                                for (const intermediateId in props.data) {
                                  if (props.data?.[intermediateId]?.[referencedField.name] === record.id) {
                                    mutate(
                                      {
                                        type: 'delete',
                                        resource: intermediateResourceName,
                                        payload: { id: intermediateId },
                                      },
                                      {
                                        onSuccess: ({ data }) => {
                                          refresh();
                                        },
                                      }
                                    );
                                  }
                                }
                              }}
                            />
                          </SingleFieldList>
                        </ReferenceArrayField>
                      </ListContext.Provider>
                    </Labeled>
                  );
                }}
              </ReferenceManyFieldControllerWithAdd>
            );
          } else if (isM2OField(field)) {
            const referencedResourceName = getReferenceResourceName(field);
            const referencedResourceSchema = schemata.get(referencedResourceName) as IntrospectionObjectType;
            const referenceField = getSourceResourceField(field, resourceName, schemata);

            if (referencedResourceName === 'camunda_User' && fields.has(`${field.name}_id`)) {
              return (
                <ReferenceInput
                  key={i}
                  label={labelFromField(fields.get(`${field.name}_id`) as IntrospectionField)}
                  source={`${field.name}_id`}
                  reference={referencedResourceName}
                  allowEmpty={true}
                  fullWidth={true}
                  filterToQuery={(q: string) => {
                    if (q) {
                      return {
                        q,
                      };
                    }
                  }}
                >
                  <AutocompleteInput optionText={'name'} />
                </ReferenceInput>
              );
            } else if (!referenceField) {
              return null;
            }

            console.log(`M2O ${field.name}: ${schema.name}.${referenceField.name} => ${referencedResourceName}.id`);

            return (
              <ReferenceInput
                key={i}
                label={labelFromField(referenceField, labelFromSchema(referencedResourceSchema))}
                source={referenceField.name}
                reference={referencedResourceName}
                allowEmpty={true}
                fullWidth={true}
                helperText={false}
                filterToQuery={(q: string) => {
                  if (q) {
                    return {
                      name: {
                        format: 'hasura-raw-query',
                        value: {
                          _ilike: `%${q}%`,
                        },
                      },
                    };
                  }
                }}
                sort={{ field: 'name', order: 'ASC' }}
              >
                <AutocompleteInput optionText={'name'} />
              </ReferenceInput>
            );
          } else if (isO2MField(field)) {
            const referencingResourceName = getReferenceResourceName(field);
            const referencingField = getReferencingResourceField(field, resourceName, schemata);
            const mainIdentifierColumn = getMainIdentifierColumn(referencingResourceName, schemata);

            if (!referencingField) {
              return null;
            }

            const referencingResourceSchema = schemata.get(referencingResourceName) as IntrospectionObjectType;

            console.log(
              `O2M ${field.name}: ${referencingResourceName}.${referencingField?.name} (${mainIdentifierColumn})`
            );

            return (
              <ReferenceManyField
                key={i}
                label={labelFromSchema(referencingResourceSchema)}
                reference={referencingResourceName}
                target={referencingField.name}
              >
                <SingleFieldList linkType="show">
                  <ChipField source={mainIdentifierColumn} />
                </SingleFieldList>
              </ReferenceManyField>
            );
          } else {
            return null;
          }
        })}
      </SimpleForm>
    </Edit>
  ) : null;
};

export default EntityEdit;
