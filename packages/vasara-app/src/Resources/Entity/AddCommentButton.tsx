import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { CreateButton } from 'react-admin';

const styles = {
  button: {
    marginTop: '1em',
    marginBottom: '1em',
  },
};

const AddCommentButton = ({ classes, record, resource }: any) => (
  <CreateButton
    className={classes.button}
    to={`/${resource}_comment/create?entity_id=${record.id}&business_key=${record.business_key}`}
  />
);

export default withStyles(styles)(AddCommentButton);
