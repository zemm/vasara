import EntityCreate from './EntityCreate';
import EntityEdit from './EntityEdit';
import EntityList from './EntityList';
import EntityShow from './EntityShow';

const resource = {
  create: EntityCreate,
  edit: EntityEdit,
  show: EntityShow,
  list: EntityList,
};

export default resource;
