import { withStyles } from '@material-ui/core/styles';
import { Add } from '@material-ui/icons';
import React from 'react';
import { Button, useRedirect } from 'react-admin';

const styles = {
  button: {},
};

const AddRelatedEntityButton = ({
  classes,
  basePath,
  reference,
  referenced,
  resource,
  target,
  record,
  ...rest
}: any) => {
  const redirect = useRedirect();
  return (
    <Button
      label="ra.action.create"
      className={classes.button}
      onClick={() => {
        redirect(
          `/${referenced}/create?reference=${reference}&referenceInitialField=${target}&referenceInitialValue=${record.id}&redirectTo=${basePath}/${record.id}`
        );
      }}
    >
      <Add />
    </Button>
  );
};

export default withStyles(styles)(AddRelatedEntityButton);
