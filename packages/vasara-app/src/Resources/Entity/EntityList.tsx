import { IntrospectionEnumValue, IntrospectionField } from 'graphql';
import React, { cloneElement, useContext, useEffect, useState } from 'react';
import {
  Datagrid,
  DateField,
  Filter,
  List,
  Loading,
  SelectField,
  SelectInput,
  TextField,
  TextInput,
  TopToolbar,
  sanitizeListRestProps,
  useDataProvider,
  useListContext,
  useLocale,
} from 'react-admin';

import HasuraContext from '../../DataProviders/HasuraContext';
import { MAIN_IDENTIFIER_COLUMNS, PAGE_SIZE, SCALARCOMPONENTS_MAP } from '../../util/constants';
import {
  getFieldTypeName,
  isAggregateField,
  isComputedField,
  isEnumField,
  isImplicitField,
  isScalarField,
  labelFromField,
  labelFromSchema,
  toLabel,
} from '../../util/helpers';

const ListActions = (props: any) => {
  const { className, exporter, filters, maxResults, ...rest } = props;
  const { resource, displayedFilters, filterValues, showFilter } = useListContext();
  return (
    <TopToolbar className={className} {...sanitizeListRestProps(rest)}>
      {filters &&
        cloneElement(filters, {
          resource,
          showFilter,
          displayedFilters,
          filterValues,
          context: 'button',
        })}
    </TopToolbar>
  );
};

const EntityFilters = ({ filters, columns, ...rest }: any) => {
  const { enums } = useContext(HasuraContext);
  const locale = useLocale();
  return (
    <Filter {...rest}>
      {filters.map((field: any, i: number) => {
        if (isEnumField(field)) {
          const type_ = enums.get(getFieldTypeName(field.type));
          if (type_) {
            const choices = type_.enumValues.map((value: IntrospectionEnumValue) => {
              return {
                id: value.name,
                name: value.description,
              };
            });
            return <SelectInput key={i} source={field.name} choices={choices} label={labelFromField(field)} alwaysOn />;
          }
          return null;
        } else {
          return (
            <TextInput
              key={i}
              label={columns.get(field.name)?.label?.[locale] ?? labelFromField(field)}
              source={`${field.name}@_ilike`}
              alwaysOn
            />
          );
        }
      })}
    </Filter>
  );
};

const EntityList = (props: any) => {
  const { schemata, enums } = useContext(HasuraContext);
  const resourceName = props.resource;
  const dataProvider = useDataProvider();
  const [loading, setLoading] = useState(true);
  const [columns, setColumns]: any = useState([]);
  const locale = useLocale();

  useEffect(() => {
    (async () => {
      try {
        const settings = await dataProvider.getList('vasara_entity_settings', {
          filter: { id: resourceName },
          pagination: { page: 1, perPage: 1 },
          sort: { field: 'id', order: 'ASC' },
        });
        if (settings.data?.length ?? 0) {
          setColumns(settings.data[0]?.columns ?? []);
        }
        setLoading(false);
      } catch (e) {
        setLoading(false);
      }
    })();
  }, [dataProvider, resourceName]);

  if (loading) {
    return <Loading />;
  }

  const schema = schemata.get(resourceName);
  const label = schema ? labelFromSchema(schema) : toLabel(resourceName);
  const fields: Map<string, IntrospectionField> = schema
    ? new Map(schema.fields.map((field: IntrospectionField) => [field.name, field]))
    : new Map();
  const columnByPath: Map<string, any> = new Map(columns.map((column: any) => [column.path, column]));

  const identifiers: any[] = columns.length
    ? schema
      ? columns.map((column: any) => fields.get(column?.path ?? 'n/a') || column).filter((field: any) => !!field)
      : []
    : schema
    ? MAIN_IDENTIFIER_COLUMNS.map(name => fields.get(name)).filter(field => !!field)
    : [];
  if (identifiers.length === 0) {
    for (const field of schema?.fields ?? []) {
      if (isImplicitField(field) || isAggregateField(field) || isComputedField(field)) {
        continue;
      } else if (isScalarField(field)) {
        identifiers.push(field);
      }
      if (identifiers.length > 2) {
        break;
      }
    }
  }
  const filters = identifiers.filter((field: any) => !!field.name && columnByPath.get(field.name)?.filter);

  return (
    <List
      {...props}
      perPage={PAGE_SIZE}
      title={label}
      bulkActionButtons={false}
      hasCreate={false}
      actions={<ListActions />}
      filters={<EntityFilters filters={filters} columns={columnByPath} />}
      sort={{ field: 'updated_at', order: 'DESC' }}
    >
      <Datagrid rowClick="show">
        {(identifiers as any[]).map((field: any, i: number) => {
          if (field.path) {
            return (
              <TextField
                key={i}
                source={field?.path ?? 'id'}
                label={field?.label?.[locale] ?? null}
                sortable={field?.sortable ?? false}
              />
            );
          } else if (isScalarField(field)) {
            const typeName = getFieldTypeName(field.type);
            const FieldComponent = SCALARCOMPONENTS_MAP[typeName] || TextField;
            return FieldComponent === DateField ? (
              <DateField
                key={i}
                source={field.name}
                showTime={true}
                locales="fi-FI"
                label={columnByPath.get(field.name)?.label?.[locale] ?? labelFromField(field)}
              />
            ) : (
              <FieldComponent
                key={i}
                source={field.name}
                label={columnByPath.get(field.name)?.label?.[locale] ?? labelFromField(field)}
              />
            );
          } else if (isEnumField(field)) {
            const type_ = enums.get(getFieldTypeName(field.type));
            if (type_) {
              const choices = type_.enumValues.map((value: IntrospectionEnumValue) => {
                return {
                  id: value.name,
                  name: value.description,
                };
              });
              return (
                <SelectField
                  key={i}
                  source={field.name}
                  choices={choices}
                  label={columnByPath.get(field.name)?.label?.[locale] ?? labelFromField(field)}
                />
              );
            }
            return null;
          } else {
            return null;
          }
        })}
      </Datagrid>
    </List>
  );
};

export default EntityList;
