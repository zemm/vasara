import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { EditButton } from 'react-admin';

const styles = {
  button: {
    marginTop: '1em',
    marginBottom: '1em',
  },
};

const EditCommentButton = ({ classes, record, resource, ...rest }: any) => {
  return record ? <EditButton className={classes.button} to={`/${resource}/${record.id}/edit`} /> : null;
};

export default withStyles(styles)(EditCommentButton);
