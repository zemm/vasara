import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { IntrospectionEnumValue, IntrospectionField, IntrospectionObjectType } from 'graphql';
import { parse } from 'query-string';
import React, { useContext } from 'react';
import {
  Datagrid,
  DateField,
  EditButton,
  ListContextProvider,
  NumberField,
  ReferenceField,
  ReferenceManyField,
  ReferenceManyFieldController,
  RichTextField,
  SelectField,
  Show,
  SingleFieldList,
  Tab,
  TabbedShowLayout,
  TextField,
  TopToolbar,
} from 'react-admin';
import { ReactFlowProvider } from 'react-flow-renderer';

import { AccordionBpmnField } from '../../Components/Accordion';
import BackButton from '../../Components/BackButton';
import VaultTextField from '../../Components/VaultTextField';
import VaultTextFieldPlain from '../../Components/VaultTextFieldPlain';
import HasuraContext from '../../DataProviders/HasuraContext';
import { RelationsViewer, useElements } from '../../RelationsViewer';
import { SCALARCOMPONENTS_MAP } from '../../util/constants';
import {
  getFieldTypeName,
  getReferenceResourceName,
  getSourceResourceField,
  isAggregateField,
  isComputedField,
  isEnumField,
  isFkField,
  isImplicitField,
  isM2MField,
  isM2OField,
  isO2MField,
  isScalarField,
  labelFromField,
  labelFromSchema,
  orderFromField,
} from '../../util/helpers';
import AddCommentButton from './AddCommentButton';
import EditCommentButton from './EditCommentButton';

const useStyles = makeStyles({
  multiline: {
    whiteSpace: 'pre-line',
  },
  fullWidth: {
    '& > .MuiFormControl-root': {
      display: 'flex',
      width: '100%',
    },
    '& > .MuiAccordion-root': {
      width: '100%',
    },
    display: 'flex',
    marginBottom: '8px',
  },
  mr: {
    marginRight: '1em',
  },
  tabSpinner: {
    '& .MuiTab-wrapper > *:first-child': {
      width: '30px !important',
      height: '30px !important',
      position: 'absolute',
    },
  },
});

const CustomActions = ({ basePath, data }: any) => {
  const classes = useStyles();
  const user_task_id = localStorage.getItem('user_task_id');
  return (
    <TopToolbar>
      <BackButton className={classes.mr} redirectTo={user_task_id ? undefined : basePath} />
      <EditButton basePath={basePath} record={data} />
    </TopToolbar>
  );
};

const onlyWithProcesses = (WrappedComponent: any) => {
  return class extends React.Component<any, any> {
    render() {
      const { children, ...rest } = this.props;
      const withoutProcess = (children as any).slice(0, (children as any).length - 1);
      const processes = !!this.props?.record?.business_key;
      return processes ? (
        <WrappedComponent {...this.props} />
      ) : (
        <WrappedComponent children={withoutProcess} {...rest} />
      );
    }
  };
};

const CustomTabbedShowLayout = onlyWithProcesses(TabbedShowLayout);

const EntityShow = (props: any) => {
  const { schemata, enums, fields: fieldsByName, introspection } = useContext(HasuraContext);
  const resourceName = props.resource;
  const elements = useElements(introspection, resourceName, props.id);
  const schema = schemata.get(resourceName) as IntrospectionObjectType;
  const label = labelFromSchema(schema);
  const fields = fieldsByName.get(resourceName) as Map<string, IntrospectionField>;
  const sorted: IntrospectionField[] = schema.fields
    .map(field => [
      orderFromField(
        field,
        fields.has(`${field.name}_id`)
          ? orderFromField(fields.get(`${field.name}_id`) as IntrospectionField)
          : undefined
      ),
      field.name,
    ])
    .sort()
    .map(pair => fields.get(pair[1]) as IntrospectionField);

  const comments = schemata.get(resourceName + '_comment');
  const relations = sorted.some(field => {
    if (isM2MField(field)) {
      return true;
    } else if (isO2MField(field)) {
      const referencingResourceName = getReferenceResourceName(field);
      return !referencingResourceName.startsWith('vasara_') && !referencingResourceName.endsWith('_comment');
    } else {
      return false;
    }
  });
  const { user_task_id } = parse(props.location.search);
  if (user_task_id) {
    localStorage.setItem('user_task_id', user_task_id as string);
  }

  const classes = useStyles();
  const { hasShow, ...rest } = props;

  return schema ? (
    <Show {...rest} actions={<CustomActions />} title={label}>
      <CustomTabbedShowLayout>
        {/* @ts-ignore */}
        <Tab label="vasara.form.record">
          {sorted.map((field: IntrospectionField, i: number) => {
            if (isImplicitField(field) || isFkField(field) || isAggregateField(field) || isComputedField(field)) {
              return null;
            } else if (isScalarField(field)) {
              const typeName = getFieldTypeName(field.type);
              const FieldComponent = SCALARCOMPONENTS_MAP[typeName] || TextField;
              if (FieldComponent === DateField) {
                return <FieldComponent key={i} source={field.name} locales="fi-FI" label={labelFromField(field)} />;
              } else if (FieldComponent === NumberField) {
                return <NumberField key={i} source={field.name} locales={['fi']} label={labelFromField(field)} />;
              } else {
                return (
                  <FieldComponent
                    key={i}
                    source={field.name}
                    className={
                      [VaultTextField, VaultTextFieldPlain, TextField].includes(FieldComponent)
                        ? classes.multiline
                        : null
                    }
                    label={labelFromField(field)}
                  />
                );
              }
            } else if (isEnumField(field)) {
              const type_ = enums.get(getFieldTypeName(field.type));
              if (type_) {
                const choices = type_.enumValues.map((value: IntrospectionEnumValue) => {
                  return {
                    id: value.name,
                    name: value.description,
                  };
                });
                return <SelectField key={i} source={field.name} choices={choices} label={labelFromField(field)} />;
              }
              return null;
            } else if (isM2MField(field)) {
              return null;
            } else if (isM2OField(field)) {
              const referencedResourceName = getReferenceResourceName(field);
              const referenceField = getSourceResourceField(field, resourceName, schemata);
              const referencedResourceSchema = schemata.get(referencedResourceName) as IntrospectionObjectType;

              if (referencedResourceName === 'camunda_User' && fields.has(`${field.name}_id`)) {
                return (
                  <ReferenceField
                    key={i}
                    label={labelFromField(fields.get(`${field.name}_id`) as IntrospectionField)}
                    source={`${field.name}_id`}
                    reference={referencedResourceName}
                    link={false}
                  >
                    <TextField source="name" />
                  </ReferenceField>
                );
              } else if (!referenceField) {
                return null;
              }

              console.log(`M2O ${field.name}: ${schema.name}.${referenceField.name} => ${referencedResourceName}.id`);

              return (
                <ReferenceField
                  key={i}
                  label={labelFromField(referenceField, labelFromSchema(referencedResourceSchema))}
                  source={referenceField.name}
                  reference={referencedResourceName}
                  link={referencedResourceName.startsWith('vocabulary_') ? false : 'show'}
                >
                  <TextField source="name" />
                </ReferenceField>
              );
            } else if (isO2MField(field)) {
              return null;
            } else {
              return null;
            }
          })}
        </Tab>
        {relations ? (
          <Tab
            className={classes.tabSpinner}
            disabled={!elements.length}
            icon={!elements.length ? <CircularProgress /> : <></>}
            label="vasara.form.relations"
            path="relations"
          >
            <div style={{ height: '800px' }}>
              <ReactFlowProvider>
                <RelationsViewer initialElements={elements} rootType={resourceName} />
              </ReactFlowProvider>
            </div>
          </Tab>
        ) : null}
        {comments ? (
          <Tab label="vasara.form.comments" path="comments">
            <ReferenceManyField
              addLabel={false}
              reference={resourceName + '_comment'}
              target="entity_id"
              sort={{ field: 'updated_at', order: 'DESC' }}
            >
              <Datagrid>
                <TextField source="author.name" label="vasara.column.author" />
                {/*<RichTextField source="subject" label="vasara.column.subject" />*/}
                <RichTextField source="body" label="vasara.column.body" />
                <DateField source="updated_at" label="vasara.column.modified" locales="fi-FI" showTime={true} />
                <EditCommentButton />
              </Datagrid>
            </ReferenceManyField>
            <AddCommentButton />
          </Tab>
        ) : null}
        <Tab label="vasara.form.processes" path="processes">
          <ReferenceManyField
            label=""
            reference="camunda_HistoricProcessInstance"
            source="business_key"
            target="businessKey"
            className={classes.fullWidth}
          >
            <SingleFieldList linkType={false} className={classes.fullWidth}>
              <AccordionBpmnField
                source="processDefinition.diagram"
                label="processDefinition.name"
                defaultExpanded={true}
              />
            </SingleFieldList>
          </ReferenceManyField>
        </Tab>
        <Tab label="vasara.form.activities" path="activities">
          <ReferenceManyFieldController
            resource="camunda_HistoricProcessInstance"
            basePath="camunda_HistoricProcessInstance"
            reference="camunda_HistoricActivityInstance"
            source="business_key"
            target="businessKey"
          >
            {(props: any) => {
              return (
                <ListContextProvider
                  value={{
                    ...props,
                    ids: (props?.ids ?? []).filter((id: string) => props.data[id].activityName),
                  }}
                >
                  <Datagrid>
                    <TextField source="activityName" label="vasara.column.userTask" />
                    <TextField source="assignee.name" label="vasara.column.author" />
                    <TextField source="endTime" label="vasara.column.endTime" />
                    <TextField source="processDefinition.name" label="vasara.column.processDefinitionName" />
                  </Datagrid>
                </ListContextProvider>
              );
            }}
          </ReferenceManyFieldController>
        </Tab>
      </CustomTabbedShowLayout>
    </Show>
  ) : null;
};

export default EntityShow;
