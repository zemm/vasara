import React from 'react';
import { Route } from 'react-router-dom';

import { GuestForm } from './GuestForm';

export const customRoutes = [
  <Route exact path="/guest" component={GuestForm} />,
  <Route exact path="/guest/" component={GuestForm} />,
  <Route path="/guest/:needle" component={GuestForm} />,
];
