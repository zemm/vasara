import React from 'react';
import { Button } from 'react-admin';
import { useHistory } from 'react-router';

const CancelButton = (props: any) => {
  const history = useHistory();
  return (
    <Button
      {...props}
      size="small"
      variant="text"
      label="ra.action.cancel"
      onClick={() => {
        history.go(-1);
      }}
    />
  );
};

export default CancelButton;
