import { makeStyles } from '@material-ui/core/styles';
import LockIcon from '@material-ui/icons/Lock';
import React, { useState } from 'react';
import { Button, FormDataConsumer, TextInput, useMutation, useNotify, useTranslate } from 'react-admin';

import { vault_transit_decrypt_request_batch } from '../DataProviders/Actions/types';

const useStyles = makeStyles(theme => ({
  confidential: {
    height: '50px',
    overflow: 'hidden',
  },
}));

export const withSecret = (WrappedComponent: any) => ({ ...props }) => {
  const [decrypted, setDecrypted] = useState(new Date().getTime() - 1000);
  const [mutate] = useMutation();
  const translate = useTranslate();
  const notify = useNotify();
  const classes = useStyles();
  const create = (resource: any, data: any) =>
    new Promise<any>((resolve, reject) => {
      mutate(
        {
          type: 'create',
          resource: resource,
          payload: { data },
        },
        {
          onSuccess: ({ data }) => {
            resolve(data);
          },
          onFailure: error => {
            notify(translate('vasara.notification.openConfidentialFailed'), 'warning');
            reject(error);
          },
        }
      );
    });
  return new Date().getTime() - decrypted < 1000 ? (
    <WrappedComponent {...props} />
  ) : (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const value = formData?.[props.source] ?? '';
        const request: vault_transit_decrypt_request_batch = {
          batch: [{ ciphertext: value }],
        };
        return value.startsWith('vault:') ? (
          <>
            <WrappedComponent {...props} helperText={false} className={classes.confidential} disabled />
            <Button
              label="vasara.action.showConfidential"
              variant="outlined"
              onClick={async e => {
                try {
                  const result = await create('vault_transit_decrypt', request);
                  if (result.batch[0].plaintext) {
                    formData[props.source] = result.batch[0].plaintext;
                    setDecrypted(new Date().getTime());
                  } else {
                    notify('vasara.notification.openConfidentialFailed', 'warning');
                  }
                } catch (e) {}
                e.preventDefault();
              }}
            >
              <LockIcon />
            </Button>
          </>
        ) : (
          <WrappedComponent {...props} />
        );
      }}
    </FormDataConsumer>
  );
};

const VaultTextInput = withSecret(TextInput);

export default VaultTextInput;
