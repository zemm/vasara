import { Assignment, AssignmentTurnedIn } from '@material-ui/icons';
import React, { useState } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';

interface Props {
  value: string;
}

export const Clippy: React.FC<Props> = ({ value, children }) => {
  const [mouseOver, setMouseOver] = useState(false);
  const [copied, setCopied] = useState(false);
  return (
    <span
      onMouseOver={() => {
        if (!mouseOver) {
          setMouseOver(true);
        }
      }}
      onMouseLeave={() => {
        setMouseOver(false);
        setCopied(false);
      }}
      style={{ display: 'flex', alignItems: 'center' }}
    >
      {children}
      {mouseOver ? (
        <CopyToClipboard text={value} onCopy={() => setCopied(true)}>
          <button
            style={{
              padding: 0,
              margin: 0,
              display: 'inline',
              border: 'none',
              background: 'transparent',
              cursor: 'pointer',
              fontSize: '100%',
            }}
            onClick={e => {
              e.preventDefault();
            }}
          >
            {' '}
            {copied ? (
              <AssignmentTurnedIn
                style={{ marginLeft: '5px', marginRight: '5px', fontSize: '100%', color: 'green', display: 'flex' }}
              />
            ) : (
              <Assignment
                style={{ marginLeft: '5px', marginRight: '5px', fontSize: '100%', color: '#002957', display: 'flex' }}
              />
            )}
          </button>
        </CopyToClipboard>
      ) : null}
    </span>
  );
};
