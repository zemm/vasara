import React from 'react';
import { FormDataConsumer, TextInput } from 'react-admin';

import VaultTextFieldPlain from './VaultTextFieldPlain';

export const withReadOnlySecret = (WrappedComponent: any) => ({ ...props }) => {
  return (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const value = formData?.[props.source] ?? '';
        return value.startsWith('vault:') ? <VaultTextFieldPlain {...props} /> : <WrappedComponent {...props} />;
      }}
    </FormDataConsumer>
  );
};

const VaultTextInput = withReadOnlySecret(TextInput);

export default VaultTextInput;
