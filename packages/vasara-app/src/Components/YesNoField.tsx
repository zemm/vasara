import Typography from '@material-ui/core/Typography';
import get from 'lodash/get';
import * as React from 'react';
import { FC, memo } from 'react';
import { TextFieldProps, sanitizeFieldRestProps, useTranslate } from 'react-admin';

const YesNoField: FC<TextFieldProps> = memo<TextFieldProps>(
  ({ className, source, record = {}, emptyText, ...rest }) => {
    const value = get(record, source as string);
    const translate = useTranslate();

    return (
      <Typography component="span" variant="body2" className={className} {...sanitizeFieldRestProps(rest)}>
        {value === true || value === 'true' ? translate('vasara.form.yes') : translate('vasara.form.no')}
      </Typography>
    );
  }
);

export default YesNoField;
