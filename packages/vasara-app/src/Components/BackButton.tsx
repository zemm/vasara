import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import React from 'react';
import { Button, useRedirect, useRefresh } from 'react-admin';
import { useHistory } from 'react-router';

const BackButton = (props: any) => {
  const history = useHistory();
  const redirect = useRedirect();
  const refresh = useRefresh();
  const { redirectTo, go, startIcon, ...rest } = props;
  return (
    <Button
      {...rest}
      size="small"
      label="ra.action.back"
      onClick={() => {
        const user_task_id = localStorage.getItem('user_task_id');
        if (!!redirectTo) {
          if (user_task_id) {
            delete localStorage['user_task_id'];
          }
          redirect(redirectTo);
        } else if (user_task_id && !go) {
          delete localStorage['user_task_id'];
          redirect(`/UserTask/${user_task_id}/edit`);
          refresh();
        } else {
          history.go(go || -1);
        }
      }}
    >
      <ArrowBackIcon />
    </Button>
  );
};

export default BackButton;
