import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { SaveAlt } from '@material-ui/icons';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/styles';
import lodashGet from 'lodash/get';
import React from 'react';
import { ReferenceManyField } from 'react-admin';

import BpmnField from './BpmnField';
import JSONBField from './JSONBField';
import TextFileField from './TextFileField';

const styles = (theme: Theme) => ({
  download: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 'auto',
    zIndex: 1,
    '& > :nth-child(2) a': {
      color: theme.palette.secondary.main,
      textDecoration: 'none',
      display: 'inline-block',
      marginLeft: '-2em',
      paddingLeft: '2.5em',
    },
  },
});

const inAccordion = (WrappedComponent: any, download?: boolean) => {
  return class extends React.Component<any, any> {
    render() {
      const classes = this.props.classes;
      const { id, record, source } = this.props;
      const { defaultExpanded, disabled, ...rest } = this.props;
      let label = lodashGet(record, this.props.label) || this.props.label;
      let match = label.match(/%{([^}]*)}/);
      while (match) {
        label = label.replace(match[0], lodashGet(record, match[1]) || match[1]);
        match = label.match(/%{([^}]*)}/);
      }
      const prefix = `${id}-${source}`;
      return (
        <Accordion defaultExpanded={defaultExpanded}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls={`${prefix}-details`} id={`${prefix}-header`}>
            <Typography>{label}</Typography>
            {download ? (
              disabled ? null : (
                <span
                  className={classes.download}
                  onClick={e => e.stopPropagation()}
                  onKeyDown={e => e.stopPropagation()}
                >
                  <TextFileField
                    source="diagram"
                    title="name"
                    filename="resource"
                    contentType="application/xml"
                    record={record}
                    className={classes.download}
                  >
                    <SaveAlt />
                  </TextFileField>
                </span>
              )
            ) : null}
          </AccordionSummary>
          <AccordionDetails id={`${prefix}-details`}>
            {disabled ? null : <WrappedComponent {...rest} />}
          </AccordionDetails>
        </Accordion>
      );
    }
  };
};

export const AccordionJsonbField = inAccordion(JSONBField);
export const AccordionBpmnField = React.memo(inAccordion(BpmnField), (a, b): any => {
  return a?.record?.id === b?.record?.id;
});
export const AccordionBpmnFieldWithDownload = React.memo(
  withStyles(styles)(inAccordion(BpmnField, true)),
  (a, b): any => {
    return a?.record?.id === b?.record?.id;
  }
);
export const AccordionReferenceManyField = inAccordion(ReferenceManyField);
