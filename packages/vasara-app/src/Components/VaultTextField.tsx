import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import LockIcon from '@material-ui/icons/Lock';
import get from 'lodash/get';
import React, { FC, memo, useState } from 'react';
import {
  Button,
  Labeled,
  TextFieldProps,
  sanitizeFieldRestProps,
  useMutation,
  useNotify,
  useTranslate,
} from 'react-admin';

import { vault_transit_decrypt_request_batch } from '../DataProviders/Actions/types';

const useStyles = makeStyles(theme => ({
  confidential: {
    marginTop: '1rem',
  },
}));

const VaultTextField: FC<TextFieldProps> = memo<TextFieldProps>(
  ({ className, source, record = {}, emptyText, ...rest }) => {
    const [value, setValue] = useState(get(record, source as string));
    const [mutate] = useMutation();
    const translate = useTranslate();
    const notify = useNotify();
    const classes = useStyles();
    const create = (resource: any, data: any) =>
      new Promise<any>((resolve, reject) => {
        mutate(
          {
            type: 'create',
            resource: resource,
            payload: { data },
          },
          {
            onSuccess: ({ data }) => {
              resolve(data);
            },
            onFailure: error => {
              notify(translate('vasara.notification.openConfidentialFailed'), 'warning');
              reject(error);
            },
          }
        );
      });
    return (value && value.startsWith && value.startsWith('vault:')) || false ? (
      <Labeled label={rest.label}>
        <Button
          label="vasara.action.showConfidential"
          variant="outlined"
          onClick={async e => {
            const request: vault_transit_decrypt_request_batch = {
              batch: [{ ciphertext: value }],
            };
            try {
              const result = await create('vault_transit_decrypt', request);
              if (result.batch[0].plaintext) {
                setValue(result.batch[0].plaintext);
              } else {
                notify(translate('vasara.notification.openConfidentialFailed'), 'warning');
              }
            } catch (e) {}
            e.preventDefault();
          }}
        >
          <LockIcon />
        </Button>
      </Labeled>
    ) : (
      <Labeled label={rest.label}>
        <Typography component="span" variant="body2" {...sanitizeFieldRestProps(rest)} className={classes.confidential}>
          {value != null && typeof value !== 'string' ? JSON.stringify(value) : value || emptyText}
        </Typography>
      </Labeled>
    );
  }
);

export default VaultTextField;
