import React, { useEffect } from 'react';
import { Error, ErrorProps } from 'react-admin';
import { Annotation, BatchRecorder, ExplicitContext, Tracer, jsonEncoder } from 'zipkin';
import { HttpLogger } from 'zipkin-transport-http';

import { getEnv } from '../util/env';

let TRACER: any = null;

const traceError = (message: any, url: any, fileName: any, lineNo: any, columnNo: any, error: any) => {
  if (TRACER !== null) {
    TRACER.recordServiceName('Vasara');
    TRACER.recordAnnotation(new Annotation.LocalOperationStart(`${message}`));
    TRACER.recordBinary('error.kind', 'RuntimeError');
    if (url !== null) {
      TRACER.recordBinary('http.url', url);
    }
    if (message !== null) {
      TRACER.recordBinary('message', message);
    }
    if (error?.stack) {
      TRACER.recordBinary('stack', error.stack || '');
    } else if (fileName !== null && lineNo !== null && columnNo !== null) {
      TRACER.recordBinary('stack', `${fileName}:${lineNo}:${columnNo}`);
    }
    TRACER.recordAnnotation(new Annotation.LocalOperationStop());
  }
};

if (!!getEnv('REACT_APP_ZIPKIN_COLLECT_URL')) {
  const ctxImpl = new ExplicitContext();
  TRACER = new Tracer({
    ctxImpl,
    recorder: new BatchRecorder({
      logger: new HttpLogger({
        endpoint: getEnv('REACT_APP_ZIPKIN_COLLECT_URL') as string,
        jsonEncoder: jsonEncoder.JSON_V2,
      }),
    }),
    localServiceName: 'Vasara',
  });
  window.addEventListener('error', function (err) {
    const id = TRACER.id;
    try {
      TRACER.setId(TRACER.createChildId());
      traceError(err.message, window.location.href, err.filename, err.lineno, err.colno, err.error);
    } finally {
      TRACER.setId(id);
    }
  });
}

const TracedError = (props: ErrorProps): JSX.Element => {
  const { error } = props;
  useEffect(() => {
    if (TRACER !== null) {
      const id = TRACER.id;
      try {
        TRACER.setId(TRACER.createChildId());
        traceError(error.message, window.location.href, null, null, null, error);
      } finally {
        TRACER.setId(id);
      }
    }
  }, [error]);
  return <Error {...props} />;
};

export default TracedError;
