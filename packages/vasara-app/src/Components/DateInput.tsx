import DateFnsUtils from '@date-io/date-fns';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { DatePicker, LocalizationProvider } from '@material-ui/pickers';
import fi from 'date-fns/locale/fi';
import React, { FunctionComponent, useCallback } from 'react';
import { FieldTitle, InputProps, useTranslate } from 'react-admin';
import { InputHelperText, sanitizeInputRestProps, useInput } from 'react-admin';

/**
 * Convert Date object to String
 *
 * @param {Date} value value to convert
 * @returns {String} A standardized date (yyyy-MM-dd), to be passed to an <input type="date" />
 */
const convertDateToString = (value: Date) => {
  if (!(value instanceof Date) || isNaN(value.getDate())) return;
  const pad = '00';
  const yyyy = value.getFullYear().toString();
  const MM = (value.getMonth() + 1).toString();
  const dd = value.getDate().toString();
  return `${yyyy}-${(pad + MM).slice(-2)}-${(pad + dd).slice(-2)}`;
};

const defaultInputLabelProps = { shrink: true };

const dateStringValidator = (value: string) => {
  if (value === null || value === undefined || value === '') {
    return;
  }
  value = value.split('T')[0];
  return isNaN(new DateFnsUtils({ locale: fi }).parse(value, 'yyyy-MM-dd').getTime()) &&
    isNaN(new DateFnsUtils({ locale: fi }).parse(value, 'dd.MM.yyyy').getTime())
    ? 'vasara.validation.invalidDate'
    : null;
};

const getStringFromDate = (value: string | Date) => {
  // null, undefined and empty string values should not go through dateFormatter
  // otherwise, it returns undefined and will make the input an uncontrolled one.
  if (value == null || value === '') {
    return '';
  }

  if (value instanceof Date) {
    return convertDateToString(value);
  }

  const dateFns = new DateFnsUtils({ locale: fi });
  const isoDate = dateFns.parse(value, 'yyyy-MM-dd');
  if (!isNaN(isoDate.getTime())) {
    return convertDateToString(isoDate);
  }

  const fiDate = dateFns.parse(value, 'dd.MM.yyyy');
  if (!isNaN(fiDate.getTime())) {
    return convertDateToString(fiDate);
  }

  return value;
};

const DateInput: FunctionComponent<InputProps<TextFieldProps> & Omit<TextFieldProps, 'helperText' | 'label'>> = ({
  format = getStringFromDate,
  label,
  options,
  source,
  resource,
  helperText,
  margin = 'dense',
  onBlur,
  onChange,
  onFocus,
  parse,
  validate,
  variant = 'filled',
  fullWidth,
  ...rest
}) => {
  const {
    id,
    input,
    isRequired,
    meta: { error, touched },
  } = useInput({
    format,
    onBlur,
    onChange,
    onFocus,
    parse,
    resource,
    source,
    validate: validate
      ? Array.isArray(validate)
        ? validate.concat([dateStringValidator])
        : [validate, dateStringValidator]
      : [dateStringValidator],
    fullWidth,
    ...rest,
  });
  const translate = useTranslate();
  const handleChange = useCallback(
    value => {
      input.onChange(getStringFromDate(value));
    },
    [input]
  );
  return (
    <LocalizationProvider dateAdapter={DateFnsUtils} locale={fi}>
      <DatePicker
        clearText={translate('ra.action.clear_input_value')}
        cancelText={translate('ra.action.cancel')}
        value={input.value ? new Date(input.value) : null}
        onChange={(date: any) => handleChange(date)}
        inputFormat="dd.MM.yyyy"
        disableMaskedInput={true}
        renderInput={(props: any) => (
          <TextField
            {...options}
            {...sanitizeInputRestProps(rest)}
            {...input}
            {...props}
            onChange={e => handleChange(e.target.value)}
            id={id}
            variant={variant}
            margin={margin}
            InputLabelProps={defaultInputLabelProps}
            error={!!(touched && error)}
            label={<FieldTitle label={label} source={source} resource={resource} isRequired={isRequired} />}
            helperText={<InputHelperText touched={touched || false} error={error} helperText={helperText} />}
            fullWidth={fullWidth}
          />
        )}
      />
    </LocalizationProvider>
  );
};

export default DateInput;
