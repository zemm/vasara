import get from 'lodash/get';
import PropTypes from 'prop-types';
import * as React from 'react';
import {
  ArrayInput,
  AutocompleteArrayInput,
  FormDataConsumer,
  NumberInput,
  SimpleFormIterator,
  TextInput,
  required,
} from 'react-admin';

import DateInput from './DateInput';
import VaultTextReadOnlyInputPlain from './VaultTextReadOnlyInputPlain';
import YesNoInput from './YesNoInput';

const getInputComponent = (value: any) => {
  if (value === null) {
    return TextInput;
  }
  switch (typeof value) {
    case 'object':
      return AutocompleteArrayInput;
    case 'boolean':
      return YesNoInput;
    case 'number':
      return NumberInput;
    case 'string':
      if (value === 'true' || value === 'false') {
        // Fix issues where boolean was saved as a string
        return YesNoInput;
      } else if (value.match(/^\d{4}-\d{2}-\d{2}T?.*/)) {
        return DateInput;
      } else if (value.startsWith('vault:')) {
        return VaultTextReadOnlyInputPlain;
      } else {
        return TextInput;
      }
    default:
      return TextInput;
  }
};

const JSONBInput = ({ source, record = {}, label, className, ...rest }: any) => {
  const content = get(record, source) || {};
  const keys = content['@order'] || Object.keys(content).sort((a: string, b: string) => (a < b ? -1 : a > b ? 1 : 0));

  const fullWidth = rest?.fullWidth ?? true;
  return Array.isArray(content) ? (
    <ArrayInput label={label} source={source} fullWidth={true}>
      <SimpleFormIterator disableAdd={true}>
        <JSONBInput source={''} record={record} {...rest} fullWidth={false} />
      </SimpleFormIterator>
    </ArrayInput>
  ) : (
    <>
      {keys.map((key: string) => {
        const InputComponent = getInputComponent(get(record, source)[key]);
        return InputComponent === AutocompleteArrayInput ? (
          <FormDataConsumer key={key} subscription={{ values: true }}>
            {({ formData, ...rest }: any) => {
              const value = get(formData, source)[key] || [];
              const choices = value.map((choice: string) => {
                return {
                  id: choice,
                  name: choice,
                };
              });
              return (
                <InputComponent
                  className={className}
                  source={`${source}.${key}`}
                  label={key}
                  choices={choices}
                  matchSuggestion={(filterValue: any, suggestion: any) => true}
                  setFilter={(searchText: string) => {
                    if (searchText) {
                      while (choices.length > value.length) {
                        choices.pop();
                      }
                      choices.push({
                        id: searchText,
                        name: searchText,
                      });
                    }
                  }}
                  fullWidth={fullWidth}
                  helperText={false}
                />
              );
            }}
          </FormDataConsumer>
        ) : InputComponent === TextInput ? (
          <InputComponent
            key={key}
            source={`${source}.${key}`}
            label={key}
            fullWidth={fullWidth}
            multiline={true}
            helperText={false}
          />
        ) : InputComponent === NumberInput ? (
          <InputComponent
            key={key}
            source={`${source}.${key}`}
            label={key}
            fullWidth={fullWidth}
            min={0}
            validate={[required()]}
            helperText={false}
          />
        ) : (
          <InputComponent key={key} source={`${source}.${key}`} label={key} fullWidth={fullWidth} helperText={false} />
        );
      })}
    </>
  );
};

JSONBInput.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
};

export default JSONBInput;
