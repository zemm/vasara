import React from 'react';
import { FormDataConsumer, TextInput } from 'react-admin';

import VaultTextField from './VaultTextField';

export const withReadOnlySecret = (WrappedComponent: any) => ({ ...props }) => {
  return (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const value = formData?.[props.source] ?? '';
        return value.startsWith('vault:') ? <VaultTextField {...props} /> : <WrappedComponent {...props} />;
      }}
    </FormDataConsumer>
  );
};

const VaultTextInput = withReadOnlySecret(TextInput);

export default VaultTextInput;
