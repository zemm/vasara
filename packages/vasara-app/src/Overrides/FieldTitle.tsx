import { makeStyles } from '@material-ui/core/styles';
import LensIcon from '@material-ui/icons/Lens';
import get from 'lodash/get';
import React, { memo } from 'react';
import { FunctionComponent, ReactElement } from 'react';
import { getFieldLabelTranslationArgs, useTranslate } from 'react-admin';

import { Clippy } from '../Components/Clippy';

interface Props {
  isRequired?: boolean;
  resource?: string;
  source?: string;
  record?: any;
  label?: string | ReactElement;
}

const useStyles = makeStyles(theme => ({
  required: {
    color: '#cc0000',
    fontSize: '1rem',
    marginLeft: theme.spacing(1),
  },
}));

export const FieldTitle: FunctionComponent<Props> = ({ record, resource, source, label, isRequired }) => {
  const classes = useStyles();
  const translate = useTranslate();
  const value = get(record, source as string);
  if (label && typeof label !== 'string') {
    return label;
  }
  return value !== undefined && value !== null ? (
    <Clippy value={value}>
      {translate(
        ...getFieldLabelTranslationArgs({
          label: label as string,
          resource: resource as string,
          source: source as string,
        })
      )}
      {isRequired && <LensIcon className={classes.required} titleAccess={translate('ra.validation.required')} />}
    </Clippy>
  ) : (
    <span>
      {translate(
        ...getFieldLabelTranslationArgs({
          label: label as string,
          resource: resource as string,
          source: source as string,
        })
      )}
      {isRequired && <LensIcon className={classes.required} titleAccess={translate('ra.validation.required')} />}
    </span>
  );
};

// wat? TypeScript looses the displayName if we don't set it explicitly
FieldTitle.displayName = 'FieldTitle';

export default memo(FieldTitle);
