import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { useChoices } from 'ra-core';
import * as React from 'react';
import { FormDataConsumer } from 'react-admin';
import { useField } from 'react-final-form';

const RadioButtonGroupInputItem = ({ choice, optionText, optionValue, source, translateChoice, onChange }: any) => {
  const { getChoiceText, getChoiceValue } = useChoices({
    optionText,
    optionValue,
    translateChoice,
  });
  const label = getChoiceText(choice);
  const value_ = getChoiceValue(choice);
  const value = value_ === 'true' ? true : value_ === 'false' ? false : value_;
  const {
    input: { type, ...inputProps },
  } = useField(source, {
    type: 'radio',
    value,
  });

  const nodeId = `${source}_${value}`;

  return (
    <FormDataConsumer subscription={{ values: true }}>
      {({ formData, ...rest }: any) => {
        const value_ = formData?.[source] ?? '';
        return (
          <FormControlLabel
            label={label}
            htmlFor={nodeId}
            control={
              value_ === value ? (
                <Radio
                  id={nodeId}
                  color="primary"
                  {...inputProps}
                  onKeyDown={e => {
                    if (e.keyCode === 13) {
                      onChange(null);
                      e.preventDefault();
                    }
                  }}
                  onClick={e => {
                    onChange(null);
                    e.preventDefault();
                  }}
                />
              ) : (
                <Radio
                  id={nodeId}
                  color="primary"
                  {...inputProps}
                  onChange={(_, isActive) => isActive && onChange(value)}
                />
              )
            }
          />
        );
      }}
    </FormDataConsumer>
  );
};

export default RadioButtonGroupInputItem;
