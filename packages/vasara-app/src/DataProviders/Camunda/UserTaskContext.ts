import { UserTask } from 'bpmn-moddle';
import { Context, createContext } from 'react';

const UserTaskContext: Context<UserTask> = createContext({} as UserTask);

export default UserTaskContext;
