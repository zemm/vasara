import gql from 'graphql-tag';
import { CreateParams, GetListParams, GetManyReferenceParams, GetOneParams, HttpError } from 'ra-core';

import { RaFetchType } from '../types';
import { CamundaDataProvider } from './types';

const RE_UUID = /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/;

const ProcessDefinitionDataProvider: CamundaDataProvider = (introspectionResults, raFetchType, resource, params) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation MyMutation($name: String!, $resources: [camunda_DeploymentResource!]!, $migrate: Boolean!) {
            insert_camunda_ProcessDefinition(name: $name, resources: $resources, migrate: $migrate) {
              id
              key
              name
              description
              diagram
              resource
              suspended
              version
              versionTag
              startableInTasklist
            }
          }
        `,
        variables: createParams.data,
        parseResponse: (response: any) => {
          return {
            data: response.data.insert_camunda_ProcessDefinition,
          };
        },
      };
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
      const getListParams = params as GetListParams;
      const getListKey = getListParams.filter.key;
      return {
        query: gql`
          query MyQuery(
            $key: String
            $latest: Boolean
            $startableInTasklist: Boolean
            $firstResult: Int
            $maxResults: Int
          ) {
            camunda_ProcessDefinitions(
              key: $key
              latest: $latest
              startableInTasklist: $startableInTasklist
              firstResult: $firstResult
              maxResults: $maxResults
            ) {
              data {
                id
                key
                name
                description
                diagram
                resource
                suspended
                version
                versionTag
                startableInTasklist
              }
              total
            }
          }
        `,
        variables: {
          key: getListKey,
          latest: getListParams.filter.latest,
          startableInTasklist: getListParams.filter.startableInTasklist,
          firstResult: getListKey ? null : (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
          maxResults: getListKey ? null : getListParams.pagination.perPage,
        },
        parseResponse: (response: any) => {
          return getListKey
            ? {
                data: response.data.camunda_ProcessDefinitions.data.sort((a: any, b: any) => b.version - a.version),
                total: response.data.camunda_ProcessDefinitions.data.total,
              }
            : response.data.camunda_ProcessDefinitions;
        },
      };
    case RaFetchType.GET_MANY_REFERENCE:
      const getManyReferenceParams = params as GetManyReferenceParams;
      const getManyKey = (getManyReferenceParams.id as string).split(':')[0];
      return {
        query: gql`
          query MyQuery($key: String, $firstResult: Int, $maxResults: Int) {
            camunda_ProcessDefinitions(key: $key, firstResult: $firstResult, maxResults: $maxResults) {
              data {
                id
                key
                name
                description
                diagram
                resource
                suspended
                version
                versionTag
                startableInTasklist
              }
              total
            }
          }
        `,
        variables: {
          key: getManyKey,
          firstResult: getManyKey
            ? null
            : (getManyReferenceParams.pagination.page - 1) * getManyReferenceParams.pagination.perPage,
          maxResults: getManyKey ? null : getManyReferenceParams.pagination.perPage,
        },
        parseResponse: (response: any) => {
          return getManyReferenceParams.id
            ? {
                data: response.data.camunda_ProcessDefinitions.data
                  .sort((a: any, b: any) => b.version - a.version)
                  .slice(
                    (getManyReferenceParams.pagination.page - 1) * getManyReferenceParams.pagination.perPage,
                    getManyReferenceParams.pagination.page * getManyReferenceParams.pagination.perPage
                  ),
                total: response.data.camunda_ProcessDefinitions.total,
              }
            : response.data.camunda_ProcessDefinitions.data;
        },
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return (getOneParams.id as string).match(/:/) || (getOneParams.id as string).match(RE_UUID)
        ? {
            query: gql`
              query MyQuery($id: String!) {
                camunda_ProcessDefinition(id: $id) {
                  id
                  key
                  name
                  description
                  diagram
                  resource
                  suspended
                  version
                  versionTag
                  startableInTasklist
                }
              }
            `,
            variables: {
              id: `${getOneParams.id}`,
            },
            parseResponse: (response: any) => {
              if (response.data.camunda_ProcessDefinition !== null) {
                return {
                  data: response.data.camunda_ProcessDefinition,
                };
              }
              return Promise.reject(new HttpError('404 Not found', 404));
            },
          }
        : {
            query: gql`
              query MyQuery($key: String!) {
                camunda_ProcessDefinitions(key: $key, latest: true) {
                  data {
                    id
                    key
                    name
                    description
                    diagram
                    resource
                    suspended
                    version
                    versionTag
                    startableInTasklist
                  }
                }
              }
            `,
            variables: {
              key: `${getOneParams.id}`,
            },
            parseResponse: (response: any) => {
              if (response.data.camunda_ProcessDefinitions.data.length) {
                return {
                  data: {
                    ...(response.data.camunda_ProcessDefinitions.data?.[0] || {}),
                    id: getOneParams.id,
                  },
                };
              }
              return Promise.reject(new HttpError('404 Not found', 404));
            },
          };

    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};

export default ProcessDefinitionDataProvider;
