import gql from 'graphql-tag';
import { CreateParams, GetListParams, GetOneParams, UpdateParams } from 'ra-core';

import { RaFetchType } from '../types';
import { CamundaDataProvider } from './types';

// mutation MyMutation {
//   update_camunda_ProcessInstance_correlate(messageName: "cancel", processInstanceId: "680c2105-b4f3-11eb-8af1-0242ac1f0009") {
//     businessKey
//   }
// }

interface ProcessInstanceUpdateParams extends UpdateParams {
  action: 'correlate' | 'modify';
}

const ProcessInstanceDataProvider: CamundaDataProvider = (introspectionResults, raFetchType, resource, params) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation MyMutation($processDefinitionKey: String!) {
            insert_camunda_ProcessInstance(processDefinitionKey: $processDefinitionKey) {
              id
            }
          }
        `,
        variables: {
          processDefinitionKey: createParams.data.processDefinitionKey,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.insert_camunda_ProcessInstance ? response.data.insert_camunda_ProcessInstance : {},
          };
        },
      };
    case RaFetchType.GET_LIST:
    case RaFetchType.GET_MANY:
    case RaFetchType.GET_MANY_REFERENCE:
      const getListParams = params as GetListParams;
      return {
        query: gql`
          query MyQuery($firstResult: Int, $maxResults: Int) {
            camunda_ProcessInstances(firstResult: $firstResult, maxResults: $maxResults) {
              data {
                id
                businessKey
                ended
                suspended
                variables {
                  key
                  value
                  valueType
                }
                historicActivityInstances {
                  activityId
                  startTime
                  endTime
                  calledProcessInstanceId
                }
                incidents {
                  activityId
                  incidentType
                  incidentMessage
                  incidentTimestamp
                }
              }
              total
            }
          }
        `,
        variables: {
          ...getListParams.filter,
          firstResult: (getListParams.pagination.page - 1) * getListParams.pagination.perPage,
          maxResults: getListParams.pagination.perPage,
        },
        parseResponse: (response: any) => response.data.camunda_ProcessInstances,
      };
    case RaFetchType.GET_ONE:
      const getOneParams = params as GetOneParams;
      return {
        query: gql`
          query MyQuery($id: String!) {
            camunda_ProcessInstance(id: $id) {
              id
              businessKey
              ended
              suspended
              variables {
                key
                value
                valueType
              }
              historicActivityInstances {
                activityId
                startTime
                endTime
                calledProcessInstanceId
              }
              incidents {
                activityId
                incidentType
                incidentMessage
                incidentTimestamp
              }
            }
          }
        `,
        variables: {
          id: `${getOneParams.id}`,
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.camunda_ProcessInstance,
          };
        },
      };
    case RaFetchType.UPDATE:
      const updateParams = params as ProcessInstanceUpdateParams;
      return {
        query: gql`
          mutation MyMutation($id: String!, $message: String!, $variables: [camunda_KeyValuePairInput!]) {
            camunda_ProcessInstance: update_camunda_ProcessInstance_correlate(
              messageName: $message
              processInstanceId: $id
              processVariables: $variables
            ) {
              id
              businessKey
              ended
              suspended
              variables {
                key
                value
                valueType
              }
              historicActivityInstances {
                activityId
                startTime
                endTime
                calledProcessInstanceId
              }
              incidents {
                activityId
                incidentType
                incidentMessage
                incidentTimestamp
              }
            }
          }
        `,
        variables: {
          id: `${updateParams.id}`,
          message: `${updateParams.data.message}`,
          variables: updateParams.data.variables || {},
        },
        parseResponse: (response: any) => {
          return {
            data: response.data.camunda_ProcessInstance,
          };
        },
      };
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};

export default ProcessInstanceDataProvider;
