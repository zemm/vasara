import {
  CreateParams,
  DeleteManyParams,
  DeleteParams,
  GetListParams,
  GetManyParams,
  GetManyReferenceParams,
  UpdateManyParams,
  UpdateParams,
} from 'ra-core';
import buildDataProvider from 'ra-data-graphql';

import { CamundaHasuraClient } from '../HasuraClient';
import { GraphQLDataProvider, RaFetchType } from '../types';
import CommentDataProvider from './CommentDataProvider';
import DecisionDefinitionDataProvider from './DecisionDefinitionDataProvider';
import GroupDataProvider from './GroupDataProvider';
import HistoricActivityInstanceDataProvider from './HistoricActivityInstanceDataProvider';
import HistoricProcessInstanceDataProvider from './HistoricProcessInstanceDataProvider';
import ProcessDefinitionDataProvider from './ProcessDefinitionDataProvider';
import ProcessDefinitionUserTaskDataProvider from './ProcessDefinitionUserTaskDataProvider';
import ProcessInstanceDataProvider from './ProcessInstanceDataProvider';
import ProcessInstanceUserTaskDataProvider from './ProcessInstanceUserTaskDataProvider';
import UserDataProvider from './UserDataProvider';
import UserTaskDataProvider from './UserTaskDataProvider';

const buildQuery = (introspectionResults: any) => (
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) => {
  switch (resource) {
    case 'ProcessDefinition':
      return ProcessDefinitionDataProvider(introspectionResults, raFetchType, resource, params);
    case 'DecisionDefinition':
      return DecisionDefinitionDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_ProcessDefinition_UserTask':
      return ProcessDefinitionUserTaskDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_ProcessInstance_UserTask':
      return ProcessInstanceUserTaskDataProvider(introspectionResults, raFetchType, resource, params);
    case 'ProcessInstance':
      return ProcessInstanceDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_HistoricProcessInstance':
      return HistoricProcessInstanceDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_HistoricActivityInstance':
      return HistoricActivityInstanceDataProvider(introspectionResults, raFetchType, resource, params);
    case 'UserTask':
      return UserTaskDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_UserTask_Comment':
      return CommentDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_User':
      return UserDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_Group':
      return GroupDataProvider(introspectionResults, raFetchType, resource, params);
    default:
      throw new Error(`Unknown resource ${resource}.`);
  }
};

const defaultOptions = {
  buildQuery,
};

const provider = async () => {
  const dataProvider = await buildDataProvider({ ...defaultOptions, client: CamundaHasuraClient });
  const dataProviderImpl: GraphQLDataProvider = (raFetchType, resource, params) => {
    return dataProvider(raFetchType, resource, params);
  };
  return dataProviderImpl;
};

export default provider;
