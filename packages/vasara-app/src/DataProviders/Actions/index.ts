import { CreateParams } from 'ra-core';
import buildDataProvider from 'ra-data-graphql';

import { ActionsHasuraClient } from '../HasuraClient';
import { GraphQLDataProvider, RaFetchType } from '../types';
import GuestProcessDefinitionDataProvider from './GuestProcessDefinitionDataProvider';
import GuestProcessInstanceDataProvider from './GuestProcessInstanceDataProvider';
import VaultTransitDecryptDataProvider from './VaultTransitDecryptDataProvider';
import VaultTransitEncryptDataProvider from './VaultTransitEncryptDataProvider';

const buildQuery = (introspectionResults: any) => (
  raFetchType: RaFetchType,
  resource: string,
  params: CreateParams
) => {
  switch (resource) {
    case 'vault_transit_encrypt':
      return VaultTransitEncryptDataProvider(introspectionResults, raFetchType, resource, params);
    case 'vault_transit_decrypt':
      return VaultTransitDecryptDataProvider(introspectionResults, raFetchType, resource, params);
    case 'camunda_guest_ProcessDefinition':
      return GuestProcessDefinitionDataProvider(introspectionResults, raFetchType, resource, params);
    case 'insert_camunda_guest_ProcessInstance':
      return GuestProcessInstanceDataProvider(introspectionResults, raFetchType, resource, params);
    default:
      throw new Error(`Unknown resource ${resource}.`);
  }
};

const defaultOptions = {
  buildQuery,
};

const provider = async () => {
  const dataProvider = await buildDataProvider({ ...defaultOptions, client: ActionsHasuraClient });
  const dataProviderImpl: GraphQLDataProvider = (raFetchType, resource, params) => {
    return dataProvider(raFetchType, resource, params);
  };
  return dataProviderImpl;
};

export default provider;
