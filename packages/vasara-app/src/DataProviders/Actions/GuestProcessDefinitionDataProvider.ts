import gql from 'graphql-tag';
import { GetListResult } from 'ra-core';

import { RaFetchType } from '../types';
import { ActionsDataProvider } from './types';

const CamundaGuestProcessDefinitionDataProvider: ActionsDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.GET_LIST:
      return {
        query: gql`
          query {
            camunda_guest_ProcessDefinition {
              id
              key
              name
              startableInTasklist
            }
          }
        `,
        variables: {},
        parseResponse: (response: any): GetListResult => {
          return {
            data: response.data.camunda_guest_ProcessDefinition,
            total: response.data.camunda_guest_ProcessDefinition.length,
          };
        },
      } as any;
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};

export default CamundaGuestProcessDefinitionDataProvider;
