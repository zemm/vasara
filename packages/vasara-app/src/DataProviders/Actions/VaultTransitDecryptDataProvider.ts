import gql from 'graphql-tag';
import { CreateParams, CreateResult } from 'ra-core';
import { v4 as uuid } from 'uuid';

import { RaFetchType } from '../types';
import { ActionsDataProvider } from './types';

const VaultTransitDecryptDataProvider: ActionsDataProvider = (introspectionResults, raFetchType, resource, params) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          query($batch: [vault_transit_decrypt_request!]!) {
            vault_transit_decrypt(batch: $batch) {
              plaintext
            }
          }
        `,
        variables: {
          batch: createParams.data.batch,
        },
        parseResponse: (response: any): CreateResult => {
          return {
            data: {
              id: uuid(),
              batch: response.data.vault_transit_decrypt,
            },
            validUntil: new Date(new Date().getTime() + 60000), // a minute
          };
        },
      } as any;
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: {},
      };
  }
};

export default VaultTransitDecryptDataProvider;
