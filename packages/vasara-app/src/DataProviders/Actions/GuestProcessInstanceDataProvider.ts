import gql from 'graphql-tag';
import { CreateParams, CreateResult } from 'ra-core';

import { RaFetchType } from '../types';
import { ActionsDataProvider } from './types';

const CamundaGuestProcessInstanceDataProvider: ActionsDataProvider = (
  introspectionResults,
  raFetchType,
  resource,
  params
) => {
  switch (raFetchType) {
    case RaFetchType.CREATE:
      const createParams = params as CreateParams;
      return {
        query: gql`
          mutation MyMutation(
            $processDefinitionKey: String!
            $firstName: String!
            $lastName: String!
            $email: String!
          ) {
            insert_camunda_guest_ProcessInstance(
              processDefinitionKey: $processDefinitionKey
              firstName: $firstName
              lastName: $lastName
              email: $email
            ) {
              id
            }
          }
        `,
        variables: {
          processDefinitionKey: createParams.data.processDefinitionKey,
          firstName: createParams.data.firstName,
          lastName: createParams.data.lastName,
          email: createParams.data.email,
        },
        parseResponse: (response: any): CreateResult => {
          return {
            data: response.data.insert_camunda_guest_ProcessInstance
              ? response.data.insert_camunda_guest_ProcessInstance
              : {},
          };
        },
      } as any;
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};

export default CamundaGuestProcessInstanceDataProvider;
