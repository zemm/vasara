import { IntrospectionSchema } from 'graphql';
import { CreateParams, CreateResult } from 'ra-core';

import { RaFetchType } from '../types';

export type ActionsDataProvider = (
  introspectionResults: IntrospectionSchema,
  raFetchType: RaFetchType,
  resource: string,
  params: CreateParams
) => CreateResult;

export type vault_transit_encrypt_request = {
  businessKey: string;
  confidential: boolean;
  pii: boolean;
  plaintext: string;
  sources: string[];
};

export type vault_transit_encrypt_request_batch = {
  batch: vault_transit_encrypt_request[];
};

export type vault_transit_encrypt_response = {
  ciphertext: string;
};

export type vault_transit_decrypt_request = {
  ciphertext: string;
};

export type vault_transit_decrypt_request_batch = {
  batch: vault_transit_decrypt_request[];
};

export type vault_transit_decrypt_response = {
  plaintext?: string;
};
