import { ApolloQueryResult } from 'apollo-client';
import { IntrospectionEnumType, IntrospectionField, IntrospectionObjectType } from 'graphql';
import { IntrospectionQuery, IntrospectionSchema, IntrospectionType } from 'graphql/utilities/getIntrospectionQuery';
import {
  CreateParams,
  CreateResult,
  DeleteManyParams,
  DeleteManyResult,
  DeleteParams,
  DeleteResult,
  GetListParams,
  GetListResult,
  GetManyParams,
  GetManyReferenceParams,
  GetManyReferenceResult,
  GetManyResult,
  GetOneParams,
  GetOneResult,
  UpdateManyParams,
  UpdateManyResult,
  UpdateParams,
  UpdateResult,
} from 'ra-core';

export enum RaFetchType {
  GET_LIST = 'GET_LIST',
  GET_ONE = 'GET_ONE',
  GET_MANY = 'GET_MANY',
  GET_MANY_REFERENCE = 'GET_MANY_REFERENCE',
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  UPDATE_MANY = 'UPDATE_MANY',
  DELETE = 'DELETE',
  DELETE_MANY = 'DELETE_MANY',
}

export type GraphQLDataProvider = (
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetOneParams
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) =>
  | GetOneResult
  | GetListResult
  | GetManyResult
  | GetManyReferenceResult
  | UpdateResult
  | UpdateManyResult
  | CreateResult
  | DeleteResult
  | DeleteManyResult;

export type HasuraTypeKind =
  | 'SCALAR'
  | 'OBJECT'
  | 'INTERFACE'
  | 'UNION'
  | 'ENUM'
  | 'INPUT_OBJECT'
  | 'LIST'
  | 'NON_NULL';

export interface HasuraSchema extends IntrospectionSchema {
  readonly resources: IntrospectionType[];
  readonly schema: IntrospectionSchema;
}

export interface HasuraContext {
  readonly introspectionQueryResponse: ApolloQueryResult<IntrospectionQuery>;
  readonly introspection: HasuraSchema;
  readonly schemata: Map<string, IntrospectionObjectType>;
  readonly fields: Map<string, Map<string, IntrospectionField>>;
  readonly vocabularies: Map<string, IntrospectionObjectType>;
  readonly enums: Map<string, IntrospectionEnumType>;
}
