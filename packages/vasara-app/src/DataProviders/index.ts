import { IAppState } from '../types';
import { RaFetchType } from './types';

export const DataProvider = (type: RaFetchType, resource: string, params: any, state: IAppState) => {
  const { camundaDataProvider, hasuraDataProvider, actionsDataProvider, customDataProvider } = state;
  try {
    switch (resource) {
      case 'UserTask':
      case 'ProcessInstance':
      case 'ProcessDefinition':
      case 'DecisionDefinition':
      case 'camunda_HistoricActivityInstance':
      case 'camunda_HistoricProcessInstance':
      case 'camunda_ProcessDefinition_UserTask':
      case 'camunda_ProcessInstance_UserTask':
      case 'camunda_UserTask_Comment':
      case 'camunda_User':
      case 'camunda_Group':
        return camundaDataProvider
          ? (camundaDataProvider(type, resource, params) as Promise<any>)
          : new Promise(resolve => resolve({}));
      case 'vault_transit_encrypt':
      case 'vault_transit_decrypt':
      case 'camunda_guest_ProcessDefinition':
      case 'insert_camunda_guest_ProcessInstance':
        return actionsDataProvider
          ? (actionsDataProvider(type, resource, params) as Promise<any>)
          : new Promise(resolve => resolve({}));
      case 'AvailableResources':
        return customDataProvider
          ? (customDataProvider(type, resource, params) as Promise<any>)
          : new Promise(resolve => resolve({}));
      default:
        return hasuraDataProvider
          ? (hasuraDataProvider(type, resource, params) as Promise<any>)
          : new Promise(resolve => resolve({}));
    }
  } catch (e) {
    console.log('FETCH FAILED');
    console.log(e);
    throw e;
  }
};
