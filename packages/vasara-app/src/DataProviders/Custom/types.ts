import { IntrospectionSchema } from 'graphql';
import { GetListParams, GetListResult } from 'ra-core';

import { RaFetchType } from '../types';

export type CustomDataProvider = (
  introspectionResults: IntrospectionSchema,
  raFetchType: RaFetchType,
  resource: string,
  params: GetListParams
) => GetListResult;
