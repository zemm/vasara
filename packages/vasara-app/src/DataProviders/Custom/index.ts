import { GetListParams } from 'ra-core';
import buildDataProvider from 'ra-data-graphql';

import client from '../HasuraClient';
import { GraphQLDataProvider, RaFetchType } from '../types';
import AvailableResourcesDataProvider from './AvailableResourcesDataProvider';

const buildQuery = (introspectionResults: any) => (
  raFetchType: RaFetchType,
  resource: string,
  params: GetListParams
) => {
  switch (resource) {
    case 'AvailableResources':
      return AvailableResourcesDataProvider(introspectionResults, raFetchType, resource, params);
    default:
      throw new Error(`Unknown resource ${resource}.`);
  }
};

const defaultOptions = {
  buildQuery,
};

const provider = async () => {
  const dataProvider = await buildDataProvider({ ...defaultOptions, client });
  const dataProviderImpl: GraphQLDataProvider = (raFetchType, resource, params) => {
    return dataProvider(raFetchType, resource, params);
  };
  return dataProviderImpl;
};

export default provider;
