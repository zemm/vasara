import gql from 'graphql-tag';
import { GetListParams, GetListResult } from 'ra-core';

import { RaFetchType } from '../types';
import { CustomDataProvider } from './types';

const AvailableResourcesDataProvider: CustomDataProvider = (introspectionResults, raFetchType, resource, params) => {
  switch (raFetchType) {
    case RaFetchType.GET_LIST:
      const getListParams = params as GetListParams;
      return {
        query: gql(
          `
          query {` +
            (getListParams.filter?.resources || []).map(
              (resource: string) => `
            ${resource}(limit: 1) {
              id
            }
          `
            ) +
            `
          }
        `
        ),
        variables: {},
        parseResponse: (response: any): GetListResult => {
          const resources = Object.keys(response.data)
            .map((key: string) => [key, response.data[key].length])
            .filter(item => item[1]);
          return {
            data: resources.map(item => {
              return {
                id: item[0],
              };
            }),
            total: resources.length,
          };
        },
      } as any;
    default:
      console.error(`Unsupported fetch type ${raFetchType}`);
      return {
        data: [],
      };
  }
};

export default AvailableResourcesDataProvider;
