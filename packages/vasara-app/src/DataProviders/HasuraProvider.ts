import { ApolloQueryResult } from 'apollo-client';
import { IntrospectionQuery, IntrospectionType, getIntrospectionQuery } from 'graphql';
import * as gqlTypes from 'graphql-ast-types-browser';
import gql from 'graphql-tag';

import { RE_CAMUNDA } from '../util/constants';
import HasuraClient from './HasuraClient';
// XXX: ra-data-hasura-graphql 0.1.12 is vendored, because the built version does not
// export all functions required to customize queries
// TODO: check the latest ra-data-hasura-graphql with new customization options
import buildHasuraProvider from './ra-data-hasura-graphql';
import {
  buildApolloArgs,
  buildArgs,
  buildFields,
  buildGqlQuery,
  buildMetaArgs,
} from './ra-data-hasura-graphql/buildGqlQuery';
import { buildQueryFactory } from './ra-data-hasura-graphql/buildQuery';
import buildVariables from './ra-data-hasura-graphql/buildVariables';
import getResponseParser from './ra-data-hasura-graphql/getResponseParser';
import { HasuraSchema } from './types';

interface HasuraDataProviderAndIntrospection {
  introspectionQueryResponse: ApolloQueryResult<IntrospectionQuery>;
  hasuraIntrospectionResults: any;
  hasuraDataProvider: any;
}
const buildFieldsCustom = (type: any, withBytea: boolean) => {
  const fields: string[] = (type?.fields ?? []).map((field: any) => field.name).filter((field: any) => !!field);
  let res = buildFields(type, withBytea);
  if (type.name.endsWith('_comment') || fields.includes('author')) {
    res.push(
      gqlTypes.field(
        gqlTypes.name('author'),
        null,
        null,
        null,
        gqlTypes.selectionSet([gqlTypes.field(gqlTypes.name('name'))])
      )
    );
  }
  if (fields.includes('editor')) {
    res.push(
      gqlTypes.field(
        gqlTypes.name('editor'),
        null,
        null,
        null,
        gqlTypes.selectionSet([gqlTypes.field(gqlTypes.name('name'))])
      )
    );
  }
  return res;
};

const buildGqlQueryCustom = (iR: any) =>
  buildGqlQuery(iR, buildFieldsCustom, buildMetaArgs, buildArgs, buildApolloArgs);

const myBuildQuery = buildQueryFactory(buildVariables, buildGqlQueryCustom, getResponseParser);

const buildHasuraProviderWithIntrospection = async (): Promise<HasuraDataProviderAndIntrospection> => {
  return new Promise<HasuraDataProviderAndIntrospection>((resolve, reject) => {
    // Get schema introspection query response to use with graphql-voyager component
    HasuraClient.query<IntrospectionQuery>({
      fetchPolicy: 'network-only',
      query: gql`
        ${getIntrospectionQuery()}
      `,
    }).then((introspectionQueryResponse: ApolloQueryResult<IntrospectionQuery>) => {
      let hasuraIntrospectionResults: any;
      buildHasuraProvider({
        buildQuery: (introspectionResults: HasuraSchema) => {
          hasuraIntrospectionResults = introspectionResults;
          try {
            return myBuildQuery(introspectionResults);
          } catch (e) {
            console.log(e);
          }
        },
        introspection: {
          // 'schema' will be reused from previously fetched introspection response to avoid repeating of request.
          schema: introspectionQueryResponse.data.__schema,
          exclude: (type: IntrospectionType) => {
            return type.name.match(RE_CAMUNDA);
          },
        },
        client: HasuraClient,
      })
        .then((hasuraDataProvider: any) => {
          resolve({
            introspectionQueryResponse,
            hasuraIntrospectionResults,
            hasuraDataProvider,
          });
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  });
};

export default buildHasuraProviderWithIntrospection;
