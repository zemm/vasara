{ pkgs ? import ../../nix { nixpkgs = sources."nixpkgs-20.09"; }
, sources ? import ../../nix/sources.nix
, vasara-middleware ? import ./default.nix { inherit pkgs; }
, name ? "artifact"
}:

with pkgs;

let

  env = buildEnv {
    name = "env";
    paths = [
      bashInteractive
      coreutils
      netcat
      vasara-middleware
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${netcat}/bin \
  --prefix PATH : ${vasara-middleware}/bin \
  --prefix PATH : ${tini}/bin

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*-python3.9-*" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --files-from=${closure} \
  usr > $out || true
''
