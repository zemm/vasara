{ pkgs ? import ../../nix { nixpkgs = sources."nixpkgs-20.09"; }
, sources ? import ../../nix/sources.nix
, setup ? import ./setup.nix { inherit pkgs; }
}:

setup.build
