{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
, setup ? import ./setup.nix { pkgs = import sources."nixpkgs-20.09" {}; }
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cachix
    jfrog-cli
    jq
    libffi
    pip2nix.python39
    setup.env
  ];
}
