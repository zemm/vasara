"""Service configuration."""
from enum import Enum
from pydantic import BaseSettings


# openssl ecparam -name prime256v1 -genkey -noout -out ec-prime256v1-priv-key.pem
DEFAULT_VASARA_TRANSIENT_USER_PRIVATE_KEY: str = """\
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIJ45rrC0ir5Nmb6kYdQhU4yo7P8uhwas/OGJdnZyLh10oAoGCCqGSM49
AwEHoUQDQgAE2VQ3u7QMgSib7dKxikVNE5ZE+U3UIOG4ZPlx6rq6n9EbdZieTvJk
wPw7DIXt0udeGzkTQLbBNzEXrcpdc/Jfbw==
-----END EC PRIVATE KEY-----
"""

# openssl ec -in ec-prime256v1-priv-key.pem -pubout > ec-prime256v1-pub-key.pem
DEFAULT_VASARA_TRANSIENT_USER_PUBLIC_KEY: str = """\
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE2VQ3u7QMgSib7dKxikVNE5ZE+U3U
IOG4ZPlx6rq6n9EbdZieTvJkwPw7DIXt0udeGzkTQLbBNzEXrcpdc/Jfbw==
-----END PUBLIC KEY-----
"""


class Settings(BaseSettings):
    """Settings."""

    VASARA_TRANSIENT_USER_PRIVATE_KEY: str = DEFAULT_VASARA_TRANSIENT_USER_PRIVATE_KEY
    VASARA_TRANSIENT_USER_PUBLIC_KEY: str = DEFAULT_VASARA_TRANSIENT_USER_PUBLIC_KEY
    VASARA_TRANSIENT_USER_PRIVATE_KEY_ALG: str = "ES256"


class Topic(str, Enum):
    """External task topics."""

    VASARA_TRANSIENT_USER_CREATE = "VasaraTransientUserLifecycle.create"
    VASARA_TRANSIENT_USER_DELETE = "VasaraTransientUserLifecycle.delete"
