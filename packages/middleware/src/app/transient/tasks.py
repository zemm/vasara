"""Service external tasks."""
from app.config import logger
from app.config import settings
from app.services.camunda import camunda_session
from app.transient.config import DEFAULT_VASARA_TRANSIENT_USER_PRIVATE_KEY
from app.transient.config import Topic
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.worker import ExternalTaskComplete
from app.utils import verify_response_status
from pydantic import BaseModel
import datetime
import json
import jwt
import secrets
import uuid


def validate_uuid4(uuid_string: str) -> bool:
    """Validate that a UUID string is in fact a valid uuid4."""
    try:
        return str(uuid.UUID(uuid_string, version=4)) == uuid_string
    except ValueError:
        return False


class RequiredStringValue(BaseModel):
    """Required Camunda string variable value."""

    value: str


class DeleteTransientUserVariables(BaseModel):
    """Delete transient user."""

    id: RequiredStringValue


class CreateTransientUserVariables(BaseModel):
    """Create transient user."""

    firstName: RequiredStringValue
    lastName: RequiredStringValue
    email: RequiredStringValue


async def create_transient_user(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Create transient user."""
    if (
        settings.VASARA_TRANSIENT_USER_PRIVATE_KEY
        == DEFAULT_VASARA_TRANSIENT_USER_PRIVATE_KEY
    ):
        logger.critical(
            "Using hard coded demo private key for signing user. "
            "Please, set VASARA_TRANSIENT_USER_PRIVATE_KEY environment variable."
        )

    # Parse variables
    variables = CreateTransientUserVariables(**(task.dict().get("variables") or {}))
    id_ = f"guest-{uuid.uuid4()}"
    profile = {
        "id": id_,
        "firstName": variables.firstName.value,
        "lastName": variables.lastName.value,
        "email": variables.email.value,
    }
    claims = profile.copy()
    claims["created"] = datetime.datetime.utcnow().isoformat().split(".")[0]
    token = jwt.encode(
        claims,
        settings.VASARA_TRANSIENT_USER_PRIVATE_KEY,
        algorithm=settings.VASARA_TRANSIENT_USER_PRIVATE_KEY_ALG,
    )
    credentials = {"password": secrets.token_urlsafe(256)}
    payload = {
        "profile": profile,
        "credentials": credentials,
    }
    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/user/create"
        create_user = await http.post(url, data=json.dumps(payload))
        await verify_response_status(create_user, status=(204,))
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/group/camunda-transient/members/{id_}"
        logger.info(url)
        update_group = await http.put(url)
        if update_group.status != 204:
            # "Rollback"
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/user/{id_}"
            logger.info(url)
            delete_user = await http.delete(url)
            await verify_response_status(delete_user, status=(204, 404))
        await verify_response_status(update_group, status=(204,))
    login_url = f"{settings.SUBSTITUTION_BASE_URL}?guest={token}"
    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(
            workerId=task.workerId,
            variables={
                "id": VariableValueDto(value=id_, type=ValueType.String),
                "token": VariableValueDto(value=token, type=ValueType.String),
                "url": VariableValueDto(value=login_url, type=ValueType.String),
            },
        ),
    )


async def delete_transient_user(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Delete transient user."""
    # Parse variables
    variables = DeleteTransientUserVariables(**(task.dict().get("variables") or {}))

    # Validate (without guest- -prefix)
    prefix_length = len("guest-")
    assert variables.id.value.startswith("guest-")
    assert validate_uuid4(variables.id.value[prefix_length:])

    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/user/{variables.id.value}"
        delete_user = await http.delete(url)
        await verify_response_status(delete_user, status=(204, 404))

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )


TASKS = {
    Topic.VASARA_TRANSIENT_USER_CREATE: create_transient_user,
    Topic.VASARA_TRANSIENT_USER_DELETE: delete_transient_user,
}
