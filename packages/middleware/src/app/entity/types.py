"""Entity related types."""

from pydantic import BaseModel
from typing import Any
from typing import Dict


class EntityMutationQuery(BaseModel):
    """Substitution values mutation."""

    query: str
    variables: Dict[str, Any]
