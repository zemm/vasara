"""Service external tasks."""
from app.config import settings
from app.entity.config import Topic
from app.entity.utils import build_update_query
from app.entity.utils import mutate_entity_variables
from app.entity.utils import query_raw_values
from app.services.camunda import camunda_session
from app.services.substitute import build_substitution_query
from app.services.substitute import query_substitution_values
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.worker import ExternalTaskComplete
from app.utils import verify_response_status
from mimetypes import guess_type
from typing import Any
from typing import Dict
import base64
import binascii


async def load_from_entity_to_process(
    task: LockedExternalTaskDto,
) -> ExternalTaskComplete:
    """Complete task."""
    assert task.businessKey

    mapping = {}
    variables = {}
    for name, value in (task.variables or {}).items():
        if (
            value.type == ValueType.String
            and value.value
            and len([v for v in value.value.split(".") if v.strip()]) == 2
        ):
            mapping[value.value] = name
            mapping[value.value.split(".")[0] + ".metadata"] = "placeholder"
            variables[name] = VariableValueDto(value=None, type=ValueType.Bytes)
    assert mapping, "No variables configured"

    query = build_substitution_query(list(mapping.keys()))
    result = await query_substitution_values(query, task.businessKey)

    for key, value_ in result.items():
        if not mapping.get(key):
            continue
        if value_.startswith("\\x"):
            encoded_value = binascii.unhexlify(value_[2:])
            value_info = {"encoding": "utf-8"}
            entity, field = key.split(".", 1)
            if f"{entity}.metadata.{field}.name" in result:
                value_info["filename"] = result[f"{entity}.metadata.{field}.name"]
            if f"{entity}.metadata.{field}.type" in result:
                value_info["mimeType"] = result[f"{entity}.metadata.{field}.type"]
                value_info["mimetype"] = result[f"{entity}.metadata.{field}.type"]
            variables[mapping[key]] = VariableValueDto(
                value=base64.b64encode(encoded_value),
                type=ValueType.File,
                valueInfo=value_info,
            )

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(workerId=task.workerId, variables=variables),
    )


async def save_from_process_to_entity(  # noqa: R0914
    task: LockedExternalTaskDto,
) -> ExternalTaskComplete:
    """Complete task."""
    assert task.businessKey

    mapping = {}
    for name, value in (task.variables or {}).items():
        if len([v for v in name.split(".") if v.strip()]) == 2:
            mapping[name.split(".")[0] + ".metadata"] = False
    query = build_substitution_query(list(mapping.keys()))
    old = await query_raw_values(query, task.businessKey)
    assert mapping, "No variables configured"

    arguments = {}
    variables: Dict[str, Any] = {"businessKey": task.businessKey}
    for name, value in (task.variables or {}).items():
        if (
            value.type == ValueType.String
            and len([part for part in name.split(".") if part.strip()]) == 2
        ):
            arguments[name] = "String"
            variables[name] = f"{value.value}"
        elif (
            value.type == ValueType.File
            and len([part for part in name.split(".") if part.strip()]) == 2
        ):
            arguments[name] = "bytea"
            async with camunda_session() as http:
                url = f"{settings.CAMUNDA_REST_ENDPOINT}/execution/{task.executionId}/localVariables/{name}/data"
                get = await http.get(
                    url, headers={"Accept": "application/octet-stream"}
                )
                await verify_response_status(get, status=(200,))
                data = await get.read()
                variables[name] = "\\x" + binascii.hexlify(data).decode("utf-8")
            entity, field = name.split(".", 1)
            arguments[f"{entity}.metadata"] = "jsonb"
            variables[f"{entity}.metadata"] = (old.get(entity) or {}).get(
                "metadata"
            ) or {}
            variables[f"{entity}.metadata"].setdefault(field, {})
            variables[f"{entity}.metadata"][field]["size"] = len(data)
            if value.valueInfo:
                filename = value.valueInfo.get("filename")
                if filename:
                    variables[f"{entity}.metadata"][field]["name"] = filename
                mime_type = value.valueInfo.get("mimeType") or value.valueInfo.get(
                    "mimetype"
                )
                if not mime_type:
                    mime_type = guess_type(filename)[0] or "plain/text"
                if mime_type:
                    variables[f"{entity}.metadata"][field]["type"] = mime_type

    query = build_update_query(arguments)
    variables = {key.replace(".", "_"): value for key, value in variables.items()}
    await mutate_entity_variables(query, variables)

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )


TASKS = {
    Topic.VASARA_ENTITY_LOAD: load_from_entity_to_process,
    Topic.VASARA_ENTITY_SAVE: save_from_process_to_entity,
}
