"""Entity related utilities."""
from app.config import logger
from app.config import settings
from app.entity.types import EntityMutationQuery
from app.services.hasura import hasura_session
from app.types.hasura import SubstitutionValuesQuery
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.hasura import SubstitutionValuesQueryVariables
from app.utils import asdict
from app.utils import asjson
from typing import Any
from typing import Dict
from typing import List


async def query_raw_values(query: str, business_key: str) -> Dict[str, Any]:
    """Query entity substitution values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = SubstitutionValuesQuery(
            query=query,
            variables=SubstitutionValuesQueryVariables(businessKey=business_key),
        )
        post = await http.post(url, data=asjson(query_))
        if post.status not in (200,):
            logger.info(await post.json())
            return {}
        data = await post.json()
        logger.debug(data)
        response = SubstitutionValuesQueryResponse(**data)
        return {
            key: response.data[key][0]
            for key in response.data.keys()
            if response.data[key]
        }


def build_update_query(types: Dict[str, str]) -> str:
    """Build GraphQL mutation for values to be updated."""
    current: List[str] = []
    previous: List[str] = []
    keys = sorted(types.keys())
    variables = ", ".join([f"${key.replace('.', '_')}: {types[key]}!" for key in keys])
    query = f"mutation ($businessKey: String!, {variables}) {{\n"
    for key in keys:
        variable = key.replace(".", "_")
        parts = key.split(".")
        for part in parts:
            current.append(part)
            len_previous = len(previous)
            len_current = len(current)
            if previous[0:len_current] != current:
                for i in range(max(0, len_previous - len_current)):
                    padded = f"{'  ' * len_current}}}"
                    query += f"\n{padded}"
                if len_current == 1:
                    temp = " " * len(part)
                    query += f"  update_{part}(where: {{ business_key: {{_eq: $businessKey }} }},\n"
                    query += f"         {temp}  _set:"
                elif len_current > len_previous:
                    padded = f"{'  ' * len_current}{part}: ${variable}"
                    query += f" {{\n{padded}"
                elif current[0 : len_current - 1] != previous[0 : len_current - 1]:
                    padded = f"{'  ' * len_current}{part}: ${variable}"
                    query += f" {{\n{padded}"
                else:
                    padded = f"{'  ' * len_current}{part}: ${variable}"
                    query += f"\n{padded}"
        previous = current
        current = []
    for i in range(1, len(previous)):
        padded = (
            f"{'  ' * (len(previous) - i)}}}) {{ returning {{ id business_key }} }}"
        )
        query += f"\n{padded}"
    query += "\n}\n"
    return query


async def mutate_entity_variables(mutation: str, variables: Dict[str, Any]) -> None:
    """Query entity substitution values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = EntityMutationQuery(query=mutation, variables=variables)
        logger.debug(asdict(query_))
        post = await http.post(url, data=asjson(query_))
        if post.status not in (200,):
            logger.info(await post.json())
        data = await post.json()
        logger.debug(data)
        for item in (data.get("data") or {}).values():
            assert item.get(
                "returning"
            ), f"Unexpected mutation {mutation} result: {data}"
