"""Service configuration."""
from enum import Enum
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings."""


class Topic(str, Enum):
    """External task topics."""

    VASARA_ENTITY_LOAD = "vasara.load"
    VASARA_ENTITY_SAVE = "vasara.save"
