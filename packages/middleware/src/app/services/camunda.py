"""Camunda REST API helpers."""
from aiohttp import ClientResponse
from aiohttp import ClientTimeout
from app.config import emcee
from app.config import settings
from app.services.vault import decrypt_base64
from app.services.vault import decrypt_text
from app.types.camunda import ActivityInstanceDto
from app.types.camunda import CorrelationMessageDto
from app.types.camunda import ExternalTaskDto
from app.types.camunda import HistoricProcessInstanceDto
from app.types.camunda import HistoricTaskInstanceDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import PatchVariablesDto
from app.types.camunda import ProcessInstanceWithVariablesDto
from app.types.camunda import State
from app.types.camunda import TaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.utils import asjson
from app.utils import verify_response_status
from asyncio import Future
from contextlib import asynccontextmanager
from dataclasses import fields
from fastapi.exceptions import HTTPException
from mimetypes import guess_type
from operator import itemgetter
from typing import AsyncGenerator
from typing import Dict
from typing import List
from typing import Optional
from typing import Type
from typing import TypeVar
from typing import Union
import aiohttp
import asyncio
import base64
import json


T = TypeVar("T")


@asynccontextmanager
async def camunda_session(
    authorization: str = settings.CAMUNDA_REST_AUTHORIZATION,
) -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with Camunda headers."""
    headers = (
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": authorization,
        }
        if authorization
        else {"Content-Type": "application/json", "Accept": "application/json"}
    )
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.CAMUNDA_TIMEOUT),
    ) as session:
        yield session


async def set_variable_value(
    variable: VariableValueDto, fetch: Future[ClientResponse]
) -> VariableValueDto:
    """Set process instance variable."""
    response: ClientResponse = fetch.result()

    if response.status in (200,) and variable.type == ValueType.File:
        filename = (
            response.content_disposition.filename
            if response.content_disposition
            else None
        ) or "download"
        mime_type = guess_type(filename)[0] or "plain/text"
        if variable.valueInfo is not None:
            variable.valueInfo["filename"] = filename
            variable.valueInfo["mimetype"] = mime_type
        if filename.endswith("json.txt") and mime_type == "text/plain":
            text_value = (await response.read()).decode("utf-8")
            if text_value.startswith("vault:v"):
                variable.value = json.loads(await decrypt_text(text_value))
                variable.valueInfo = {}
        elif filename.endswith(".txt") and mime_type == "text/plain":
            binary_value = await response.read()
            if binary_value.startswith(b"vault:v"):
                filename = filename.rsplit(".txt", 1)[0]
                mime_type = guess_type(filename)[0] or "text/plain"
                variable.value = await decrypt_base64(binary_value)
                if variable.valueInfo is not None:
                    variable.valueInfo["filename"] = filename
                    variable.valueInfo["mimetype"] = mime_type
        else:
            variable.value = f"data:{mime_type};base64,".encode("utf-8")
            variable.value += base64.b64encode(await response.read())

    elif response.status in (200,) and variable.type == ValueType.Json:
        variable.value = json.loads(
            VariableValueDto(**await response.json()).value or "null"
        )

    return variable


async def get_process_instance(instance_id: str) -> HistoricProcessInstanceDto:
    """Get process instance."""
    instance_url = (
        f"{settings.CAMUNDA_REST_ENDPOINT}/history/process-instance/{instance_id}"
    )
    async with camunda_session() as http:
        get = await http.get(instance_url)
        await verify_response_status(get, status=(200,), error_status=404)
        instance = HistoricProcessInstanceDto(**await get.json())
        return instance


async def get_process_instances_by_key(
    business_key: str,
) -> List[HistoricProcessInstanceDto]:
    """Get process instances by business key."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/process-instance"
    async with camunda_session() as http:
        get = await http.get(url, params={"processInstanceBusinessKey": business_key})
        await verify_response_status(get, status=(200,), error_status=404)
        instances = [
            HistoricProcessInstanceDto(**instance) for instance in await get.json()
        ]
        return instances


async def get_process_instance_variables(
    instance_id: str,
) -> Dict[str, VariableValueDto]:
    """Get process instance variables."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}/variables"
    async with camunda_session() as http:
        get = await http.get(url, params={"deserializeValues": "false"})
        if get.status in (404, 500):
            return await get_historic_process_instance_variables(instance_id)
        await verify_response_status(get, status=(200,), error_status=404)
        variables = await update_file_variables(
            url,
            {
                key: VariableValueDto(**value)
                for key, value in (await get.json()).items()
            },
        )
        return variables


async def get_historic_process_instance_variables(
    instance_id: str,
) -> Dict[str, VariableValueDto]:
    """Get process instance variables."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/variable-instance"
    async with camunda_session() as http:
        get = await http.get(
            url, params={"processInstanceId": instance_id, "deserializeValues": "false"}
        )
        await verify_response_status(get, status=(200,), error_status=404)
        variables = await update_file_variables(
            url,
            {
                value["name"]: VariableValueDto(**value)
                for value in sorted(await get.json(), key=itemgetter("createTime"))
            },
        )
        return variables


async def update_file_variables(
    variables_url: str, variables: Dict[str, VariableValueDto]
) -> Dict[str, VariableValueDto]:
    """Update variables with file variable values."""
    files = {}
    async with camunda_session() as http:
        for name in variables:
            if variables[name].type == ValueType.File:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{variables_url}/{name}/data",
                        headers={"Accept": "application/octet-stream"},
                    )
                )
            elif variables[name].type == ValueType.Json:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{variables_url}/{name}", params={"deserializeValue": "false"},
                    )
                )
        if files:
            await asyncio.wait(files.values())

        # Updated file variable
        for name, get_variable in files.items():
            variables[name] = await set_variable_value(variables[name], get_variable)

    return variables


async def get_instance_variables(
    instance_id: str, fetch_files: Optional[bool] = True,
) -> Dict[str, VariableValueDto]:
    """Get process instance variables."""
    instance_url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}"

    async with camunda_session() as http:
        get_variables = await http.get(f"{instance_url}/variables")
        await verify_response_status(get_variables, status=(200,))
        variables = {
            key: VariableValueDto(**value)
            for key, value in (await get_variables.json()).items()
        }

        if not fetch_files:
            return variables

        files = {}

        for name in variables:
            if variables[name].type == ValueType.File:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{instance_url}/variables/{name}/data",
                        headers={"Accept": "application/octet-stream"},
                    )
                )
            elif variables[name].type == ValueType.Json:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{instance_url}/variables/{name}",
                        params={"deserializeValue": "false"},
                    )
                )
        if files:
            await asyncio.wait(files.values())

        for name, get_variable in files.items():
            variables[name] = await set_variable_value(variables[name], get_variable)

        return variables


async def get_instance_with_variables(
    instance_id: str,
) -> ProcessInstanceWithVariablesDto:
    """Get process instance with variables."""
    instance_url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}"

    async with camunda_session() as http:
        # 2) Fetch instance and inline variables
        get_instance_task, get_variables_task = (
            asyncio.create_task(http.get(instance_url)),
            asyncio.create_task(http.get(f"{instance_url}/variables")),
        )
        await asyncio.wait([get_instance_task, get_variables_task])
        get_instance, get_variables = (
            get_instance_task.result(),
            get_variables_task.result(),
        )
        await verify_response_status(get_instance, status=(200,), error_status=404)
        await verify_response_status(get_variables, status=(200,))
        instance = ProcessInstanceWithVariablesDto(**await get_instance.json())
        instance.variables = {
            key: VariableValueDto(**value)
            for key, value in (await get_variables.json()).items()
        }

        # 3) Fetch file variables
        files = {}
        for name in instance.variables:
            if instance.variables[name].type == ValueType.File:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{instance_url}/variables/{name}/data",
                        headers={"Accept": "application/octet-stream"},
                    )
                )
            elif instance.variables[name].type == ValueType.Json:
                files[name] = asyncio.create_task(
                    http.get(
                        f"{instance_url}/variables/{name}",
                        params={"deserializeValue": "false"},
                    )
                )
        if files:
            await asyncio.wait(files.values())

        # 4) Updated file variable
        for name, get_variable in files.items():
            instance.variables[name] = await set_variable_value(
                instance.variables[name], get_variable
            )

        return instance


async def update_variables(
    instance_id: str, variables: Dict[str, VariableValueDto],
) -> None:
    """Update process instance variables."""
    async with camunda_session() as http:
        url = (
            f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}/variables"
        )
        data = PatchVariablesDto(modifications=variables)
        post = await http.post(url, data=asjson(data))
        await verify_response_status(post, status=(204,), error_status=400)


async def post_message(
    instance_id: str,
    message_id: str,
    variables: Optional[Dict[str, VariableValueDto]] = None,
    wait_for: Optional[str] = None,
) -> None:
    """Post correlated message and optionally wait for a task to be processed."""
    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/message"
        data = CorrelationMessageDto(
            messageName=message_id,
            processInstanceId=instance_id,
            resultEnabled=False,
            processVariables=variables,
        )
        post = await http.post(url, data=asjson(data))
        if post.status in [204] and wait_for:
            await emcee.wait(wait_for)


def task_variable_int(task: LockedExternalTaskDto, name: str) -> int:
    """Return named int task variable or raise KeyError."""
    if task.variables is not None:
        value = task.variables[name].value
        if isinstance(value, int):
            return value
    raise KeyError(name)


def task_variable_str(task: LockedExternalTaskDto, name: str) -> str:
    """Return named str task variable or raise KeyError."""
    if task.variables is not None:
        value = task.variables[name].value
        if isinstance(value, str):
            return value
    raise KeyError(name)


def task_variable_list(task: LockedExternalTaskDto, name: str) -> List[str]:
    """Return named str task variable or raise KeyError."""
    if task.variables is not None:
        variable = task.variables[name]
        if (
            variable is not None
            and isinstance(variable.value, str)
            and variable.type == "Object"
            and variable.valueInfo is not None
            and variable.valueInfo["serializationDataFormat"] == "application/json"
        ):
            value = task.variables[name].value
            return [str(x) for x in json.loads(value or "[]") or []]
    raise KeyError(name)


def task_variables(task: LockedExternalTaskDto, klass: Type[T]) -> T:
    """Parse task variables into data class instance."""
    data = {}
    if task.variables:
        for field in fields(klass):
            if field.name in task.variables:
                data[field.name] = task.variables[field.name].value
    return klass(**data)  # type: ignore


async def get_external_tasks(instance_id: str,) -> List[ExternalTaskDto]:
    """Get open process instance external tasks."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/external-task"
    async with camunda_session() as http:
        get = await http.get(url, params={"processInstanceId": instance_id})
        if get.status in [200]:
            return list([ExternalTaskDto(**task) for task in await get.json()])
    return []


async def get_process_activity_instances(
    instance_id: str, state: Optional[State] = State.ACTIVE,
) -> List[ActivityInstanceDto]:
    """Get active process instance jobs."""
    if state == State.ACTIVE:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}/activity-instances"
        async with camunda_session() as http:
            get = await http.get(url)
            await verify_response_status(get, status=(200,), error_status=404)
            root = ActivityInstanceDto(**await get.json())
            return root.childActivityInstances or []
    return []


async def get_user_tasks_by_key(business_key: str) -> List[TaskDto]:
    """Get open user tasks by business key."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/task"
    async with camunda_session() as http:
        get = await http.get(url, params={"processInstanceBusinessKey": business_key})
        if get.status in [200]:
            return list([TaskDto(**task) for task in await get.json()])
    return []


async def get_user_task_form_variables(task_id: str) -> Dict[str, VariableValueDto]:
    """Get all user task form variables."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/task/{task_id}/form-variables"
    async with camunda_session() as http:
        get = await http.get(url)
        await verify_response_status(get, status=(200,), error_status=404)
        data = await get.json()
        return {name: VariableValueDto(**value) for name, value in data.items()}


async def get_user_task_variables(task_id: str) -> Dict[str, VariableValueDto]:
    """Get user task variables."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/task/{task_id}/variables"
    async with camunda_session() as http:
        get = await http.get(url, params={"deserializeValues": "false"})
        await verify_response_status(get, status=(200,), error_status=404)
        data = await get.json()
        return {name: VariableValueDto(**value) for name, value in data.items()}


async def get_historic_user_task_variables(
    instance_id: str,
) -> Dict[str, VariableValueDto]:
    """Get historic user task variables."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/variable-instance"
    async with camunda_session() as http:
        get = await http.get(
            url,
            params={
                "activityInstanceIdIn": instance_id,
                "deserializeValues": "false",
                "includeDeleted": "true",
            },
        )
        await verify_response_status(get, status=(200,), error_status=404)
        data = await get.json()
        return {
            item["name"]: VariableValueDto(**item)
            for item in sorted(data, key=itemgetter("createTime"))
        }


async def get_user_task_by_key(
    task_definition_key: str, business_key: str
) -> Union[TaskDto, HistoricTaskInstanceDto]:
    """Get open task by business key."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/task"
    async with camunda_session() as http:
        get = await http.get(
            url,
            params={
                "processInstanceBusinessKey": business_key,
                "taskDefinitionKey": task_definition_key,
            },
        )
        await verify_response_status(get, status=(200,), error_status=404)
        for task in await get.json():
            return TaskDto(**task)
        return await get_historic_user_task_by_key(task_definition_key, business_key)


async def get_historic_user_task(
    task_definition_key: str, instance_id: Optional[str] = None
) -> HistoricTaskInstanceDto:
    """Get historic task."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/task"
    async with camunda_session() as http:
        get = await http.get(
            url,
            params={
                "processInstanceId": instance_id,
                "taskDefinitionKey": task_definition_key,
            },
        )
        await verify_response_status(get, status=(200,), error_status=404)
        for task in await get.json():
            return HistoricTaskInstanceDto(**task)
        raise HTTPException(
            status_code=404,
            detail=f'Task "{task_definition_key}" for "{instance_id}" not found.',
        )


async def get_historic_user_task_by_key(
    task_definition_key: str, business_key: str
) -> HistoricTaskInstanceDto:
    """Get historic task by business key."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/task"
    async with camunda_session() as http:
        get = await http.get(
            url,
            params={
                "processInstanceBusinessKey": business_key,
                "taskDefinitionKey": task_definition_key,
            },
        )
        await verify_response_status(get, status=(200,), error_status=404)
        for task in await get.json():
            return HistoricTaskInstanceDto(**task)
        raise HTTPException(
            status_code=404,
            detail=f'Task "{task_definition_key}" for "{business_key}" not found.',
        )


async def get_historic_user_tasks_by_keys(
    task_definition_keys: List[str], business_key: str
) -> List[HistoricTaskInstanceDto]:
    """Get historic tasks by task definition keys and business key."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/task"
    async with camunda_session() as http:
        get = await http.get(
            url,
            params={
                "processInstanceBusinessKey": business_key,
                "taskDefinitionKeyIn": ",".join(task_definition_keys),
            },
        )
        await verify_response_status(get, status=(200,), error_status=404)
        return [HistoricTaskInstanceDto(**task) for task in await get.json()]
