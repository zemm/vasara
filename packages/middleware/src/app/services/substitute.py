"""Data based string substitutions."""
from app.config import settings
from app.services.hasura import hasura_session
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.hasura import SubstitutionValuesQuery
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.hasura import SubstitutionValuesQueryVariables
from app.utils import asdict
from app.utils import asjson
from string import Template
from typing import Any
from typing import Dict
from typing import List
from typing import Set
import json
import logging
import re


logger = logging.getLogger(__name__)

RE_DATE = re.compile(r"^(\d{4})-(\d{2})-(\d{2})")


class DottedTemplate(Template):
    """Template with dotted identifiers."""

    delimiter = "%"
    idpattern = r"(?a:[_a-z][_a-z0-9\.]*)"


def dict_to_substitutions(
    variables: Dict[str, Any], prefix: str = ""
) -> Dict[str, str]:
    """Flatten variables dictionary."""
    result = {}
    for name, value in variables.items():
        if isinstance(value, str):
            result[f"{prefix}{name}"] = (
                value.split("T")[0] if RE_DATE.match(value) else value
            )
        elif isinstance(value, bool):
            result[f"{prefix}{name}"] = "Kyllä / Yes" if value else "Ei / No"
        elif isinstance(value, int):
            result[f"{prefix}{name}"] = str(value)
        elif isinstance(value, float):
            result[f"{prefix}{name}"] = str(value)
        elif isinstance(value, dict):
            result.update(dict_to_substitutions(value, prefix=f"{prefix}{name}."))
            if "@order" in value:
                results = []
                for key in [k for k in value["@order"] if k in value]:
                    result_ = f"{key}: "
                    result_ += "\n".join(
                        dict_to_substitutions({"_": value[key]}).values()
                    )
                    results.append(result_)
                result[f"{prefix}{name}"] = "\n".join(results)
        elif isinstance(value, list) and len(value) > 0:
            if isinstance(value[0], dict):
                result.update(
                    dict_to_substitutions(value[0], prefix=f"{prefix}{name}.")
                )
            else:
                result__ = dict_to_substitutions(
                    {str(idx): value[idx] for idx in range(len(value))}
                )
                result[f"{prefix}{name}"] = "\n".join(result__.values())
    return result


def variables_to_substitutions(
    variables: Dict[str, VariableValueDto], prefix: str = ""
) -> Dict[str, str]:
    """Flatten variables dictionary."""
    result: Dict[str, str] = {
        "baseUrl": settings.SUBSTITUTION_BASE_URL,
    }
    for name, variable in variables.items():
        if variable.type == ValueType.String:
            value = variable.value or ""
            if RE_DATE.match(value):
                result[f"{prefix}{name}"] = value.split("T")[0]
            else:
                result[f"{prefix}{name}"] = value
        elif variable.type == ValueType.Boolean:
            result[f"{prefix}{name}"] = "Kyllä / Yes" if variable.value else "Ei / No"
        elif variable.type == ValueType.Integer:
            result[f"{prefix}{name}"] = str(variable.value)
        elif variable.type == ValueType.Double:
            result[f"{prefix}{name}"] = str(variable.value)
        elif (
            variable.type == ValueType.Object
            and isinstance(variable.value, str)
            and variable.type == "Object"
            and variable.valueInfo is not None
            and variable.valueInfo["serializationDataFormat"] == "application/json"
        ):
            value = json.loads(variable.value)
            if isinstance(value, dict):
                result.update(dict_to_substitutions(value, prefix=f"{prefix}{name}."))
            elif isinstance(value, list) and len(value) > 0:
                if isinstance(value[0], dict):
                    result.update(
                        dict_to_substitutions(value[0], prefix=f"{prefix}{name}.")
                    )

                else:
                    result_ = dict_to_substitutions(
                        {str(idx): value[idx] for idx in range(len(value))}
                    )
                    result[f"{prefix}{name}"] = "\n".join(result_.values())
    return result


async def safe_substitute(
    task: LockedExternalTaskDto, templates: List[str], substitutes: List[str]
) -> List[str]:
    """Substitute template."""
    assert task.businessKey
    # First pass
    templates = [
        DottedTemplate(template).safe_substitute(
            **variables_to_substitutions(task.variables or {})
        )
        for template in templates
    ]
    # Second pass
    keys: Set[str] = set()
    for template in templates:
        t = DottedTemplate(template)
        keys = keys | {match[2] for match in t.pattern.findall(t.template) if "." in match[2]}  # type: ignore
    keys = keys.intersection(set(substitutes))  # apply substitute allow list
    if keys:
        query = build_substitution_query(list(keys))
        values = {key: "" for key in keys}
        values.update(await query_substitution_values(query, task.businessKey))
        templates = [
            DottedTemplate(template).safe_substitute(**values) for template in templates
        ]
    return templates


def build_substitution_query(keys: List[str]) -> str:
    """Build GraphQL query for substitution values to be fetched."""
    current: List[str] = []
    previous: List[str] = []
    keys = sorted(keys)
    query = "query ($businessKey: String) {\n"
    for key in keys:
        parts = key.split(".")
        for part in parts:
            current.append(part)
            len_previous = len(previous)
            len_current = len(current)
            if previous[0:len_current] != current:
                for i in range(max(0, len_previous - len_current)):
                    padded = f"{'  ' * len_current}}}"
                    query += f"\n{padded}"
                if len_current == 1:
                    query += (
                        f"  {part}(where: {{ business_key: {{ _eq: $businessKey }} }})"
                    )
                elif len_current > len_previous:
                    padded = f"{'  ' * len_current}{part}"
                    query += f" {{\n{padded}"
                elif current[0 : len_current - 1] != previous[0 : len_current - 1]:
                    padded = f"{'  ' * len_current}{part}"
                    query += f" {{\n{padded}"
                else:
                    padded = f"{'  ' * len_current}{part}"
                    query += f"\n{padded}"
        previous = current
        current = []
    for i in range(1, len(previous)):
        padded = f"{'  ' * (len(previous) - i)}}}"
        query += f"\n{padded}"
    query += "\n}\n"
    return query


async def query_substitution_values(query: str, business_key: str) -> Dict[str, str]:
    """Query entity substitution values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = SubstitutionValuesQuery(
            query=query,
            variables=SubstitutionValuesQueryVariables(businessKey=business_key),
        )
        logger.debug(asdict(query_))
        post = await http.post(url, data=asjson(query_))
        if post.status not in (200,):
            logger.info(await post.json())
            return {}
        data = await post.json()
        logger.debug(data)
        response = SubstitutionValuesQueryResponse(**data)
        return dict_to_substitutions(response.data)
