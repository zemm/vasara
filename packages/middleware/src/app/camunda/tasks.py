"""Service external tasks."""
from app.camunda.config import Topic
from app.handlers.simple import complete


TASKS = {Topic.CAMUNDA_PLACEHOLDER: complete}  # placeholder
