"""Email task handler."""
from app.services.camunda import task_variable_list
from app.services.substitute import safe_substitute
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.worker import ExternalTaskComplete


async def substitute(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Complete task."""
    assert task.businessKey
    assert task.processInstanceId

    try:
        substitutes = task_variable_list(task, "substitutes")
    except KeyError:
        substitutes = []

    names = []
    templates = []
    for name, value in (task.variables or {}).items():
        if value.type == ValueType.String and value.value and "%{" in value.value:
            names.append(name)
            templates.append(value.value)
    values = []
    if templates:
        values = await safe_substitute(task, templates, substitutes)
    assert len(names) == len(templates) == len(values)

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(
            workerId=task.workerId,
            variables={
                names[idx]: VariableValueDto(value=values[idx])
                for idx in range(len(names))
                if values[idx] != templates[idx]
            },
        ),
    )
