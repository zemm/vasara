"""Dummy external task handler."""
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.worker import ExternalTaskComplete


async def complete(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Complete task."""
    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )
