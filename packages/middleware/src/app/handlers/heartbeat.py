"""Camunda ExternalTask handler for Heartbeat tasks."""
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import VariableValueDto
from app.types.worker import ExternalTaskComplete
from datetime import datetime
from pydantic.dataclasses import dataclass


@dataclass
class State:
    """Service health check state."""

    timestamp: str = datetime.utcnow().isoformat()


state = State()


async def handler(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Update health check timestamp."""
    state.timestamp = datetime.utcnow().isoformat()
    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(
            workerId=task.workerId,
            variables={
                "timestamp": VariableValueDto(value=state.timestamp, type="string"),
            },
        ),
    )
