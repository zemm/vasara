"""Email task handler."""
from app.config import settings
from app.services.camunda import camunda_session
from app.services.camunda import task_variable_list
from app.services.camunda import task_variable_str
from app.services.hasura import query_camunda_principals
from app.services.substitute import build_substitution_query
from app.services.substitute import query_substitution_values
from app.services.substitute import safe_substitute
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.worker import ExternalTaskComplete
from app.utils import asdict
from app.utils import asjson
from app.utils import verify_response_status
from email.charset import Charset
from email.charset import QP
from email.encoders import encode_base64
from email.generator import Generator
from email.header import Header
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from enum import Enum
from html.parser import HTMLParser
from io import StringIO
from mimetypes import guess_type
from pydantic import BaseModel
from pydantic import Field
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
import binascii
import email_validator  # type: ignore
import json
import logging
import smtplib


CHARSET = Charset("utf-8")
CHARSET.header_encoding = QP
CHARSET.body_encoding = QP


logger = logging.getLogger(__name__)


class MarkupCleaner(HTMLParser):
    """HTML markup cleaner."""

    def __init__(self) -> None:
        """Initialize parser and buffer."""
        super().__init__(convert_charrefs=True)
        self.reset()
        self.text = StringIO()

    def handle_data(self, data: str) -> None:
        """Feed cleaned data into buffer."""
        self.text.write(data)

    def get_data(self) -> str:
        """Return cleaned data from buffer."""
        return self.text.getvalue()

    def error(self, message: str) -> None:
        """Log error in parsing markup."""
        logger.warning(message)


def strip_tags(s: str) -> str:
    """Strip HTML tags from string."""
    cleaner = MarkupCleaner()
    cleaner.feed(s)
    return cleaner.get_data()


class AuthorizationDto(BaseModel):
    """Camunda authorization result."""

    id: str
    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class CreateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class UpdateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class ProcessResourceType(int, Enum):
    """Camunda resource type."""

    PROCESS_INSTANCE = 8
    HISTORIC_PROCESS_INSTANCE = 20


async def ensure_read_authorization(
    process_id: str, group_ids: List[str], resource_type: ProcessResourceType
) -> None:
    """Ensure read permissions for given group ids for certain process."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization"
    async with camunda_session() as http:
        response = await http.get(
            f"{url}",
            params={
                "type": 1,  # Grant
                "resourceType": resource_type,
                "resourceId": process_id,
                "groupIdIn": ",".join(group_ids),
            },
        )
        await verify_response_status(response, status=(200,), error_status=404)
        old = [AuthorizationDto(**item) for item in await response.json()]

    updates: Dict[str, UpdateAuthorizationDto] = {}
    for authorization in old:
        if "READ" not in authorization.permissions:
            authorization.permissions.append("READ")
            updates[authorization.id] = UpdateAuthorizationDto(**asdict(authorization))
        if authorization.groupId in group_ids:
            group_ids.remove(authorization.groupId)

    creates: List[CreateAuthorizationDto] = []
    for group_id in group_ids:
        creates.append(
            CreateAuthorizationDto(
                type=1,  # Grant
                permissions=["READ"],
                userId=None,
                groupId=group_id,
                resourceType=resource_type,
                resourceId=process_id,
            )
        )

    async with camunda_session() as http:
        for id_, update in updates.items():
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/{id_}"
            response = await http.put(url, data=asjson(update))
            if response.status != 204:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to update %s as %s with %s", url, update, error)
        for create in creates:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/create"
            response = await http.post(url, data=asjson(create))
            if response.status != 200:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to create %s as %s with %s", url, create, error)


def split_email_address(email: str) -> Tuple[str, str]:
    """Split email into descriptive part and address."""
    if "<" not in email:
        descriptive = address = email_validator.validate_email(email).email.strip()
        return descriptive, address
    descriptive, address = email.split("<", 1)
    address = email_validator.validate_email(address.split(">")[0]).email.strip()
    return descriptive.strip().strip('"'), address


class Attachment(BaseModel):
    """Attachment as a bytestring with metadata."""

    data: bytes
    filename: str
    mimetype: str


def create_email(  # noqa: R9014
    sender: str,
    recipients: List[str],
    subject: str,
    body: str,
    attachments: List[Attachment],
) -> str:
    """Create encoded email."""
    sender_parts = split_email_address(sender)
    recipients_parts = map(split_email_address, recipients)

    msg = MIMEMultipart()
    msg["Subject"] = strip_tags(subject)
    msg["From"] = f"{Header(sender_parts[0], CHARSET).encode()} <{sender_parts[1]}>"
    msg["To"] = ",".join(
        [
            f"{Header(recipient[0], CHARSET).encode()} <{recipient[1]}>"
            for recipient in recipients_parts
        ]
    )

    main = MIMEMultipart("alternative")
    plain = strip_tags(body).replace("\n", "\r\n")
    html = plain.replace("\r\n", "<br/>")

    # noinspection PyTypeChecker
    main.attach(MIMEText(plain, "plain", CHARSET))  # type: ignore
    main.attach(MIMEText(html, "html", "utf-8"))
    msg.attach(main)

    # attachments
    for attachment in attachments:
        mime = (
            attachment.mimetype.split("/")
            if attachment.mimetype.count("/") == 1
            else "application/octet-stream".split("/")
        )
        part = MIMEBase(*mime)
        part.set_payload(attachment.data)
        encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename={attachment.filename or 'attachment'}",
        )
        msg.attach(part)

    str_io = StringIO()
    g = Generator(str_io, False)
    g.flatten(msg)
    return str_io.getvalue()


async def get_process_attachments(
    task: LockedExternalTaskDto, paths: List[str]
) -> List[Attachment]:
    """Get process file variables as attachments."""
    attachments = []
    for name in paths:
        variable = (task.variables or {}).get(name)
        if not variable or variable.type != ValueType.File:
            continue
        value_info = variable.valueInfo or {}
        async with camunda_session() as http:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/execution/{task.executionId}/localVariables/{name}/data"
            get = await http.get(url, headers={"Accept": "application/octet-stream"})
            await verify_response_status(get, status=(200,))
            data = await get.read()
        filename = value_info.get("filename") or "attachment"
        mimetype = (
            value_info.get("mimeType")
            or value_info.get("mimetype")
            or guess_type(filename)[0]
            or "application/octet-stream"
        )
        attachments.append(Attachment(data=data, filename=filename, mimetype=mimetype))
    return attachments


async def get_entity_attachments(
    business_key: str, paths: List[str]
) -> List[Attachment]:
    """Get attachments."""
    entity_keys = []

    for path in paths:
        if path.count(".") == 1:
            entity_keys.append(path)
            entity_keys.append(path.split(".")[0] + ".metadata")
    if not paths:
        return []

    query = build_substitution_query(entity_keys)
    result = await query_substitution_values(query, business_key)

    attachments = []
    for key, value_ in result.items():
        if key not in entity_keys:
            continue
        if value_.startswith("\\x"):
            binary = binascii.unhexlify(value_[2:])
            entity, field = key.split(".", 1)
            if f"{entity}.metadata.{field}.name" in result:
                filename = result[f"{entity}.metadata.{field}.name"]
            else:
                filename = "attachment"
            if f"{entity}.metadata.{field}.type" in result:
                mimetype = result[f"{entity}.metadata.{field}.type"]
            else:
                mimetype = "application/octet-stream"
            attachments.append(
                Attachment(data=binary, filename=filename, mimetype=mimetype,)
            )

    return attachments


async def mail_send(task: LockedExternalTaskDto) -> ExternalTaskComplete:  # noqa: R9014
    """Complete task."""
    assert task.businessKey
    assert task.processInstanceId

    # "to" may contain user ids, group ids or raw email addresses
    to = [addr for addr in task_variable_list(task, "to") if addr]
    subject = task_variable_str(task, "subject")
    text = task_variable_str(task, "text")
    try:
        attachments = task_variable_list(task, "attachments")
    except KeyError:
        attachments = []
    try:
        substitutes = task_variable_list(task, "substitutes")
    except KeyError:
        substitutes = []
    templates = await safe_substitute(task, [subject, text], substitutes)
    assert len(templates) >= 2

    users, result = await query_camunda_principals(
        user_ids=to, group_ids=[addr for addr in to if "@" not in addr]
    )
    recipients = set()
    for user in users:
        try:
            email_validator.validate_email(user.email)
            recipients.add(f'"{user.firstName} {user.lastName}" <{user.email}>')
        except email_validator.EmailNotValidError:
            pass
        # remove duplicate raw user email from to
        if user.email in to:
            to.remove(user.email)

    # collect all user ids and group ids from to and add rest to recipients
    ids = [user.id for user in result.users] + [group.id for group in result.groups]
    for addr in [addr for addr in to if addr not in ids]:
        try:
            recipients.add(split_email_address(addr)[-1])
        except email_validator.EmailNotValidError:
            pass

    logger.debug("Subject: %s", subject)
    logger.debug("To: %s Candidates: %s", to, json.dumps(asdict(result), indent=2))
    logger.debug("To: %s Recipients: %s", to, json.dumps(list(recipients), indent=2))
    message = create_email(
        settings.SMTP_SENDER,
        list(recipients),
        templates[0],
        templates[1],
        (
            await get_entity_attachments(task.businessKey, attachments)
            + await get_process_attachments(task, attachments)
        ),
    )
    s = smtplib.SMTP(settings.SMTP_HOST, settings.SMTP_PORT)

    if settings.SMTP_TLS:
        s.ehlo()
        s.starttls()
    #   s.ehlo()
    #   s.login("", "")

    sender_email = split_email_address(settings.SMTP_SENDER)[-1]
    recipient_emails = [split_email_address(recipient)[-1] for recipient in recipients]
    if "%{baseUrl}" in text and result.groups:
        await ensure_read_authorization(
            task.processInstanceId,
            [group.id for group in result.groups],
            ProcessResourceType.PROCESS_INSTANCE,
        )
        await ensure_read_authorization(
            task.processInstanceId,
            [group.id for group in result.groups],
            ProcessResourceType.HISTORIC_PROCESS_INSTANCE,
        )

    assert len(recipient_emails) > 0, f"No recipients: {recipient_emails}"
    logger.debug(s.sendmail(sender_email, recipient_emails, message))

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId)
    )
