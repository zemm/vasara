"""Service configuration."""
from enum import Enum
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings."""

    VASARA_PURGE_RUNTIME_ENABLED: bool = False


class Topic(str, Enum):
    """External task topics."""

    VASARA_PURGE_RUNTIME = "vasara.purge.runtime"
    VASARA_PURGE_ENTITIES = "vasara.purge.entities"
    VASARA_PURGE_BY_BUSINESS_KEY = "vasara.purge.businessKey"
    VASARA_PURGE_BY_BUSINESS_KEY_ = "vasara.process.remove.all"
