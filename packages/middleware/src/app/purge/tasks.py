"""Service external tasks."""
from aiohttp import ClientResponse
from app.config import settings
from app.purge.config import Topic
from app.purge.types import VasaraEntityDelete
from app.purge.types import VasaraEntityDeleteResponse
from app.purge.types import VasaraEntityDeleteVariables
from app.purge.types import VasaraEntityQuery
from app.purge.types import VasaraEntityQueryResponse
from app.purge.types import VasaraEntityQueryVariables
from app.services.camunda import camunda_session
from app.services.hasura import hasura_session
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import ExternalTaskFailureDto
from app.types.camunda import HistoricProcessInstanceDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ProcessDefinitionDto
from app.types.camunda import ProcessInstanceDto
from app.types.worker import ExternalTaskComplete
from app.types.worker import ExternalTaskFailure
from app.utils import asjson
from app.utils import verify_response_status
from asyncio import Task
from fastapi.exceptions import HTTPException
from pydantic import BaseModel
from typing import List
from typing import Union
import asyncio


class RequiredStringValue(BaseModel):
    """Required Camunda string variable value."""

    value: str


class PurgeRuntimeVariables(BaseModel):
    """Purge runtime processes and their entities task variables."""

    needle: RequiredStringValue


async def purge_runtime(  # noqa: R0914
    task: LockedExternalTaskDto,
) -> ExternalTaskComplete:
    """Purge runtime processes and their entities."""
    assert settings.VASARA_PURGE_RUNTIME_ENABLED is True, "Purge not enabled"

    # Parse variables
    variables = PurgeRuntimeVariables(**(task.dict().get("variables") or {}))
    if not variables.needle.value:
        return ExternalTaskComplete(
            task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
        )

    # Fetch all running processes
    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance"
        get_processes = await http.get(url)
        await verify_response_status(get_processes, status=(200,), error_status=404)
        processes = [ProcessInstanceDto(**item) for item in await get_processes.json()]

    # Fetch related process definitions
    definition_ids = {
        process.definitionId for process in processes if process.definitionId
    }

    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-definition"
        get_definitions = await http.get(
            url, params={"processDefinitionIdIn": ",".join(definition_ids)}
        )
        await verify_response_status(get_definitions, status=(200,), error_status=404)
        definitions = [
            ProcessDefinitionDto(**item) for item in await get_definitions.json()
        ]
        keys = {definition.id: definition.key for definition in definitions}

    # Filter processes to purge
    processes = [
        process
        for process in processes
        if process.id != task.processInstanceId
        and (
            keys.get(process.definitionId)
            and variables.needle.value in (keys.get(process.definitionId) or "")
        )
    ]

    # Collect business keys to fetch related entities
    business_keys = []
    for process in processes:
        if process.businessKey and process.businessKey not in business_keys:
            business_keys.append(process.businessKey)

    # Purge related entities
    async with hasura_session() as http:
        # NOTE: We could do this with a single query instead, but because this is low
        # volume task, it should be ok to be careful and delete entities one at time.
        url = settings.VASARA_GRAPHQL_ENDPOINT
        for business_key in business_keys:
            # Get related entities
            query = VasaraEntityQuery(
                variables=VasaraEntityQueryVariables(businessKey=business_key)
            )
            post = await http.post(url, data=asjson(query))
            await verify_response_status(post, (200,))

            # Delete found entities
            deleted_entities = VasaraEntityQueryResponse(**await post.json())
            for entity in deleted_entities.data.vasara_entity:
                # Sanity check
                if entity.business_key != business_key:
                    continue
                # Delete
                entity_delete = VasaraEntityDelete(
                    variables=VasaraEntityDeleteVariables(id=entity.id)
                )
                post = await http.post(url, data=asjson(entity_delete))
                await verify_response_status(post, (200,))
                # Assert
                VasaraEntityDeleteResponse(**await post.json())

    # Stop processes
    async with camunda_session() as http:
        for process in processes:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{process.id}"
            process_delete = await http.delete(
                url, params={"skipIoMappings": "true", "skipCustomListeners": "true"}
            )
            await verify_response_status(
                process_delete, status=(204, 404), error_status=404
            )

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )


async def purge_entities(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Purge process related entities."""
    assert task.businessKey

    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT

        # Get related entities
        query = VasaraEntityQuery(
            variables=VasaraEntityQueryVariables(businessKey=task.businessKey)
        )
        post = await http.post(url, data=asjson(query))
        await verify_response_status(post, (200,))

        # Delete found entities
        deleted_entities = VasaraEntityQueryResponse(**await post.json())
        for entity in deleted_entities.data.vasara_entity:
            # Sanity check
            if entity.business_key != task.businessKey:
                continue
            # Delete
            entity_delete = VasaraEntityDelete(
                variables=VasaraEntityDeleteVariables(id=entity.id)
            )
            post = await http.post(url, data=asjson(entity_delete))
            await verify_response_status(post, (200,))
            # Assert
            VasaraEntityDeleteResponse(**await post.json())

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )


async def verify_delete_tasks(tasks: List[Task[ClientResponse]]) -> None:
    """Ensure list of delete tasks being successfully awaited."""
    if len(tasks) > 0:
        done, pending = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
        assert len(pending) == 0
        for future in done:
            await verify_response_status(future.result(), status=(204, 404))


async def _purge_data(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Purge process related entities and variables."""
    assert task.businessKey

    # Purge entities by business key
    result = await purge_entities(task)
    if not isinstance(result.response, CompleteExternalTaskDto):
        return result

    # Fetch all processes by business key
    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/process-instance"
        get_processes = await http.get(
            url, params={"processInstanceBusinessKey": task.businessKey}
        )
        await verify_response_status(get_processes, status=(200,), error_status=404)
        processes = [
            HistoricProcessInstanceDto(**item) for item in await get_processes.json()
        ]

    # Purge process variables by business key
    async with camunda_session() as http:
        for process in processes:
            if process.businessKey != task.businessKey:
                continue

            # Purge runtime variables by business key
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{process.id}/variables"
            get = await http.get(url)  # may return 404 for completed process
            variables = await get.json() if get.status == 200 else []

            tasks = []
            for name in variables:
                tasks.append(asyncio.create_task(http.delete(f"{url}/{name}")))
            await verify_delete_tasks(tasks)

            url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/process-instance"
            for i in range(0, 4):  # We have seen one call not clearing everything /o\
                await asyncio.sleep(i)
                await verify_delete_tasks(
                    [
                        asyncio.create_task(
                            http.delete(f"{url}/{process.id}/variable-instances")
                        ),
                    ]
                )

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId),
    )


async def purge_data(
    task: LockedExternalTaskDto,
) -> Union[ExternalTaskComplete, ExternalTaskFailure]:
    """Purge process related entities and variables."""
    assert task.businessKey
    try:
        return await _purge_data(task)
    except HTTPException as e:
        return ExternalTaskFailure(
            task=task,
            response=ExternalTaskFailureDto(
                workerId=task.workerId,
                errorMessage=f"{e}",
                retries=1,
                retryTimeout=60 * 1000,
            ),
        )


TASKS = {
    Topic.VASARA_PURGE_RUNTIME: purge_runtime,
    Topic.VASARA_PURGE_ENTITIES: purge_entities,
    Topic.VASARA_PURGE_BY_BUSINESS_KEY: purge_data,
    Topic.VASARA_PURGE_BY_BUSINESS_KEY_: purge_data,
}
