{ pkgs ? import ../../nix {}
, name ? "artifact"
}:

with pkgs;

let

  python = python38;
  pythonPackages = python38Packages;

  jsondiff = ps: with ps; buildPythonPackage rec {
    pname = "jsondiff";
    version = "1.3.0";
    src = fetchPypi {
      inherit pname version;
      sha256 = "0wcxb4z1kw3cxs25hyijbbfhspbwm2262di9n0nv0cd0113vy8ji";
    };
  };

  xmldiff = ps: with ps; buildPythonPackage rec {
    pname = "xmldiff";
    version = "2.4";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-Bb6iDOHyyWeGg7zODDupmB+H2StwnRkOAYvL8Efsz2M=";
    };
    propagatedBuildInputs = [ lxml setuptools six ];
  };

  keycloak = ps: with ps; buildPythonPackage rec {
    pname = "python-keycloak";
    version = "0.20.0";
    src = pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "10z5asky68gg6p5x9n7xfixxnvq273m4jab2g7b0vzncspkqa7xp";
    };
    doCheck = false;
    propagatedBuildInputs = [
      python-jose
      requests
    ];
  };

  pythonWithPackages = python.withPackages(ps: with ps; [
    requests
    click
    ldap
    (jsondiff ps)
    (keycloak ps)
    (xmldiff ps)
  ]);

  env = buildEnv {
    name = "env";
    paths = [
      bashInteractive
      coreutils
      csvkit
      curl
      findutils
      gawk
      git
      openssh
      gnused
      gitlab-runner
      gnugrep
      gnumake
      hasura-cli-full
      hostname
      jq
      postgresql
      pythonWithPackages
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --set SHELL /usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${csvkit}/bin \
  --prefix PATH : ${curl}/bin \
  --prefix PATH : ${findutils}/bin \
  --prefix PATH : ${gawk}/bin \
  --prefix PATH : ${gitlab-runner}/bin \
  --prefix PATH : ${git}/bin \
  --prefix PATH : ${openssh}/bin \
  --prefix PATH : ${gnused}/bin \
  --prefix PATH : ${gnumake}/bin \
  --prefix PATH : ${gnugrep}/bin \
  --prefix PATH : ${hasura-cli-full}/bin \
  --prefix PATH : ${hostname}/bin \
  --prefix PATH : ${jq}/bin \
  --prefix PATH : ${postgresql}/bin \
  --prefix PATH : ${pythonWithPackages}/bin \
  --prefix PATH : ${tini}/bin \
  --set HASURA_CLI_DOT_HASURA ${hasura-cli-dot-hasura}

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*-python3.8-*" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --files-from=${closure} ${hasura-cli-dot-hasura} \
  usr > $out || true
''
