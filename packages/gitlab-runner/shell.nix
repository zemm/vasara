{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    jfrog-cli
  ];
}
