{ pkgs ? import ../../nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    jfrog-cli
    jq
  ];
}
