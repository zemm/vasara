{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
}:

let

  linkFarm = name: entries: pkgs.runCommand name { preferLocalBuild = true; allowSubstitutes = false; }
  ''mkdir -p $out
    cd $out
    ${pkgs.lib.concatMapStrings (x: ''
        mkdir -p "$(dirname ${pkgs.lib.escapeShellArg x.name})"
        ln -s ${pkgs.lib.escapeShellArg x.path} ${pkgs.lib.escapeShellArg x.name}
    '') entries}
    cat > $out/net/minidev/json-smart/maven-metadata-central.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<metadata>
  <groupId>net.minidev</groupId>
  <artifactId>json-smart</artifactId>
  <versioning>
    <versions>
      <version>2.3</version>
    </versions>
  </versioning>
</metadata>
EOF
    cat > $out/com/nimbusds/lang-tag/maven-metadata-central.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<metadata>
  <groupId>com.nimbusds</groupId>
  <artifactId>lang-tag</artifactId>
  <versioning>
    <versions>
      <version>1.5</version>
    </versions>
  </versioning>
</metadata>
EOF
    cat > $out/com/nimbusds/nimbus-jose-jwt/maven-metadata-central.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<metadata>
  <groupId>com.nimbusds</groupId>
  <artifactId>nimbus-jose-jwt</artifactId>
  <versioning>
    <versions>
      <version>7.8</version>
    </versions>
  </versioning>
</metadata>
EOF
  '';

  buildMavenRepository = { dependencies }:
    let
      dependenciesAsDrv = (pkgs.lib.forEach (builtins.attrValues dependencies) (dependency: {
        drv = builtins.fetchurl {
          url = dependency.url;
          sha256 = dependency.sha256;
        };
        layout = dependency.layout;
      }));
    in linkFarm "mvn2nix-repository" (pkgs.lib.forEach dependenciesAsDrv (dependency: {
      name = dependency.layout;
      path = dependency.drv;
    }));

  buildMavenRepositoryFromLockFile = { file }:
    let
      dependencies = (builtins.fromJSON (builtins.readFile file)).dependencies;
    in buildMavenRepository { inherit dependencies; };

  mavenRepository = buildMavenRepositoryFromLockFile {
    file = ./mvn2nix-lock.json;
  };

in

pkgs.stdenv.mkDerivation rec {
  pname = "app";
  version = "0.1.0-SNAPSHOT";
  name = "${pname}-${version}.jar";
  src = pkgs.gitignoreSource ./.;

  buildInputs = with pkgs; [ jdk11_headless maven ];
  buildPhase = ''
    find . -print0|xargs -0 touch
    echo "mvn package --offline -Dmaven.repo.local=${mavenRepository}"
    mvn package --offline -Dmaven.repo.local=${mavenRepository}
  '';

  installPhase = ''
    cp app/target/${name} $out
    jar i $out
  '';
}
