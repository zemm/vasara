// Wait until the element is present. We will use a Observer for it, but you can use other methods to observe it as well
let observer = new MutationObserver(() => {
  const logoutButton = document.querySelectorAll(
    ".Header > .UserInformation .DropdownOption > button"
  )[1];

  if (logoutButton) {
    logoutButton.addEventListener("click", () => {
      window.location.href = "/logout";
    });
    observer.disconnect();
  }
});

observer.observe(document.body, {
  childList: true,
  subtree: true,
  attributes: false,
  characterData: false
});
