package fi.jyu.vasara;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.auth.DefaultAuthorizationProvider;
import org.camunda.bpm.engine.impl.context.Context;
import org.camunda.bpm.engine.impl.history.event.HistoryEvent;
import org.camunda.bpm.engine.impl.identity.Authentication;
import org.camunda.bpm.engine.impl.persistence.entity.AuthorizationEntity;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.IdentityLinkType;
import org.camunda.bpm.engine.task.Task;

import java.lang.reflect.Array;
import java.util.Date;

import static org.camunda.bpm.engine.authorization.Permissions.*;
import static org.camunda.bpm.engine.authorization.Resources.*;

public class CamundaAuthorizationProvider extends DefaultAuthorizationProvider {

    protected <T> T[] concatenate(T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }

    protected void provideRemovalTime(AuthorizationEntity authorization, ProcessInstance processInstance) {
        String rootProcessInstanceId = processInstance.getRootProcessInstanceId();

        if (rootProcessInstanceId != null) {
            authorization.setRootProcessInstanceId(rootProcessInstanceId);

            if (isHistoryRemovalTimeStrategyStart()) {
                HistoryEvent rootProcessInstance = findHistoricProcessInstance(rootProcessInstanceId);

                Date removalTime = null;
                if (rootProcessInstance != null) {
                    removalTime = rootProcessInstance.getRemovalTime();

                }

                authorization.setRemovalTime(removalTime);

            }
        }
    }

    // Process Definition //////////////////////////////////////

    public AuthorizationEntity[] newProcessDefinition(ProcessDefinition processDefinition) {
        ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();
        IdentityService identityService = processEngineConfiguration.getIdentityService();
        Authentication currentAuthentication = identityService.getCurrentAuthentication();

        if (currentAuthentication != null && currentAuthentication.getUserId() != null) {
            String userId = currentAuthentication.getUserId();
            String processDefinitionKey = processDefinition.getKey();

            AuthorizationEntity processDefinitionAuthorization = getGrantAuthorizationByUserId(userId, PROCESS_DEFINITION, processDefinitionKey);
            if (processDefinitionAuthorization == null) {
                processDefinitionAuthorization = createGrantAuthorization(userId, null, PROCESS_DEFINITION, processDefinitionKey, READ, CREATE_INSTANCE, UPDATE_INSTANCE, DELETE_INSTANCE, MIGRATE_INSTANCE );
            }

            AuthorizationEntity processInstanceAuthorization = getGrantAuthorizationByUserId(userId, PROCESS_INSTANCE, processDefinitionKey);
            if (processInstanceAuthorization  == null) {
                processInstanceAuthorization = createGrantAuthorization(userId, null, PROCESS_INSTANCE, processDefinitionKey, CREATE);
            }

            return new AuthorizationEntity[]{processDefinitionAuthorization, processInstanceAuthorization};
        }

        return null;
    }

    /**
     * <p>Invoked whenever a new process instance is started</p>
     *
     * @param processInstance the newly started process instance
     * @return a list of authorizations to be automatically added when a new
     * {@link ProcessInstance} is started.
     */
    public AuthorizationEntity[] newProcessInstance(ProcessInstance processInstance) {
        AuthorizationEntity[] authorizationEntities = super.newProcessInstance(processInstance);

        ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();
        IdentityService identityService = processEngineConfiguration.getIdentityService();
        Authentication currentAuthentication = identityService.getCurrentAuthentication();

        if (currentAuthentication != null && currentAuthentication.getUserId() != null) {
            String userId = currentAuthentication.getUserId();
            String processInstanceId = processInstance.getId();

            AuthorizationEntity processInstanceAuthorization = createAuthorization(userId, null, PROCESS_INSTANCE, processInstanceId, READ);
            AuthorizationEntity historicProcessInstanceAuthorization = createAuthorization(userId, null, HISTORIC_PROCESS_INSTANCE, processInstanceId, READ);
            provideRemovalTime(historicProcessInstanceAuthorization, processInstance);

            if (authorizationEntities == null) {
                return new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization};
            } else {
                return concatenate(authorizationEntities, new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization});
            }
        }

        return authorizationEntities;
    }

    // Task /////////////////////////////////////////////////

    /**
     * <p>Invoked whenever an user has been assigned to a task.</p>
     *
     * @param task        the task on which the assignee has been changed
     * @param oldAssignee the old assignee of the task
     * @param newAssignee the new assignee of the task
     * @return a list of authorizations to be automatically added when an
     * assignee of a task changes.
     */
    public AuthorizationEntity[] newTaskAssignee(Task task, String oldAssignee, String newAssignee) {
        AuthorizationEntity[] authorizationEntities = super.newTaskAssignee(task, oldAssignee, newAssignee);

        if (newAssignee != null) {
            String processInstanceId = task.getProcessInstanceId();
            AuthorizationEntity processInstanceAuthorization = getGrantAuthorizationByUserId(newAssignee, PROCESS_INSTANCE, processInstanceId);
            if (processInstanceAuthorization == null) {
                processInstanceAuthorization = createAuthorization(newAssignee, null, PROCESS_INSTANCE, processInstanceId, READ);
            }
            AuthorizationEntity historicProcessInstanceAuthorization = getGrantAuthorizationByUserId(newAssignee, HISTORIC_PROCESS_INSTANCE, processInstanceId);
            if (historicProcessInstanceAuthorization == null) {
                historicProcessInstanceAuthorization = createAuthorization(newAssignee, null, HISTORIC_PROCESS_INSTANCE, processInstanceId, READ);
                provideRemovalTime(historicProcessInstanceAuthorization, task);
            }
            if (authorizationEntities == null) {
                return new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization};
            } else {
                return concatenate(authorizationEntities, new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization});
            }
        }

        return authorizationEntities;
    }

    /**
     * <p>Invoked whenever an user has been set as the owner of a task.</p>
     *
     * @param task     the task on which the owner has been changed
     * @param oldOwner the old owner of the task
     * @param newOwner the new owner of the task
     * @return a list of authorizations to be automatically added when the
     * owner of a task changes.
     */
    public AuthorizationEntity[] newTaskOwner(Task task, String oldOwner, String newOwner) {
        AuthorizationEntity[] authorizationEntities = super.newTaskOwner(task, oldOwner, newOwner);

        if (newOwner != null) {
            String processInstanceId = task.getProcessInstanceId();
            AuthorizationEntity processInstanceAuthorization = getGrantAuthorizationByUserId(newOwner, PROCESS_INSTANCE, processInstanceId);
            if (processInstanceAuthorization == null) {
                processInstanceAuthorization = createAuthorization(newOwner, null, PROCESS_INSTANCE, processInstanceId, READ);
            }
            AuthorizationEntity historicProcessInstanceAuthorization = getGrantAuthorizationByUserId(newOwner, HISTORIC_PROCESS_INSTANCE, processInstanceId);
            if (historicProcessInstanceAuthorization == null) {
                historicProcessInstanceAuthorization = createAuthorization(newOwner, null, HISTORIC_PROCESS_INSTANCE, processInstanceId, READ);
                provideRemovalTime(historicProcessInstanceAuthorization, task);
            }
            if (authorizationEntities == null) {
                return new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization};
            } else {
                return concatenate(authorizationEntities, new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization});
            }
        }

        return authorizationEntities;
    }

    /**
     * <p>Invoked whenever a new user identity link has been added to a task.</p>
     *
     * @param task   the task on which a new identity link has been added
     * @param userId the user for which the identity link has been created
     * @param type   the type of the identity link (e.g. {@link IdentityLinkType#CANDIDATE})
     * @return a list of authorizations to be automatically added when
     * a new user identity link has been added.
     */
    public AuthorizationEntity[] newTaskUserIdentityLink(Task task, String userId, String type) {
        AuthorizationEntity[] authorizationEntities = super.newTaskUserIdentityLink(task, userId, type);

        if (userId != null) {
            String processInstanceId = task.getProcessInstanceId();
            AuthorizationEntity processInstanceAuthorization = getGrantAuthorizationByUserId(userId, PROCESS_INSTANCE, processInstanceId);
            if (processInstanceAuthorization == null) {
                processInstanceAuthorization = createAuthorization(userId, null, PROCESS_INSTANCE, processInstanceId, READ);
            }
            AuthorizationEntity historicProcessInstanceAuthorization = getGrantAuthorizationByUserId(userId, HISTORIC_PROCESS_INSTANCE, processInstanceId);
            if (historicProcessInstanceAuthorization == null) {
                historicProcessInstanceAuthorization = createAuthorization(userId, null, HISTORIC_PROCESS_INSTANCE, processInstanceId, READ);
                provideRemovalTime(historicProcessInstanceAuthorization, task);
            }
            if (authorizationEntities == null) {
                return new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization};
            } else {
                return concatenate(authorizationEntities, new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization});
            }
        }

        return authorizationEntities;
    }

    /**
     * <p>Invoked whenever a new group identity link has been added to a task.</p>
     *
     * @param task    the task on which a new identity link has been added
     * @param groupId the group for which the identity link has been created
     * @param type    the type of the identity link (e.g. {@link IdentityLinkType#CANDIDATE})
     * @return a list of authorizations to be automatically added when
     * a new group identity link has been added.
     */
    public AuthorizationEntity[] newTaskGroupIdentityLink(Task task, String groupId, String type) {
        AuthorizationEntity[] authorizationEntities = super.newTaskGroupIdentityLink(task, groupId, type);

        if (groupId != null) {
            String processInstanceId = task.getProcessInstanceId();
            AuthorizationEntity processInstanceAuthorization = getGrantAuthorizationByGroupId(groupId, PROCESS_INSTANCE, processInstanceId);
            if (processInstanceAuthorization == null) {
                processInstanceAuthorization = createAuthorization(null, groupId, PROCESS_INSTANCE, processInstanceId, READ);
            }
            AuthorizationEntity historicProcessInstanceAuthorization = getGrantAuthorizationByGroupId(groupId, HISTORIC_PROCESS_INSTANCE, processInstanceId);
            if (historicProcessInstanceAuthorization == null) {
                historicProcessInstanceAuthorization = createAuthorization(null, groupId, HISTORIC_PROCESS_INSTANCE, processInstanceId, READ);
                provideRemovalTime(historicProcessInstanceAuthorization, task);
            }
            if (authorizationEntities == null) {
                return new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization};
            } else {
                return concatenate(authorizationEntities, new AuthorizationEntity[]{processInstanceAuthorization, historicProcessInstanceAuthorization});
            }
        }

        return authorizationEntities;
    }
}
