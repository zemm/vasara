package fi.jyu.vasara;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.rest.security.auth.AuthenticationResult;
import org.camunda.bpm.engine.rest.security.auth.impl.ContainerBasedAuthenticationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static fi.jyu.vasara.scim.ScimIdentityUtilities.hasActiveUser;

public class OidcAuthenticationProvider extends ContainerBasedAuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(OidcAuthenticationProvider.class);

    @Override
    public AuthenticationResult extractAuthenticatedUser(HttpServletRequest request, ProcessEngine engine) {
        final OAuth2AuthenticationToken authentication = (OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            LOG.debug("OAuth2 token not found");
            return AuthenticationResult.unsuccessful();
        }

        final DefaultOidcUser userAuthentication = (DefaultOidcUser) authentication.getPrincipal();
        if (userAuthentication == null || userAuthentication.getUserInfo() == null) {
            LOG.debug("OIDC principal not found");
            return AuthenticationResult.unsuccessful();
        }

        OidcAuthenticationUtilities utils =
                CamundaApplicationContext.getBean(OidcAuthenticationUtilities.class);

        final OidcUserInfo userInfo = userAuthentication.getUserInfo();
        final String userId = !userInfo.getPreferredUsername().isEmpty()
                ? userInfo.getPreferredUsername() + (
                        !userInfo.getPreferredUsername().contains("@")
                        ? utils.vasaraOidcTenant
                        : "")
                : "";

        if (!StringUtils.isEmpty(userId) && hasActiveUser(userId, utils.processEngineConfiguration)) {
            LOG.debug("Authenticated: " + userId);
            final AuthenticationResult authenticationResult = new AuthenticationResult(userId, true);
            authenticationResult.setGroups(getUserGroups(userId, engine));
            return authenticationResult;

        } else if (!StringUtils.isEmpty(userId)) {
            LOG.debug("Denied: " + userId);
            throw new AccessDeniedException(userId);
        }

        LOG.debug("Unauthorized");
        return AuthenticationResult.unsuccessful();
    }


    private List<String> getUserGroups(String userId, ProcessEngine engine) {
        final List<String> groupIds = new ArrayList<>();
        engine.getIdentityService().createGroupQuery().groupMember(userId).list()
                .forEach(g -> groupIds.add(g.getId()));
        return groupIds;
    }

}
