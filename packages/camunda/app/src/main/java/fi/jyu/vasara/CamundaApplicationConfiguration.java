package fi.jyu.vasara;

import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.impl.form.type.LongFormType;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.camunda.bpm.spring.boot.starter.configuration.impl.AbstractCamundaConfiguration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(Ordering.DEFAULT_ORDER + 1)
public class CamundaApplicationConfiguration extends AbstractCamundaConfiguration {

    @Override
    public void preInit(SpringProcessEngineConfiguration springProcessEngineConfiguration) {
        // These are default for Camunda Spring Boot
        springProcessEngineConfiguration.setJobExecutorDeploymentAware(false);
        springProcessEngineConfiguration.setDatabaseSchemaUpdate("true");
        springProcessEngineConfiguration.setAuthorizationEnabled(true);
        //

        // Register 'json' as valid custom modeler form type
        if (springProcessEngineConfiguration.getCustomFormTypes() == null) {
            springProcessEngineConfiguration.setCustomFormTypes(new ArrayList<>());
        }
        List<AbstractFormFieldType> formTypes = springProcessEngineConfiguration.getCustomFormTypes();
        for (Object formType : formTypes) {
            if (formType instanceof LongFormType) {
                formTypes.remove(formType);
            }
        }
        formTypes.add(new CamundaJsonFormType());
        formTypes.add(new CamundaNumberFormType());

        springProcessEngineConfiguration.setEnableExceptionsAfterUnhandledBpmnError(true);

        springProcessEngineConfiguration.setUserResourceWhitelistPattern("[a-zA-Z0-9-@\\.]+");
        springProcessEngineConfiguration.setGroupResourceWhitelistPattern("[a-zA-Z0-9-]+");
        springProcessEngineConfiguration.setEnableHistoricInstancePermissions(true);
        springProcessEngineConfiguration.setResourceAuthorizationProvider(new CamundaAuthorizationProvider());
        springProcessEngineConfiguration.setHistoryEventProducer(new CamundaHistoryEventProducer());

        springProcessEngineConfiguration.setHistory(ProcessEngineConfiguration.HISTORY_FULL);
        springProcessEngineConfiguration.setHistoryCleanupBatchWindowStartTime("22:00");
        springProcessEngineConfiguration.setHistoryCleanupBatchWindowEndTime("06:00");
        springProcessEngineConfiguration.setHistoryCleanupStrategy("removalTimeBased");
        springProcessEngineConfiguration.setHistoryRemovalTimeStrategy("end");
        springProcessEngineConfiguration.setHistoryCleanupDegreeOfParallelism(1);
        springProcessEngineConfiguration.setHistoryCleanupBatchSize(100);

        springProcessEngineConfiguration.setDefaultSerializationFormat("application/json");

        // Register implicit start event listener for all processes to ensure that all variables defined
        // in start event Form -definition are initialized.
        List<BpmnParseListener> postParseListeners = springProcessEngineConfiguration.getCustomPostBPMNParseListeners();
        postParseListeners.add(new CamundaBpmnParseListener());
        springProcessEngineConfiguration.setCustomPostBPMNParseListeners(postParseListeners);
    }
}
