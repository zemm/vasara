package fi.jyu.vasara;

import org.camunda.bpm.engine.ProcessEngineException;
import org.camunda.bpm.engine.impl.form.type.LongFormType;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.DoubleValue;
import org.camunda.bpm.engine.variable.value.LongValue;
import org.camunda.bpm.engine.variable.value.TypedValue;


public class CamundaNumberFormType extends LongFormType {
    public TypedValue convertValue(TypedValue propertyValue) {
        if(propertyValue instanceof LongValue) {
            return propertyValue;
        }
        else if(propertyValue instanceof DoubleValue) {
            return propertyValue;
        }
        else {
            Object value = propertyValue.getValue();
            if(value == null) {
                return Variables.longValue(null, propertyValue.isTransient());
            }
            else if((value instanceof Double) || (value instanceof String)) {
                return Variables.doubleValue(new Double(value.toString()), propertyValue.isTransient());
            }
            else if((value instanceof Number) || (value instanceof String)) {
                return Variables.longValue(new Long(value.toString()), propertyValue.isTransient());
            }
            else {
                throw new ProcessEngineException("Value '"+value+"' is not of type Long or Double.");
            }
        }
    }
}
