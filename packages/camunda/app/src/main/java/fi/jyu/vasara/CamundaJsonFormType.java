package fi.jyu.vasara;

import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.camunda.spin.plugin.variable.SpinValues;
import org.camunda.spin.plugin.variable.value.impl.JsonValueImpl;

public class CamundaJsonFormType extends AbstractFormFieldType {

    public final static String TYPE_NAME = "json";

    public String getName() {
        return TYPE_NAME;
    }

    public TypedValue convertToModelValue(TypedValue propertyValue) {
        if (propertyValue instanceof JsonValueImpl) {
            return propertyValue;
        }
        Object value = propertyValue.getValue();
        if (value == null) {
            return SpinValues.jsonValue("null").create();
        } else {
           return SpinValues.jsonValue(value.toString()).create();
        }
    }

    public TypedValue convertToFormValue(TypedValue modelValue) {
        if(modelValue.getValue() == null) {
            return Variables.stringValue("null", modelValue.isTransient());
        } else {
            return Variables.stringValue(modelValue.getValue().toString(), modelValue.isTransient());
        }
    }

    // deprecated /////////////////////////////////////////////////

    public Object convertFormValueToModelValue(Object propertyValue) {
        if (propertyValue == null) {
           return SpinValues.jsonValue("null").create();
        } else {
           return SpinValues.jsonValue(propertyValue.toString()).create();
        }
    }

    public String convertModelValueToFormValue(Object modelValue) {
        if (modelValue == null) {
            return "null";
        } else {
            return modelValue.toString();
        }
    }

}
