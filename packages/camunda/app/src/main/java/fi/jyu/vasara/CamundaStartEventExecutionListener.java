package fi.jyu.vasara;

import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.StartFormData;

import java.util.List;

public class CamundaStartEventExecutionListener implements ExecutionListener {

    public void notify(DelegateExecution execution) throws Exception {
        ProcessEngine processEngine = execution.getProcessEngine();
        FormService formService = processEngine.getFormService();
        StartFormData startFormData = formService.getStartFormData(execution.getProcessDefinitionId());
        if (startFormData != null) {
            List<FormField> formFieldList = startFormData.getFormFields();
            for (FormField formField : formFieldList) {
                if (!execution.hasVariable(formField.getId())) {
                    execution.setVariable(formField.getId(), formField.getValue());
                }
            }
        }
    }
}
