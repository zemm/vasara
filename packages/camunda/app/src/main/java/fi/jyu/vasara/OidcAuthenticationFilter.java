package fi.jyu.vasara;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import fi.jyu.vasara.graphql.types.TransientAuthentication;
import org.camunda.bpm.engine.identity.GroupQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.converter.ClaimTypeConverter;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.StandardClaimNames;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.firewall.FirewalledRequest;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.*;

import static fi.jyu.vasara.graphql.Constants.TENANT_TRANSIENT;
import static fi.jyu.vasara.scim.ScimIdentityUtilities.hasActiveUser;

class OidcUserCache {

    private final LinkedHashSet<String> order;
    private final HashMap<String, Date> dates;
    private final HashMap<String, OidcUser> users;

    private static final int MAX_SIZE = 100;
    private static final int TTL = 60 * 1000;

    private static final Logger LOG = LoggerFactory.getLogger(OidcUserCache.class);

    public OidcUserCache() {
        order = new LinkedHashSet<>();
        dates = new HashMap<>();
        users = new HashMap<>();
    }

    public OidcUser get(String token) {
        Date inserted = dates.get(token);
        if (inserted != null) {
            Date now = new Date();
            if (now.getTime() - inserted.getTime() > TTL) {
                LOG.debug("Cached OIDC user expired");
                return null;
            } else {
                LOG.debug("Cached OIDC user returned");
                return users.get(token);
            }
        }
        return null;
    }

    public OidcUser put(String token, OidcUser user) {
        Date now = new Date();

        // purge
        if (order.size() > MAX_SIZE) {
            LOG.debug("Purging cached OIDC users");
            ArrayList<String> order_ = new ArrayList<>(order);
            String expired = order_.get(0);
            Date inserted = dates.get(expired);
            if (inserted == null || now.getTime() - inserted.getTime() > TTL) {
                order.remove(expired);
                dates.remove(expired);
                users.remove(expired);
            }
        }

        LOG.debug("Caching OIDC user");
        order.remove(token);
        order.add(token);
        dates.put(token, now);
        users.put(token, user);
        return user;
    }

}


class OidcAuthenticationFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(OidcAuthenticationFilter.class);

    private final ClientRegistration clientRegistration;
    private final OidcUserCache cache;


    public OidcAuthenticationFilter(ClientRegistration clientRegistration) {
        this.clientRegistration = clientRegistration;
        this.cache = new OidcUserCache();
    }

    public static Converter<Map<String, Object>, Map<String, Object>> claimTypeConverterFactory(
            ClientRegistration clientRegistration) {
        Map<String, Converter<Object, ?>> converters = OidcUserService.createDefaultClaimTypeConverters();
        converters.put(StandardClaimNames.SUB, source -> "");
        return new ClaimTypeConverter(converters);
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        OidcAuthenticationUtilities utils =
                CamundaApplicationContext.getBean(OidcAuthenticationUtilities.class);

        // Always clear authentication
        utils.identityService.clearAuthentication();

        // Only ever use this filter for authenticating calls on /rest or /graphql
        final String path = ((FirewalledRequest) request).getServletPath();
        if (!path.equals("/engine-rest") && !path.equals("/graphql")) {
            LOG.debug("Forbidden: Custom filter called for unsupported path");
            ((HttpServletResponse) response).sendError(
                    HttpServletResponse.SC_FORBIDDEN,
                    "Forbidden"
            );
            return;
        }

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final String authorizationHeader = httpRequest.getHeader("Authorization");
        final String hasuraRole = httpRequest.getHeader("X-Hasura-Role");

        // Support Hasura remote schema introspection query, auth and middleware queries
        if (path.equals("/graphql") &&
                utils.vasaraGraphQLSecret != null && utils.vasaraGraphQLSecret.length() > 0 &&
                (hasuraRole == null || hasuraRole.equals("admin"))) {
            // X-Vasara-GraphQL-Secret should only be used by Hasura remote schema introspection query.
            // User requests should require token and users requests through Hasura without token are
            // configured to have X-Hasura-Role: anonymous.
            final String graphQLSecret = httpRequest.getHeader("X-Vasara-GraphQL-Secret");
            // Authorization: Bearer GraphQL secret is already used by graphql-oidc-auth and middleware -services
            if (authorizationHeader == null && graphQLSecret != null && graphQLSecret.equals(utils.vasaraGraphQLSecret)) {
                LOG.debug("Authenticated: X-Vasara-GraphQL-Secret for GraphQL access");
                chain.doFilter(request, response);
                return;
            } else if (authorizationHeader != null && authorizationHeader.equals("Bearer " + utils.vasaraGraphQLSecret)) {
                LOG.debug("Authenticated: Bearer X-Vasara-GraphQL-Secret for GraphQL access");
                chain.doFilter(request, response);
                return;
            }
        }

        // Support SCIM management queries
        final String pathInfo = ((FirewalledRequest) request).getPathInfo();
        if (path.equals("/engine-rest") && pathInfo != null && pathInfo.startsWith("/scim/") &&
                utils.vasaraScimSecret != null && utils.vasaraScimSecret.length() > 0 &&
                (hasuraRole == null || hasuraRole.equals("admin"))) {
            final String scimSecret = httpRequest.getHeader("X-Vasara-Scim-Secret");
            if (authorizationHeader == null && scimSecret != null && scimSecret.equals(utils.vasaraScimSecret)) {
                LOG.debug("Authenticated: X-Vasara-Scim-Secret for Scim access");
                chain.doFilter(request, response);
                return;
            }
            if (authorizationHeader != null && authorizationHeader.equals("Bearer " + utils.vasaraScimSecret)) {
                LOG.debug("Authenticated: Bearer X-Vasara-Scim-Secret for Scim access");
                chain.doFilter(request, response);
                return;
            }
        } else if (pathInfo != null && pathInfo.startsWith("/scim/")) {
            LOG.debug("Forbidden: /scim endpoint called without shared secret");
            ((HttpServletResponse) response).sendError(
                    HttpServletResponse.SC_FORBIDDEN,
                    "Forbidden"
            );
            return;
        }

        // Support REST secret (external task client)
        if (path.equals("/engine-rest") && pathInfo != null && !pathInfo.startsWith("/scim/") &&
                utils.vasaraRestSecret != null && utils.vasaraRestSecret.length() > 0 &&
                (hasuraRole == null || hasuraRole.equals("admin"))) {
            final String restSecret = httpRequest.getHeader("X-Vasara-Rest-Secret");
            if (authorizationHeader == null && restSecret != null && restSecret.equals(utils.vasaraRestSecret)) {
                LOG.debug("Authenticated: X-Vasara-Rest-Secret for Rest access");
                chain.doFilter(request, response);
                return;
            }
            if (authorizationHeader != null && authorizationHeader.equals("Bearer " + utils.vasaraRestSecret)) {
                LOG.debug("Authenticated: Bearer X-Vasara-Rest-Secret for Rest access");
                chain.doFilter(request, response);
                return;
            }
        }

        // Support Transient user JWT
        if (authorizationHeader != null && authorizationHeader.startsWith("Transient ")) {
            final String token = authorizationHeader.replace("Transient ", "");
            try {
                JWK publicKey = JWK.parseFromPEMEncodedObjects(utils.vasaraTransientUserPublicKey);
                SignedJWT jwt = SignedJWT.parse(token);
                JWSVerifier verifier = new ECDSAVerifier((ECKey) publicKey);
                if (jwt.verify(verifier)) {
                    JWTClaimsSet claims = jwt.getJWTClaimsSet();
                    final String userId = claims.getStringClaim("id");
                    if (!StringUtils.isEmpty(userId) && userId.startsWith("guest-") && hasActiveUser(userId, utils.processEngineConfiguration)) {
                        LOG.debug("Authenticated: " + userId);
                        utils.identityService.setAuthentication(userId, getUserGroups(userId, null));
                        chain.doFilter(request, response);
                        return;

                    } else if (!StringUtils.isEmpty(userId)) {
                        LOG.debug("Denied: " + userId);
                        ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_FORBIDDEN);
                        response.setContentType("application/json;charset=UTF-8");
                        PrintWriter out = response.getWriter();
                        out.print("{\"errors\": [{\"status\": 401, \"message\": \"Expired guest user account\"}]}");
                        return;
                    }
                }
            } catch (ParseException | JOSEException e) {
                // Will raise 401 at the end
                LOG.debug("Denied: " + e.toString());
            }
        }

        // Support OIDC access token
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            final String token = authorizationHeader.replace("Bearer ", "");
            final OAuth2AccessToken accessToken = new OAuth2AccessToken(
                    OAuth2AccessToken.TokenType.BEARER,
                    token,
                    null,
                    null
            );

            // XXX: This may look sketchy, because it is. We may don't know the "subject" for the user info
            // we are looking, because we must be able to support encrypted access tokens that only the
            // user info endpoint itself can decrypt. That said, this should be ok, because we trust the IdP
            // we are calling.
            final OidcUserService oidcUserService = new OidcUserService();
            oidcUserService.setClaimTypeConverterFactory(OidcAuthenticationFilter::claimTypeConverterFactory);
            final HashMap<String, Object> claims = new HashMap<>();
            claims.put("sub", "");

            OidcUser user = this.cache.get(token);
            if (user == null) {
                final OidcUserRequest oidcUserRequest = new OidcUserRequest(
                        this.clientRegistration,
                        accessToken,
                        new OidcIdToken(token, null, null, claims)
                );
                oidcUserService.setAccessibleScopes(new HashSet<>());  // force UserInfo retrieval
                try {
                    user = oidcUserService.loadUser(oidcUserRequest);
                    this.cache.put(token, user);
                } catch (OAuth2AuthenticationException e) {
                    LOG.debug(e.toString());
                    ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.setContentType("application/json;charset=UTF-8");
                    PrintWriter out = response.getWriter();
                    out.print("{\"errors\": [{\"status\": 401, \"message\": \"Expired or invalid token\"}]}");
                    return;
                }
            }

            if (user == null) {
                LOG.debug("IdP UserInfoEndpoint unresponsive");
                ((HttpServletResponse) response).sendError(
                        HttpServletResponse.SC_GATEWAY_TIMEOUT,
                        "IdP timeout"
                );
                return;
            }

            final OidcUserInfo userInfo = user.getUserInfo();
            final String userId = !userInfo.getPreferredUsername().isEmpty()
                    ? userInfo.getPreferredUsername() + (
                            !userInfo.getPreferredUsername().contains("@")
                            ? utils.vasaraOidcTenant
                            : "")
                    : "";

            if (!StringUtils.isEmpty(userId) && hasActiveUser(userId, utils.processEngineConfiguration)) {
                LOG.debug("Authenticated: " + userId);
                utils.identityService.setAuthentication(userId, getUserGroups(userId, userInfo));
                chain.doFilter(request, response);
                return;

            } else if (!StringUtils.isEmpty(userId)) {
                LOG.debug("Authenticated: " + userId);
                // User with valid token, but missing user, is treated like a transient "guest" user,
                List<String> groupIds = getUserGroups(userId, userInfo);
                groupIds.add("camunda-transient");
                TransientAuthentication authentication = new TransientAuthentication(
                   userId, groupIds, Collections.singletonList(TENANT_TRANSIENT),
                   userInfo.getGivenName(), userInfo.getFamilyName(), userInfo.getEmail()
                );
                utils.identityService.setAuthentication(authentication);
                chain.doFilter(request, response);
                return;
            }
        }

        LOG.debug("Invalid or missing token");
        ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print("{\"errors\": [{\"status\": 401, \"message\": \"Invalid or missing token\"}]}");
    }

    private List<String> getUserGroups(String userId, OidcUserInfo userInfo) {
        final List<String> groupIds = new ArrayList<>();
        OidcAuthenticationUtilities utils =
                CamundaApplicationContext.getBean(OidcAuthenticationUtilities.class);
        utils.identityService.createGroupQuery().groupMember(userId).list()
                .forEach(g -> groupIds.add(g.getId()));
        if (userInfo != null) {
            List<String> transientGroupIds = userInfo.getClaimAsStringList("groups");
            if (transientGroupIds != null) {
                for (String groupId : userInfo.getClaimAsStringList("groups")) {
                    // XXX: JYU IdP returns groups as LDAP object paths like
                    // "cn=foo,ou=bar,ou=groups,ou=data,o=vault"
                    String groupId_ = groupId.split(",", 1)[0];
                    String[] parts = groupId_.split("=", 1);
                    if (!groupIds.contains(parts[parts.length - 1])) {
                        groupIds.add(parts[parts.length - 1]);
                    }
                }
            }
        }
        LOG.info(groupIds.toString());
        return groupIds;
    }
}
