/*
 * Copyright Camunda Services GmbH and/or licensed to Camunda Services GmbH
 * under one or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information regarding copyright
 * ownership. Camunda licenses this file to you under the Apache License,
 * Version 3.0; you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fi.jyu.vasara;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.GroupQuery;
import org.camunda.bpm.engine.identity.Tenant;
import org.camunda.bpm.engine.identity.TenantQuery;
import org.camunda.bpm.engine.migration.MigrationInstructionsBuilder;
import org.camunda.bpm.engine.migration.MigrationPlan;
import org.camunda.bpm.engine.migration.MigrationPlanExecutionBuilder;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.camunda.bpm.spring.boot.starter.event.ProcessApplicationStartedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;

import static fi.jyu.vasara.Constants.*;

@SpringBootApplication(scanBasePackages = {
        "fi.jyu.vasara",
        "fi.jyu.vasara.graphql",
})
@EnableProcessApplication
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class CamundaApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(CamundaApplication.class);

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private RepositoryService repositoryService;

    public static void main(String... args) {
        SpringApplication.run(CamundaApplication.class, args);
    }

    @EventListener
    public void handleProcessApplicationStartedEvent(ProcessApplicationStartedEvent processApplicationStartedEvent) {
        // Ensure that built-in tenants exist

        TenantQuery systemTenantQuery = identityService.createTenantQuery();
        Tenant system = systemTenantQuery.tenantId(TENANT_SYSTEM).singleResult();
        if (system == null) {
            system = identityService.newTenant(TENANT_SYSTEM);
            system.setName("Vasara internal");
            identityService.saveTenant(system);
        }

        TenantQuery transientTenantQuery = identityService.createTenantQuery();
        Tenant transient_ = transientTenantQuery.tenantId(TENANT_TRANSIENT).singleResult();
        if (transient_ == null) {
            transient_ = identityService.newTenant(TENANT_TRANSIENT);
            transient_.setName("Transient users");
            identityService.saveTenant(transient_);
        }

        GroupQuery transientGroupQuery = identityService.createGroupQuery();
        Group transient__ = transientGroupQuery.groupId(GROUP_TRANSIENT).singleResult();
        if (transient__ == null) {
            transient__ = identityService.newGroup(GROUP_TRANSIENT);
            transient__.setName("camunda BPM Guest Users");
            transient__.setType("SYSTEM");
            identityService.saveGroup(transient__);
        }
    }

    @EventListener
    private void processPostDeploy(PostDeployEvent event) {
        // Optimistic naive happy path automatic migration for SYSTEM and TRANSIENT process instances

        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.tenantIdIn(TENANT_SYSTEM).list();
        processDefinitionList.addAll(processDefinitionQuery.tenantIdIn(TENANT_TRANSIENT).list());

        for (ProcessDefinition processDefinition: processDefinitionList) {

            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            List<ProcessInstance> processInstanceList = processInstanceQuery.processDefinitionKey(processDefinition.getKey()).list();

            for (ProcessInstance processInstance: processInstanceList) {

                String processDefinitionId = processInstance.getProcessDefinitionId();
                ProcessDefinition localProcessDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();

                if (localProcessDefinition.getVersion() < processDefinition.getVersion()) {

                    List<String> processInstanceIds = new ArrayList<>();
                    processInstanceIds.add(processInstance.getId());
                    MigrationInstructionsBuilder instructionsBuilder = runtimeService.createMigrationPlan(localProcessDefinition.getId(), processDefinition.getId()).mapEqualActivities();
                    MigrationPlan migrationPlan = instructionsBuilder.updateEventTriggers().build();
                    MigrationPlanExecutionBuilder executionBuilder = runtimeService.newMigration(migrationPlan).processInstanceIds(processInstanceIds);

                    try {
                        executionBuilder.execute();
                    } catch(Exception e) {
                        LOGGER.warn(e.toString());
                    }

                }
            }
        }
    }

}
