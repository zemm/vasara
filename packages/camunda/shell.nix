{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cachix
    jdk11_headless
    jfrog-cli
    jq
    maven
    mvn2nix
  ];
}
