package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.authorization.Permissions;
import org.camunda.bpm.engine.authorization.Resources;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.identity.IdentityOperationResult;
import org.camunda.bpm.engine.impl.identity.db.DbIdentityServiceProvider;
import org.camunda.bpm.engine.impl.persistence.entity.UserEntity;

public class ScimIdentityProvider extends DbIdentityServiceProvider {

    @Override
    public IdentityOperationResult saveUser(User user) {
        UserEntity userEntity = (UserEntity) user;
        userEntity.encryptPassword();
        String operation = null;
        if (userEntity.getRevision() == 0) {
            operation = IdentityOperationResult.OPERATION_CREATE;
            this.checkAuthorization(Permissions.CREATE, Resources.USER, null);
            this.getDbEntityManager().insert(userEntity);
            this.createDefaultAuthorizations(userEntity);
        } else {
            operation = IdentityOperationResult.OPERATION_UPDATE;
            this.checkAuthorization(Permissions.UPDATE, Resources.USER, user.getId());
            this.getDbEntityManager().merge(userEntity);
        }

        return new IdentityOperationResult(userEntity, operation);
    }
}
