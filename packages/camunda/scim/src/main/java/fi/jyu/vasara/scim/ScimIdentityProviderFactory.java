package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.impl.interceptor.Session;
import org.camunda.bpm.engine.impl.interceptor.SessionFactory;

public class ScimIdentityProviderFactory implements SessionFactory {

    @Override
    public Class<?> getSessionType() {
        return ScimIdentityProvider.class;
    }

    @Override
    public Session openSession() {
        return new ScimIdentityProvider();
    }
}
