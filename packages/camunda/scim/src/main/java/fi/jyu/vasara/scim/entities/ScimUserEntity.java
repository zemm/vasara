package fi.jyu.vasara.scim.entities;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.ScimResource;
import com.unboundid.scim2.common.annotations.Schema;
import com.unboundid.scim2.common.types.EnterpriseUserExtension;
import com.unboundid.scim2.common.types.Group;
import com.unboundid.scim2.common.types.Meta;
import com.unboundid.scim2.common.utils.JsonUtils;

import javax.persistence.Entity;
import java.util.*;

@Schema(
        id = "urn:ietf:params:scim:schemas:core:2.0:User",
        name = "User",
        description = "User Account"
)
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"revisionNext", "displayName"})
@JsonPropertyOrder({"schemaUrns", "id", "externalId"})
public class ScimUserEntity extends org.camunda.bpm.engine.impl.persistence.entity.UserEntity implements ScimResource {

    private String externalId;
    private boolean active;
    private List<Group> groups;
    private EnterpriseUserExtension enterpriseExtension;

    public ScimUserEntity() {
    }

    @JsonIgnore
    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = (HashMap) super.getPersistentState();
        persistentState.put("externalId", this.externalId);
        persistentState.put("active", this.active);
        persistentState.put("groups", this.groups);

        return persistentState;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUserName() {
        return this.id;
    }

    public void setUserName(String userName) {
        // TODO: Maybe assert that userName equals id
    }

    @Override
    public Meta getMeta() {
        return new Meta();
    }

    @Override
    public void setMeta(Meta meta) {
        // XXX: Should be set and saved into JSONB column?
    }

    public boolean getActive() {
        return this.externalId == null || this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setActive(String active) {
        this.active = active == null || active.toLowerCase().equals(("true"));
    }

    public List<Group> getGroups() {
        return this.groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @JsonProperty("schemas")
    @Override
    public Collection<String> getSchemaUrns() {
        return Arrays.asList("urn:ietf:params:scim:schemas:core:2.0:User", "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User");
    }

    @Override
    public void setSchemaUrns(Collection<String> schemaUrns) {
    }

    @Override
    public GenericScimResource asGenericScimResource() {
        ObjectNode object = JsonUtils.valueToNode(this);
        return new GenericScimResource(object);
    }

    public EnterpriseUserExtension getEnterpriseExtension() {
        return this.enterpriseExtension;
    }

    public void setEnterpriseExtension(EnterpriseUserExtension enterpriseExtension) {
        this.enterpriseExtension = enterpriseExtension;
    }
}
