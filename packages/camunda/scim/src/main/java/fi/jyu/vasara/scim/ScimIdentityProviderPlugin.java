package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScimIdentityProviderPlugin implements ProcessEnginePlugin {

    @Autowired
    public ScimIdentityProviderPlugin() {

    }

    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        ScimIdentityProviderFactory identityProviderFactory = new ScimIdentityProviderFactory();
        processEngineConfiguration.setIdentityProviderSessionFactory(identityProviderFactory);
    }

    @Override
    public void postInit(ProcessEngineConfigurationImpl processEngineConfiguration) {

    }

    @Override
    public void postProcessEngineBuild(ProcessEngine processEngine) {

    }
}
