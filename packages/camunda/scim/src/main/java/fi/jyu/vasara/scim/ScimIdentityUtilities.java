package fi.jyu.vasara.scim;

import fi.jyu.vasara.scim.entities.ScimUserEntity;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;

public class ScimIdentityUtilities {

    public static Boolean hasActiveUser(String userId, ProcessEngineConfiguration processEngineConfiguration) {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        ScimUserEntity user = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<ScimUserEntity>() {
            @Override
            public ScimUserEntity execute(CommandContext commandContext) {
                return commandContext.getDbEntityManager().selectById(ScimUserEntity.class, userId);
            }
        });
        return user != null && user.getActive();
    }
}
