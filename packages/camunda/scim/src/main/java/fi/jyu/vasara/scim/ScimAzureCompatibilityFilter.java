package fi.jyu.vasara.scim;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Provider
@PreMatching
public class ScimAzureCompatibilityFilter implements ContainerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(ScimAzureCompatibilityFilter.class);

    @Override
    public void filter(ContainerRequestContext request) {
        String requestUri = request.getUriInfo().getRequestUri().toString();

        // Restrict filter scope
        if (!requestUri.contains("/scim/")) { return; }

        // Fix issue where AzureAD expects objectId to match userName
        if (requestUri.contains("Users?filter=userName+eq") && !requestUri.contains("externalId+eq")) {
            UriBuilder builder = request.getUriInfo().getRequestUriBuilder();
            MultivaluedMap<String, String> queries = request.getUriInfo().getQueryParameters();
            for (Map.Entry<String, List<String>> query : queries.entrySet()) {
                String key = query.getKey();
                List<String> values = query.getValue();
                builder.replaceQueryParam(key);
                for (String value : values) {
                    if (value.contains("userName eq") && !value.contains("externalId eq")) {
                        builder.replaceQueryParam(key, value.replaceAll(
                                "userName eq \"([^\"]+)\"",
                                "externalId eq \"$1\" or userName eq \"$1\""
                                )
                        );
                    }
                }
            }
            request.setRequestUri(builder.build());
        }

        LOG.debug(request.getMethod() + " " + request.getUriInfo().getRequestUri());

        if (request.getMediaType() != null &&
                (request.getMediaType().toString().contains("application/json") ||
                        request.getMediaType().toString().contains("application/scim+json"))) {
            try {
                String json = IOUtils.toString(request.getEntityStream(), "UTF-8");
                // Fixes https://github.com/pingidentity/scim2/issues/35
                json = json.replaceAll("\"op\":\"Add\"", "\"op\":\"add\"");
                json = json.replaceAll("\"op\":\"Remove\"", "\"op\":\"remove\"");
                json = json.replaceAll("\"op\":\"Replace\"", "\"op\":\"replace\"");
                // Work around https://github.com/pingidentity/scim2/issues/135
                json = json.replaceAll("\"op\":\"add\",\"path\":\"emails\\[([^\\]]*)\\].value\",\"value\":\"([^\"]*)\"",
                        "\"op\":\"add\",\"path\":\"emails\",\"value\":[{\"type\":\"work\",\"value\":\"$2\"}]");
                // Work around removing member removing all members, because partial SCIM patch support
                json = json.replaceAll("\"op\":\"remove\",\"path\":\"members\",\"value\":\\[\\{\"value\":\"([^\"]*)\"[^\\]]+\\]",
                        "\"op\":\"remove\",\"path\":\"members[value eq \\\\\"$1\\\\\"]\"");

                LOG.debug(json);

                InputStream in = IOUtils.toInputStream(json, "UTF-8");
                request.setEntityStream(in);
            } catch (IOException ex) {
                // We never raise
            }
        }
    }
}
