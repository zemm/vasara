package fi.jyu.vasara.scim.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.ScimResource;
import com.unboundid.scim2.common.exceptions.BadRequestException;
import com.unboundid.scim2.common.exceptions.ResourceNotFoundException;
import com.unboundid.scim2.common.exceptions.ScimException;
import com.unboundid.scim2.common.filters.Filter;
import com.unboundid.scim2.common.messages.PatchRequest;
import com.unboundid.scim2.common.types.*;
import com.unboundid.scim2.common.utils.ApiConstants;
import com.unboundid.scim2.common.utils.JsonUtils;
import com.unboundid.scim2.common.utils.Parser;
import com.unboundid.scim2.server.annotations.ResourceType;
import com.unboundid.scim2.server.utils.ResourcePreparer;
import com.unboundid.scim2.server.utils.ResourceTypeDefinition;
import com.unboundid.scim2.server.utils.SchemaAwareFilterEvaluator;
import com.unboundid.scim2.server.utils.SimpleSearchResults;
import fi.jyu.vasara.scim.ScimSessionFactory;
import fi.jyu.vasara.scim.entities.ScimUserEntity;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.unboundid.scim2.common.exceptions.BadRequestException.INVALID_VALUE;
import static com.unboundid.scim2.common.exceptions.BadRequestException.MUTABILITY;
import static com.unboundid.scim2.common.utils.ApiConstants.MEDIA_TYPE_SCIM;

/**
 * End point to access User resources.
 * <p>
 * 10.8.2020: Attributes from EnterpriseUserExtension are handled but not saved
 */
@Component
@ResourceType(
        description = "Camunda User",
        name = "User",
        schema = UserResource.class,
        optionalSchemaExtensions = EnterpriseUserExtension.class
)
@Path("/scim/Users")
public class UsersEndpoint {

    private static final ResourceTypeDefinition RESOURCE_TYPE_DEFINITION =
            ResourceTypeDefinition.fromJaxRsResource(UsersEndpoint.class);

    @Inject
    private Application application;

    @Inject
    private IdentityService identityService;

    @Inject
    private ProcessEngineConfiguration processEngineConfiguration;

    @Value("${scim2.baseUrl}")
    private String baseUri;

    /**
     * Retrieve all users with HTTP GET
     *
     * @param filterString       filter (optional)
     * @param attributes         shown attributes (optional)
     * @param excludedAttributes hidden attributes (optional)
     * @return all users that match query parameters
     * @throws ScimException      if filtering fails or requested attributes are invalid
     * @throws URISyntaxException if URI is invalid
     */
    @GET
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public SimpleSearchResults<ScimUserEntity> search(
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_FILTER) final String filterString,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_ATTRIBUTES) final String attributes,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        List<ScimUserEntity> scimUsers = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<List<ScimUserEntity>>() {
            @Override
            public List<ScimUserEntity> execute(CommandContext commandContext) {
                return (List<ScimUserEntity>) commandContext.getDbEntityManager().selectList("selectScimUsers");
            }
        });
        final SimpleSearchResults<ScimUserEntity> results =
                new SimpleSearchResults<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        for (ScimUserEntity user : scimUsers) {
            user.setGroups(convertGroups(user));
            results.add(user);
        }
        return results;
    }

    /**
     * Retrieve user with HTTP GET
     *
     * @param userId             id of retrieved user
     * @param attributes         shown attributes (optional)
     * @param excludedAttributes hidden attributes (optional)
     * @return user
     * @throws ResourceNotFoundException if no user with given id exists
     * @throws URISyntaxException        if URI is invalid
     */
    @GET
    @Path("/{id}")
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource get(
            @PathParam("id") final String userId,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_ATTRIBUTES) final String attributes,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, BadRequestException {
        ScimUserEntity scimUser = getUser(userId);
        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return resourcePreparer.trimRetrievedResource(scimUser.asGenericScimResource());
    }

    /**
     * Create user with HTTP POST. If primary email is provided, it's set as user's only email.
     * If 'active' attribute is false, user is not created. If it's true or not included in
     * request (i.e. null), user is created normally.
     *
     * @param user user
     * @return created user
     * @throws ScimException if an user with the same username already exists
     */
    @POST
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response post(
            final UserResource user,
            @Context final UriInfo uriInfo
    ) throws ScimException {
        String username = user.getUserName();
        if (username == null) throw new BadRequestException("Username is missing", INVALID_VALUE);
        if (identityService.createUserQuery().userId(username).singleResult() != null)
            throw new ScimException(409, "uniqueness", "User with username " + username + " already exists");

        User camundaUser = identityService.newUser(username);
        if (user.getEmails() != null) {
            for (Email email : user.getEmails()) {
                if (email.getPrimary()) {
                    camundaUser.setEmail(email.getValue().toLowerCase());
                    break;
                }
            }
        }

        if (user.getName() != null) {
            camundaUser.setFirstName(user.getName().getGivenName());
            camundaUser.setLastName(user.getName().getFamilyName());
        }

        identityService.saveUser(camundaUser);
        if (user.getActive() && user.getGroups() != null && !user.getGroups().isEmpty()) {
            for (Group group : user.getGroups()) {
                identityService.createMembership(username, group.getValue());
            }
        }

        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        ScimUserEntity scimUser = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<ScimUserEntity>() {
            @Override
            public ScimUserEntity execute(CommandContext commandContext) {
                ScimUserEntity scimUser = commandContext.getDbEntityManager().selectById(ScimUserEntity.class, camundaUser.getId());
                scimUser.setExternalId(user.getExternalId());
                scimUser.setActive(user.getActive());
                if (user.getActive()) {
                    scimUser.setGroups(user.getGroups());
                } else {
                    scimUser.setGroups(new ArrayList<>());
                }
                // FIXME:
                // scimUser.setEnterpriseExtension(user.getExtension(EnterpriseUserExtension.class));
                commandContext.getDbEntityManager().merge(scimUser);
                return scimUser;
            }
        });

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        GenericScimResource createdResource = resourcePreparer.trimCreatedResource(scimUser.asGenericScimResource(), user.asGenericScimResource());
        return Response.created(createdResource.getMeta().getLocation()).entity(createdResource).build();
    }

    /**
     * Replace user with HTTP PUT. All attribute values except groups are set to null
     * if not provided in request. If primary email is provided, it's set as user's only email.
     *
     * @param userId id
     * @param user   user with replacing values
     * @return user with new values
     * @throws ResourceNotFoundException if resource is not found
     * @throws URISyntaxException        if URI for any group is invalid
     */
    @PUT
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource put(
            @PathParam("id") final String userId,
            final UserResource user,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        ScimUserEntity oldUser = getUser(userId);
        oldUser.setFirstName(user.getName() == null ? null : user.getName().getGivenName());
        oldUser.setLastName(user.getName() == null ? null : user.getName().getFamilyName());
        oldUser.setExternalId(user.getExternalId());
        oldUser.setActive(user.getActive());

        // FIXME:
        // replacedUser.setEnterpriseExtension(user.getExtension(EnterpriseUserExtension.class));

        if (user.getEmails() != null) {
            for (Email email : user.getEmails()) {
                if (email.getPrimary()) {
                    oldUser.setEmail(email.getValue().toLowerCase());
                    break;
                }
            }
        } else {
            oldUser.setEmail(null);
        }

        if (user.getActive() && oldUser.getGroups().isEmpty() && user.getGroups() != null) {
            updateMemberships(userId, oldUser.getGroups(), user.getGroups());
            oldUser.setGroups(user.getGroups());
        } else if (!user.getActive()) {
            oldUser.setGroups(new ArrayList<>());
        }

        updateUserInDatabase(oldUser);

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        ScimUserEntity replacedUser = getUser(userId);
        return resourcePreparer.trimReplacedResource(oldUser.asGenericScimResource(), replacedUser.asGenericScimResource());
    }

    /**
     * Modify user with HTTP PATCH.
     *
     * @param userId       id
     * @param patchRequest patch request
     * @return modified user
     * @throws ScimException           if one or more patch operations are invalid or if attempted to change id
     * @throws URISyntaxException      if URI for any group is invalid
     * @throws JsonProcessingException if JSON processing fails
     */
    @PATCH
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource patch(
            @PathParam("id") final String userId,
            final PatchRequest patchRequest,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException, JsonProcessingException {
        ScimUserEntity user = getUser(userId);
        List<Group> oldGroups = user.getGroups();
        GenericScimResource genericResource = user.asGenericScimResource();
        ObjectNode objectNode = genericResource.getObjectNode();

        // XXX: simplify
        // create nodes for complex attributes so patch request can be applied
        ObjectNode name = JsonUtils.valueToNode(new Name().setFamilyName(user.getLastName()).setGivenName(user.getFirstName()));
        objectNode.set("name", name);
        List<Email> emailList = new ArrayList<>();
        emailList.add(new Email().setValue(user.getEmail()).setPrimary(true));
        ArrayNode emails = JsonUtils.valueToNode(emailList);
        objectNode.set("emails", emails);

        patchRequest.apply(genericResource);

        JsonNode id = objectNode.get("id");
        if (id != null && !id.asText().equals(user.getId()))
            throw new BadRequestException("Attribute 'id' is readOnly", MUTABILITY);

        // set patched values of complex attributes to corresponding atomic attributes
        objectNode.set("lastName", name.get("familyName"));
        objectNode.set("firstName", name.get("givenName"));
        if (emails == null || emails.get(0).get("value") == null) {
            user.setEmail(null);
        } else {
            Iterator<JsonNode> emailIterator = emails.iterator();
            while (emailIterator.hasNext()) {
                JsonNode entry = emailIterator.next();
                if (entry.get("primary").asBoolean()) {
                    objectNode.set("email", entry.get("value"));
                    break;
                }
            }
        }

        // remove nodes created to handle complex attributes
        objectNode.remove("name");
        objectNode.remove("emails");

        user = JsonUtils.getObjectReader().treeToValue(objectNode, ScimUserEntity.class);
        if (!user.getActive()) user.setGroups(new ArrayList<>());
        List<Group> newGroups = user.getGroups();
        updateMemberships(userId, oldGroups, newGroups);
        updateUserInDatabase(user);

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        ScimUserEntity patchedUser = getUser(userId);
        return resourcePreparer.trimModifiedResource(patchedUser.asGenericScimResource(), patchRequest);
    }

    /*, @Context final UriInfo uriInfo*
     * Delete user with HTTP DELETE
     *
     * @param userId id of deleted user
     * @throws ResourceNotFoundException if no user with given id exists
     * @throws URISyntaxException        if URI for any group is invalid
     */
    @DELETE
    @Path("/{id}")
    public Response delete(
            @PathParam("id") final String userId
    ) throws ResourceNotFoundException, URISyntaxException {
        getUser(userId); // throws exception if not found
        identityService.deleteUser(userId);
        return Response.noContent().build();
    }

    // Helpers:

    /**
     * Retrieve user from database
     *
     * @param userId id of the user
     * @return user
     * @throws ResourceNotFoundException if user is not found
     * @throws URISyntaxException        if URI for any group is invalid
     */
    public ScimUserEntity getUser(
            final String userId
    ) throws ResourceNotFoundException, URISyntaxException {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        ScimUserEntity user = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<ScimUserEntity>() {
            @Override
            public ScimUserEntity execute(CommandContext commandContext) {
                return commandContext.getDbEntityManager().selectById(ScimUserEntity.class, userId);
            }
        });

        if (user == null) throw new ResourceNotFoundException("No resource with ID " + userId);

        user.setGroups(convertGroups(user));
        return user;
    }

    /**
     * Converts user's groups from Camunda groups to SCIM groups
     *
     * @param user user
     * @return list of converted groups
     * @throws URISyntaxException if URI for any group is invalid
     */
    public List<Group> convertGroups(ScimUserEntity user) throws URISyntaxException {
        List<org.camunda.bpm.engine.identity.Group> camundaGroups = identityService.createGroupQuery().groupMember(user.getId()).list();
        if (camundaGroups.isEmpty()) return new ArrayList<>();

        List<Group> scimGroups = new ArrayList<>();
        for (org.camunda.bpm.engine.identity.Group camundaGroup : camundaGroups) {
            Group scimGroup = convertGroup(camundaGroup);
            scimGroups.add(scimGroup);
        }

        return scimGroups;
    }

    /**
     * Convert single Camunda group to a SCIM group
     *
     * @param scimGroup group
     * @return converted group
     * @throws URISyntaxException if URI is invalid
     */
    public Group convertGroup(org.camunda.bpm.engine.identity.Group scimGroup) throws URISyntaxException {
        Group group = new Group();
        String id = scimGroup.getId();
        group.setValue(id);
        group.setDisplay(id);
        group.setType(scimGroup.getType());
        group.setRef(new URI(baseUri + "/scim/Groups/" + id));

        return group;
    }

    /**
     * Filter users with query parameters
     *
     * @param users        list of all users
     * @param filterString filter expression
     * @return filtered users
     * @throws ScimException if parsing the filter string or filtering fails
     */
    public List<ScimUserEntity> filter(List<ScimUserEntity> users, String filterString) throws ScimException {
        Filter filter = Parser.parseFilter(filterString);
        SchemaAwareFilterEvaluator filterEvaluator = new SchemaAwareFilterEvaluator(RESOURCE_TYPE_DEFINITION);
        List<ScimUserEntity> foundUsers = new ArrayList<>();

        for (ScimUserEntity userEntity : users) {
            if (filter.visit(filterEvaluator, userEntity.asGenericScimResource().getObjectNode()))
                foundUsers.add(userEntity);
        }

        return foundUsers;
    }

    /**
     * Updates user's attributes in database
     *
     * @param user updated user
     */
    public void updateUserInDatabase(ScimUserEntity user) {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                commandContext.getDbEntityManager().update(ScimUserEntity.class, "replaceScimUser", user);
                return null;
            }
        });
    }

    /**
     * Removes unnecessary memberships and creates new ones
     *
     * @param userId        user id
     * @param oldGroups     list of original groups
     * @param updatedGroups list of updated groups
     */
    public void updateMemberships(String userId, List<Group> oldGroups, List<Group> updatedGroups) {
        for (Group group : oldGroups) {
            if (!updatedGroups.contains(group)) identityService.deleteMembership(userId, group.getValue());
        }

        for (Group group : updatedGroups) {
            if (!oldGroups.contains(group)) identityService.createMembership(userId, group.getValue());
        }
    }
}
