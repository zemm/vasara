package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.impl.SchemaOperationsProcessEngineBuild;
import org.camunda.bpm.engine.impl.db.PersistenceSession;
import org.camunda.bpm.engine.impl.db.sql.DbSqlSession;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;

public class ScimSchemaOperationCommand extends SchemaOperationsProcessEngineBuild {
    public Void execute(CommandContext commandContext) {
        super.execute(commandContext);

        String resourceName = "activiti.postgres.update.identity.sql";
        DbSqlSession persistenceSession = (DbSqlSession) commandContext.getSession(PersistenceSession.class);
        persistenceSession.executeSchemaResource("update", "identity", resourceName, false);

        return null;
    }
}
