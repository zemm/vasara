package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.interceptor.CommandContextInterceptor;
import org.camunda.bpm.engine.impl.interceptor.CommandInterceptor;
import org.camunda.bpm.engine.impl.interceptor.LogInterceptor;
import org.camunda.bpm.engine.impl.util.ReflectUtil;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// https://docs.camunda.org/manual/7.6/examples/tutorials/custom-queries/
// https://github.com/camunda-consulting/code/tree/master/snippets/custom-queries

public class ScimSessionFactory extends StandaloneProcessEngineConfiguration {

    private String resourceName;

    protected void init() {
        throw new IllegalArgumentException("Normal 'init' on process engine only used for extended Batis mappings is not allowed.");
    }

    public void initFromProcessEngineConfiguration(ProcessEngineConfigurationImpl processEngineConfiguration, String resourceName) {
        this.resourceName = resourceName;
        setDataSource(processEngineConfiguration.getDataSource());
        initDataSource();
        initCommandContextFactory();
        initTransactionFactory();
        initTransactionContextFactory();
        initCommandExecutors();
        initSqlSessionFactory();
        initIncidentHandlers();
        initIdentityProviderSessionFactory();
        initSessionFactories();
    }

    /**
     * In order to always open a new command context set the property
     * "alwaysOpenNew" to true inside the CommandContextInterceptor.
     * <p>
     * If you execute the custom queries inside the process engine
     * (for example in a service task), you have to do this.
     */
    @Override
    protected Collection<? extends CommandInterceptor> getDefaultCommandInterceptorsTxRequired() {
        List<CommandInterceptor> defaultCommandInterceptorsTxRequired = new ArrayList<CommandInterceptor>();
        defaultCommandInterceptorsTxRequired.add(new LogInterceptor());
        defaultCommandInterceptorsTxRequired.add(new CommandContextInterceptor(commandContextFactory, this, true));
        return defaultCommandInterceptorsTxRequired;
    }

    @Override
    protected InputStream getMyBatisXmlConfigurationSteam() {
        return ReflectUtil.getResourceAsStream(resourceName);
    }
}
