package fi.jyu.vasara.scim.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.ScimResource;
import com.unboundid.scim2.common.exceptions.BadRequestException;
import com.unboundid.scim2.common.exceptions.ResourceNotFoundException;
import com.unboundid.scim2.common.exceptions.ScimException;
import com.unboundid.scim2.common.filters.Filter;
import com.unboundid.scim2.common.messages.PatchRequest;
import com.unboundid.scim2.common.types.GroupResource;
import com.unboundid.scim2.common.types.Member;
import com.unboundid.scim2.common.utils.ApiConstants;
import com.unboundid.scim2.common.utils.JsonUtils;
import com.unboundid.scim2.common.utils.Parser;
import com.unboundid.scim2.server.annotations.ResourceType;
import com.unboundid.scim2.server.utils.ResourcePreparer;
import com.unboundid.scim2.server.utils.ResourceTypeDefinition;
import com.unboundid.scim2.server.utils.SchemaAwareFilterEvaluator;
import com.unboundid.scim2.server.utils.SimpleSearchResults;
import fi.jyu.vasara.scim.ScimSessionFactory;
import fi.jyu.vasara.scim.entities.ScimGroupEntity;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.unboundid.scim2.common.utils.ApiConstants.MEDIA_TYPE_SCIM;

/**
 * End point to access Group resources.
 */
@Component
@ResourceType(
        description = "Access Group Resources",
        name = "Group",
        schema = GroupResource.class
)
@Path("/scim/Groups")
public class GroupsEndpoint {

    private static final ResourceTypeDefinition RESOURCE_TYPE_DEFINITION =
            ResourceTypeDefinition.fromJaxRsResource(GroupsEndpoint.class);

    @Inject
    private Application application;

    @Inject
    private IdentityService identityService;

    @Inject
    private ProcessEngineConfiguration processEngineConfiguration;

    @Value("${scim2.baseUrl}")
    private String baseUri;

    /**
     * Retrieve all groups with HTTP GET
     *
     * @param filterString       filter (optional)
     * @param attributes         included attributes (optional)
     * @param excludedAttributes excluded attributes (optional)
     * @return all groups that match query parameters
     * @throws ScimException           if filtering fails or requested attributes are invalid
     * @throws URISyntaxException      if URI is invalid
     * @throws JsonProcessingException if JSON processing fails
     */
    @GET
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public SimpleSearchResults<ScimGroupEntity> search(
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_FILTER) final String filterString,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_ATTRIBUTES) final String attributes,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        List<ScimGroupEntity> scimGroups = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<List<ScimGroupEntity>>() {
            @Override
            public List<ScimGroupEntity> execute(CommandContext commandContext) {
                return (List<ScimGroupEntity>) commandContext.getDbEntityManager().selectList("selectScimGroups");
            }
        });
        final SimpleSearchResults<ScimGroupEntity> results =
                new SimpleSearchResults<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        for (ScimGroupEntity group : scimGroups) {
            group.setMembers(convertMembers(group));
            results.add(group);
        }
        return results;
    }

    /**
     * Retrieve group with HTTP GET
     *
     * @param groupId            id of retrieved group
     * @param attributes         shown attributes (optional)
     * @param excludedAttributes hidden attributes (optional)
     * @return group
     * @throws ResourceNotFoundException if no group with given id exists
     * @throws URISyntaxException        if URI is invalid
     * @throws BadRequestException       if request has invalid attributes
     * @throws JsonProcessingException   if JSON processing fails
     */
    @GET
    @Path("/{id}")
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource get(
            @PathParam("id") final String groupId,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_ATTRIBUTES) final String attributes,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, JsonProcessingException, BadRequestException {
        ScimGroupEntity group = getGroup(groupId);
        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return resourcePreparer.trimRetrievedResource(group.asGenericScimResource());
    }

    /**
     * Create group with HTTP POST. Azure AD requires a 'displayName' attribute for
     * group queries which is why group name is not set to Camunda's default 'name' attribute.
     *
     * @param group group
     * @return created group
     * @throws ScimException if a group with the same id already exists
     */
    @POST
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response post(
            GroupResource group,
            @Context final UriInfo uriInfo
    ) throws ScimException {
        String groupName = group.getDisplayName();
        if (identityService.createGroupQuery().groupId(groupName).singleResult() != null)
            throw new ScimException(409, "uniqueness", "Group with id " + groupName + " already exists");

        Group camundaGroup = identityService.newGroup(groupName);
        camundaGroup.setType("SCIM");

        identityService.saveGroup(camundaGroup);
        if (group.getMembers() != null) {
            for (Member member : group.getMembers()) {
                String userId = member.getValue();
                if (userId != null && userId.length() > 0 && identityService.createUserQuery().userId(userId).list().size() > 0) {
                    identityService.createMembership(member.getValue(), groupName);
                }
            }
        }

        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        ScimGroupEntity scimGroup = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<ScimGroupEntity>() {
            @Override
            public ScimGroupEntity execute(CommandContext commandContext) {
                ScimGroupEntity scimGroup = commandContext.getDbEntityManager().selectById(ScimGroupEntity.class, camundaGroup.getId());
                scimGroup.setDisplayName(group.getDisplayName());
                scimGroup.setExternalId(group.getExternalId());
                scimGroup.setMembers(group.getMembers());
                commandContext.getDbEntityManager().merge(scimGroup);
                return scimGroup;
            }
        });

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        GenericScimResource createdResource = resourcePreparer.trimCreatedResource(scimGroup.asGenericScimResource(), group.asGenericScimResource());
        return Response.created(createdResource.getMeta().getLocation()).entity(createdResource).build();
    }

    /**
     * Replace group with HTTP PUT. All attribute values except displayName and members are
     * set to null if not provided in request. Changing displayName is not supported.
     *
     * @param groupId id
     * @param group   group with replacing values
     * @return updated group
     * @throws ResourceNotFoundException if no group with given id exists
     * @throws URISyntaxException        if URI for any member is invalid
     */
    @PUT
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource put(
            @PathParam("id") final String groupId,
            final GroupResource group,
            @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, BadRequestException {
        ScimGroupEntity oldGroup = getGroup(groupId);

        oldGroup.setDisplayName(groupId);
        oldGroup.setExternalId(group.getExternalId());
        updateGroupInDatabase(oldGroup);

        if (oldGroup.getMembers().isEmpty() && group.getMembers() != null) {
            List<String> oldMembers = new ArrayList<>();
            if (group.getMembers () != null) {
                for (Member member : oldGroup.getMembers()) {
                    oldMembers.add(member.getValue());
                }
            }
            List<String> newMembers = new ArrayList<>();
            if (group.getMembers () != null) {
                for (Member member : group.getMembers()) {
                    String userId = member.getValue();
                    if (userId != null && userId.length() > 0 && identityService.createUserQuery().userId(userId).list().size() > 0) {
                        newMembers.add(member.getValue());
                    }
                }
            }
            updateMemberships(groupId, oldMembers, newMembers);
            oldGroup.setMembers(group.getMembers());
        }

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        ScimGroupEntity replacedGroup = getGroup(groupId);
        return resourcePreparer.trimReplacedResource(replacedGroup.asGenericScimResource(), replacedGroup.asGenericScimResource());
    }

    /**
     * Modify group with HTTP PATCH
     *
     * @param groupId      id
     * @param patchRequest patch request
     * @return modified group
     * @throws ScimException           if one or more patch operations are invalid or if attempted to change id
     * @throws URISyntaxException      if URI for any member is invalid
     * @throws JsonProcessingException if JSON processing fails
     */
    @PATCH
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public ScimResource patch(
            @PathParam("id") final String groupId,
            final PatchRequest patchRequest,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException, JsonProcessingException {
        ScimGroupEntity group = getGroup(groupId);
        List<String> oldMembers = new ArrayList<>();
        if (group.getMembers () != null) {
            for (Member member : group.getMembers()) {
                oldMembers.add(member.getValue());
            }
        }

        GenericScimResource genericResource = group.asGenericScimResource();
        patchRequest.apply(genericResource);
        updateGroupInDatabase(group);

        ObjectNode objectNode = genericResource.getObjectNode();
        JsonNode id = objectNode.get("id");
        if (!id.asText().equals(group.getId()))
            throw new BadRequestException("Attribute 'id' is readOnly", "mutability");

        group = JsonUtils.getObjectReader().treeToValue(objectNode, ScimGroupEntity.class);
        List<String> newMembers = new ArrayList<>();
        if (group.getMembers () != null) {
            for (Member member : group.getMembers()) {
                String userId = member.getValue();
                if (userId != null && userId.length() > 0 && identityService.createUserQuery().userId(userId).list().size() > 0) {
                    newMembers.add(member.getValue());
                }
            }
        }

        updateMemberships(groupId, oldMembers, newMembers);

        ResourcePreparer<GenericScimResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        ScimGroupEntity patchedGroup = getGroup(groupId);
        return resourcePreparer.trimModifiedResource(patchedGroup.asGenericScimResource(), patchRequest);
    }

    /**
     * Delete group with HTTP DELETE
     *
     * @param groupId id of deleted group
     * @throws ResourceNotFoundException if no group with given id exists
     * @throws URISyntaxException        if URI for any member is invalid
     */
    @DELETE
    @Path("/{id}")
    public Response delete(
            @PathParam("id") final String groupId
    ) throws ResourceNotFoundException, URISyntaxException {
        getGroup(groupId); // throws exception if not found
        identityService.deleteGroup(groupId);
        return Response.noContent().build();
    }

    /**
     * Retrieve group from database
     *
     * @param groupId id of retrieved group
     * @return retrieved group
     * @throws ResourceNotFoundException if no group with given id exists
     * @throws URISyntaxException        if URI for any member is invalid
     */
    public ScimGroupEntity getGroup(String groupId) throws ResourceNotFoundException, URISyntaxException {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        ScimGroupEntity group = scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<ScimGroupEntity>() {
            @Override
            public ScimGroupEntity execute(CommandContext commandContext) {
                return commandContext.getDbEntityManager().selectById(ScimGroupEntity.class, groupId);
            }
        });

        if (group == null) throw new ResourceNotFoundException("Resource " + groupId + " not found");

        group.setMembers(convertMembers(group));
        return group;
    }

    /**
     * Converts group's members from Camunda users to SCIM members
     *
     * @param group group
     * @return list of converted members
     * @throws URISyntaxException if URI for any member is invalid
     */
    public List<Member> convertMembers(ScimGroupEntity group) throws URISyntaxException {
        List<User> camundaUsers = identityService.createUserQuery().memberOfGroup(group.getId()).list();
        if (camundaUsers.isEmpty()) return new ArrayList<>();

        List<Member> scimMembers = new ArrayList<>();
        for (User camundaUser : camundaUsers) {
            Member scimMember = convertMember(camundaUser);
            scimMembers.add(scimMember);
        }

        return scimMembers;
    }

    /**
     * Convert single Camunda user to SCIM member
     *
     * @param scimUser user
     * @return converted member
     * @throws URISyntaxException if URI is invalid
     */
    public Member convertMember(User scimUser) throws URISyntaxException {
        Member member = new Member();
        String id = scimUser.getId();
        member.setValue(id);
        member.setDisplay(scimUser.getFirstName() + " " + scimUser.getLastName());
        member.setRef(new URI(baseUri + "/scim/Users/" + id));

        return member;
    }

    /**
     * Filter groups with query parameters
     *
     * @param groups       list of all groups
     * @param filterString filter
     * @return filtered groups
     * @throws ScimException if parsing the filter string or filtering fails
     */
    public List<ScimGroupEntity> filter(List<ScimGroupEntity> groups, String filterString) throws ScimException {
        Filter filter = Parser.parseFilter(filterString);
        SchemaAwareFilterEvaluator filterEvaluator = new SchemaAwareFilterEvaluator(RESOURCE_TYPE_DEFINITION);
        List<ScimGroupEntity> foundGroups = new ArrayList<>();

        for (ScimGroupEntity groupEntity : groups) {
            if (filter.visit(filterEvaluator, groupEntity.asGenericScimResource().getObjectNode()))
                foundGroups.add(groupEntity);
        }

        return foundGroups;
    }

    /**
     * Trim group's attributes based on query parameters
     *
     * @param group            group
     * @param resourcePreparer resourcepreparer containing information about resourcetype
     * @return trimmed group
     * @throws JsonProcessingException if JSON processing fails
     */
    public ScimGroupEntity trimAttributes(ScimGroupEntity group,
                                          ResourcePreparer<GenericScimResource> resourcePreparer) throws JsonProcessingException {
        GenericScimResource genericGroup = resourcePreparer.trimRetrievedResource(group.asGenericScimResource());
        group = JsonUtils.getObjectReader().treeToValue(genericGroup.getObjectNode(), ScimGroupEntity.class);

        return group;
    }

    /**
     * Trim attributes of all groups based on query parameters
     *
     * @param groups             list of groups
     * @param attributes         shown attributes
     * @param excludedAttributes hidden attributes
     * @return list of trimmed groups
     * @throws BadRequestException     if request has invalid attributes
     * @throws JsonProcessingException if JSON processing fails
     */
    public List<ScimGroupEntity> trimAll(List<ScimGroupEntity> groups,
                                         String attributes,
                                         String excludedAttributes) throws BadRequestException, JsonProcessingException, URISyntaxException {
        List<ScimGroupEntity> trimmedGroups = new ArrayList<>();
        try {
            Constructor<ResourcePreparer> constructor =
                    ResourcePreparer.class.getDeclaredConstructor(ResourceTypeDefinition.class, String.class, String.class, URI.class);
            constructor.setAccessible(true);
            try {
                ResourcePreparer<GenericScimResource> resourcePreparer = constructor.newInstance(
                        RESOURCE_TYPE_DEFINITION, attributes, excludedAttributes, new URI(baseUri));
                for (ScimGroupEntity groupEntity : groups) {
                    groupEntity = trimAttributes(groupEntity, resourcePreparer);
                    trimmedGroups.add(groupEntity);
                }
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            }
        } catch (NoSuchMethodException e) {
        }

        return trimmedGroups;
    }

    /**
     * Update group's information in database
     *
     * @param group updated group
     */
    public void updateGroupInDatabase(ScimGroupEntity group) {
        ScimSessionFactory scimSessionFactory = new ScimSessionFactory();
        scimSessionFactory.initFromProcessEngineConfiguration(((ProcessEngineConfigurationImpl) processEngineConfiguration), "mappings.xml");
        scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                commandContext.getDbEntityManager().update(ScimGroupEntity.class, "replaceScimGroup", group);
                return null;
            }
        });
    }

    /**
     * Removes unnecessary memberships and creates new ones
     *
     * @param groupId        group id
     * @param oldMembers     listo of original members
     * @param updatedMembers list of updated members
     */
    public void updateMemberships(String groupId, List<String> oldMembers, List<String> updatedMembers) {
        for (String userId : oldMembers) {
            if (!updatedMembers.contains(userId)) {
                identityService.deleteMembership(userId, groupId);
            }
        }
        for (String userId : updatedMembers) {
            if (!oldMembers.contains(userId)) {
                identityService.createMembership(userId, groupId);
            }
        }
    }
}
