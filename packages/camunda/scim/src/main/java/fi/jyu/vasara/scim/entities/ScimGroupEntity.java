package fi.jyu.vasara.scim.entities;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.ScimResource;
import com.unboundid.scim2.common.annotations.Schema;
import com.unboundid.scim2.common.types.Member;
import com.unboundid.scim2.common.types.Meta;
import com.unboundid.scim2.common.utils.JsonUtils;

import javax.persistence.Entity;
import java.util.*;

@Schema(
        id = "urn:ietf:params:scim:schemas:core:2.0:Group",
        name = "Group",
        description = "Group"
)
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("revisionNext")
@JsonPropertyOrder({"schemaUrns", "id", "externalId"})
public class ScimGroupEntity extends org.camunda.bpm.engine.impl.persistence.entity.GroupEntity implements ScimResource {

    private String externalId;
    private List<Member> members;

    public ScimGroupEntity() {
    }

    @JsonIgnore
    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = (HashMap) super.getPersistentState();
        persistentState.put("name", this.name);
        persistentState.put("externalId", this.externalId);
        persistentState.put("members", this.members);

        return persistentState;
    }

    public String getDisplayName() {
        return this.name;
    }

    public void setDisplayName(String displayName) {
        this.name = displayName;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public Meta getMeta() {
        return new Meta();
    }

    @Override
    public void setMeta(Meta meta) { }

    public List<Member> getMembers() {
        return this.members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @JsonProperty("schemas")
    @Override
    public Collection<String> getSchemaUrns() {
        return Arrays.asList("urn:ietf:params:scim:schemas:core:2.0:Group");
    }

    @Override
    public void setSchemaUrns(Collection<String> collection) {
    }

    @Override
    public GenericScimResource asGenericScimResource() {
        ObjectNode object = JsonUtils.valueToNode(this);
        return new GenericScimResource(object);
    }
}
