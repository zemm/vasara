-- Azure AD SCIM provisioning may depend on being able to save additional info on users
-- https://docs.microsoft.com/en-us/azure/active-directory/app-provisioning/use-scim-to-provision-users-and-groups

alter table ACT_ID_GROUP
    ADD COLUMN IF NOT EXISTS EXTERNAL_ID_ varchar(255);

alter table ACT_ID_USER
    ADD COLUMN IF NOT EXISTS EXTERNAL_ID_ varchar(255),
    ADD COLUMN IF NOT EXISTS ACTIVE_ boolean;
