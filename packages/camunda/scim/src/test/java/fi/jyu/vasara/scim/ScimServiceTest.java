package fi.jyu.vasara.scim;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unboundid.scim2.common.types.Email;
import com.unboundid.scim2.common.types.Name;
import com.unboundid.scim2.common.types.UserResource;
import fi.jyu.vasara.scim.endpoints.UsersEndpoint;
import fi.jyu.vasara.scim.entities.ScimUserEntity;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.db.entitymanager.DbEntityManager;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.camunda.bpm.engine.impl.interceptor.CommandExecutor;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.unboundid.scim2.common.utils.ApiConstants.MEDIA_TYPE_SCIM;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// FIXME: not up to date
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = UsersEndpoint.class)
class ScimServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ScimSessionFactory scimSessionFactory;

    @MockBean
    private IdentityService identityService;

    @Inject
    private UsersEndpoint scimService;

    @Mock
    private UserQuery userQuery;

    @Mock
    private DbEntityManager dbEntityManager;

    @Mock
    private CommandExecutor commandExecutor;

    @Mock
    private CommandContext commandContext;

    @Test
    void getUserResponseIsOk() throws Exception {
        UserResource user = createTestUser("aaltant", "Aaltonen", "Antti");
        ScimUserEntity scimUser = new ScimUserEntity();
        scimUser.setId("aaltant");

        Mockito.when(identityService.newUser("aaltant")).thenReturn(scimUser);
        Mockito.when(scimSessionFactory.getCommandExecutorTxRequired()).thenReturn(commandExecutor);
        Mockito.when(scimSessionFactory.getCommandExecutorTxRequired().execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                Mockito.when(commandContext.getDbEntityManager()).thenReturn(dbEntityManager);
                Mockito.when(commandContext.getDbEntityManager().selectById(ScimUserEntity.class, "aaltant")).thenReturn(scimUser);

                ScimUserEntity scimUser = commandContext.getDbEntityManager().selectById(ScimUserEntity.class, "aaltant");
                scimUser.setExternalId(user.getExternalId());
                commandContext.getDbEntityManager().merge(scimUser);
                return null;
            }
        })).thenReturn(null);

        // scimService.create(user);

        Mockito.when(identityService.createUserQuery()).thenReturn(userQuery);
        Mockito.when(identityService.createUserQuery().userId(user.getId())).thenReturn(userQuery);
        Mockito.when(identityService.createUserQuery().userId(user.getId()).singleResult()).thenReturn(scimUser);

        this.mockMvc.perform(get("/scim/Users/aaltant"))
                .andExpect(content().contentType(MEDIA_TYPE_SCIM))
                .andExpect(status().isOk());
    }

    @Test
    void postUserResponseIsOk() throws Exception {
        UserResource user = createTestUser("aaltant", "Aaltonen", "Antti");
        ScimUserEntity scimUser = new ScimUserEntity();
        scimUser.setId("aaltant");

        Mockito.when(identityService.newUser(user.getUserName())).thenReturn(scimUser);

        this.mockMvc.perform(post("/scim/Users")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    private UserResource createTestUser(String id, String familyName, String firstName) {
        UserResource testUser = new UserResource();
        testUser.setId(id);
        testUser.setUserName(id);

        List<Email> emails = new ArrayList<>();
        StringBuilder email = new StringBuilder();
        email.append(familyName.toLowerCase()).append(firstName.toLowerCase()).append("@test.jyu.fi");
        emails.add(new Email().setValue(email.toString()));

        testUser.setEmails(emails);
        testUser.setName(new Name().setFamilyName(familyName).setGivenName(firstName));
        testUser.setExternalId("12345");
        return testUser;
    }
}
