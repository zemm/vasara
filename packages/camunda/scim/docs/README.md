Using Azure/SCIM Postman tests as base:
https://github.com/AzureAD/SCIMReferenceCode/wiki/Test-Your-SCIM-Endpoint
https://github.com/AzureAD/SCIMReferenceCode/blob/master/PostmanCollection.json

Changes made to the collection:
* removed authorization
* added X-Hasura-Camunda-Secret headers

## Install and run tests with Newman

```bash
$ npm i newman
$ npx newman run postman_tests.json -e camunda-scim_envvar.json
```