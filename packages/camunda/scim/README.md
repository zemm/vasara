fi.jyu.vasara.scim
==================

SCIM endpoints must be registered in the final application project.

```
@Component
public class WebAppJerseyConfig extends CamundaJerseyResourceConfig {
    private static final Logger log = LoggerFactory.getLogger(WebAppJerseyConfig.class);

    protected void registerAdditionalResources() {
        super.registerAdditionalResources();
        log.info("Configuring SCIM rest api.");

        // Exception Mappers
        this.register(ScimExceptionMapper.class);
        this.register(RuntimeExceptionMapper.class);
        this.register(JsonProcessingExceptionMapper.class);

        // JSON provider
        final JacksonJsonProvider provider =
                new JacksonJsonProvider(JsonUtils.createObjectMapper());
        provider.configure(JaxRSFeature.ALLOW_EMPTY_INPUT, false);
        this.register(provider);

        // Filters
        this.register(DotSearchFilter.class);
        this.register(DefaultContentTypeFilter.class);

        // Metadata endpoints
        this.register(ResourceTypesEndpoint.class);
        this.register(SchemasEndpoint.class);
        this.register(ServiceProviderConfigEndpoint.class);

        // Resource type endpoints
        this.register(UsersEndpoint.class);
        this.register(GroupsEndpoint.class);

        log.info("Finished configuring SCIM rest api.");
    }
}
```
