package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.TransientGroup;
import fi.jyu.vasara.graphql.types.TransientUser;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.authorization.Authorization;
import org.camunda.bpm.engine.authorization.AuthorizationQuery;
import org.camunda.bpm.engine.authorization.HistoricProcessInstancePermissions;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstanceQuery;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.history.HistoricTaskInstanceQuery;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.GroupQuery;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.identity.Authentication;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.camunda.bpm.engine.authorization.Authorization.AUTH_TYPE_GRANT;
import static org.camunda.bpm.engine.authorization.Permissions.*;
import static org.camunda.bpm.engine.authorization.Resources.*;

@Component
public class UserResolver implements GraphQLResolver<User> {

    private final static Logger LOGGER = Logger.getLogger(UserResolver.class.getName());

    @Autowired
    TaskService taskService;

    @Autowired
    IdentityService identityService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    HistoryService historyService;

    @Autowired
    AuthorizationService authorizationService;

    public UserResolver() {
    }

    public String getName(User user) {
        return user.getLastName() != null && user.getFirstName() != null
                ? user.getLastName().concat(", ").concat(user.getFirstName())
                : user.getId();
    }

    public List<Task> getTasks(User user) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.taskAssignee(user.getId());
        taskQuery.initializeFormKeys();
        return taskQuery.list();
    }

    public List<HistoricTaskInstance> getHistoricTasks(User user) {
        HistoricTaskInstanceQuery taskQuery = historyService.createHistoricTaskInstanceQuery();
        taskQuery.taskAssignee(user.getId());
        return taskQuery.list();
    }

    public List<Group> getGroups(User user) {
        if (user instanceof TransientUser) {
            TransientUser transientUser = (TransientUser) user;
            List<Group> groups = new ArrayList<>();
            for (String groupId : transientUser.getGroupIds()) {
                groups.add(new TransientGroup(groupId));
            }
            return groups;
        } else {
            GroupQuery groupQuery = identityService.createGroupQuery();
            groupQuery.groupMember(user.getId());
            return groupQuery.list();
        }
    }

    // TODO: getVasaraAllowedIds and getVasaraManagedIds should be replaced by
    // connecting Hasura directly to Camunda database and performing authorization
    // checks within Hasura.

    public List<String> getVasaraAllowedIds(User user) {
        Authentication authentication = identityService.getCurrentAuthentication();
        if (authentication != null && authentication.getUserId().equals(user.getId())) {
            // Allow authenticated user ot fetch their authorizations
            try {
                identityService.clearAuthentication();
                return getVasaraAllowedIdsImpl(user);
            } finally {
                identityService.setAuthentication(authentication);
            }
        } else {
            return getVasaraAllowedIdsImpl(user);
        }
    }

    private List<String> getVasaraAllowedIdsImpl(User user) {
        TreeSet<String> processInstanceIds = new TreeSet<>();

        AuthorizationQuery userProcessInstanceAuthorizationQuery = authorizationService.createAuthorizationQuery();
        userProcessInstanceAuthorizationQuery.userIdIn(user.getId());
        userProcessInstanceAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
        userProcessInstanceAuthorizationQuery.resourceType(HISTORIC_PROCESS_INSTANCE);
        userProcessInstanceAuthorizationQuery.hasPermission(HistoricProcessInstancePermissions.READ);

        for (Authorization authorization : userProcessInstanceAuthorizationQuery.list()) {
            String resourceId = authorization.getResourceId();
            if (resourceId != null && !resourceId.equals("*")) {
                processInstanceIds.add(authorization.getResourceId());
            }
        }

        ArrayList<String> groupIds = new ArrayList<>();
        List<Group> groups = this.getGroups(user);
        for (Group group : groups) {
            groupIds.add(group.getId());
        }
        String[] groupIdsArray = groupIds.toArray(new String[0]);

        if (groupIds.size() > 0) {
            AuthorizationQuery groupProcessInstanceAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupProcessInstanceAuthorizationQuery.groupIdIn(groupIdsArray);
            groupProcessInstanceAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
            groupProcessInstanceAuthorizationQuery.resourceType(HISTORIC_PROCESS_INSTANCE);
            groupProcessInstanceAuthorizationQuery.hasPermission(HistoricProcessInstancePermissions.READ);

            for (Authorization authorization : groupProcessInstanceAuthorizationQuery.list()) {
                String resourceId = authorization.getResourceId();
                if (resourceId != null && !resourceId.equals("*")) {
                    processInstanceIds.add(authorization.getResourceId());
                }
            }
        }

        TreeSet<String> allowedIds = new TreeSet<>();

        if (processInstanceIds.size() > 0) {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            processInstanceQuery.processInstanceIds(processInstanceIds);
            for (HistoricProcessInstance processInstance : processInstanceQuery.list()) {
                String businessKey = processInstance.getBusinessKey();
                if (businessKey != null && businessKey.length() > 0) {
                    allowedIds.add(businessKey);
                }
            }
        }

        AuthorizationQuery userProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
        userProcessDefinitionAuthorizationQuery.userIdIn(user.getId());
        userProcessDefinitionAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
        userProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);
        userProcessDefinitionAuthorizationQuery.hasPermission(READ);

        for (Authorization authorization : userProcessDefinitionAuthorizationQuery.list()) {
            String resourceId = authorization.getResourceId();
            if (resourceId != null && !resourceId.equals("*")) {
                allowedIds.add(authorization.getResourceId());
            }
        }

        if (groupIds.size() > 0) {
            AuthorizationQuery groupProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupProcessDefinitionAuthorizationQuery.groupIdIn(groupIdsArray);
            groupProcessDefinitionAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
            groupProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);
            groupProcessDefinitionAuthorizationQuery.hasPermission(READ);

            for (Authorization authorization : groupProcessDefinitionAuthorizationQuery.list()) {
                String resourceId = authorization.getResourceId();
                if (resourceId != null && !resourceId.equals("*")) {
                    allowedIds.add(authorization.getResourceId());
                }
            }
        }

        if (allowedIds.size() > 0) {
            return new ArrayList<>(allowedIds);
        } else {
            return new ArrayList<>();
        }
    }

    public List<String> getVasaraManagedIds(User user) {
        Authentication authentication = identityService.getCurrentAuthentication();
        if (authentication != null && authentication.getUserId().equals(user.getId())) {
            // Allow authenticated user ot fetch their authorizations
            try {
                identityService.clearAuthentication();
                return getVasaraManagedIdsImpl(user);
            } finally {
                identityService.setAuthentication(authentication);
            }
        } else {
            return getVasaraManagedIdsImpl(user);
        }
    }

    private List<String> getVasaraManagedIdsImpl(User user) {
        TreeSet<String> managedIds = new TreeSet<>();

        AuthorizationQuery userProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
        userProcessDefinitionAuthorizationQuery.userIdIn(user.getId());
        userProcessDefinitionAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
        userProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);
        userProcessDefinitionAuthorizationQuery.hasPermission(UPDATE_INSTANCE);

        for (Authorization authorization : userProcessDefinitionAuthorizationQuery.list()) {
            String resourceId = authorization.getResourceId();
            if (resourceId != null && !resourceId.equals("*")) {
                managedIds.add(authorization.getResourceId());
            }
        }

        ArrayList<String> groupIds = new ArrayList<>();
        List<Group> groups = this.getGroups(user);
        for (Group group : groups) {
            groupIds.add(group.getId());
        }
        String[] groupIdsArray = groupIds.toArray(new String[0]);

        if (groupIds.size() > 0) {
            AuthorizationQuery groupProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupProcessDefinitionAuthorizationQuery.groupIdIn(groupIdsArray);
            groupProcessDefinitionAuthorizationQuery.authorizationType(AUTH_TYPE_GRANT);
            groupProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);
            groupProcessDefinitionAuthorizationQuery.hasPermission(UPDATE_INSTANCE);

            for (Authorization authorization : groupProcessDefinitionAuthorizationQuery.list()) {
                String resourceId = authorization.getResourceId();
                if (resourceId != null && !resourceId.equals("*")) {
                    managedIds.add(authorization.getResourceId());
                }
            }
        }

        TreeSet<String> processInstanceIds = new TreeSet<>();

        TaskQuery assigneeTaskQuery = taskService.createTaskQuery();
        assigneeTaskQuery.taskAssignee(user.getId());
        List<Task> assigneeTasks = assigneeTaskQuery.list();
        for (Task task : assigneeTasks) {
            String processInstanceId = task.getProcessInstanceId();
            if (processInstanceId != null && processInstanceId.length() > 0) {
                processInstanceIds.add(processInstanceId);
            }
        }

        if (groupIds.size() > 0) {
            TaskQuery candidateGroupTaskQuery = taskService.createTaskQuery();
            candidateGroupTaskQuery.taskCandidateGroupIn(groupIds);
            List<Task> candidateGroupTasks = candidateGroupTaskQuery.list();
            for (Task task : candidateGroupTasks) {
                String processInstanceId = task.getProcessInstanceId();
                if (processInstanceId != null && processInstanceId.length() > 0) {
                    processInstanceIds.add(processInstanceId);
                }
            }
        }

        if (processInstanceIds.size() > 0) {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            processInstanceQuery.processInstanceIds(processInstanceIds);
            for (ProcessInstance processInstance : processInstanceQuery.list()) {
                String businessKey = processInstance.getBusinessKey();
                if (businessKey != null && businessKey.length() > 0) {
                    managedIds.add(businessKey);
                }
            }
        }

        return new ArrayList<>(managedIds);
    }

    public List<Authorization> getAuthorizations(User user) {
        // NOTE: Currently limited (optimized) to process authorizations

        ArrayList<String> groupIds = new ArrayList<>();
        List<Group> groups = this.getGroups(user);
        for (Group group : groups) {
            groupIds.add(group.getId());
        }
        String[] groupIdsArray = groupIds.toArray(new String[0]);

        AuthorizationQuery userProcessInstanceAuthorizationQuery = authorizationService.createAuthorizationQuery();
        userProcessInstanceAuthorizationQuery.userIdIn(user.getId());
        userProcessInstanceAuthorizationQuery.resourceType(PROCESS_INSTANCE);

        AuthorizationQuery userProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
        userProcessDefinitionAuthorizationQuery.userIdIn(user.getId());
        userProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);

        List<Authorization> userAuthorizations = Stream.concat(
                userProcessInstanceAuthorizationQuery.list().stream(),
                userProcessDefinitionAuthorizationQuery.list().stream()
        ).collect(Collectors.toList());

        if (groupIds.size() == 0) {
            return userAuthorizations;
        } else {
            AuthorizationQuery groupProcessInstanceAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupProcessInstanceAuthorizationQuery.groupIdIn(groupIdsArray);
            groupProcessInstanceAuthorizationQuery.resourceType(PROCESS_INSTANCE);

            AuthorizationQuery groupProcessDefinitionAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupProcessDefinitionAuthorizationQuery.groupIdIn(groupIdsArray);
            groupProcessDefinitionAuthorizationQuery.resourceType(PROCESS_DEFINITION);

            List<Authorization> groupAuthorizations = Stream.concat(
                    groupProcessInstanceAuthorizationQuery.list().stream(),
                    groupProcessDefinitionAuthorizationQuery.list().stream()
            ).collect(Collectors.toList());

            return Stream.concat(
                userAuthorizations.stream(),
                groupAuthorizations.stream()
            ).collect(Collectors.toList());
        }
    }
}
