package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.identity.User;

import java.util.List;

public class TransientUser implements User {
    private final String userId;
    private final List<String> groupIds;
    private final String firstName;
    private final String lastName;
    private final String email;

    public TransientUser(String transientUserId, List<String> transientUserGroupIds, String transientUserFirstName, String transientUserLastName, String transientUserEmail ) {
        userId = transientUserId;
        groupIds = transientUserGroupIds;
        firstName = transientUserFirstName;
        lastName = transientUserLastName;
        email = transientUserEmail;
    }

    public List<String> getGroupIds() {
        return groupIds;
    }

    @Override
    public String getId() {
        return userId;
    }

    @Override
    public void setId(String s) {
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String s) {
    }

    @Override
    public void setLastName(String s) {
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setEmail(String s) {
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public void setPassword(String s) {
    }
}
