package fi.jyu.vasara.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import fi.jyu.vasara.graphql.types.*;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.authorization.Authorization;
import org.camunda.bpm.engine.authorization.AuthorizationQuery;
import org.camunda.bpm.engine.history.*;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.GroupQuery;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.identity.Authentication;
import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.camunda.bpm.engine.repository.DecisionDefinitionQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.logging.Logger;


class DescendingActivityEndDateComparator implements Comparator<HistoricActivityInstance> {
    @Override
    public int compare(HistoricActivityInstance a, HistoricActivityInstance b) {
        Date at = a.getEndTime();
        Date bt = b.getEndTime();
        if (at.after(bt)) {
            return -1;
        } else if (bt.after(at)) {
            return 1;
        } else {
            return 0;
        }
    }
}


@Component
public class Query implements GraphQLQueryResolver {

    private final static Logger LOGGER = Logger.getLogger(Query.class.getName());

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    IdentityService identityService;

    @Autowired
    AuthorizationService authorizationService;

    @Autowired
    HistoryService historyService;

    public Query() {
    }

    public ProcessDefinitionResults camunda_ProcessDefinitions(String key, String name, Integer version, Boolean suspended, Boolean startableInTasklist, Boolean latest,
                                                               Integer firstResult, Integer maxResults) {
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        processDefinitionQuery = (name != null) ? processDefinitionQuery.processDefinitionName(name) : processDefinitionQuery;
        processDefinitionQuery = (key != null) ? processDefinitionQuery.processDefinitionKey(key) : processDefinitionQuery;
        processDefinitionQuery = (version != null) ? processDefinitionQuery.processDefinitionVersion(version) : processDefinitionQuery;
        processDefinitionQuery = (suspended != null && suspended) ? processDefinitionQuery.suspended() : processDefinitionQuery;
        processDefinitionQuery = (startableInTasklist != null && startableInTasklist) ? processDefinitionQuery.startableInTasklist() : processDefinitionQuery;
        processDefinitionQuery = (latest != null && latest) ? processDefinitionQuery.latestVersion() : processDefinitionQuery;
        if (key == null) {
            processDefinitionQuery = processDefinitionQuery.withoutTenantId();
        }
        processDefinitionQuery = processDefinitionQuery.orderByProcessDefinitionName().asc();
        if (firstResult != null && maxResults != null) {
            return new ProcessDefinitionResults(
                processDefinitionQuery.listPage(firstResult, maxResults),
                processDefinitionQuery.count()
            );
        } else {
            return new ProcessDefinitionResults(
                processDefinitionQuery.list(),
                processDefinitionQuery.count()
            );
        }
    }

    public ProcessDefinition camunda_ProcessDefinition(String id) {
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        return processDefinitionQuery.processDefinitionId(id).singleResult();
    }

    public DecisionDefinitionResults camunda_DecisionDefinitions(Boolean latest, Integer firstResult, Integer maxResults) {
        DecisionDefinitionQuery definitionDefinitionQuery = repositoryService.createDecisionDefinitionQuery();
        definitionDefinitionQuery = (latest != null && latest) ? definitionDefinitionQuery.latestVersion() : definitionDefinitionQuery;
        definitionDefinitionQuery = definitionDefinitionQuery.orderByDecisionDefinitionName().asc();
        if (firstResult != null && maxResults != null) {
            return new DecisionDefinitionResults(
                definitionDefinitionQuery.listPage(firstResult, maxResults),
                definitionDefinitionQuery.count()
            );
        } else {
            return new DecisionDefinitionResults(
                definitionDefinitionQuery.list(),
                definitionDefinitionQuery.count()
            );
        }
    }

    public DecisionDefinition camunda_DecisionDefinition(String id) {
        DecisionDefinitionQuery decisionDefinitionQuery = repositoryService.createDecisionDefinitionQuery();
        return decisionDefinitionQuery.decisionDefinitionId(id).singleResult();
    }

    public ProcessInstanceResults camunda_ProcessInstances(String businessKey, List<String> ids,
                                                           Integer firstResult, Integer maxResults) {
        ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
        if (businessKey != null) {
            processInstanceQuery = processInstanceQuery.processInstanceBusinessKey(businessKey);
        } else if (ids != null){
            processInstanceQuery = processInstanceQuery.processInstanceIds(new HashSet<>(ids));
        }
        if (firstResult != null && maxResults != null) {
            return new ProcessInstanceResults(
                processInstanceQuery.listPage(firstResult, maxResults),
                processInstanceQuery.count()
            );
        } else {
            return new ProcessInstanceResults(
                processInstanceQuery.list(),
                processInstanceQuery.count()
            );
        }
    }

    public ProcessInstance camunda_ProcessInstance(String id) {
        ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
        return processInstanceQuery.processInstanceId(id).singleResult();
    }

    public List<HistoricProcessInstance> camunda_HistoricProcessInstances(String businessKey, List<String> ids) {
        if (businessKey != null) {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            processInstanceQuery = processInstanceQuery.processInstanceBusinessKey(businessKey).orderByProcessInstanceStartTime().asc();
            return processInstanceQuery.list();
        } else {
            List<HistoricProcessInstance> result = new ArrayList<>();
            if (ids != null) {
                for (String id : ids) {
                    HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
                    processInstanceQuery = processInstanceQuery.processInstanceId(id);
                    HistoricProcessInstance processInstance = processInstanceQuery.singleResult();
                    if (processInstance != null) {
                        result.add(processInstance);
                    }
                }
            }
            return result;
        }
    }

    public HistoricProcessInstance camunda_HistoricProcessInstance(String id, String businessKey, String processDefinitionKey) {
        HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
        if (id != null) {
            return processInstanceQuery.processInstanceId(id).singleResult();
        } else if (businessKey != null && processDefinitionKey != null) {
            return processInstanceQuery.processInstanceBusinessKey(businessKey).processDefinitionKey(processDefinitionKey).singleResult();
        } else {
            return null;
        }
    }

    public HistoricVariableInstance camunda_HistoricVariableInstance(String id, String name, String businessKey, String processDefinitionKey) {
        HistoricVariableInstanceQuery historicVariableInstanceQuery = historyService.createHistoricVariableInstanceQuery();
        if (id != null) {
            return historicVariableInstanceQuery.variableId(id).singleResult();
        } else if (businessKey != null && processDefinitionKey != null) {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            HistoricProcessInstance pi = processInstanceQuery.processInstanceBusinessKey(businessKey).processDefinitionKey(processDefinitionKey).singleResult();
            if (pi != null) {
                return historicVariableInstanceQuery.processInstanceId(pi.getId()).variableName(name).singleResult();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public TaskResults camunda_Tasks(String assignee, String name, String nameLike, String businessKey,
                                     Integer firstResult, Integer maxResults) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery = (assignee != null) ? taskQuery.taskAssignee(assignee) : taskQuery;
        taskQuery = (businessKey!= null) ? taskQuery.processInstanceBusinessKey(businessKey) : taskQuery;
        taskQuery = (name != null) ? taskQuery.taskName(name) : taskQuery;
        taskQuery = (nameLike != null) ? taskQuery.taskNameLike(nameLike) : taskQuery;
        taskQuery.initializeFormKeys();
        taskQuery = taskQuery.orderByTaskCreateTime().desc();
        if (firstResult != null && maxResults != null) {
            return new TaskResults(
                taskQuery.listPage(firstResult, maxResults),
                taskQuery.count()
            );
        } else {
            return new TaskResults(
                taskQuery.list(),
                taskQuery.count()
            );
        }
    }

    public Task camunda_Task(String id) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery = taskQuery.taskId(id);
        taskQuery.initializeFormKeys();
        return taskQuery.singleResult();
    }

    public List<HistoricTaskInstance> camunda_HistoricTasks(String assignee, String name, String nameLike) {
        HistoricTaskInstanceQuery taskQuery = historyService.createHistoricTaskInstanceQuery();
        taskQuery = (assignee != null) ? taskQuery.taskAssignee(assignee) : taskQuery;
        taskQuery = (name != null) ? taskQuery.taskName(name) : taskQuery;
        taskQuery = (nameLike != null) ? taskQuery.taskNameLike(nameLike) : taskQuery;
        return taskQuery.orderByHistoricTaskInstanceEndTime().asc().list();
    }

    public List<HistoricActivityInstance> camunda_HistoricActivityInstances(String businessKey, List<String> processInstanceIds) {
        List<HistoricActivityInstance> activityInstances = new ArrayList<>();
        if (businessKey != null) {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            processInstanceQuery = processInstanceQuery.processInstanceBusinessKey(businessKey);
            if (processInstanceIds == null) {
                processInstanceIds = new ArrayList<>();
            }
            for (HistoricProcessInstance historicProcessInstance: processInstanceQuery.list()) {
                processInstanceIds.add(historicProcessInstance.getId());
            }
        }
        if (processInstanceIds != null) {
            for (String processInstanceId : processInstanceIds) {
                HistoricActivityInstanceQuery activityQuery = historyService.createHistoricActivityInstanceQuery();
                activityQuery.processInstanceId(processInstanceId).finished();
                activityInstances.addAll(activityQuery.list());
            }
        }
        activityInstances.sort(new DescendingActivityEndDateComparator());
        return activityInstances;
    }

    public HistoricTaskInstance camunda_HistoricTask(String id) {
        HistoricTaskInstanceQuery taskQuery = historyService.createHistoricTaskInstanceQuery();
        taskQuery = taskQuery.taskId(id);
        return taskQuery.singleResult();
    }

    public List<Comment> camunda_Comments(String id) {
        return taskService.getTaskComments(id);
    }

    public Comment camunda_Comment(String id, String commentId) {
        return taskService.getTaskComment(id, commentId);
    }

    public List<User> camunda_Users(List<String> ids, String firstName, String firstNameLike, String lastName, String lastNameLike, String email, String emailLike, String groupId, Integer limit, Integer offset) {
        if (ids != null) {
            List<User> users = new ArrayList<>();
            for (String id : ids) {
                User user = camunda_User(id);
                if (user != null) {
                    users.add(user);
                }
            }
            return users;
        } else {
            UserQuery query = identityService.createUserQuery();
            query = (firstName != null && firstName.length() > 0) ? query.userFirstName(firstName) : query;
            query = (firstNameLike != null && firstNameLike.length() > 0) ? query.userFirstNameLike(firstNameLike) : query;
            query = (lastName != null && lastName.length() > 0) ? query.userLastName(lastName) : query;
            query = (lastNameLike != null && lastNameLike.length() > 0) ? query.userLastNameLike(lastNameLike) : query;
            query = (email != null && email.length() > 0) ? query.userEmail(email) : query;
            query = (emailLike != null && emailLike.length() > 0 ) ? query.userEmailLike(emailLike) : query;
            query = (groupId != null && groupId.length() > 0) ? query.memberOfGroup(groupId) : query;
            return query.listPage(offset != null ? offset : 0, limit != null ? limit : 25);
        }
    }

    public User camunda_User(String id) {
        if (id != null) {
            return identityService.createUserQuery().userId(id).singleResult();
        } else {
            return null;
        }
    }

    public User camunda_AuthenticatedUser() {
        Authentication authentication = identityService.getCurrentAuthentication();
        if (authentication != null) {
            User user = identityService.createUserQuery().userId(authentication.getUserId()).singleResult();
            if (user != null) {
                return user;
            } else if (authentication instanceof TransientAuthentication) {
                TransientAuthentication transientAuthentication = (TransientAuthentication) authentication;
                return new TransientUser(
                    transientAuthentication.getUserId(),
                    transientAuthentication.getGroupIds(),
                    transientAuthentication.getFirstName(),
                    transientAuthentication.getLastName(),
                    transientAuthentication.getEmail()
                );
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<Group> camunda_Groups(List<String> ids, String groupName, String groupNameLike, String groupType, Integer limit, Integer offset) {
        if (ids != null) {
            List<Group> groups = new ArrayList<>();
            for (String id : ids) {
                Group group = camunda_Group(id);
                if (group != null) {
                    groups.add(group);
                }
            }
            return groups;
        } else {
            GroupQuery query = identityService.createGroupQuery();
            query = (groupName != null && groupName.length() > 0) ? query.groupName(groupName) : query;
            query = (groupNameLike != null && groupNameLike.length() > 0) ? query.groupNameLike(groupNameLike) : query;
            query = (groupType != null && groupType.length() > 0) ? query.groupType(groupType) : query;
            return query.listPage(offset != null ? offset : 0, limit != null ? limit : 25);
        }
    }

    public Group camunda_Group(String groupId) {
        if (groupId != null) {
            return identityService.createGroupQuery().groupId(groupId).singleResult();
        } else {
            return null;
        }
    }

    public List<Authorization> camunda_Authorizations(List<String> userIdIn, List<String> groupIdIn, List<String> resourceIdIn) {
        List<Authorization> authorizations = new ArrayList<>();

        // Update groupIdIn from userIdIn group memberships
        if (groupIdIn == null) {
            groupIdIn = new ArrayList<>();
        }
        if (userIdIn != null && userIdIn.size() > 0) {
            for (String userId : userIdIn) {
                GroupQuery groupQuery = identityService.createGroupQuery().groupMember(userId);
                for (Group group : groupQuery.list()) {
                    String groupId = group.getId();
                    if (!groupIdIn.contains(groupId)) {
                        groupIdIn.add(groupId);
                    }
                }
            }
        }

        // Query authorizations for userIdIn or userIdIn AND resourceIdIn
        if (userIdIn != null && userIdIn.size() > 0) {
            String[] userIds = userIdIn.toArray(new String[0]);
            AuthorizationQuery userAuthorizationQuery = authorizationService.createAuthorizationQuery();
            userAuthorizationQuery = userAuthorizationQuery.userIdIn(userIds);
            if (resourceIdIn != null && resourceIdIn.size() > 0) {
                for (String resourceId : resourceIdIn) {
                    authorizations.addAll(userAuthorizationQuery.resourceId(resourceId).list());
                }
            } else {
                authorizations.addAll(userAuthorizationQuery.list());
            }
        }

        // AND query authorizations for groupIdIn or groupIdIn AND resourceIdIn
        if (groupIdIn.size() > 0) {
            String[] groupIds = groupIdIn.toArray(new String[0]);
            AuthorizationQuery groupAuthorizationQuery = authorizationService.createAuthorizationQuery();
            groupAuthorizationQuery = groupAuthorizationQuery.groupIdIn(groupIds);
            if (resourceIdIn != null && resourceIdIn.size() > 0) {
                for (String resourceId : resourceIdIn) {
                    authorizations.addAll(groupAuthorizationQuery.resourceId(resourceId).list());
                }
            } else {
                authorizations.addAll(groupAuthorizationQuery.list());
            }
        }

        // OR query authorizations for just resourceIdIn
        if ((userIdIn == null || userIdIn.size() == 0) && groupIdIn.size() == 0) {
            AuthorizationQuery authorizationQuery = authorizationService.createAuthorizationQuery();
            if (resourceIdIn != null && resourceIdIn.size() > 0) {
                for (String resourceId : resourceIdIn) {
                    authorizations.addAll(authorizationQuery.resourceId(resourceId).list());
                }
            }
        }

        return authorizations;
    }

    public Authorization camunda_Authorization(String id) {
        AuthorizationQuery authorizationQuery = authorizationService.createAuthorizationQuery();
        return authorizationQuery.authorizationId(id).singleResult();
    }
}
