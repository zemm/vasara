package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcessInstanceResults {

    private List<ProcessInstance> data = null;
    private Long total = null;

    public ProcessInstanceResults() {
    }

    public ProcessInstanceResults(List<ProcessInstance> data, Long total) {
        this.data = data;
        this.total = total;
    }

    public List<ProcessInstance> getData () {
        return data;
    }

    public Long getTotal() {
        return total;
    }
}
