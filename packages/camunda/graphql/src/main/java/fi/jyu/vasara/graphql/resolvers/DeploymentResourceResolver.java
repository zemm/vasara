package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.DeploymentResource;
import org.springframework.stereotype.Component;

@Component
public class DeploymentResourceResolver implements GraphQLResolver<DeploymentResource> {

    public DeploymentResourceResolver() {
    }

}
