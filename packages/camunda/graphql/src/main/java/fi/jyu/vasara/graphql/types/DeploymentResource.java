package fi.jyu.vasara.graphql.types;

import org.springframework.stereotype.Component;

@Component
public class DeploymentResource {

    private String name = null;
    private String payload = null;

    public DeploymentResource() {
    }

    public DeploymentResource(String name, String payload) {
        this.name = name;
        this.payload = payload;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getName() {
        return name;
    }

    public String getPayload() {
        return payload;
    }
}
