package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.ProcessDefinitionResults;
import org.springframework.stereotype.Component;

@Component
public class ProcessDefinitionResultsResolver implements GraphQLResolver<ProcessDefinitionResults> {
}
