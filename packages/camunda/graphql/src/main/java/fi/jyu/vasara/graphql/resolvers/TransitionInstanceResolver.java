package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransitionInstanceResolver implements GraphQLResolver<TransitionInstance> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    public TransitionInstanceResolver() {
    }

    public ProcessInstance getProcessInstance(TransitionInstance transitionInstance) {
        String processInstanceId = transitionInstance.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public ProcessDefinition getProcessDefinition(TransitionInstance transitionInstance) {
        String processDefinitionId = transitionInstance.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public Execution getExecution(TransitionInstance transitionInstance) {
        String executionId = transitionInstance.getExecutionId();
        if (executionId == null) {
            return null;
        } else {
            ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
            return executionQuery.executionId(executionId).singleResult();
        }
    }

    public List<Incident> getIncidents(TransitionInstance transitionInstance) {
        String[] incidentIds = transitionInstance.getIncidentIds();
        if (incidentIds == null) {
            return new ArrayList<>();
        } else {
            List<Incident> incidents = new ArrayList<>();
            for (String incidentId : incidentIds) {
                IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
                Incident incident = incidentQuery.incidentId(incidentId).singleResult();
                if (incident != null) {
                    incidents.add(incident);
                }
            }
            return incidents;
        }
    }

}
