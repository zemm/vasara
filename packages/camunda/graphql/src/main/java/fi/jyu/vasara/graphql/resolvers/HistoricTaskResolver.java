package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.Util;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.history.*;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.impl.VariableMapImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;

@Component
public class HistoricTaskResolver implements GraphQLResolver<HistoricTaskInstance> {

    private final static Logger LOGGER = Logger.getLogger(HistoricTaskResolver.class.getName());

    @Autowired
    HistoryService historyService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    IdentityService identityService;

    public HistoricTaskResolver() {
    }

    public ProcessDefinition getProcessDefinition(HistoricTaskInstance task) {
        String processDefinitionId = task.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }


    public HistoricProcessInstance getProcessInstance(HistoricTaskInstance task) {
        String processInstanceId = task.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery historicProcessInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return historicProcessInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public User getOwner(HistoricTaskInstance task) {
        String userId = task.getOwner();
        if (userId == null) {
            return null;
        } else {
            UserQuery userQuery = identityService.createUserQuery();
            return userQuery.userId(userId).singleResult();
        }
    }

    public String getOwnerId(HistoricTaskInstance task) {
        return task.getOwner();
    }

    public User getAssignee(HistoricTaskInstance task) {
        String userId = task.getAssignee();
        if (userId == null) {
            return null;
        } else {
            UserQuery userQuery = identityService.createUserQuery();
            return userQuery.userId(userId).singleResult();
        }
    }

    public String getAssigneeId(HistoricTaskInstance task) {
        return task.getAssignee();
    }

    public String getStartTime(HistoricTaskInstance task) {
        Date startTime = task.getStartTime();
        if (startTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(startTime);
        }
    }

    public String getEndTime(HistoricTaskInstance task) {
        Date endTime = task.getEndTime();
        if (endTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(endTime);
        }
    }

    public Long getDuration(HistoricTaskInstance task) {
        return task.getDurationInMillis();
    }

    public String getDue(HistoricTaskInstance task) {
        Date dueDate = task.getDueDate();
        if (dueDate == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(dueDate);
        }
    }

    public String getFollowUp(HistoricTaskInstance task) {
        Date followUpDate = task.getFollowUpDate();
        if (followUpDate == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(followUpDate);
        }
    }

    public List<KeyValuePair> getVariables(HistoricTaskInstance task) {
        List<KeyValuePair> keyValuePairs;

        String activityInstanceId = task.getActivityInstanceId();
        if (activityInstanceId == null) {
            return null;
        }

        HistoricDetailQuery historicDetailQuery = historyService.createHistoricDetailQuery();
        List<HistoricDetail> details = historicDetailQuery.activityInstanceId(activityInstanceId).orderByTime().asc().list();
        VariableMap variableMap = new VariableMapImpl();
        for (HistoricDetail detail: details) {
            if (detail instanceof HistoricDetailVariableInstanceUpdateEntity) {
                HistoricDetailVariableInstanceUpdateEntity variable = (HistoricDetailVariableInstanceUpdateEntity) detail;
                variableMap.putValueTyped(variable.getName(), variable.getTypedValue());
            }
        }
        keyValuePairs = Util.getKeyValuePairs(variableMap);

        return keyValuePairs;
    }

}
