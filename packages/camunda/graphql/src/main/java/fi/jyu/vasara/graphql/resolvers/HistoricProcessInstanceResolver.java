package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.Util;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.*;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.Incident;
import org.camunda.bpm.engine.runtime.IncidentQuery;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.impl.VariableMapImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class HistoricProcessInstanceResolver implements GraphQLResolver<HistoricProcessInstance> {

    @Autowired
    IdentityService identityService;

    @Autowired
    HistoryService historyService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    public HistoricProcessInstanceResolver() {
    }

    public ProcessDefinition getProcessDefinition(HistoricProcessInstance processInstance) {
        String processDefinitionId = processInstance.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processInstance.getProcessDefinitionId()).singleResult();
        }
    }

    public String getStartTime(HistoricProcessInstance processInstance) {
        Date startTime = processInstance.getStartTime();
        if (startTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(startTime);
        }
    }

    public String getEndTime(HistoricProcessInstance processInstance) {
        Date endTime = processInstance.getEndTime();
        if (endTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(endTime);
        }
    }

    public String getRemovalTime(HistoricProcessInstance processInstance) {
        Date removalTime = processInstance.getRemovalTime();
        if (removalTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(removalTime);
        }
    }

    public User getStartUser(HistoricProcessInstance processInstance) {
        String userId = processInstance.getStartUserId();
        if (userId == null) {
            return null;
        } else {
            return identityService.createUserQuery().userId(userId).singleResult();
        }
    }

    public List<HistoricActivityInstance> getActivityInstances(HistoricProcessInstance processInstance) {
        HistoricActivityInstanceQuery historicActivityInstanceQuery = historyService.createHistoricActivityInstanceQuery();
        return historicActivityInstanceQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Incident> getIncidents(HistoricProcessInstance processInstance) {
        IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
        return incidentQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<KeyValuePair> getVariables(HistoricProcessInstance processInstance) {
        List<KeyValuePair> keyValuePairs;

        String processInstanceId = processInstance.getId();
        if (processInstanceId == null) {
            return null;
        }

        HistoricVariableInstanceQuery historicVariableInstanceQuery = historyService.createHistoricVariableInstanceQuery();
        List<HistoricVariableInstance> variables = historicVariableInstanceQuery.processInstanceId(processInstanceId).list();
        VariableMap variableMap = new VariableMapImpl();
        for (HistoricVariableInstance variable : variables) {
            variableMap.putValueTyped(variable.getName(), variable.getTypedValue());
        }
        keyValuePairs = Util.getKeyValuePairs(variableMap);

        return keyValuePairs;
    }

    public HistoricProcessInstance getTopProcessInstance(HistoricProcessInstance processInstance) {
        List<HistoricProcessInstance> processInstances = getDomProcessInstances(processInstance);
        if (processInstances.size() == 0) {
            return processInstance;
        } else {
            return processInstances.get(processInstances.size() - 1);
        }
    }

    public HistoricProcessInstance getRootProcessInstance(HistoricProcessInstance processInstance) {
        String processInstanceId = processInstance.getRootProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public List<HistoricProcessInstance> getDomProcessInstances(HistoricProcessInstance processInstance) {
        List<HistoricProcessInstance> processInstances = new ArrayList<>();
        int[] depth = {1, 2, 3, 4, 5};
        for (int ignored : depth) {
            String processInstanceId = processInstance.getId();
            processInstance = getRootProcessInstance(processInstance);
            if (processInstance == null || processInstanceId.equals(processInstance.getId())) {
                break;
            } else {
                processInstances.add(processInstance);
            }
        }
        return processInstances;
    }

    public List<HistoricProcessInstance> getSubProcessInstances(HistoricProcessInstance processInstance) {
        String processInstanceId = processInstance.getId();
        if (processInstanceId  == null) {
            return new ArrayList<>();
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.superProcessInstanceId(processInstanceId).list();
        }
    }
}
