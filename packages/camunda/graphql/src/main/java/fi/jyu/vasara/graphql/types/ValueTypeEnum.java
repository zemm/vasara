package fi.jyu.vasara.graphql.types;


import org.camunda.bpm.engine.variable.type.ValueType;

/**
 * Created by danielvogel on 23.06.17.
 */

public enum ValueTypeEnum {
    OBJECT,
    INT,
    FLOAT, /* camunda has no float type? */
    STRING,
    ENUM,
    BOOLEAN,
    LONG,
    DATE,
    DOUBLE,
    JSON;

    public static ValueTypeEnum from(ValueType type) {
        switch (type.getName()) {
            case "object": return OBJECT;
            case "int": return INT;
            case "long": return LONG;
            case "double": return DOUBLE;
            case "string": return STRING;
            case "enum": return ENUM;
            case "boolean": return BOOLEAN;
            case "date": return DATE;
            case "json": return JSON;
            default: break;
        }
        return null;
    }
}
