package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ActivityInstanceResolver implements GraphQLResolver<ActivityInstance> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    public ActivityInstanceResolver() {
    }

    public ProcessInstance getProcessInstance(ActivityInstance activityInstance) {
        String processInstanceId = activityInstance.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public ProcessDefinition getProcessDefinition(ActivityInstance activityInstance) {
        String processDefinitionId = activityInstance.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public List<Execution> getExecutions(ActivityInstance activityInstance) {
        String[] executionIds = activityInstance.getExecutionIds();
        if (executionIds == null) {
            return new ArrayList<>();
        } else {
            List<Execution> executions = new ArrayList<>();
            for (String executionId : executionIds) {
                ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
                Execution execution = executionQuery.executionId(executionId).singleResult();
                if (execution != null) {
                    executions.add(execution);
                }
            }
            return executions;
        }
    }

    public List<Incident> getIncidents(ActivityInstance activityInstance) {
        String[] incidentIds = activityInstance.getIncidentIds();
        if (incidentIds == null) {
            return new ArrayList<>();
        } else {
            List<Incident> incidents = new ArrayList<>();
            for (String incidentId : incidentIds) {
                IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
                Incident incident = incidentQuery.incidentId(incidentId).singleResult();
                if (incident != null) {
                    incidents.add(incident);
                }
            }
            return incidents;
        }
    }

}
