package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.management.JobDefinition;
import org.camunda.bpm.engine.management.JobDefinitionQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class IncidentResolver implements GraphQLResolver<Incident> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ManagementService managementService;

    public IncidentResolver() {
    }

    public ProcessDefinition getProcessDefinition(Incident incident) {
        String processDefinitionId = incident.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public ProcessInstance getProcessInstance(Incident incident) {
        String processInstanceId = incident.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public String getIncidentTimestamp(Incident incident) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(incident.getIncidentTimestamp());
    }

    public Execution getExecution(Incident incident) {
        String executionId = incident.getExecutionId();
        if (executionId == null) {
            return null;
        } else {
            ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
            return executionQuery.executionId(executionId).singleResult();
        }
    }

    public Incident getCauseIncident(Incident incident) {
        String incidentId = incident.getCauseIncidentId();
        if (incidentId == null) {
            return null;
        } else {
            IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
            return incidentQuery.incidentId(incidentId).singleResult();
        }
    }

    public Incident getRootCauseIncident(Incident incident) {
        String incidentId = incident.getRootCauseIncidentId();
        if (incidentId == null) {
            return null;
        } else {
            IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
            return incidentQuery.incidentId(incidentId).singleResult();
        }
    }

    public JobDefinition getJobDefinition(Incident incident) {
        String jobDefinitionId = incident.getJobDefinitionId();
        if (jobDefinitionId == null) {
            return null;
        } else {
            JobDefinitionQuery jobDefinitionQuery = managementService.createJobDefinitionQuery();
            return jobDefinitionQuery.jobDefinitionId(jobDefinitionId).singleResult();
        }
    }

}
