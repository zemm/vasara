package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.management.JobDefinition;
import org.camunda.bpm.engine.management.JobDefinitionQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class JobResolver implements GraphQLResolver<Job> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ManagementService managementService;

    public JobResolver() {
    }

    public JobDefinition getJobDefinition(Job job) {
        String jobDefinitionId = job.getJobDefinitionId();
        if (jobDefinitionId == null) {
            return null;
        } else {
            JobDefinitionQuery jobDefinitionQuery = managementService.createJobDefinitionQuery();
            return jobDefinitionQuery.jobDefinitionId(jobDefinitionId).singleResult();
        }
    }

    public String getDueDate(Job job) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(job.getDuedate());
    }

    public ProcessInstance getProcessInstance(Job job) {
        String processInstanceId = job.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public Execution getExecution(Job job) {
        String executionId = job.getExecutionId();
        if (executionId == null) {
            return null;
        } else {
            ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
            return executionQuery.executionId(executionId).singleResult();
        }
    }

    public ProcessDefinition getProcessDefinition(Job job) {
        String processDefinitionId = job.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public Boolean getSuspended(Job job) {
        return job.isSuspended();
    }

    public String getCreateTime(Job job) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(job.getCreateTime());
    }

}
