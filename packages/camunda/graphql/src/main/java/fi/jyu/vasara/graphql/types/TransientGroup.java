package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.identity.Group;

public class TransientGroup implements Group {
    private final String groupId;

    public TransientGroup(String id) {
        groupId = id;
    }

    @Override
    public String getId() {
        return groupId;
    }

    @Override
    public void setId(String s) {
    }

    @Override
    public String getName() {
        return groupId;
    }

    @Override
    public void setName(String s) {
    }

    @Override
    public String getType() {
        return "TRANSIENT";
    }

    @Override
    public void setType(String s) {
    }
}
