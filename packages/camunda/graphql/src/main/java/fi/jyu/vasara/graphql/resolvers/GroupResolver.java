package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Component
public class GroupResolver implements GraphQLResolver<Group> {

    @Autowired
    ProcessEngine processEngine;

    @Autowired
    IdentityService identityService;

    public GroupResolver() {
    }

    public List<User> getMembers(Group group) {
        String groupId = group.getId();
        if(groupId != null) {
            List<User> members = identityService.createUserQuery()
                    .memberOfGroup(groupId)
                    .list();
            return members;
        } else {
            return null;
        }
    }
}
