type camunda_HistoricProcessInstance @doc(d: "Definition of a historic process instance resource")
{
    id: String @doc(d: "The id of the process instance.")
    rootProcessInstanceId: String @doc(d: "The process instance id of the root process instance that initiated the process.")
    superProcessInstanceId: String @doc(d: "The id of the parent process instance, if it exists.")
    superCaseInstanceId: String @doc(d: "The id of the parent case instance, if it exists.")
    caseInstanceId: String @doc(d: "The id of the parent case instance, if it exists.")
    processDefinition: camunda_ProcessDefinition
    processDefinitionName: String @doc(d: "The name of the process definition that this process instance belongs to.")
    processDefinitionKey: String @doc(d: "The key of the process definition that this process instance belongs to.")
    processDefinitionVersion: Int @doc(d: "The version of the process definition that this process instance belongs to.")
    processDefinitionId: String @doc(d: "The id of the process definition that this process instance belongs to.")
    businessKey: String @doc(d: "The business key of the process instance.")
    startTime: String @doc(d: "The time the instance was started. Default format* yyyy-MM-dd’T’HH:mm:ss.SSSZ.")
    endTime: String @doc(d: "The time the instance ended. Default format* yyyy-MM-dd’T’HH:mm:ss.SSSZ.")
    removalTime: String @doc(d: "The time after which the instance should be removed by the History Cleanup job. Default format* yyyy-MM-dd’T’HH:mm:ss.SSSZ.")
    durationInMillis: Int @doc(d: "The time the instance took to finish (in milliseconds).")
    startUserId: String @doc(d: "The id of the user who started the process instance.")
    startUser: camunda_User
    startActivityId: String @doc(d: "The id of the initial activity that was executed (e.g., a start event).")
    deleteReason: String @doc(d: "The provided delete reason in case the process instance was canceled during execution.")
    tenantId: String @doc(d: "The tenant id of the process instance.")
    state: String @doc(d: "last state of the process instance: ACTIVE, SUSPENDED, COMPLETED, EXTERNALLY_TERMINATED or INTERNALLY_TERMINATED.")

    activityInstances: [camunda_HistoricActivityInstance]
    incidents: [camunda_Incident]
    variables: [ camunda_KeyValuePair ]

    topProcessInstance: camunda_HistoricProcessInstance
    rootProcessInstance: camunda_HistoricProcessInstance
    domProcessInstances: [ camunda_HistoricProcessInstance ]
    subProcessInstances: [ camunda_HistoricProcessInstance ]
}
