{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
, setup ? import ./setup.nix { inherit pkgs; }
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cachix
    jfrog-cli
    jq
    pip2nix.python39
    setup.env
  ];
}
