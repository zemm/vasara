#!/usr/bin/env python
from aiohttp import client
from aiohttp import web
from aiohttp import ServerTimeoutError
from aiohttp import ContentTypeError
from asyncio import TimeoutError
from asyncio import Lock
from os import environ as env

import logging
import aiocache

CACHE_TTL = int(env.get("CACHE_TTL", "10") or "10")
AIOHTTP_PORT = env.get("AIOHTTP_PORT", "8080")
CAMUNDA_GRAPHQL_ENDPOINT = env.get(
    "CAMUNDA_GRAPHQL_ENDPOINT", "http://localhost:8800/graphql"
)
CAMUNDA_GRAPHQL_AUTHORIZATION = env.get(
    "CAMUNDA_GRAPHQL_AUTHORIZATION", "Bearer vasara-graphql-secret"
)
ADMIN_GROUP = env.get("ADMIN_GROUP")
GUEST_GROUP = "camunda-transient"
USER_QUERY = """\
{
  camunda_AuthenticatedUser {
    id
    name
    email
    groups {
      id
    }
  }
}
"""
AUTHORIZATION_QUERY = """\
query {
  camunda_AuthenticatedUser {
    id
    vasaraAllowedIds
    vasaraManagedIds
  }
}
"""


logger = logging.getLogger("hasura-auth-webhook")
logging.basicConfig(level=logging.DEBUG)
routes = web.RouteTableDef()


@aiocache.cached(ttl=CACHE_TTL, serializer=aiocache.serializers.JsonSerializer())
async def get_user(authorization):
    async with client.ClientSession(
        headers={
            "Authorization": authorization,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        for timeout in range(5):
            try:
                # TODO: ensure correct certs in container to avoid ssl=False
                async with session.post(
                    CAMUNDA_GRAPHQL_ENDPOINT,
                    json={"query": USER_QUERY, "variables": {}, "operationName": None},
                    timeout=timeout + 1,
                    ssl=False,
                ) as response:
                    return ((await response.json()).get("data") or {}).get(
                        "camunda_AuthenticatedUser"
                    ) or {}
            except (ServerTimeoutError, TimeoutError):
                pass
            except ContentTypeError:
                return {}
        return {}


@aiocache.cached(ttl=1, serializer=aiocache.serializers.JsonSerializer())
async def get_authorizations(authorization):
    async with client.ClientSession(
        headers={
            "Authorization": authorization,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        for timeout in range(5):
            try:
                # TODO: ensure correct certs in container to avoid ssl=False
                async with session.post(
                    CAMUNDA_GRAPHQL_ENDPOINT,
                    json={
                        "query": AUTHORIZATION_QUERY,
                        "variables": {},
                        "operationName": None,
                    },
                    timeout=timeout + 1,
                    ssl=False,
                ) as response:
                    return ((await response.json()).get("data") or {}).get(
                        "camunda_AuthenticatedUser"
                    ) or {}
            except (ServerTimeoutError, TimeoutError):
                pass
            except ContentTypeError:
                return {}
        return {}


GLOBAL_LOCK = Lock()
USERID_LOCKS = dict()


@routes.get("/")
async def read_authorize_header(request):
    authorization = request.headers.get("Authorization") or ""
    is_bearer = authorization.startswith("Bearer ")
    is_transient = authorization.startswith("Transient ")

    # No valid authorization header
    if not is_bearer and not is_transient:
        logger.debug({"X-Hasura-Role": "anonymous"})
        return web.json_response({"X-Hasura-Role": "anonymous"})

    # TODO: Maybe lock per token, but it requires auto-cleaning dict
    async with GLOBAL_LOCK:
        user = await get_user(authorization)

    # Expired or invalid authorization header
    if not user.get("id"):
        logger.warning(f"Invalid user_info: {user}")
        logger.debug({"X-Hasura-Role": "anonymous"})
        return web.json_response({"X-Hasura-Role": "anonymous"})

    schema = request.headers.get("X-Vasara-Remote-Schema") or None
    logger.debug(f"Valid user: {user}")

    if schema == "Camunda":
        # Request to Camunda don't need authorizations, because Camunda checks them
        authorizations = {
            "vasaraAllowedIds": [],
            "vasaraManagedIds": [],
        }
    elif schema == "Actions":
        # Also actions do their own authorization checks when necessary
        authorizations = {
            "vasaraAllowedIds": [],
            "vasaraManagedIds": [],
        }
    else:
        if user["id"] not in USERID_LOCKS:
            USERID_LOCKS[user["id"]] = Lock()
        async with USERID_LOCKS[user["id"]]:
            authorizations = await get_authorizations(authorization)

    group_ids = [group["id"] for group in user["groups"]]
    groups = "{" + ",".join(group_ids) + "}"
    principal_ids = [f"{user['id']}"] + [f"{group_id}" for group_id in group_ids]
    principals = "{" + ",".join(principal_ids) + "}"
    role = (
        "guest"
        if is_transient or GUEST_GROUP in group_ids
        else "admin"
        if ADMIN_GROUP and ADMIN_GROUP in group_ids
        else "user"
    )

    allowed_ids = authorizations.get("vasaraAllowedIds") or []
    managed_ids = authorizations.get("vasaraManagedIds") or []
    allowed_ids_psql = "{" + ",".join(set(allowed_ids)) + "}"
    managed_ids_psql = "{" + ",".join(set(managed_ids)) + "}"

    logger.debug(
        {
            "X-Hasura-Role": role,
            "X-Hasura-User-Id": user["id"],
            "X-Hasura-User-Name": user["name"],
            "X-Hasura-User-Email": user["email"],
            "X-Hasura-Groups": groups,
            "X-Hasura-Principals": principals,
            "X-Hasura-Allowed-Ids": allowed_ids_psql,
            "X-Hasura-Managed-Ids": managed_ids_psql,
            "X-Vasara-Remote-Schema": schema,
        }
    )
    return web.json_response(
        {
            "X-Hasura-Role": role,
            "X-Hasura-User-Id": user["id"],
            "X-Hasura-User-Name": user["name"],
            "X-Hasura-User-Email": user["email"],
            "X-Hasura-Groups": groups,
            "x-hasura-principals": principals,
            "x-hasura-allowed-ids": allowed_ids_psql,
            "x-hasura-managed-ids": managed_ids_psql,
        }
    )


def main():
    app = web.Application()
    app.add_routes(routes)
    web.run_app(app, port=int(AIOHTTP_PORT))


if __name__ == "__main__":
    main()
