{ pkgs ? import ../../nix {}
, sources ? import ../../nix/sources.nix
, name ? "artifact"
}:

with pkgs;

let

  version = "2.23.9";

  jar = fetchurl {
    url = "https://search.maven.org/remotecontent?filepath=io/zipkin/zipkin-server/${version}/zipkin-server-${version}-exec.jar";
    sha256 = "1sh38fgd0lbmy1zvcaclvvjg3d5m9k4m39zzr61z8lrmfr7qmi3p";
  };

  env = buildEnv {
    name = "env";
    paths = [
      bashInteractive
      coreutils
      netcat
      adoptopenjdk-jre-hotspot-bin-13
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${netcat}/bin \
  --prefix PATH : ${adoptopenjdk-jre-hotspot-bin-13}/bin \
  --prefix PATH : ${tini}/bin \
  --set LOG4J_FORMAT_MSG_NO_LOOKUPS true \
  --set JAR ${jar}

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --exclude="*pixman-1*/pixman-1*/pixman-1*" \
  --files-from=${closure} ${jar} \
  usr > $out || true
''
