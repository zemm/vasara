{ pkgs ? import ../../nix {}
, name ? "artifact"
}:

with pkgs;

let

  env = buildEnv {
    name = "env";
    paths = [
      bashInteractive
      coreutils
      hasura-graphql-engine
      hasura-console-assets
      netcat
      tini
    ];
  };

  closure = (writeReferencesToFile env);

in

runCommand name {
  buildInputs = [ makeWrapper ];
} ''
# aliases
mkdir -p usr/local/bin
for filename in ${env}/bin/??*; do
  cat > usr/local/bin/$(basename $filename) << EOF
#!/usr/local/bin/sh
set -e
exec $(basename $filename) "\$@"
EOF
done
rm -f usr/local/bin/sh
chmod a+x usr/local/bin/*

# shell
makeWrapper ${bashInteractive}/bin/sh usr/local/bin/sh \
  --set SHELL /usr/local/bin/sh \
  --prefix PATH : ${coreutils}/bin \
  --prefix PATH : ${netcat}/bin \
  --prefix PATH : ${hasura-graphql-engine}/bin \
  --prefix PATH : ${tini}/bin \
  --set HASURA_GRAPHQL_CONSOLE_ASSETS_DIR ${hasura-console-assets}

# artifact
tar cvzhP \
  --hard-dereference \
  --exclude="${env}" \
  --exclude="*-python3.8-*" \
  --exclude="*ncurses*/ncurses*/ncurses*" \
  --files-from=${closure} \
  usr > $out || true
''
