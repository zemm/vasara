{ pkgs ? import ../../nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cachix
    jfrog-cli
    hasura-cli-full
  ];
}
