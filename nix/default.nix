# https://github.com/nmattia/niv
{ sources ? import ./sources.nix
, nixpkgs ? sources."nixpkgs"
}:

let

  nixpkgs_unstable = import sources."nixpkgs-unstable" {};

  nixpkgs_21_11 = import sources."nixpkgs-21.11" {};

  nixpkgs_20_09 = import sources."nixpkgs-20.09" {};

  hasura-graphql-engine-src = nixpkgs_unstable.callPackage ./pkgs/hasura-graphql-engine-src {};

  overlay = _: pkgs: rec {

    gitignoreSource = pkgs.callPackage ./pkgs/gitignore-source {};

    gitlab-runner = pkgs.callPackage ./pkgs/gitlab-runner {};

    hasura-graphql-engine = nixpkgs_unstable.hasura-graphql-engine;

    hasura-console-assets = nixpkgs_unstable.callPackage ./pkgs/hasura-console-assets {
      inherit hasura-graphql-engine-src;
    };

    hasura-cli-full = nixpkgs_unstable.callPackage ./pkgs/hasura-cli-full {
      inherit hasura-graphql-engine-src;
      inherit hasura-console-assets;
    };

    hasura-cli-dot-hasura = pkgs.stdenv.mkDerivation {
      name = "hasura-cli-dot-hasura";
      phases = [ "installPhase" ];
      installPhase = ''
        mkdir -p $out/actions-codegen-assets/.git
        mkdir -p $out/cli-ext
        mkdir -p $out/plugins/bin
        mkdir -p $out/plugins/index/.git
        mkdir -p $out/plugins/receipts
        mkdir -p $out/plugins/store
        cat > $out/config.json << EOF
{
  "uuid": "00000000-0000-0000-0000-000000000000",
  "enable_telemetry": false,
  "show_update_notification": false,
  "cli_environment": "default"
}
EOF
      '';
    };

    jfrog-cli = pkgs.callPackage ./pkgs/jfrog-cli {};

    mockoon = pkgs.callPackage ./pkgs/mockoon {};

    micromamba = nixpkgs_21_11.micromamba;

    mvn2nix = pkgs.callPackage ./pkgs/mvn2nix { nixpkgs = sources."nixpkgs"; };

    nginx = pkgs.callPackage ./pkgs/nginx { inherit nixpkgs; };

    node2nix = pkgs.callPackage ./pkgs/node2nix { nixpkgs = sources."nixpkgs"; };

    pip2nix = nixpkgs_20_09.callPackage ./pkgs/pip2nix {};

    carrot-rcc = pkgs.callPackage ./pkgs/carrot-rcc {};

    rcc = pkgs.callPackage ./pkgs/rcc {};

    rccFHSUserEnv  = pkgs.callPackage ./pkgs/rcc {};

    zipkin = pkgs.callPackage ./pkgs/zipkin { zipkin = pkgs.zipkin; };

  };

  pkgs = import nixpkgs {
    overlays = [ overlay ];
    config = {
      allowUnfree = true;
      permittedInsecurePackages = [
        "adoptopenjdk-jre-hotspot-bin-13.0.2"
      ];
    };
  };

in pkgs
