{ callPackage, buildFHSUserEnv, buildGoPackage, go-bindata, rake, zip, micromamba
, which, cacert, dbus-glib, libGL }:

let rcc = callPackage ./rcc.nix {
  inherit buildGoPackage go-bindata rake zip;
};

in buildFHSUserEnv {
  name = "rcc";
  targetPkgs = pkgs: (with pkgs; [
    micromamba
    rcc
    which
  ]);
  # symlinks to link certs from host don't survive Nomad artifact tarball
  extraBuildCommands = ''
    chmod u+w $out/etc
    rm $out/etc/ssl
    mkdir -p $out/etc/ssl $out/etc/pki/tls
    cp -aL ${cacert}/etc/ssl/certs $out/etc/ssl
    cp -aL ${cacert}/etc/ssl/certs $out/etc/pki/tls
    chmod u-w $out/etc
  '';
  runScript = "rcc";
  # these are not easily available at Conda
  profile = ''
    export LD_LIBRARY_PATH="${dbus-glib}/lib:${libGL}/lib"
  '';
}
