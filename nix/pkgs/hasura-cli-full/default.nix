{ callPackage, fetchgit, fetchurl, hasura-cli, hasura-graphql-engine-src, hasura-console-assets, lib, nix-gitignore, nodejs-12_x, nodejs-14_x, python2, makeWrapper, runCommand, stdenv, writeTextFile
}:

let

  node-packages = import ./node-packages.nix {
    inherit fetchurl nix-gitignore stdenv lib fetchgit;
    nodeEnv = callPackage ./node-env.nix { nodejs = nodejs-14_x; libtool = null; };
  };

  node-modules = node-packages.package.override {
    src = "${hasura-graphql-engine-src}/cli-ext";
    postInstall = ''
      mv $out/lib/node_modules/*/node_modules /tmp/_; rm -rf $out; mv /tmp/_ $out
    '';
  };

  pkgfetch = builtins.fetchurl {
    url = "https://github.com/vercel/pkg-fetch/releases/download/v3.1/node-v12.22.1-linuxstatic-x64";
    sha256 = "04dvvj8k2n254f3jfxnhzrbvr354vinfrbmagd3c5czkfd1c1gjg";
  };

  hasura-cli-ext = stdenv.mkDerivation rec {
    name = "hasura-cli-ext";
    src = "${hasura-graphql-engine-src}/cli-ext";
    phases = [ "unpackPhase" "buildPhase" "installPhase" ];
    buildPhase = ''
      source $stdenv/setup;

      # setup pkg-fetch dependencies
      mkdir -p .pkg-cache/v3.1
      cp -a ${pkgfetch} .pkg-cache/v3.1/built-v12.22.1-linux-x64
      chmod u+x .pkg-cache/v3.1/built-v12.22.1-linux-x64

      cp -a ${node-modules} node_modules
      substituteInPlace package.json \
        --replace 'rm -rf src/shared && cp -r ../console/src/shared ./src/shared' \
                  'rm -rf src/shared && cp -r ${hasura-graphql-engine-src}/console/src/shared ./src/shared' \
        --replace 'node12-linux-x64,node12-macos-x64,node12-win-x64,node12-linux-arm64,node16-macos-arm64' \
                  'node12-linux-x64'
      HOME=$(pwd) npm run build
    '';
    installPhase = ''
      mkdir $out
      cp -a bin $out/bin
    '';
    buildInputs = [ nodejs-12_x ];
  };

in

stdenv.mkDerivation rec {
  name = hasura-cli.name;
  buildInputs = [ makeWrapper hasura-cli ];
  phases = [ "installPhase" ];
  installPhase = ''
    mkdir -p $out/bin
    ln -s ${hasura-cli}/bin/hasura $out/bin/hasura
    ln -s ${hasura-cli}/bin/hasura $out/bin/hasura-console
    wrapProgram $out/bin/hasura \
      --add-flags "--cli-ext-path" \
      --add-flags "${hasura-cli-ext}/bin/cli-ext-hasura"
    wrapProgram $out/bin/hasura-console \
      --add-flags "--cli-ext-path" \
      --add-flags "${hasura-cli-ext}/bin/cli-ext-hasura" \
      --add-flags "--static-dir" \
      --add-flags "${hasura-console-assets}"
  '';
}
