{ zipkin }:

zipkin.overrideDerivation(old: rec {
  version = "2.23.2";
  name = "zipkin-${version}";
  src = pkgs.fetchurl {
    url = "https://search.maven.org/remotecontent?filepath=io/zipkin/zipkin-server/${version}/zipkin-server-${version}-exec.jar";
    sha256 = "0yx2xndhc5a0k57fsx7y23qzqanavf4qsx771m35layzh7czw18k";
  };
  buildCommand = with pkgs; ''
    mkdir -p $out/share/java
    cp ${src} $out/share/java/zipkin-server-${version}-exec.jar
    mkdir -p $out/bin
    makeWrapper ${jre_headless}/bin/java $out/bin/zipkin-server \
      --add-flags "-cp $out/share/java/zipkin-server-${version}-exec.jar org.springframework.boot.loader.JarLauncher"
  '';
})
