{ appimageTools }:

appimageTools.wrapType2 {
  name = "mockoon-1.16.0";
  src = fetchurl {
    url ="https://github.com/mockoon/mockoon/releases/download/v1.16.0/mockoon-1.16.0-x86_64.AppImage";
    sha256 = "0ig2m00l92g7vgcd9dncrflmc6vzcsm7w3pmgr9wspv4b8cp7yqn";
  };
}
