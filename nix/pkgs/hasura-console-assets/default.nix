{ callPackage, fetchgit, fetchurl, gzip, hasura-graphql-engine-src, lib, nix-gitignore, nodejs-14_x, python2, runCommand, stdenv, writeTextFile }:

let

  node-packages = import ./node-packages.nix {
    inherit fetchurl nix-gitignore stdenv lib fetchgit;
    nodeEnv = callPackage ./node-env.nix { nodejs = nodejs-14_x; libtool = null; };
  };

  node-modules = node-packages.package.override {
    src = "${hasura-graphql-engine-src}/console";
    buildInputs = [ python2 ];
    preRebuild = ''
      # Do not download Cypress
      substituteInPlace node_modules/cypress/package.json \
        --replace '"postinstall": "node index.js --exec install",' ""
    '';
    postInstall = ''
      mv $out/lib/node_modules/*/*/node_modules /tmp/_; rm -rf $out; mv /tmp/_ $out
    '';
  };

  console-assets-common= ./console-assets-common.tar.gz;

in

stdenv.mkDerivation rec {
  name = "hasura-console-assets";
  src = "${hasura-graphql-engine-src}/console";
  buildPhase = ''
    source $stdenv/setup;
    cp -a ${node-modules} node_modules
    substituteInPlace Makefile --replace \
      'gsutil -m cp -r gs://$(BUCKET_NAME)/console/assets/common "$(DIST_PATH)"' \
      'tar xzvf ${console-assets-common}'
    HOME=$(pwd) make server-build
  '';
  installPhase = ''
    mkdir -p $out
    cp -a static/dist/common $out
    cp -a static/dist/versioned $out
  '';
  buildInputs = [ nodejs-14_x gzip ];
}
