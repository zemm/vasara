{ fetchFromGitHub }:

fetchFromGitHub {
  owner = "hasura";
  repo = "graphql-engine";
  rev = "v2.0.9";
  sha256 = "0ky23f700pmzb6anx44xzh6dixixmn7kq1ypj0yy4kqiqzqdb2dg";
}
