CREATE SCHEMA vasara;




CREATE TABLE vasara.entity (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb DEFAULT jsonb_build_object(),
    business_key text DEFAULT public.gen_random_uuid() NOT NULL,
    author_id text,
    editor_id text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);




COMMENT ON TABLE vasara.entity IS 'Perusentiteetti';



CREATE FUNCTION vasara.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;




CREATE TABLE vasara.entity_comment (
    id uuid DEFAULT public.gen_random_uuid(),
    metadata jsonb DEFAULT jsonb_build_object(),
    author_id text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    "to" text,
    subject text,
    body text NOT NULL,
    entity_id uuid NOT NULL
)
INHERITS (vasara.entity);




COMMENT ON TABLE vasara.entity_comment IS 'Kommentti';



COMMENT ON COLUMN vasara.entity_comment."to" IS 'Vastaanottaja #10';



COMMENT ON COLUMN vasara.entity_comment.subject IS 'Aihe #20';



COMMENT ON COLUMN vasara.entity_comment.body IS 'Sisältö #30';



COMMENT ON COLUMN vasara.entity_comment.entity_id IS 'Asia #40';



CREATE TABLE vasara."authorization" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    principal text NOT NULL,
    can_insert boolean NOT NULL,
    can_select boolean NOT NULL,
    can_update boolean NOT NULL,
    can_delete boolean NOT NULL,
    resource text NOT NULL,
    specifier text NOT NULL
);




COMMENT ON TABLE vasara."authorization" IS 'Käyttöoikeudet';



COMMENT ON COLUMN vasara."authorization".principal IS 'Käyttäjätunnus tai -ryhmä';



COMMENT ON COLUMN vasara."authorization".can_insert IS 'Voi lisätä';



COMMENT ON COLUMN vasara."authorization".can_select IS 'Voi lukea';



COMMENT ON COLUMN vasara."authorization".can_update IS 'Voi muuttaa';



COMMENT ON COLUMN vasara."authorization".can_delete IS 'Voi poistaa';



COMMENT ON COLUMN vasara."authorization".resource IS 'Tietueen tyyppi tai yksilöivä tunniste';



COMMENT ON COLUMN vasara."authorization".specifier IS 'Käyttöoikeuden lisämäärite (esim. organisaatio)';



CREATE VIEW vasara.entity_pii AS
 SELECT (entity.tableoid)::regclass AS entity,
    entity.id,
    jsonb_array_elements_text((entity.metadata -> 'PII'::text)) AS field
   FROM vasara.entity;




CREATE TABLE vasara.entity_settings (
    id text NOT NULL,
    columns jsonb NOT NULL
);




COMMENT ON TABLE vasara.entity_settings IS 'Resource settings';



CREATE TABLE vasara.user_task_form (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    metadata jsonb DEFAULT jsonb_build_object(),
    process_definition_key text NOT NULL,
    process_definition_version integer NOT NULL,
    user_task_id text NOT NULL,
    schema jsonb DEFAULT jsonb_build_object() NOT NULL,
    process_definition_version_tag text
);




COMMENT ON TABLE vasara.user_task_form IS 'Tehtävälomake';



CREATE VIEW vasara.user_task_form_source AS
 SELECT form.process_definition_key,
    form.process_definition_version,
    form.process_definition_version_tag,
    form.user_task_id,
    jsonb_array_elements_text(sources.sources) AS source
   FROM vasara.user_task_form form,
    LATERAL ( SELECT fieldset.fields
           FROM jsonb_to_recordset(form.schema) fieldset(fields jsonb)) fields,
    LATERAL ( SELECT field.id,
            field.sources
           FROM jsonb_to_recordset(fields.fields) field(id text, sources jsonb)) sources;




ALTER TABLE ONLY vasara.entity_comment ALTER COLUMN business_key SET DEFAULT public.gen_random_uuid();



ALTER TABLE ONLY vasara."authorization"
    ADD CONSTRAINT authorization_id_key UNIQUE (id);



ALTER TABLE ONLY vasara."authorization"
    ADD CONSTRAINT authorization_pkey PRIMARY KEY (principal, can_insert, can_select, can_update, can_delete, resource, specifier);



ALTER TABLE ONLY vasara.entity_comment
    ADD CONSTRAINT entity_comment_pkey PRIMARY KEY (id);



ALTER TABLE ONLY vasara.entity
    ADD CONSTRAINT entity_pkey PRIMARY KEY (id);



ALTER TABLE ONLY vasara.entity_settings
    ADD CONSTRAINT entity_settings_pkey PRIMARY KEY (id);



ALTER TABLE ONLY vasara.user_task_form
    ADD CONSTRAINT user_task_form_pkey PRIMARY KEY (process_definition_key, process_definition_version, user_task_id);



CREATE TRIGGER set_vasara_entity_comment_updated_at BEFORE UPDATE ON vasara.entity_comment FOR EACH ROW EXECUTE PROCEDURE vasara.set_current_timestamp_updated_at();



COMMENT ON TRIGGER set_vasara_entity_comment_updated_at ON vasara.entity_comment IS 'trigger to set value of column "updated_at" to current timestamp on row update';



CREATE TRIGGER set_vasara_entity_updated_at BEFORE UPDATE ON vasara.entity FOR EACH ROW EXECUTE PROCEDURE vasara.set_current_timestamp_updated_at();



COMMENT ON TRIGGER set_vasara_entity_updated_at ON vasara.entity IS 'trigger to set value of column "updated_at" to current timestamp on row update';
