CREATE OR REPLACE VIEW "vasara"."user_task_form_latest" AS
 SELECT DISTINCT ON (user_task_form.process_definition_key, user_task_form.user_task_id) user_task_form.id,
    user_task_form.metadata,
    user_task_form.process_definition_key,
    user_task_form.process_definition_version,
    user_task_form.user_task_id,
    user_task_form.schema,
    user_task_form.process_definition_version_tag,
    user_task_form.settings
   FROM vasara.user_task_form
  ORDER BY user_task_form.process_definition_key, user_task_form.user_task_id, user_task_form.process_definition_version DESC;
