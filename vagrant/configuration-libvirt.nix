{ config, pkgs, ... }:

let

  sources = import ../nix/sources.nix;
  nixpkgs = sources."nixpkgs";

in {
  imports = [
    "${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
    ./modules/cachix
    ./modules/graphical.nix
    ./modules/libvirt.nix
    ./modules/vagrant-libvirt-image.nix
    ./modules/vagrant-user.nix
    ./modules/vasara
  ];
  swapDevices = [{
    device = "/var/swap";
    size = 4096;
  }];
  nixpkgs.pkgs = import ../nix {};
}
