{ config, pkgs, ... }:

let

  sources = import ../nix/sources.nix;
  nixpkgs = sources."nixpkgs";

in {
  imports = [
    "${nixpkgs}/nixos/modules/virtualisation/vagrant-virtualbox-image.nix"
    "${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
    ./modules/cachix
    ./modules/graphical.nix
    ./modules/vagrant-guest.nix
    ./modules/vagrant-user.nix
    ./modules/vasara
    ./modules/virtualbox.nix
  ];
  swapDevices = [{
    device = "/var/swap";
    size = 4096;
  }];
  nixpkgs.pkgs = import ../nix {};
}
