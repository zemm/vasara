{ config, pkgs, ... }:

{
  imports = [
    ./vagrant-guest.nix
    ./libvirt-image.nix
  ];

  sound.enable = false;
  documentation.man.enable = false;
  documentation.nixos.enable = false;

  # generate the box v1 format which is much easier to generate
  # https://www.vagrantup.com/docs/boxes/format.html
  system.build.vagrantLibvirt = pkgs.runCommand
    "libvirt-vagrant.box"
    {}
    ''
      mkdir workdir
      cd workdir

      # 1. create that metadata.json file
      echo '{"format":"qcow2","provider":"libvirt","virtual_size":4096}' > metadata.json

      # 2. create a default Vagrantfile config
      cat <<VAGRANTFILE > Vagrantfile
      Vagrant.configure("2") do |config|
        config.vm.base_mac = "0800275F0936"
      end
      VAGRANTFILE

      # 3. move the qcow2 to the fixed location
      cp ${config.system.build.qcow2}/*.qcow2 box.img

      # 4. generate qcow2 manifest file
      touch box.mf
      for fname in *; do
        checksum=$(sha256sum $fname | cut -d' ' -f 1)
        echo "SHA256($fname)= $checksum" >> box.mf
      done

      # 5. compress everything back together
      tar --owner=0 --group=0 --sort=name --numeric-owner -czf $out .
    '';
}
