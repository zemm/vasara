{ config, pkgs, ... }:

let

  sources = import ../../nix/sources.nix;
  nixpkgs = sources."nixpkgs";

in

{
  imports = [
    "${nixpkgs}/nixos/modules/virtualisation/vagrant-guest.nix"
  ];

  users.extraUsers.vagrant.extraGroups = [ "vagrant" ];

  users.groups.vagrant = {
    gid = 1000;
    members = [ "vagrant" ];
  };
}
