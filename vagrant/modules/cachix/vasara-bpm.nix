
{
  nix = {
    binaryCaches = [
      "https://vasara-bpm.cachix.org"
    ];
    binaryCachePublicKeys = [
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4="
    ];
  };
}
    