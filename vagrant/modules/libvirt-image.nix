{ config, lib, pkgs, ... }:

with lib;

let

  sources = import ../../nix/sources.nix;
  nixpkgs = sources."nixpkgs";

  cfg = config.libvirt;

in {
  options = {
    libvirt = {
      baseImageSize = mkOption {
        type = with types; either (enum [ "auto" ]) int;
        default = "auto";
        example = 50 * 1024;
        description = ''
          The size of the libvirt base image in MiB.
        '';
      };
      baseImageFreeSpace = mkOption {
        type = with types; int;
        default = 30 * 1024;
        description = ''
          Free space in the libvirt base image in MiB.
        '';
      };
      vmDerivationName = mkOption {
        type = types.str;
        default = "nixos-qcow2-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}";
        description = ''
          The name of the derivation for the libvirt appliance.
        '';
      };
      vmName = mkOption {
        type = types.str;
        default = "NixOS ${config.system.nixos.label} (${pkgs.stdenv.hostPlatform.system})";
        description = ''
          The name of the libvirt appliance.
        '';
      };
      vmFileName = mkOption {
        type = types.str;
        default = "nixos-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}.qcow2";
        description = ''
          The file name of the libvirt appliance.
        '';
      };
    };
  };

  config = {
    system.build.qcow2 = import "${nixpkgs}/nixos/lib/make-disk-image.nix" {
      name = cfg.vmDerivationName;

      inherit pkgs lib config;
      diskSize = cfg.baseImageSize;
      format = "qcow2-compressed";
      additionalSpace = "${toString cfg.baseImageFreeSpace}M";

      postVM =
        ''
          mkdir -p $out
          fn="$out/${cfg.vmFileName}"

          mv $diskImage $fn

          mkdir -p $out/nix-support
          echo "file qcow2 $fn" >> $out/nix-support/hydra-build-products
        '';
    };

    fileSystems = {
      "/" = {
        device = "/dev/disk/by-label/nixos";
        autoResize = true;
        fsType = "ext4";
      };
    };

    boot.growPartition = true;
    boot.loader.grub.device = "/dev/sda";

    swapDevices = [{
      device = "/var/swap";
      size = 2048;
    }];

    services.qemuGuest.enable = true;
    services.spice-vdagentd.enable = true;
  };
}
