{ config, pkgs, ... }:

{
  services.xserver.videoDrivers = [ "virtualbox" "vmware" "cirrus" "vesa" ];
  users.extraUsers.vagrant.extraGroups = [ "vboxsf" ];
  virtualisation.virtualbox.guest.enable = true;
  virtualisation.virtualbox.guest.x11 = true;
}
