{ config, pkgs, ... }:

let

  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-20.09.tar.gz";
  user = "vagrant";

in

{
  imports = [
    (import "${home-manager}/nixos")
  ];

  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "${user}";

  environment.systemPackages = with pkgs; [
    git
    gnumake
    chromium
    (callPackage ../pkgs/camunda-modeler {})
  ];

  nixpkgs.config.allowUnfree = true;
  home-manager.users."${user}"= {
    home.file.".config/vagrant/camunda-modeler.desktop".source = ../files/camunda-modeler.desktop;
    home.file.".config/vagrant/camunda-modeler.png".source = ../files/camunda-modeler.png;
    home.file.".config/vagrant/chromium-browser.desktop".source = ../files/chromium-browser.desktop;
    home.file.".config/vagrant/xfce4-session-logout.desktop".source = ../files/xfce4-session-logout.desktop;
    home.file.".config/vagrant/camunda.desktop".source = ../files/camunda.desktop;
    home.file.".config/vagrant/camunda.png".source = ../files/camunda.png;
    home.file.".config/vagrant/vasara.desktop".source = ../files/vasara.desktop;
    home.file.".config/vagrant/vasara.png".source = ../files/vasara.png;
    home.file.".config/vagrant/hasura.desktop".source = ../files/hasura.desktop;
    home.file.".config/vagrant/hasura.png".source = ../files/hasura.png;
    home.file.".config/vagrant/robocorp-code.desktop".source = ../files/robocorp-code.desktop;
    home.file.".config/vagrant/robocorp-code.png".source = ../files/robocorp-code.png;
    home.file.".config/vagrant/mailhog.desktop".source = ../files/mailhog.desktop;
    home.file.".config/vagrant/mailhog.png".source = ../files/mailhog.png;
    home.file.".config/vagrant/alasin.png".source = ../files/alasin.png;
    xsession = {
      enable = true;
      windowManager.command = ''test -n "$1" && eval "$@"'';
      profileExtra = ''
        # resolve directory
        if [ -f ~/.config/user-dirs.dirs ]; then
          source ~/.config/user-dirs.dirs
        fi
        if [ -z "$XDG_DESKTOP_DIR" ]; then
          XDG_DESKTOP_DIR="Työpöytä"
        fi

        # configure background
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/last-image -t string -s ~/.config/vagrant/alasin.png
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/last-image -t string -s ~/.config/vagrant/alasin.png --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/color-style -t int -s 0
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/color-style -t int -s 0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/image-style -t int -s 1
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/image-style -t int -s 1 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/last-image -t string -s ~/.config/vagrant/alasin.png
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/last-image -t string -s ~/.config/vagrant/alasin.png --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/color-style -t int -s 0
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/color-style -t int -s 0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/image-style -t int -s 1
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/image-style -t int -s 1 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0
        if [ $? -ne 0 ]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorqxl-0/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0 --create
        fi

        # configure icons
        mkdir -p $XDG_DESKTOP_DIR
        cp -L ~/.config/vagrant/*.desktop $XDG_DESKTOP_DIR
        chmod u+w $XDG_DESKTOP_DIR/*.desktop
        rm -f $XDG_DESKTOP_DIR/Shared
        ln -s /vagrant $XDG_DESKTOP_DIR/Shared
        ln -s /var/lib/carrot-rcc $XDG_DESKTOP_DIR/Robots
        ln -s /var/lib/pdf-tools-api $XDG_DESKTOP_DIR/PDFs

        # migrate from earlier versions
        if [ -L $XDG_DESKTOP_DIR/Host ]; then
          rm $XDG_DESKTOP_DIR/Host
        fi
      '';
      initExtra= ''
        setxkbmap -layout fi
      '';
    };
    programs.vscode.enable = true;
    programs.vscode.userSettings = {
      "python.experiments.enabled" = false;
    };
    programs.vscode.package = (pkgs.vscode-fhsWithPackages (ps: with ps; [
      (ps.python3Full.withPackages(ps: [
        (ps.robotframework.overridePythonAttrs(old: rec {
          version = "4.1.1";
          src = ps.fetchPypi {
            pname = "robotframework";
            extension = "zip";
            inherit version;
            sha256 = "0ddd9dzrn9gi29w0caab78zs6mx06wbf6f99g0xrpymjfz0q8gv6";
          };
          doCheck = false;
        }))
      ]))
      pkgs.rcc
    ]));
    programs.vscode.extensions = (with pkgs.vscode-extensions; [
      ms-python.python
      # vscodevim.vim
      (pkgs.vscode-utils.buildVscodeMarketplaceExtension rec {
        mktplcRef = {
          name = "robotframework-lsp";
          publisher = "robocorp";
          version = "0.32.0";
          sha256 = "sha256:07s3jg9qf20gramdy7hzxq9n78gb37kkl0by3x0xv6waqn5zx9gg";
        };
      })
      (pkgs.vscode-utils.buildVscodeMarketplaceExtension rec {
        mktplcRef = {
          name = "robocorp-code";
          publisher = "robocorp";
          version = "0.20.0";
          sha256 = "09dl08fb0qrnnna4x5d6z3jmj0kkl6gzkjwj12bi7v7khwm0r92a";
        };
        postInstall = ''
          mkdir -p $out/share/vscode/extensions/robocorp.robocorp-code/bin
          ln -s ${pkgs.rcc}/bin/rcc $out/share/vscode/extensions/robocorp.robocorp-code/bin
        '';
        })
    ]);
  };
}
