{ config, pkgs, ... }:

let auth = import ../../../packages/hasura-auth {};
    constants = import ./constants.nix {};
    migrations = ../../../fixture/migrations;
    metadata = ../../../fixture/metadata;
    hasura-cli_config_yaml = builtins.toFile "config.yaml" ''
      version: 2
      endpoint: http://localhost:8090
      metadata_directory: metadata
      migrations_directory: migrations
    '';
    hasura-cli_home = pkgs.stdenv.mkDerivation {
      name = "hasura-cli-home";
      phases = [ "installPhase" ];
      installPhase = ''
        mkdir -p $out/seeds
        ln -s ${metadata} $out/metadata
        ln -s ${migrations} $out/migrations
        ln -s ${pkgs.hasura-cli-dot-hasura} $out/.hasura
        cp -a ${hasura-cli_config_yaml} $out/config.yaml
      '';
    }; in

{
  environment.systemPackages = with pkgs; [ hasura-graphql-engine auth hasura-cli-full ];

  systemd.services.vasara-hasura-auth = {
    after = [ "vasara-camunda.service" ];
    before = [ "vasara-hasura.service" ];
    bindsTo = [ "vasara-camunda.service" ];
    wantedBy = [ "multi-user.target" ];
    environment = {
        AIOHTTP_PORT = "8091";
        CAMUNDA_GRAPHQL_ENDPOINT = "http://localhost:8080/graphql";
        CAMUNDA_GRAPHQL_AUTHORIZATION = "Bearer ${constants.vasara_graphql_secret}";
        ADMIN_GROUP = "camunda-admin";
    };
    serviceConfig = {
      User = "hasura";
      Group = "hasura";
      DynamicUser = true;
    };
    path = [ auth ];
    script = ''
      graphql-oidc-auth
    '';
  };

  systemd.services.vasara-hasura-init = {
    after = [ "postgresql.service" ];
    before = [ "vasara-hasura.service" ];
    bindsTo = [ "postgresql.service" ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      User = "postgres";
      Group = "postgres";
    };
    path = [ config.services.postgresql.package ];
    script = ''
      set -o errexit -o pipefail -o nounset -o errtrace
      shopt -s inherit_errexit
      create_role="$(mktemp)"
      trap 'rm -f "$create_role"' ERR EXIT
      echo "CREATE ROLE hasura WITH LOGIN PASSWORD 'hasura' CREATEDB" > "$create_role"
      psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='hasura'" | grep -q 1 || psql -tA --file="$create_role"
      psql -tAc "SELECT 1 FROM pg_database WHERE datname = 'hasura'" | grep -q 1 || psql -tAc 'CREATE DATABASE "hasura" OWNER "hasura"'
      psql -c "CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA public" -d hasura
      psql -c "CREATE EXTENSION IF NOT EXISTS postgres_fdw SCHEMA public" -d hasura
      psql -c "CREATE SERVER IF NOT EXISTS camunda FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', port '5432', dbname 'camunda')" -d hasura
      psql -c "CREATE USER MAPPING IF NOT EXISTS FOR hasura SERVER camunda OPTIONS (user 'camunda', password 'camunda')" -d hasura
      psql -c "ALTER USER MAPPING FOR hasura SERVER camunda OPTIONS (SET user 'camunda', SET password 'camunda')" -d hasura
      psql -c "GRANT USAGE ON FOREIGN SERVER camunda TO hasura" -d hasura
    '';
  };

  networking.firewall.allowedTCPPorts = [ 5432 8090 ];
  services.postgresql.enableTCPIP = true;
  services.postgresql.authentication = ''
    host  all all 0.0.0.0/0 md5
    local all hasura        md5
    local all camunda       md5
  '';

  systemd.services.vasara-hasura = {
    after = [ "vasara-hasura-init.service" "vasara-hasura-auth.service" "vasara-camunda.service" "postgresql.service" ];
    bindsTo = [ "vasara-hasura-init.service" "vasara-hasura-auth.service" "vasara-camunda.service" "postgresql.service" ];
    wantedBy = [ "multi-user.target" ];
    environment = {
      HASURA_GRAPHQL_DATABASE_URL = "postgres://hasura:hasura@localhost/hasura";
      HASURA_GRAPHQL_ADMIN_SECRET = constants.hasura_admin_secret;
      HASURA_GRAPHQL_SERVER_PORT = "8090";
      HASURA_GRAPHQL_AUTH_HOOK = "http://localhost:8091";
      HASURA_GRAPHQL_ENABLE_CONSOLE = "true";
      HASURA_GRAPHQL_CORS_DOMAIN = "*";
      HASURA_GRAPHQL_DEV_MODE = "true";
      HASURA_GRAPHQL_ENABLE_TELEMETRY = "false";
      HASURA_GRAPHQL_CONSOLE_ASSETS_DIR = "${pkgs.hasura-console-assets}";
      CAMUNDA_GRAPHQL_ENDPOINT= "http://localhost:8080/graphql";
      CAMUNDA_REST_ENDPOINT= "http://localhost:8080/engine-rest";
      VASARA_GRAPHQL_SECRET = "${constants.vasara_graphql_secret}";
      VASARA_MIDDLEWARE_ADDR = "http://localhost:8081";
    };
    unitConfig = {
      StartLimitInterval = 600;
      StartLimitBurst = 15;
    };
    serviceConfig = {
      User = "hasura";
      Group = "hasura";
      DynamicUser = true;
      Restart = "on-failure";
      RestartSec = 20;
      StateDirectory = "hasura";
    };
    path = with pkgs; [ netcat hasura-cli-full hasura-graphql-engine postgresql ];
    script = ''
      graphql-engine serve
    '';
    preStart = with pkgs; ''
      while ! nc -z localhost 8080; do
        echo "Waiting for Camunda at localhost:8080"; sleep 5
      done
      while ! nc -z localhost 8091; do
        echo "Waiting for Auth Webhook at localhost:8091"; sleep 5
      done
    '';
    postStart = with pkgs; ''
      while ! nc -z localhost 8090; do
        echo "Waiting for Hasura at localhost:8091"; sleep 5
      done
      if [ ! -f $STATE_DIRECTORY/.hasura/config.json ]; then
        mkdir -p $STATE_DIRECTORY/.hasura
        cp -a ${hasura-cli_home}/.hasura/config.json $STATE_DIRECTORY/.hasura
      fi
      cd ${hasura-cli_home}
      export HOME=$STATE_DIRECTORY
      hasura migrate apply --skip-update-check
      hasura metadata apply --skip-update-check
    '';
  };
}
