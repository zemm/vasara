{ config, pkgs, ... }:

let constants = import ./constants.nix {}; in

{
  services = {
    vault = {
      package = pkgs.vault-bin;
      enable = true;
    };
  };

  # Fix to run vault with -dev, because this is just a development VM
  systemd.services.vault.serviceConfig.ExecStart =
    pkgs.lib.mkForce "${config.services.vault.package}/bin/vault server -dev";

  # Set test secret
  systemd.services.vault.environment = {
    HOME = "/tmp";
    VAULT_DEV_ROOT_TOKEN_ID = constants.vault_root_token_id;
  };

  # Configure vault due to transient -dev mode
  systemd.services.vault.path = with pkgs; [ vault netcat ];
  systemd.services.vault.postStart = ''
      export VAULT_ADDR="http://${config.services.vault.address}";
      export VAULT_TOKEN="${constants.vault_root_token_id}";
      while ! nc -z localhost 8200; do
        echo "Waiting for Vault at localhost:8200"; sleep 5
      done
      vault secrets enable -path=transit/default transit
      vault write -f transit/default/keys/test1
  '';
}
