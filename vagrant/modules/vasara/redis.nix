{ config, pkgs, ... }:

let constants = import ./constants.nix {}; in

{
  services = {
    redis = {
      enable = true;
      requirePass = constants.redis_password;
    };
  };
}
