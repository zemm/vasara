{ config, pkgs, ... }:

let constants = import ./constants.nix {};
    middleware = import ../../../packages/middleware {}; in

{
  environment.systemPackages = [ middleware ];

  systemd.services.vasara-middleware = {
    enable = true;
    after = [ "vasara-camunda.service" "vault.service" ];
    bindsTo = [ "vasara-camunda.service" "vault.service" ];
    wantedBy = [ "multi-user.target" ];
    environment = {
        VASARA_GRAPHQL_ENDPOINT = "http://localhost:8090/v1/graphql";
        VASARA_GRAPHQL_ADMIN_SECRET = "${constants.hasura_admin_secret}";
        CAMUNDA_GRAPHQL_ENDPOINT = "http://localhost:8080/graphql";
        CAMUNDA_GRAPHQL_AUTHORIZATION = "Bearer ${constants.vasara_graphql_secret}";
        CAMUNDA_REST_AUTHORIZATION = "Bearer ${constants.vasara_rest_secret}";
        CAMUNDA_REST_ENDPOINT = "http://localhost:8080/engine-rest";
        VASARA_TRANSIENT_USER_PRIVATE_KEY = "${constants.vasara_transient_user_private_key}";
        VASARA_TRANSIENT_USER_PUBLIC_KEY = "${constants.vasara_transient_user_public_key}";
        VAULT_ADDR = "http://${config.services.vault.address}";
        VAULT_TOKEN = "${constants.vault_root_token_id}";
        VAULT_TRANSIT_DEFAULT_PATH = "transit/default";
        VAULT_TRANSIT_DEFAULT_KEY = "test1";
        VAULT_TRANSIT_PII_SENSITIVE_PATH = "transit/default";
        VAULT_TRANSIT_PII_SENSITIVE_KEY = "test1";
        VAULT_TRANSIT_PII_RESTRICTED_PATH = "transit/default";
        VAULT_TRANSIT_PII_RESTRICTED_KEY = "test1";
        VAULT_TRANSIT_DATA_RESTRICTED_PATH = "transit/default";
        VAULT_TRANSIT_DATA_RESTRICTED_KEY = "test1";
        SMTP_HOST = "localhost";
        SMTP_PORT = "${toString config.services.mailhog.smtpPort}";
        VASARA_PURGE_RUNTIME_ENABLED = "True";
    };
    unitConfig = {
      StartLimitInterval = 300;
      StartLimitBurst = 15;
    };
    path = with pkgs; [ middleware netcat ];
    serviceConfig = {
      User = "hasura";
      Group = "hasura";
      DynamicUser = true;
      Restart = "on-failure";
      RestartSec = 20;
      StateDirectory = "vasara";
    };
    script = ''
      export HOME=$STATE_DIRECTORY
      vasara-middleware --host localhost --port 8081
    '';
    preStart = with pkgs; ''
      while ! nc -z localhost 8080; do
        echo "Waiting for Camunda at localhost:8080"; sleep 5
      done
      while ! nc -z localhost 8200; do
        echo "Waiting for Vault at localhost:8200"; sleep 5
      done
      while ! nc -z localhost 8090; do
        echo "Waiting for Hasura at localhost:8090"; sleep 5
      done
    '';
  };
}
