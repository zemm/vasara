{ config, pkgs, ... }:

let constants = import ./constants.nix {};
    source = (import ../../../nix/sources.nix)."pdf-tools-api";
    jar = import source {
        pkgs = import ../../../nix {};
    };
    files = pkgs.stdenv.mkDerivation {
      name = "pdf-api-tools-files";
      buildInputs = [ source ];
      phases = [ "installPhase" ];
      installPhase = ''
        mkdir -p $out
        cp "${source}/camunda/client/dist/workers/templates/Blank.pdf" $out;
        cp "${source}/api/src/main/resources/AdobeRGB1998.icc" $out;
        cp  "${source}/api/src/main/resources/converter" $out;
      '';
    };
    example = ./Blank.pdf;
    icc_profile = "${files}/AdobeRGB1998.icc";
    converter = "${files}/converter";
    client = import "${source}/camunda/client" {
        pkgs = import ../../../nix {};
    };
in {
  systemd.services.vasara-pdf-tools-api = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [
      adoptopenjdk-jre-hotspot-bin-13
      dejavu_fonts
      gawk
      ghostscript
      glibc.bin
      gnugrep
      gnused
    ];
    environment = {
      SHELL = "/bin/sh";
      CONVERTER = converter;
      ICC_PROFILE = icc_profile;
      GS_FONTPATH = "${pkgs.dejavu_fonts}/share/fonts/truetype";
      FORM_FONT = "${pkgs.dejavu_fonts}/share/fonts/truetype/DejaVuSans.ttf";
      HTTP_PORT = "8082";
      JAR = jar;
    };
    unitConfig = {
      StartLimitInterval = 300;
      StartLimitBurst = 15;
    };
    serviceConfig = {
      User = "pdf-tools-api";
      Group = "pdf-tools-api";
      DynamicUser = true;
      Restart = "on-failure";
      RestartSec = 20;
    };
    script = ''
      java -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006 ${jar};
    '';
  };

  networking.firewall.allowedTCPPorts = [ 8082 5006 ];

  systemd.services.vasara-pdf-tools-client = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [
      client
    ];
    environment = {
      MONITOR_PORT = "8083";
      PDF_TOOLS_API_URL = "http://localhost:8082/api/v1";
      BPM_ENGINE_URL = "http://localhost:8080/engine-rest";
      AUTH_TOKEN = constants.vasara_rest_secret;
    };
    unitConfig = {
      StartLimitInterval = 300;
      StartLimitBurst = 15;
    };
    serviceConfig = {
      User = "vagrant";
      Group = "users";
      Restart = "on-failure";
      RestartSec = 20;
      StateDirectory = "pdf-tools-api";
    };
    script = ''
      rm -f $STATE_DIRECTORY/pdf-tools-api
      cp ${example} $STATE_DIRECTORY/Blank.pdf
      chmod u+w $STATE_DIRECTORY/Blank.pdf
      TEMPLATE_DIR=$STATE_DIRECTORY app
    '';
  };

  nixpkgs.config.permittedInsecurePackages = [
    "adoptopenjdk-jre-hotspot-bin-13.0.2"  # support ended
  ];
}
