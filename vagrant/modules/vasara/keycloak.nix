{ config, pkgs, ... }:

let constants = import ./constants.nix {};
    realm_export = ../../../packages/keycloak/realm-export.json; in

{
  services = {
    keycloak = {
      enable = true;
      httpPort = "8000";
      database = {
          passwordFile = builtins.toFile "db_password" "keycloak";
      };
      frontendUrl = "http://localhost:8000/auth";
      initialAdminPassword = constants.keycloak_admin_password;
    };
  };

  networking.firewall.allowedTCPPorts = [ 8000 ];

  # Apply to import for default configuration
  systemd.services.keycloak.serviceConfig.ExecStart =
    pkgs.lib.mkForce (
      "${config.services.keycloak.package}/bin/standalone.sh " +
      "-Dkeycloak.migration.action=import " +
      "-Dkeycloak.migration.provider=singleFile " +
      "-Dkeycloak.migration.file=${realm_export}"
    );
}
