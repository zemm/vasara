{ config, pkgs, ... }:

{
  imports = [
    ./app.nix
    ./camunda.nix
    ./hasura.nix
    ./keycloak.nix
    ./mailhog.nix
    ./middleware.nix
    ./redis.nix
    ./vault.nix
    ./pdf-tools.nix
    ./carrot-rcc.nix
  ];
}
