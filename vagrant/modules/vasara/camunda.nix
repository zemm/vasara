{ config, pkgs, ... }:

let constants = import ./constants.nix {};
    jar = import ../../../packages/camunda {};
    mail_config = builtins.toFile "mail.config" ''
      mail.transport.protocol=smtp
      mail.smtp.host=localhost
      mail.smtp.port=${toString config.services.mailhog.smtpPort}
      mail.smtp.auth=false
      mail.ssl.enable=false
      mail.sender=noreply@jyu.fi
    ''; in


{
  environment.systemPackages = with pkgs; [ adoptopenjdk-jre-hotspot-bin-13 ];

  systemd.services.vasara-camunda-init = {
    after = [ "postgresql.service" ];
    before = [ "vasara-camunda.service" ];
    bindsTo = [ "postgresql.service" ];
    path = [ config.services.postgresql.package ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      User = "postgres";
      Group = "postgres";
    };
    script = ''
      set -o errexit -o pipefail -o nounset -o errtrace
      shopt -s inherit_errexit
      create_role="$(mktemp)"
      trap 'rm -f "$create_role"' ERR EXIT
      echo "CREATE ROLE camunda WITH LOGIN PASSWORD 'camunda' CREATEDB" > "$create_role"
      psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='camunda'" | grep -q 1 || psql -tA --file="$create_role"
      psql -tAc "SELECT 1 FROM pg_database WHERE datname = 'camunda'" | grep -q 1 || psql -tAc 'CREATE DATABASE "camunda" OWNER "camunda"'
      psql -c "CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA public" -d camunda
    '';
  };

  systemd.services.vasara-camunda = {
    enable = true;
    after = [ "vasara-camunda-init.service" "redis.service" "keycloak.service" "postgresql.service" ];
    bindsTo = [ "vasara-camunda-init.service" "redis.service" "keycloak.service" "postgresql.service" ];
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ adoptopenjdk-jre-hotspot-bin-13 ];
    environment = {
      HTTP_PORT = "8080";
      DB_TYPE = "postgres";
      DB_DRIVER = "org.postgresql.Driver";
      DB_URL = "jdbc:postgresql://localhost/camunda";
      DB_USERNAME = "camunda";
      DB_PASSWORD = "camunda";
      OIC_ISSUER_URI = "http://localhost:8000/auth/realms/vasara";
      CAMUNDA_ADMIN_USER = "camunda-admin";
      VASARA_TENANTS_OIDC = "";
      VASARA_TRANSIENT_USER_PUBLIC_KEY = constants.vasara_transient_user_public_key;
      SCIM_BASE_URL = "http://localhost:8080/engine-rest/scim";
      LOG_LEVEL_VASARA = "DEBUG";
      LOG_LEVEL_WEB = "DEBUG";
      OIC_CLIENT_ID = constants.camunda_oidc_client_id;
      OIC_CLIENT_SECRET = constants.camunda_oidc_client_secret;
      OIC_LOGOUT_URI = "http://localhost:8000/auth/realms/vasara/protocol/openid-connect/logout";
      VASARA_GRAPHQL_SECRET = constants.vasara_graphql_secret;
      VASARA_SCIM_SECRET = constants.vasara_scim_secret;
      VASARA_REST_SECRET = constants.vasara_rest_secret;
      REDIS_PASSWORD = constants.redis_password;
      REDIS_HOST = "localhost";
      REDIS_PORT = "${toString config.services.redis.port}";
      COOKIE_SECURE = "false";
      MAX_HTTP_HEADER_SIZE = "1024KB";
      MAIL_HOST = "localhost";
      MAIL_PORT = "${toString config.services.mailhog.smtpPort}";
      MAIL_AUTH = "false";
      MAIL_SSL = "false";
      MAIL_SENDER = "noreply@jyu.fi";
      MAIL_CONFIG = mail_config;
    };
    serviceConfig = {
      ExecStart = "${pkgs.adoptopenjdk-jre-hotspot-bin-13}/bin/java -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 ${jar}";
      User = "camunda";
      Group = "camunda";
      DynamicUser = true;
    };
  };

  networking.firewall.allowedTCPPorts = [ 8080 5005 ];

  nixpkgs.config.permittedInsecurePackages = [
    "adoptopenjdk-jre-hotspot-bin-13.0.2"  # support ended
  ];
}
