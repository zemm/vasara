{ config, pkgs, ... }:

let constants = import ./constants.nix {}; in

{
  services = {
    mailhog = {
      enable = true;
    };
  };
}
