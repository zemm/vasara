{ config, pkgs, ... }:

let constants = import ./constants.nix {};
    app = mode: port: pkgs.stdenv.mkDerivation {
      name = "vasara-modeler";
      phases = [ "installPhase" ];
      installPhase = ''
        mkdir -p $out
        cd $out
        ${import ../../../packages/vasara-app {
           REACT_APP_BUILD_TARGET = mode;
        }}/bin/init
      '';
      REACT_APP_GRAPHQL_API_URL = "http://localhost:8090/v1/graphql";
      REACT_APP_CLIENT_ID = "vasara-app";
      REACT_APP_AUTHORITY = "http://localhost:8000/auth/realms/vasara";
      REACT_APP_REDIRECT_URI = "http://localhost:${port}/";
      REACT_APP_DEFAULT_TENANT =  "";
      REACT_APP_URL = "http://localhost:${port}/";
      REACT_APP_LOGOUT_URI = "http://localhost:8000/auth/realms/vasara/protocol/openid-connect/logout?redirect_uri=http://localhost:${port}/";
    };
    CSP_DEFAULT_SRC = "'self' http://localhost:8090 http://localhost:8000";

in {
  environment.systemPackages = with pkgs; [ nginx ];

  services.nginx = {
    enable = true;
    config = ''
worker_processes 4;

events {
  use epoll;
  worker_connections 128;
}

http {
  include ${pkgs.nginx}/conf/mime.types;
  default_type application/octet-stream;
  sendfile on;
  server {
    listen 3000;
    server_name localhost;
    gzip on;
    gzip_types text/css application/javascript;
    gzip_min_length 1000;
    root ${app("modeler")("3000")}/var/www;
    location / {
      index index.html;
      # kill cache
      add_header Last-Modified $date_gmt;
      add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
      if_modified_since off;
      expires off;
      etag off;
      # CSP
      add_header X-Frame-Options "deny" always;
      add_header X-XSS-Protection "mode=block" always;
      add_header X-Content-Type-Options "nosniff" always;
      add_header Referrer-Policy "same-origin" always;
      add_header Content-Security-Policy "default-src ${CSP_DEFAULT_SRC}; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
      add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
      #
      try_files $uri /index.html =440;
    }
  }
  server {
    listen 3001;
    server_name localhost;
    gzip on;
    gzip_types text/css application/javascript;
    gzip_min_length 1000;
    root ${app("tasklist")("3001")}/var/www;
    location / {
      index index.html;
      # kill cache
      add_header Last-Modified $date_gmt;
      add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
      if_modified_since off;
      expires off;
      etag off;
      # CSP
      add_header X-Frame-Options "deny" always;
      add_header X-XSS-Protection "mode=block" always;
      add_header X-Content-Type-Options "nosniff" always;
      add_header Referrer-Policy "same-origin" always;
      add_header Content-Security-Policy "default-src ${CSP_DEFAULT_SRC}; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
      add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
      #
      try_files $uri /index.html =440;
    }
  }
}
    '';
  };
}
