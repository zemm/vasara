{ config, pkgs, ... }:

{
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fi";
  };

  i18n.defaultLocale = "fi_FI.UTF-8";
  time.timeZone = "Europe/Helsinki";

  services = {
    xserver = {
      enable = true;
      layout = "fi";
      xkbOptions = "eurosign:e";
      libinput.enable = true;
      displayManager = {
        defaultSession = "xfce";
      };
      desktopManager = {
        xterm.enable = false;
        xfce.enable = true;
        xfce.noDesktop = true;
      };
    };
  };

  environment.systemPackages = with pkgs; [
    xfce.xfdesktop
  ];
}
