{ config, pkgs, ... }:

let

  sources = import ../../nix/sources.nix;
  nixpkgs = sources."nixpkgs";

in

{
  imports = [
    "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
  ];

  services.xserver.videoDrivers = [ "qxl" "cirrus" "vesa" ];
}
