{ pkgs ? import ./nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    gnumake
    niv
  ];
}
