Exporting configuration
=======================

As a foreword, we do have plans for UI to export and import for Vasara BPMN processes with Vasara user task forms.

Because Vasara is a composition of various open source software, its configuration is a bit distributed. Vasara configuration (excluding authorization configuration) is stored as:

* BPMN and DMN diagrams in Camunda BPM
* Vasara user tasks forms in PostgreSQL schema `vasara` table `user_task_form`
* Vocabularies (codebooks) in PostgreSQL schema `vocabulary`
* Entity table definitions in PostgreSQL schema `public`
* Hasura metadata for permissions and GraphQL relations.

Let's cover a few use-case for exporting and backing up configuration on this Vasara technolgoy preview.


Export all database schemas and data
------------------------------------

The fastest way to export PostgreSQL schemas and data on Vasara technology preview is to use [Hasura PG Dump API](https://hasura.io/docs/latest/graphql/core/api-reference/pgdump.html) available on the official Hasura Docker images. These exports result in plain SQL, which **can be imported back to database using Hasura SQL console**.

Export all table definitions in `public` schema:

```bash
$ curl -X POST http://localhost:8900/v1alpha1/pg_dump -d '{"opts": ["--schema", "public", "--schema-only"], "clean_output": true}' -H "X-Hasura-Admin-Secret: admin"
```

Export all table definitions and rows `vocabulary` schema:

```bash
$ curl -X POST http://localhost:8900/v1alpha1/pg_dump -d '{"opts": ["--schema", "vocabulary", "--insert"], "clean_output": true}' -H "X-Hasura-Admin-Secret: admin"
```

Export all rows in `vasara.user_task_form`:

```bash
curl -X POST http://localhost:8900/v1alpha1/pg_dump -d '{"opts": ["--table", "vasara.user_task_form", "-a", "--insert"], "clean_output": true}' -H "X-Hasura-Admin-Secret: admin"
```

Exporting only the latest user task form versions
-------------------------------------------------

To export only the latest user task form versions from technology preview, create a temporary view that only shows the latest versions of each user task form:

```sql
CREATE OR REPLACE VIEW "vasara"."user_task_form_latest" AS
 SELECT DISTINCT ON (user_task_form.process_definition_key, user_task_form.user_task_id) user_task_form.id,
    user_task_form.metadata,
    user_task_form.process_definition_key,
    user_task_form.process_definition_version,
    user_task_form.user_task_id,
    user_task_form.schema,
    user_task_form.process_definition_version_tag,
    user_task_form.settings
   FROM vasara.user_task_form
  ORDER BY user_task_form.process_definition_key, user_task_form.user_task_id, user_task_form.process_definition_version DESC;
```

Then execute SQL to remove all but those forms, which exist on the view for latest user task form versions, and replace version for the remaining forms according to your needs:

```sql
DELETE FROM "vasara"."user_task_form"
WHERE NOT EXISTS (SELECT * FROM "vasara"."user_task_form_latest" WHERE id = user_task_form.id);
UPDATE "vasara"."user_task_form" SET process_definition_version = 1;
```

Now PG Dump API will only export the latest version of the user task forms, because the other versions no longer exist:

```bash
curl -X POST http://localhost:8900/v1alpha1/pg_dump -d '{"opts": ["--table", "vasara.user_task_form", "-a", "--insert"], "clean_output": true}' -H "X-Hasura-Admin-Secret: admin"
```


Exporting and importing with PGAdmin
------------------------------------

More sophisticated exports and import can be made using [PGAadmin](https://www.pgadmin.org/docs/pgadmin4/5.0/index.html).

On Vasara technology preview, PGAdmin should be available at http://localhost:8980/ with user `camunda-admin@localhost` and password `admin`. PGAdmin should come with preconfigure database Vasara with password `postgres`.

PGAdmin allows to navigate down to specfic tables, execute a query on the table (from table context menu), and download the query results as a CSV file. That CSV file can then be imported by uploading it first and then selecting correct CSV settings (Header YES, delimiter ",", Quote '"' and Escape '"').

For example, exporting the latest versions of Vasara user task forms for a specific process definition could be done with downloading the results of the following query:

```
SELECT DISTINCT
ON (process_definition_key, user_task_id)
id, metadata, process_definition_key, 'LATEST_VERSION', user_task_id, schema, process_definition_version_tag
FROM vasara.user_task_form
WHERE process_definition_key = 'PROCESS_DEFINITION_KEY'
ORDER BY process_definition_key, user_task_id, process_definition_version DESC
```

Before export `PROCESS_DEFINITION_KEY` is replaced with the required process definition key.

Before import, `'LATEST_VERSION'` is replace with the version on the system. E.g. `1`.

Alternatively, remove line `WHERE process_definition_key = 'PROCESS_DEFINITION_KEY'` to get the latest user task forms for all available process definitions.

![](user-task-form-export-import.gif)


Exporting Hasura GraphQL metadata
---------------------------------

Hasura GraphQL configuration for tracked tables, relationships and permissions can be safely exported, if necessary, with [Hasura CLI](https://hasura.io/docs/latest/graphql/core/hasura-cli/index.html). The matching version in time of writing is v1.3.2.

Hasura CLi requires a clean directory with `./config.yaml`:

```yaml
version: 2
endpoint: http://localhost:8900
admin_secret: admin
metadata_directory: metadata
```

Export (exports all metadata once):

```bash
$ hasura metadata export
```

Import (overrides existing metadata):

```bash
$ hasura metadata export
```
