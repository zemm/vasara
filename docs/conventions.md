Limitations and conventions
===========================

Being able to "low code" process automation with Vasara requires to follow certain conventions when modeling entities with Hasura.

This documentation is kept up-to-date with known conventions need to be followed.

Database namespace conventions
------------------------------

PostgreSQL supports namespaces with schemas. Vasara expects and uses the following namespaces:

* `public`-namespaces is dedicated for business data entities and tables in `public`-namespace are expected to be visible in client apps
* `vasara`-namespaces is reserved for application configuration setting tables


Database table conventions
--------------------------

Each database table in public namespace should contain the following mandatory columns:

* `id`, Integer (auto-increment), unique, primary key
* `metadata`, JSONB, nullable

Every entity table must contain `id` column to be usable with react-admin based UI.
`id` will be hidden from the admin UI.

`metadata` column may be used by clients to store arbitrary invisible metadata on business data entities
